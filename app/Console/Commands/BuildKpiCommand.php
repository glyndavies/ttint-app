<?php

namespace App\Console\Commands;

use App\Facades\KpiService;
use Illuminate\Console\Command;
use Carbon\Carbon;

class BuildKpiCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:kpi {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculating and updating KPI with current date for all platform users';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // process all non-blocked users
        $start_time = microtime(true);

        $argument_date = $this->option('date');
        $datetime = $argument_date ? Carbon::parse($argument_date)->startOfDay() : Carbon::today()->startOfDay();

        $platformUsers = KpiService::rebuildAndRankAll($datetime);

        $end_time = microtime(true);
        $this->info('Users KPIs (' . $datetime->format('Y-m-d') . ') rebuilt: ' . $platformUsers->count() . ' users processed, rebuilding time: ' . round($end_time - $start_time, 1) . 's');
    }
}
