<?php

namespace App\Console\Commands;

use App\Models\InventoryQuantity;
use App\Facades\RebateService;
use Illuminate\Console\Command;

class CheckRebatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:rebates {--id=} {--order-number=} {--fix} {--log}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking calculated rebates';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('id')) {
            $inventoryQuantities = InventoryQuantity::where('id', $this->option('id'))->get();
        } elseif ($this->option('order-number')) {
            $inventoryQuantities = InventoryQuantity::whereHas('inventory', function($query) {
                return $query->whereRaw('order_number like "%' . $this->option('order-number') . '%"');
            })->get();
        } else {
            $inventoryQuantities = InventoryQuantity::all();
        }
        while ($inventoryQuantity = $inventoryQuantities->shift()) {
            $result = $this->checkInventoryQuantity($inventoryQuantity);
            if (!$result && $this->option('fix')) {
                $this->fixInventoryQuantity($inventoryQuantity);
            }
        }

        $this->info('Rebates checking complete');
    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @return boolean
     */
    public function checkInventoryQuantity ($inventoryQuantity)
    {
        $trustRebate = $inventoryQuantity->trustRebate;
        $referralRebate = $inventoryQuantity->referralRebate;

        $actualRebates = $inventoryQuantity->rebates()->notCancelled()->get();

        $trustDetails    = 'order ' . $inventoryQuantity->inventory->order_number . '|' . 'IQ id ' . $inventoryQuantity->id . '|' . $inventoryQuantity->updated_at->format('Y-m-d') . '|trust|'    . $inventoryQuantity->inventory->buyer      . '|' . $actualRebates->sum('trust_rebate')    . '|' . $trustRebate;
        $referralDetails = 'order ' . $inventoryQuantity->inventory->order_number . '|' . 'IQ id ' . $inventoryQuantity->id . '|' . $inventoryQuantity->updated_at->format('Y-m-d') . '|referral|' . $inventoryQuantity->inventory->referral_1 . '|' . $actualRebates->sum('referral_rebate') . '|' . $referralRebate;

        $moreDetails = '|sold-ticket:' . ($inventoryQuantity->soldTicket ? 'Y' : 'N') .
                       '|cancelled:' . $inventoryQuantity->inventory->event_cancelled .
                       '|expired:' . ($inventoryQuantity->expired ? 'Y' : 'N');

        if ($actualRebates->sum('trust_rebate') != $trustRebate) {
            $this->error($trustDetails . $moreDetails);
            return false;
        } elseif ($this->option('log')) {
            $this->info($trustDetails . $moreDetails);
        }

        if ($actualRebates->sum('referral_rebate') != $referralRebate) {
            $this->error($referralDetails . $moreDetails);
            return false;
        } elseif ($this->option('log')) {
            $this->info($referralDetails . $moreDetails);
        }

        return true;
    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @return boolean
     */
    public function fixInventoryQuantity($inventoryQuantity)
    {
        RebateService::sync($inventoryQuantity);
        $inventoryQuantity->refresh();
        foreach($inventoryQuantity->rebates as $rebate) {
            if (!$rebate->notified_at) {
                $rebate->fill([
                    'notified_at' => now(),
                ])->save();
            }
        }
    }

}
