<?php

namespace App\Console\Commands;

use App\Models\PlatformUser;
use App\Models\Rebate;
use App\Facades\RebateService;
use Illuminate\Console\Command;

class NotifyRebatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:rebates {probability} {--user=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending rebate notifications to platform users';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = 0;
        if ($this->option('user')) {
            if ($platformUser = PlatformUser::where('user_code', $this->option('user'))->first()) {
                if (RebateService::notifyWithRandomizer($platformUser, $this->argument('probability'))) {
                    $count++;
                }        
            }
            $this->info('Rebate notifications: Sent ' . $count);
        } else {
            $rebatesByUsers = Rebate::whereNull('notified_at')->get()->groupBy('buyer');
            $rebatesByUsers->each(function ($rebates, $buyer) use (&$count) {
                if ($platformUser = PlatformUser::where('user_code', $buyer)->first()) {
                    if (RebateService::notifyWithRandomizer($platformUser, $this->argument('probability'))) {
                        $count++;
                    }        
                }
            });
            $this->info('Rebate notifications: Sent ' . $count . ' / ' . $rebatesByUsers->count());
        }
    }
}
