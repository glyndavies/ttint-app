<?php

namespace App\Console\Commands;

use App\Models\PlatformUser;
use App\Facades\RebateService;
use Illuminate\Console\Command;

class SyncNotificationMultipliersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:notification-multipliers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recacluating notification multipliers for platform user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $platformUsers = PlatformUser::all();
        $platformUsers->each(function ($platformUser) {
          RebateService::syncNotificationMultiplier($platformUser->user_code);
        });

        $this->info('NotificationMultipliers: synced');
    }
}
