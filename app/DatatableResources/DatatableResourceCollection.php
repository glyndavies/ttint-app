<?php

namespace App\DatatableResources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Request;

class DatatableResourceCollection extends ResourceCollection
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'aaData';

    /**
     *
     */
    public $recordsTotal;

    /**
     *
     */
    public $recordsFiltered;

    /**
     * Builds filters for repository
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public static function repositoryFilters(Request $request)
    {
        $search = $request->input('search')['value'] ?? null;
        $partly = collect($request->input('columns'))->mapWithKeys(function($item, $index) use ($search) {
            return [ $item['data'] => $item['search']['value'] ];
        })
        ->filter();
        $filters = collect(['partly' => $partly]);
        if ($search) {
            $filters->put('search', $search);
        }

        $filters->put('start', $request->input('start'));
        $filters->put('length', $request->input('length'));

        return $filters->toArray();
    }

    /**
     * @param integer $total
     * @return self
     */
    public function withTotals($total)
    {
        $this->recordsTotal = $total;
        $this->recordsFiltered = $total;
        return $this;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Collection $collection
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function datatableWrap($collection, $request)
    {
        return [
            'iTotalRecords' => $this->recordsTotal ?? $collection->count(),
            'iTotalDisplayRecords' => $this->recordsFiltered ?? $collection->count(),
            'draw' => intval($request->input('draw')),
            'recordsTotal' => $this->recordsTotal ?? $collection->count(),
            'recordsFiltered' => $this->recordsFiltered ?? $collection->count(),
            'aaData' => $collection->values(),
        ];
    }
}
