<?php

namespace App\DatatableResources;

use Illuminate\Http\Request;

class EventFulfillmentCollection extends DatatableResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->datatableWrap($this->collection->map(function($eventFulfillment) {
            return new EventFulfillmentResource($eventFulfillment);
        }), $request);
    }
}
