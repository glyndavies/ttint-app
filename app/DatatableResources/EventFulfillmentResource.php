<?php

namespace App\DatatableResources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventFulfillmentResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $hidden_data = !empty($request->input('hiddenExportDataColumns'))? $request->input('hiddenExportDataColumns'): [];

        return [
//            'all'                 => in_array('all', $hidden_data)? '-': view('partials.toggles.event-fulfillment-input-all-action', [ 'event_fulfillment_code' => $this->event . '|' . $this->event_date . '|' . $this->time_of_event . '|' . $this->arena ])->render(),
            'event_details'       => in_array('event_details', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-event-details', ['event_fulfillment' => $this])->render(),
            'site_purchased'      => in_array('site_purchased', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-site-purchased', ['event_fulfillment' => $this])->render(),
            'paper'               => in_array('paper', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-paper', ['event_fulfillment' => $this])->render(),
            'unsold'              => in_array('unsold', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-unsold', ['event_fulfillment' => $this])->render(),
            'comparable'          => in_array('comparable', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-comparable', ['event_fulfillment' => $this])->render(),
            'to_be_fulfilled'     => in_array('to_be_fulfilled', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-to-be-fulfilled', ['event_fulfillment' => $this])->render(),
            'fulfillment_summary' => in_array('fulfillment_summary', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-fulfillment-summary', ['event_fulfillment' => $this])->render(),
            'fulfillment_total'   => in_array('fulfillment_total', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-fulfillment-total', ['event_fulfillment' => $this])->render(),
            'deadline'            => in_array('deadline', $hidden_data) ? '-' : '<b>' . formatDateDifference($this->event_date, $this->time_of_event) . '</b>',
            'site_sold'           => in_array('site_sold', $hidden_data) ? '-' : view('partials.badges.event-fulfillment-site-sold', ['event_fulfillment' => $this])->render(),
            'fulfillment_actions' => in_array('fulfillment_actions', $hidden_data)? '-': view('partials.actions.event-fulfillment-actions', ['event_fulfillment' => $this])->render(),
        ];
    }
}
