<?php

namespace App\DatatableResources;

class InventoryCollection extends DatatableResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->datatableWrap($this->collection->map(function($inventory) {
            return new InventoryResource($inventory);
        }), $request);
    }
}