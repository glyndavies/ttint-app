<?php

namespace App\DatatableResources;

class InventoryQuantityCollection extends DatatableResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->datatableWrap($this->collection->map(function($inventoryQuantity) {
            return new InventoryQuantityResource($inventoryQuantity);
        }), $request);
    }
}