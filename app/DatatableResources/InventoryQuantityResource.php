<?php

namespace App\DatatableResources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryQuantityResource extends JsonResource
{
	/**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $hidden_data = !empty($request->input('hiddenExportDataColumns'))? $request->input('hiddenExportDataColumns'): [];

        return [
            'all'                   => in_array('all', $hidden_data)? '-': view('partials.toggles.input-all-action', [ 'inventory_quantity_id' => $this->id ])->render(),
            'batch_id'              => in_array('batch_id', $hidden_data)? '-': optional($this->deliveryBatch)->batch_id,
            'transaction_number'    => in_array('transaction_number', $hidden_data)? '-': $this->purchase_number,
            'order_number'          => in_array('order_number', $hidden_data)? '-': $this->inventory->order_number,
            'event'                 => in_array('event', $hidden_data)? '-': $this->inventory->event,
            'arena'                 => in_array('arena', $hidden_data)? '-': $this->inventory->arena,
            'site_purchased'        => in_array('site_purchased', $hidden_data)? '-': $this->inventory->site_purchased,
            'type_of_ticket'        => in_array('type_of_ticket', $hidden_data)? '-': $this->inventory->type_of_ticket,
            'purchase_date'         => in_array('purchase_date', $hidden_data)? '-': dateformat(optional($this->inventory->purchase)->purchase_date),
            'event_date'            => in_array('event_date', $hidden_data)? '-': dateformat($this->inventory->event_date),
            'time_of_event'         => in_array('time_of_event', $hidden_data)? '-': $this->inventory->time_of_event,
            'day_of_event'          => in_array('day_of_event', $hidden_data)? '-': ($this->inventory->event_date ? substr($this->inventory->event_date->format('l'), 0, 3) : ''),
            'available_d'           => in_array('available_d', $hidden_data)? '-': view('partials.badges.inventory-forsale-count', [ 'inventory' => $this->inventory ])->render() . view('partials.badges.inventory-notes', [ 'inventory' => $this->inventory ])->render(),
            'available'             => in_array('available', $hidden_data)? '-': view('partials.badges.inventory-forsale-count', [ 'inventory' => $this->inventory ])->render(),
            'number_of_tickets_d'   => in_array('number_of_tickets_d', $hidden_data)? '-': view('partials.badges.inventory-quantity-status', [ 'inventory_quantity' => $this ])->render() . view('partials.badges.inventory-notes', [ 'inventory' => $this->inventory ])->render() . view('partials.badges.inventory-quantity-notes', [ 'inventory_quantity' => $this ])->render(),
            'number_of_tickets'     => in_array('number_of_tickets', $hidden_data)? '-': $this->number_of_tickets,
            'notes'                 => in_array('notes', $hidden_data)? '-': $this->inventory->notes . ' - ' . $this->notes,
            'restrictions'          => in_array('restrictions', $hidden_data)? '-': $this->inventory->restrictions,
            'facevalue_per_ticket'  => in_array('facevalue_per_ticket', $hidden_data)? '-': moneyformat(optional($this->inventory->purchase)->face_value_per_ticket),
            'seats'                 => in_array('seats', $hidden_data)? '-': $this->seats,
            'listed'                => in_array('listed', $hidden_data)? '-': $this->inventory->listed,
            'referral_1'            => in_array('referral_1', $hidden_data)? '-': $this->inventory->referral_1,
            'referral_2'            => in_array('referral_2', $hidden_data)? '-': $this->inventory->referral_2,
            'user_code'             => in_array('user_code', $hidden_data)? '-': optional($this->inventory->purchase)->buyer,
            'user_name'             => in_array('user_name', $hidden_data)? '-': optional($this->inventory->purchase)->card_used,
            'user_deposit'          => in_array('user_deposit', $hidden_data)? '-': moneyformat(optional($this->inventory->purchase)->buyer_investment),
            'sold'                  => in_array('sold', $hidden_data)? '-': 'sold',
            'total_cost_per_ticket' => in_array('total_cost_per_ticket', $hidden_data)? '-': moneyformat($this->cost_per_ticket),
            'total_cost'            => in_array('total_cost', $hidden_data)? '-': moneyformat($this->cost),
            'tickets_in_hand'       => in_array('tickets_in_hand', $hidden_data)? '-': view('partials.toggles.inventory-quantity-inhands', [ 'inventory_quantity' => $this ])->render(),
            'date_sold'             => in_array('date_sold', $hidden_data)? '-': dateformat(optional($this->soldTicket)->sold_date),
            'site_sold'             => in_array('site_sold', $hidden_data)? '-': optional($this->soldTicket)->site_sold,
            'sale_id_ref'           => in_array('sale_id_ref', $hidden_data)? '-': optional($this->soldTicket)->sale_id_ref,
            'refund'                => in_array('refund', $hidden_data)? '-': moneyformat(optional(optional($this->inventory->purchase)->companyRefund)->refunded_amount),
            'sell_price'            => in_array('sell_price', $hidden_data)? '-': moneyformat($this->sell_price),
            'profit'                => in_array('profit', $hidden_data)? '-': moneyformat($this->profit),
            'delivered'             => in_array('delivered', $hidden_data)? '-': view('partials.toggles.inventory-quantity-handling', [ 'inventory_quantity' => $this, 'hide' => 1 ])->render(),
            'delivered_status'      => in_array('delivered_status', $hidden_data)? '-': view('partials.badges.inventory-quantity-handling', [ 'inventory_quantity' => $this ])->render(),
            'delivered_date'        => in_array('delivered_date', $hidden_data)? '-': dateformat($this->delivered_date),
            'paid'                  => in_array('paid', $hidden_data)? '-': (!empty($this->soldTicket)? view('partials.toggles.inventory-quantity-paid', [ 'inventory_quantity' => $this ])->render(): ''),
            'paid_date'             => in_array('paid_date', $hidden_data)? '-': dateformat(optional($this->soldTicket)->paid_date),
            'paid_amount'           => in_array('paid_amount', $hidden_data)? '-': moneyformat($this->inventory->paid_amount),
            'ticket_state'          => in_array('ticket_state', $hidden_data)? '': view('partials.tickets.states-ticket', ['inventory_quantity' => $this])->render(),
            'sold_actions'          => in_array('sold_actions', $hidden_data)? '': view('partials.actions.sold-actions', [ 'inventory_quantity_id' => $this->id ])->render(),
            'delivered_actions'     => in_array('delivered_actions', $hidden_data)? '': view('partials.actions.delivered-actions', [ 'inventory_quantity_id' => $this->id ])->render(),
            'completed_actions'     => in_array('completed_actions', $hidden_data)? '': view('partials.actions.completed-actions', [ 'inventory_quantity_id' => $this->id, 'sold_ticket_id' => optional($this->soldTicket)->id ])->render(),
        ];
    }
}
