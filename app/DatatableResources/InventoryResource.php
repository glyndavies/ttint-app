<?php

namespace App\DatatableResources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryResource extends JsonResource
{
	/**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $hidden_data = !empty($request->input('hiddenExportDataColumns'))? $request->input('hiddenExportDataColumns'): [];

        return [
            'all'                   => in_array('all', $hidden_data)? '-': view('partials.toggles.input-all-action', [ 'inventory_quantity_id' => $this->id ])->render(),
            'batch_id'              => in_array('batch_id', $hidden_data)? '-': optional($this->deliveryBatch)->batch_id,
            'transaction_number'    => in_array('transaction_number', $hidden_data)? '-': $this->purchase_number,
            'order_number'          => in_array('order_number', $hidden_data)? '-': $this->order_number,
            'event'                 => in_array('event', $hidden_data)? '-': $this->event,
            'arena'                 => in_array('arena', $hidden_data)? '-': $this->arena,
            'site_purchased'        => in_array('site_purchased', $hidden_data)? '-': $this->site_purchased,
            'type_of_ticket'        => in_array('type_of_ticket', $hidden_data)? '-': $this->type_of_ticket,
            'purchase_date'         => in_array('purchase_date', $hidden_data)? '-': dateformat(optional($this->purchase)->purchase_date),
            'event_date'            => in_array('event_date', $hidden_data)? '-': dateformat($this->event_date),
            'time_of_event'         => in_array('time_of_event', $hidden_data)? '-': $this->time_of_event,
            'day_of_event'          => in_array('day_of_event', $hidden_data)? '-': ($this->event_date ? substr($this->event_date->format('l'), 0, 3) : ''),
            'available_d'           => in_array('available_d', $hidden_data)? '-': view('partials.badges.inventory-forsale-count', [ 'inventory' => $this ])->render() . view('partials.badges.inventory-notes', [ 'inventory' => $this ])->render(),
            'available'             => in_array('available', $hidden_data)? '-': $this->inventoryQuantities->filter(function($inventory_quantity){return $inventory_quantity->ifStatusForSale();})->sum('number_of_tickets'),
            'number_of_tickets_d'   => in_array('number_of_tickets_d', $hidden_data)? '-': view('partials.badges.inventory-statuses', [ 'inventory' => $this])->render(),
            'number_of_tickets'     => in_array('number_of_tickets', $hidden_data)? '-': view('partials.badges.excel.inventory-statuses', [ 'inventory' => $this])->render(),
            'notes'                 => in_array('notes', $hidden_data)? '-': $this->notes,
            'pending_cancelled'     => in_array('cancelled', $hidden_data) ? '-' :  view('partials.toggles.inventory-pending-cancelled', [ 'inventory_quantity' => $this ])->render(),
            'listed_on_resale'      => in_array('listed_on_resale', $hidden_data) ? '-' : view('partials.toggles.inventory-listed-on-resale', ['inventory_quantity' => $this])->render(),
            'restrictions'          => in_array('restrictions', $hidden_data)? '-': $this->restrictions,
            'facevalue_per_ticket'  => in_array('facevalue_per_ticket', $hidden_data)? '-': moneyformat(optional($this->purchase)->face_value_per_ticket),
            'seats'                 => in_array('seats', $hidden_data)? '-': view('partials.inventory-seats-list', ['inventory_quantities' => $this->inventoryQuantities])->render(),
            'listed'                => in_array('listed', $hidden_data)? '-': $this->listed,
            'referral_1'            => in_array('referral_1', $hidden_data)? '-': $this->referral_1,
            'referral_2'            => in_array('referral_2', $hidden_data)? '-': $this->referral_2,
            'user_code'             => in_array('user_code', $hidden_data)? '-': $this->buyer,
            'user_name'             => in_array('user_name', $hidden_data)? '-': optional($this->purchase)->card_used,
            'user_deposit'          => in_array('user_deposit', $hidden_data)? '-': moneyformat(optional($this->purchase)->buyer_investment),
            'sold'                  => in_array('sold', $hidden_data)? '-': 'sold',
            'total_cost_per_ticket' => in_array('total_cost_per_ticket', $hidden_data)? '-': moneyformat(optional($this->purchase)->cost_per_ticket),
            'total_cost'            => in_array('total_cost', $hidden_data)? '-': moneyformat(optional($this->purchase)->cost),
            'tickets_in_hand'       => in_array('tickets_in_hand', $hidden_data)? '-': view('partials.toggles.inventory-inhands', [ 'inventory' => $this ])->render(),
            'date_sold'             => in_array('date_sold', $hidden_data)? '-': dateformat(optional($this->soldTicket)->sold_date),
            'site_sold'             => in_array('site_sold', $hidden_data)? '-': optional($this->soldTicket)->site_sold,
            'sale_id_ref'           => in_array('sale_id_ref', $hidden_data)? '-': optional($this->soldTicket)->sale_id_ref,
            'refund'                => in_array('refund', $hidden_data)? '-': moneyformat(optional(optional($this->purchase)->companyRefund)->refunded_amount),
            'sell_price'            => in_array('sell_price', $hidden_data)? '-': moneyformat($this->sell_price),
            'profit'                => in_array('profit', $hidden_data)? '-': moneyformat($this->profit),
            'delivered'             => in_array('delivered', $hidden_data)? '-': '',
            'delivered_status'      => in_array('delivered_status', $hidden_data)? '-': '',
            'delivered_date'        => in_array('delivered_date', $hidden_data)? '-': dateformat($this->delivered_date),
            'paid'                  => in_array('paid', $hidden_data)? '-': (!empty($this->soldTicket)? view('partials.toggles.inventory-quantity-paid', [ 'inventory_quantity' => $this ])->render(): ''),
            'paid_date'             => in_array('paid_date', $hidden_data)? '-': dateformat(optional($this->soldTicket)->paid_date),
            'paid_amount'           => in_array('paid_amount', $hidden_data)? '-': moneyformat($this->paid_amount),
            'inventory_actions'     => in_array('inventory_actions', $hidden_data)? '': view('partials.actions.inventory-actions', [ 'inventory' => $this ])->render(),
        ];
    }
}
