<?php

namespace App\DatatableResources;

class PurchaseCollection extends DatatableResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->datatableWrap($this->collection->map(function($purchase) {
            return new PurchaseResource($purchase);
        }), $request);
    }

    /**
     * @param integer $total_cost
     * @return self
     */
    public function withTotalCost($total_cost)
    {
        $this->totalCostFiltered = $total_cost;

        return $this;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Collection $collection
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function datatableWrap($collection, $request)
    {
        return [
            'iTotalRecords' => $this->recordsTotal ?? $collection->count(),
            'iTotalDisplayRecords' => $this->recordsFiltered ?? $collection->count(),
            'draw' => intval($request->input('draw')),
            'totalCostFiltered' => $this->totalCostFiltered ?? 0,
            'aaData' => $collection->values(),
        ];
    }
}