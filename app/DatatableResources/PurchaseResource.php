<?php

namespace App\DatatableResources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseResource extends JsonResource
{
	/**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
    	return [
            'all' => '<input class="selectCheck" value="' . $this->id . '" type="checkbox">',
            'transaction_number' => $this->purchase_number,
            'order_number' => $this->order_number,
            'event_genre' => $this->inventory->event_genre,
            'event' => $this->inventory->event,
            'arena' => $this->inventory->arena,
            'country' => $this->inventory->country,
            'site_purchase' => $this->inventory->site_purchased,
            'type_of_ticket' => $this->inventory->type_of_ticket,
            'purchased_date' => dateformat($this->purchase_date),
            'event_date' => dateformat($this->inventory->event_date),
            'time_of_event' => $this->inventory->time_of_event,
            'day_of_event' => substr($this->inventory->event_date->format('l'), 0, 3),
            'number_of_tickets' => view('partials.badges.purchase-count', [ 'purchase' => $this ])->render() . view('partials.badges.inventory-notes', [ 'inventory' => $this->inventory ])->render(),
            'restrictions' => $this->inventory->restrictions,
            'seats' => $this->inventoryQuantities->map(function ($iq) {
                return '<span>' . $iq->seats . '</span><br>';
            })->join(''),
            'facevalue_per_ticket' => moneyformat($this->face_value_per_ticket),
            'facevalue_of_tickets' => moneyformat($this->face_value_of_tickets),
            'buyers_fees' => moneyformat($this->buyers_fees),
            'user_code' => $this->buyer,
            'user_deposit' => moneyformat($this->buyer_investment),
            'user_name' => $this->card_used,
            'total_cost_per_ticket' => moneyformat($this->cost_per_ticket),
            'total_cost' => moneyformat($this->cost),
            'tickets_in_hand' => 'Tickets in hand ! multi issue',
            'broken_limit' => $this->broken_limit,
            'insurance' => $this->insurance,
            'inputer' => $this->Imputer,
            'pending_reconciled' => $this->pending_reconciled,
            'reconciled' => $this->reconciled,
            'state_ticket' => implode('', [
                ($this->state_ticket_I)? 'I': '',
                ($this->state_ticket_S)? 'S': '',
                ($this->state_ticket_D)? 'D': '',
                ($this->state_ticket_C)? 'C': '']),
            'refund' =>view('partials.buttons.refund-button', [ 'purchase' => $this ])->render(),
            'paid_amount' => moneyformat($this->inventory->paid_amount),
    	];

    }
}

