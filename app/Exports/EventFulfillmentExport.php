<?php

namespace App\Exports;

use App\Models\InventoryQuantity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class EventFulfillmentExport implements FromCollection, WithHeadings, WithMapping, WithStyles
{
    protected $inventoryQuantities;
    protected $exportType;

    public function __construct(array $inventoryQuantitiesIds, $exportType)
    {
        $this->inventoryQuantities = InventoryQuantity::whereIn('id', $inventoryQuantitiesIds)->get();
        $this->exportType = $exportType;
    }

    public function collection()
    {
        $this->inventoryQuantities->each(function ($inventoryQuantity) {
            $inventoryQuantity->update([
                'in_progress_by_user_id' => Auth::user()->id,
            ]);
        });

        return $this->inventoryQuantities;
    }

    public function map($inventoryQuantity): array
    {
        return [
            $inventoryQuantity->id,
            $inventoryQuantity->purchase_number,
            $inventoryQuantity->inventory->order_number,
            $inventoryQuantity->inventory->event,
            $inventoryQuantity->inventory->arena,
            $inventoryQuantity->inventory->site_purchased,
            $inventoryQuantity->inventory->type_of_ticket,
            $inventoryQuantity->inventory->event_date->format('d/m/Y'),
            Carbon::parse($inventoryQuantity->inventory->time_of_event)->format('H:i'),
            $inventoryQuantity->inventory->event_date->format('D'),
            $inventoryQuantity->number_of_tickets,
            optional($inventoryQuantity->inventory)->notes,
            $inventoryQuantity->notes,
            $inventoryQuantity->inventory->restrictions,
            $inventoryQuantity->seats,
            optional(optional($inventoryQuantity->inventory)->purchase)->buyer,
            optional($inventoryQuantity->soldTicket)->site_sold,
            optional($inventoryQuantity->soldTicket)->sale_id_ref,
            moneyformat($inventoryQuantity->sell_price),
            $inventoryQuantity->isComparable() ? 'Yes' : '',
            $inventoryQuantity->isFulfilled() ? 'Yes' : ''
        ];
    }

    public function headings(): array
    {
        return [
            'id',
            'Transaction number',
            'Order Number',
            'Event',
            'Arena',
            'Site purchased',
            'Type of Ticket',
            'Event Date',
            'Time of Event',
            'Day of Event',
            'Number of Tickets',
            'Notes',
            'Notes(By Sale ID)',
            'Restrictions',
            'Seats',
            'User Code',
            'Site Sold',
            'Sale ID',
            'TSP',
            'Comparable',
            'Fulfilled'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => [
                'font' => [
                    'bold' => true
                ]
            ],
            'T1' => [
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['rgb' => 'FFFF00'],
                ],
            ],
            'U1' => [
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['rgb' => 'FFFF00'],
                ],
            ]
        ];
    }
}
