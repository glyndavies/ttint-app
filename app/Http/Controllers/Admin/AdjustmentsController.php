<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\PurchaseRepository;
use App\Facades\InventoryRepository;
use App\Facades\InventoryQuantityRepository;

class AdjustmentsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adjustment.index', [
            'cancelled_purchases'           => PurchaseRepository::getCancelled(),
            'expired_inventories'           => InventoryRepository::getExpired(),
        ]);
    }
}
