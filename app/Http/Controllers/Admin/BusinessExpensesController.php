<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BusinessExpense;
use App\Models\ExpenseCategory;
use App\Models\ExpenseSubCategory;
use App\Models\Card;
use Carbon\Carbon;

class BusinessExpensesController extends Controller
{
	/**
	 * @return View
	 */
    public function index()
    {
    	$businessExpenses = BusinessExpense::all();
        return view('finances.business-expenses.index', [
        	'businessExpenses' => $businessExpenses,
        ]);
    }

    /**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		$businessExpense = BusinessExpense::findOrFail($id);
		$categories = ExpenseCategory::all();
        $subcategories = ExpenseSubCategory::all();
		$cards = Card::all();

		return view('finances.business-expenses.show', [
            'businessExpense' => $businessExpense,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'cards' => $cards
        ]);
    }

    /**
     * @return View
     */
    public function add()
    {
		$businessExpense = new BusinessExpense([
			'date' => Carbon::today(),
		]);
		
		$categories = ExpenseCategory::all();
        $subcategories = ExpenseSubCategory::all();
		$cards = Card::all();

		return view('finances.business-expenses.show', [
            'businessExpense' => $businessExpense,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'cards' => $cards
        ]);
    }

	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
    	$businessExpense = BusinessExpense::findOrFail($id);
    	$data = $request->all();

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	$businessExpense->update($data);

		return redirect()->route('finances.business-expenses')->with('message', 'Business expense updated');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
    	$data = $request->all();
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	BusinessExpense::create($data);

		return redirect()->route('finances.business-expenses')->with('message', 'Business expense created');
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
		$businessExpense = BusinessExpense::findOrFail($id);
		$businessExpense->delete();

        return redirect()->route('finances.business-expenses')->with('message', 'Business expense deleted');
    }

    /**
     * @param array $data
     * @return Validator
     */
    public function validator($data)
    {
    	return Validator::make($data, [
            'category_id'     => ['required'],
            'sub_category_id' => ['required', 'integer', 'exists:expense_sub_categories,id'],
            'card_id'         => ['required'],
            'description'     => ['required', 'string'],
            'amount'          => ['required', 'numeric'],
            'vat'             => ['required', 'numeric'],
            'date'            => ['date'],
        ]);
    }
}
