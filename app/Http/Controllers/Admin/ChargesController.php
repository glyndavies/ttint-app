<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\ChargeService;
use App\Models\Charge;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ChargesController extends Controller
{
	/**
	 * @return View
	 */
    public function index()
    {
        return view('finances.charges.index', [
        	'charges' => Charge::all(),
        ]);
    }

	/**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		return view('finances.charges.show', [
            'charge' => Charge::findOrFail($id)
        ]);
    }

    /**
     * @return View
     */
    public function add()
    {
		return view('finances.charges.show', [
            'charge' => new Charge([
                'date' => Carbon::today(),
            ]),
        ]);
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
        ChargeService::create($request->all());
		return redirect()->route('finances.charges')->with('message', 'Charge created');
    }

  	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
        ChargeService::update(Charge::findOrFail($id), $request->all());
		return redirect()->route('finances.charges')->with('message', 'Charge updated');
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
        ChargeService::delete(Charge::findOrFail($id));
        return redirect()->route('finances.charges')->with('message', 'Charge deleted');
    }   
}
