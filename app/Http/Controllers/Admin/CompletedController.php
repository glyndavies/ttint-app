<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\InventoryQuantityRepository;
use App\DatatableResources\InventoryQuantityCollection;
use Illuminate\Http\Request;

class CompletedController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('completed.index');
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('completed.show', [
            'inventory_quantity' => InventoryQuantityRepository::findById($id)->load([
                'inventory',
                'soldTicket',
          ]),
        ]);
    }

    /**
    * @param integer $id
    * @return \Illuminate\Http\Response
    */
    public function completedTicket($id)
    {
        return view('modals.completed-modal-content', [
            'inventory_quantity' => InventoryQuantityRepository::findById($id)->load([
                'inventory',
                'soldTicket',
            ]),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\DatatableResources\InventoryQuantityCollection
     */
    public function datatable(Request $request)
    {
        $filters = InventoryQuantityCollection::repositoryFilters($request);

        return (new InventoryQuantityCollection(InventoryQuantityRepository::getCompleted($filters)))
            ->withTotals(InventoryQuantityRepository::getCompletedCount(collect($filters)->except(['start', 'length'])->toArray()));
    }
}
