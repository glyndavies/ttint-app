<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\InventoryQuantityRepository;
use App\Facades\SoldTicketService;
use App\DatatableResources\InventoryQuantityCollection;
use App\Models\InventoryQuantity;
use App\Models\SoldTicket;
use App\Models\TicketType;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DeliveredController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('delivered.index');
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('delivered.show', [
            'inventory_quantity' => InventoryQuantityRepository::findById($id)->load([
                'inventory',
                'soldTicket',
            ]),
        ]);
    }

    public function update(Request $request, $id)
    {
        try {
            SoldTicketService::update(SoldTicket::findOrFail($id), $request->all());
            return redirect()->back()->with('message', 'Sale information updated');
        } catch (ValidationException $e) {
            return back()->withInputs($request->all())->withErrors($e->errors());
        }
    }

    /**
    * @param integer $id
    * @return \Illuminate\Http\Response
    */
    public function deliveredTicket($id)
    {
        return view('modals.delivered-modal-content', [
            'inventory_quantity' => InventoryQuantityRepository::findById($id)->load([
                'inventory',
                'soldTicket',
            ]),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\DatatableResources\InventoryQuantityCollection
     */
    public function datatable(Request $request)
    {
        $filters = InventoryQuantityCollection::repositoryFilters($request);

        return (new InventoryQuantityCollection(InventoryQuantityRepository::getDelivered($filters)))
            ->withTotals(InventoryQuantityRepository::getDeliveredCount(collect($filters)->except(['start', 'length'])->toArray()));
    }
}
