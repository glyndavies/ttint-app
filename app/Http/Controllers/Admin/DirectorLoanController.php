<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\DirectorLoanEntry;
use Carbon\Carbon;

class DirectorLoanController extends Controller
{
	/**
	 *
	 */
    public function index()
    {
    	$entries = DirectorLoanEntry::all();
        return view('finances.director-loan.index', [
        	'entries' => $entries,
        ]);
    }

    /**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		$entry = DirectorLoanEntry::findOrFail($id);

		return view('finances.director-loan.show', [
            'entry' => $entry,
        ]);
    }

    /**
     * @return View
     */
    public function add()
    {
		$entry = new DirectorLoanEntry([
			'date' => Carbon::today(),
		]);
		
		return view('finances.director-loan.show', [
            'entry' => $entry,
        ]);
    }

	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
    	$entry = DirectorLoanEntry::findOrFail($id);
    	$data = $request->all();

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	$entry->update($data);

		return redirect()->route('finances.director-loan')->with('message', 'Entry updated');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
    	$data = $request->all();
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	DirectorLoanEntry::create($data);

		return redirect()->route('finances.director-loan')->with('message', 'Entry created');
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
		$entry = DirectorLoanEntry::findOrFail($id);
		$entry->delete();

        return redirect()->route('finances.director-loan')->with('message', 'Entry deleted');
    }

    /**
     * @param array $data
     * @return Validator
     */
    public function validator($data)
    {
    	return Validator::make($data, [
            'notes' => ['nullable', 'string'],
            'amount_incl_vat' => ['required', 'numeric'],
            'date' => ['date'],
        ]);
    }
}
