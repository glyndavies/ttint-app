<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use App\Models\Expense;
use App\Models\ExpenseCategory;
use App\Models\ExpenseSubCategory;
use App\Models\Card;
use Carbon\Carbon;

class ExpensesController extends Controller
{
	/**
	 * @return View
	 */
    public function index()
    {
    	$expenses = Expense::all();
        return view('finances.expenses.index', [
        	'expenses' => $expenses,
        ]);
    }


    /**
     * @param string $name_category
     * @return View
     */
    public function indexCategory($name_category)
    {
        if (! Gate::allows('manage-expense-category', $name_category)) {
            abort(403);
        }

        $category = ExpenseCategory::getByName($name_category);

        $expenses = Expense::getByCategoryId($category->id);

        return view('finances.expenses.index', [
        	'expenses' => $expenses,
            'hidden_columns' => ['category_name'],
            'category' => $category
        ]);
    }

    /**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		$expense = Expense::findOrFail($id);
		$categories = ExpenseCategory::all();
		$cards = Card::all();

		return view('finances.expenses.show', [
            'expense' => $expense,
            'categories' => $categories,
            'cards' => $cards
        ]);
    }

    /**
     * @param string $name_category
     * @param integer $id
     * @return View
     */    
    public function showCategory($name_category, $id)
    {
        if (! Gate::allows('manage-expense-category', $name_category)) {
            abort(403);
        }

		$category = ExpenseCategory::getByName($name_category);
        $subcategories = ExpenseSubCategory::all();
        
		$expense = Expense::findOrFail($id);
		$cards = Card::all();

		return view('finances.expenses.show', [
            'expense' => $expense,
            'categories' => [$category],
            'subcategories' => $subcategories,
            'cards' => $cards
        ]);
    }

    /**
     * @param integer $id
     * @return array
     */
    public function add()
    {
        $categories = ExpenseCategory::all();

        return $this->addView($categories);		
    }

    /**
     * @param integer $id
     * @return array
     */
    public function addCategory($name_category)
    {
        if (! Gate::allows('manage-expense-category', $name_category)) {
            abort(403);
        }

		$category = ExpenseCategory::getByName($name_category);

		return $this->addView([$category]);		
    }

    /**
     * @param array $categories
     * @return View
     */
    private function addView($categories)
    {
        $subcategories = ExpenseSubCategory::all();
        $expense = new Expense([
			'date' => Carbon::today(),
		]);
		
		$cards = Card::all();

		return view('finances.expenses.show', [
            'expense' => $expense,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'cards' => $cards
        ]);
    }

	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
        $redirect_to = $request->redirect_to;
        $data = $request->except('redirect_to');

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

        $expense = Expense::findOrFail($id);
        $expense->update($data);

        return redirect($redirect_to)->with('message', 'Expense updated');
    }

    /**
	 * @param Request $request
     * @param string $name_category
     * @param integer $id
     * @return Redirect
     */
    public function updateCategory(Request $request, $name_category, $id)
    {
        if (! Gate::allows('manage-expense-category', $name_category)) {
            abort(403);
        }

        $redirect_to = $request->redirect_to;
        $data = $request->except('redirect_to');

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

        $expense = Expense::findOrFail($id);
        $expense->update($data);

        return redirect($redirect_to)->with('message', 'Expense updated');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
    	$redirect_to = $request->redirect_to;
        $data = $request->except('redirect_to');
        
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	Expense::create($data);

        return redirect($redirect_to)->with('message', 'Expense created');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function createCategory(Request $request, $name_category)
    {
        if (! Gate::allows('manage-expense-category', $name_category)) {
            abort(403);
        }

    	$redirect_to = $request->redirect_to;
        $data = $request->except('redirect_to');
        
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	Expense::create($data);

        return redirect($redirect_to)->with('message', 'Expense created');
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function delete(Request $request, $id)
    {
		$expense = Expense::findOrFail($id);
		$expense->delete();

        $redirect_to = $request->redirect_to;

        return redirect($redirect_to)->with('message', 'Expense deleted');
    }

    /**
     * @param Request $request
     * @param string $name_category
     * @param integer $id
     * @return Redirect
     */
    public function deleteCategory(Request $request, $name_category, $id)
    {
        if (! Gate::allows('manage-expense-category', $name_category)) {
            abort(403);
        }

		$expense = Expense::findOrFail($id);
		$expense->delete();

        $redirect_to = $request->redirect_to;

        return redirect($redirect_to)->with('message', 'Expense deleted');
    }

    /**
     * @param array $data
     * @return Validator
     */
    public function validator($data)
    {
    	return Validator::make($data, [
            'category_id'     => ['required'],
            'sub_category_id' => ['required', 'integer', 'exists:expense_sub_categories,id'],
            'card_id'         => ['required'],
            'description'     => ['required', 'string'],
            'amount'          => ['required', 'numeric'],
            'vat'             => ['required', 'numeric'],
            'date'            => ['date'],
        ]);
    }
}
