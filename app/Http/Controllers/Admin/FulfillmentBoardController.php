<?php

namespace App\Http\Controllers\Admin;

use App\DatatableResources\EventFulfillmentCollection;
use App\Exports\EventFulfillmentExport;
use App\Facades\InventoryQuantityService;
use App\Facades\InventoryService;
use App\Http\Controllers\Controller;
use App\Facades\EventFulfillmentRepository;
use App\Imports\EventFulfillmentImport;
use App\Models\EventFulfillment;
use App\Models\Inventory;
use App\Models\InventoryQuantity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class FulfillmentBoardController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fulfillment-board.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateMass(Request $request)
    {
        $inventories = Inventory::with([
            'ticketStatus',
            'purchase',
            'inventoryQuantities',
            'inventoryQuantities.ticketStatus',
        ])
        ->where('event', $request->input('inventory-event'))
        ->where('arena', $request->input('inventory-arena'))
        ->where('event_date', $request->input('inventory-event-date'))
        ->where('time_of_event', $request->input('inventory-time-of-event'))
        ->get();

        $updatedInventories = InventoryService::updateMass($inventories, $request->all());
        $inventory = optional($updatedInventories->first()->inventoryQuantities->first())->inventory;

        return redirect()->route('fulfillment-board.index', [
            'event-fulfillment-index' => $inventory ? EventFulfillmentRepository::find([], [
                'partly' => [
                    'event_date_from' => now(),
                    'pending_cancelled' => 'N'
                ]
            ])->search(function($eventFulfillment) use ($inventory) {
                return $eventFulfillment->event == $inventory->event && $eventFulfillment->event_date == $inventory->event_date &&
                    $eventFulfillment->time_of_event == $inventory->time_of_event && $eventFulfillment->arena == $inventory->arena;
            }) : 0,
        ])->with('message', $updatedInventories->count() . ' inventory items successfully updated');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\DatatableResources\EventFulfillmentCollection
     */
    public function datatable(Request $request)
    {
        $eventFulfillmentFilters = $request->only(['start', 'length']);
        $inventoryRequest = $request->replace($request->except(['start', 'length']));

        $inventoryFilters = EventFulfillmentCollection::repositoryFilters($inventoryRequest);
        $inventoryFilters['partly'] = array_merge($inventoryFilters['partly'],
            [
                'event_date_from' => now(),
                'pending_cancelled' => 'N'
            ]
        );

        return (new EventFulfillmentCollection(EventFulfillmentRepository::find($eventFulfillmentFilters, $inventoryFilters)))
            ->withTotals(EventFulfillmentRepository::getEventFulfillmentsCount($eventFulfillmentFilters, collect($inventoryFilters)->except(['start', 'length'])));
    }

    /**
     * @param   Request  $request
     */
    public function massTicketsInHand(Request $request)
    {
        $inventoryQuantities = InventoryQuantityService::massInHand($request->all());
        $inventory = $inventoryQuantities->first()->inventory;

        return response()->json([
            'eventFulfillmentIndex' => EventFulfillmentRepository::find([], [
                'partly' => [
                    'event_date_from' => now(),
                    'pending_cancelled' => 'N'
                ]
            ])->search(function($eventFulfillment) use ($inventory) {
                return $eventFulfillment->event == $inventory->event && $eventFulfillment->event_date == $inventory->event_date &&
                    $eventFulfillment->time_of_event == $inventory->time_of_event && $eventFulfillment->arena == $inventory->arena;
            })
        ]);
    }

    /**
     * @param   Request  $request
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        $inventoryQuantityIds = explode(',', $request->input('inventory-quantity-ids'));
        $exportType = $request->input('export-type');
        return Excel::download(
            new EventFulfillmentExport($inventoryQuantityIds, $exportType),
            EventFulfillment::getExportNameFromInvQuantId($exportType, $inventoryQuantityIds[0]),
        );
    }

    /**
     * @param   Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(Request $request)
    {
        $eventFulfillmentImport = new EventFulfillmentImport;
        Excel::import($eventFulfillmentImport, $request->file('fileupload'));
        $inventory = InventoryQuantity::find($eventFulfillmentImport->inventoryQuantityIds[0])->inventory;

        return redirect()->route('fulfillment-board.index', [
            'event-fulfillment-index' => EventFulfillmentRepository::find([], [
                'partly' => [
                    'event_date_from' => now(),
                    'pending_cancelled' => 'N'
                ]
            ])->search(function($eventFulfillment) use ($inventory) {
                return $eventFulfillment->event == $inventory->event && $eventFulfillment->event_date == $inventory->event_date &&
                    $eventFulfillment->time_of_event == $inventory->time_of_event && $eventFulfillment->arena == $inventory->arena;
            })
        ])->with('message', 'Imported successfully');
    }

    /**
     * @param string
     */
    public function inventoryQuantities($inventoryQuantitiesIds)
    {
        return view('modals.event-fulfillment-inventories-modal-content', [
            'inventoryQuantities' => InventoryQuantity::whereIn('id', explode(',', $inventoryQuantitiesIds))->get()
        ]);
    }
}
