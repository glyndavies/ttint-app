<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\InventoryQuantityRepository;
use App\Facades\TicketTypeRepository;

class FulfillmentController extends Controller
{
	/**
     * @param string $ticket_type_slug
     * @return \Illuminate\Http\Response
     */
    public function index($ticket_type_slug)
    {
    	$ticketType = TicketTypeRepository::findBySlug($ticket_type_slug);

    	if ( !$ticketType ) {
    		abort(404);
    	}

        return view('fulfillment.index', [
            'ticketType' => $ticketType,
            'inventory_quantities' => InventoryQuantityRepository::getFulfillment($ticketType),
        ]);
    }
}