<?php

namespace App\Http\Controllers\Admin;

use App\Facades\KpiRepository;
use App\Models\Inventory;
use App\Models\TicketStatus;
use App\Models\InventoryQuantity;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home', [
            'events_30' => Inventory::whereBetween('event_date',[
                    Carbon::now(),
                    Carbon::now()->addMonth()
                ])->count(),
            'events_year' => Inventory::whereBetween('event_date',[
                    Carbon::now(),
                    Carbon::now()->endOfYear()
                ])->count(),
            'ticket_total' => InventoryQuantity::whereIn('ticket_status_id', [
                    TicketStatus::AVAILABLE,
                    TicketStatus::SPLIT
                ])->sum('number_of_tickets'),
            'ticket_sold' => InventoryQuantity::whereIn('ticket_status_id', [
                    TicketStatus::SOLD
                ])->sum('number_of_tickets'),
            'kpis_total_rebate' => KpiRepository::topRanks('total_rebate'),
            'kpis_trust_rebate' => KpiRepository::topRanks('trust_rebate'),
            'kpis_referral_rebate' => KpiRepository::topRanks('referral_rebate'),
            'kpis_cur_week_total_rebate' => KpiRepository::topRanks('cur_week_total_rebate'),
            'kpis_cur_month_total_rebate' => KpiRepository::topRanks('cur_month_total_rebate'),
        ]);
    }
}
