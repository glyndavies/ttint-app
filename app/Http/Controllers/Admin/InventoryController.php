<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\InventoryRepository;
use App\Facades\InventoryService;
use App\Facades\InventoryQuantityRepository;
use App\Facades\InventoryQuantityService;
use App\Models\Inventory;
use App\Models\TicketHandlingStatus;
use Illuminate\Http\Request;
use Carbon\Carbon;

class InventoryController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('inventory.index', [
            'inventories' => InventoryRepository::getInventory(),
        ]);
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('inventory.show', [
            'inventory' => InventoryRepository::findById($id),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request ,$id)
    {
        InventoryService::update(Inventory::findOrFail($id), $request->all());
        return redirect()->back()->with('message','Inventory updated');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeInHandMass(Request $request)
    {
        $inventoryQuantities = InventoryQuantityRepository::findByIds(json_decode($request->input('batchID')) ?? []);
        foreach ($inventoryQuantities as $inventoryQuantity) {
            $status = $inventoryQuantity->tickets_in_hand == '2' ? '1' : '2';
            InventoryQuantityService::inHand($inventoryQuantity, $status);
        }

        return redirect()->back()->with('message', 'In Hand changed');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handlingMass(Request $request)
    {
        $inventoryQuantities = InventoryQuantityRepository::findByIds(json_decode($request->input('batchID')) ?? []);
        $ticketHandlingStatus = TicketHandlingStatus::find($request->status);

        foreach ($inventoryQuantities as $inventoryQuantity) {
            InventoryQuantityService::handling($inventoryQuantity, $ticketHandlingStatus, $request->batchSelection);
        }

        return redirect()->back()->with('message', 'Handling status updated to ' . $ticketHandlingStatus->ticketHandlingName);
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function detailsTicket($id)
    {
        return view('modals.details-modal-content', [
            'inventory' => InventoryRepository::findById($id)->load([
                'purchase',
            ]),
        ]);
    }
}
