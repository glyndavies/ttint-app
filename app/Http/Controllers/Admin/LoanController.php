<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\LoanEntry;
use Carbon\Carbon;


class LoanController extends Controller
{
    public function index()
    {
    	$entries = LoanEntry::all();
        return view('finances.loan.index', [
        	'entries' => $entries,
        ]);
    }

    /**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		$entry = LoanEntry::findOrFail($id);

		return view('finances.loan.show', [
            'entry' => $entry,
        ]);
    }

    /**
     * @return View
     */
    public function add()
    {
		$entry = new LoanEntry([
			'date' => Carbon::today(),
		]);
		
		return view('finances.loan.show', [
            'entry' => $entry,
        ]);
    }

	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
    	$entry = LoanEntry::findOrFail($id);
    	$data = $request->all();

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	$entry->update($data);

		return redirect()->route('finances.loan')->with('message', 'Entry updated');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
    	$data = $request->all();
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	LoanEntry::create($data);

		return redirect()->route('finances.loan')->with('message', 'Entry created');
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
		$entry = LoanEntry::findOrFail($id);
		$entry->delete();

        return redirect()->route('finances.loan')->with('message', 'Entry deleted');
    }

    /**
     * @param array $data
     * @return Validator
     */
    public function validator($data)
    {
    	return Validator::make($data, [
            'notes' => ['nullable', 'string'],
            'amount_incl_vat' => ['required', 'numeric'],
            'date' => ['date'],
        ]);
    }
}
