<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Facades\PlatformUserRepository;
use App\Facades\PaymentRepository;
use App\Facades\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;

class PaymentsController extends Controller
{
    public function index()
    {
        return view('finances.payments.index', [
        	'payments' => PaymentRepository::findAll(),
        ]);
    }

	/**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		return view('finances.payments.show', [
            'payment' => PaymentRepository::findOrFail($id),
            'platformUsers' => PlatformUserRepository::findAll(),
        ]);
    }

    /**
     * @return View
     */
    public function add()
    {
		return view('finances.payments.show', [
            'payment' => new Payment([
                'date' => Carbon::today(),
            ]),
            'platformUsers' => PlatformUserRepository::findAll(),
        ]);
    }

    /**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
        try {
            PaymentService::update(PaymentRepository::findOrFail($id), $request->all());
            return redirect()->route('finances.payments')->with('message', 'Payment updated');
        } catch (ValidationException $e) {
            return back()->withInput()->withErrors($e->errors());
        }		
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
        try {
            PaymentService::create($request->all());
            return redirect()->route('finances.payments')->with('message', 'Payment created');
        } catch (ValidationException $e) {
            return back()->withInput()->withErrors($e->errors());
        }		
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
        PaymentService::delete(PaymentRepository::findOrFail($id));
        return redirect()->route('finances.payments')->with('message', 'Payment deleted');
    }
}
