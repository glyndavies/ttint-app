<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\PlatformUserRepository;
use App\Facades\PlatformUserService;
use App\Models\PlatformUser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PlatformUserController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        return view('platform-users.index', [
            'platformUsers' => PlatformUserRepository::findAll(),
        ]);
    }

    /**
     * @param integer $id
     * @return view
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function show($id)
    {
        return view('platform-users.show', [
            'platformUser' => PlatformUserRepository::findOrFail($id),
        ]);
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return RedirectResponse
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(Request $request, $id)
    {
        try {
            PlatformUserService::update(PlatformUserRepository::findOrFail($id), $request->all());
            return redirect()->route('platform-users')->with('message', 'User updated');
        } catch (ValidationException $e) {
            return back()->withInput()->withErrors($e->errors());
        }
    }

    /**
     * @param Request $request
     * @param integer $id
     */
    public function delete(Request $request, $id)
    {
        PlatformUserService::delete(PlatformUserRepository::findOrFail($id));
        return redirect()->route('platform-users')->with('message', 'User deleted');
    }
}
