<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\PurchaseRepository;
use App\Facades\PurchaseService;
use App\DatatableResources\PurchaseCollection;
use Illuminate\Validation\ValidationException;
use App\Models\Purchase;
use Illuminate\Http\Request;

class PurchasesController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('purchases.index', [
                'totalCost' => PurchaseRepository::getPurchasesTotalCost()
            ])
        ;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Resource\PurchaseResource[]
     */
    public function datatable(Request $request)
    {
        $filters = PurchaseCollection::repositoryFilters($request);

        return (new PurchaseCollection(PurchaseRepository::getPurchases($filters)))
            ->withTotals(PurchaseRepository::getPurchasesCount(collect($filters)->except(['start', 'length'])->toArray()))
            ->withTotalCost(PurchaseRepository::getPurchasesTotalCost(collect($filters)->except(['start', 'length'])->toArray()));
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('purchases.show', [
            'ticket' => PurchaseRepository::findById($id),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            PurchaseService::update(PurchaseRepository::findById($id), $request->all());
            return back()->with('message', 'Purchase details updated');
        } catch (ValidationException $e) {
            return back()->withInputs($request->all())->withErrors($e->errors());
        }
    }

    /**
     * @param \Illuminate\Http\Request
     * @param string $purchase_number
     * @return \Illuminate\Http\Response
     */
    public function updateBuyerInfo(Request $request, $purchase_number)
    {
        PurchaseService::updateBuyerInfo(Purchase::where('purchase_number', $purchase_number)->first(), $request->all());
        return redirect()->back()->with('message','Buyer info updated');
    }

    public function investmentCalculator($data)
    {
        $buyfees = $data->buyers_fees;
        $FVOT  = $data->face_value_of_tickets;
        $u3 = $data->buyer;
        $totalcost = $FVOT + $buyfees;

        if(!$u3) {
            $investment = 0.00;
        } else {
            if ($u3 == 'CROW') {
                $investment =  $totalcost*0.5;
            } else {
                $investment =$totalcost*0.35;
            }
        }
        
        return $investment;
    }

    public function reconcileMass(Request $request)
    {
        $this->validate($request,[
            'batchID'=>'required',
           // 'status'=>'required',
        ]);

        $replaced = str_replace(
            array('[',']','"'),
            array('', '',''),
            $request->batchID
        );

        $ids = explode(',',$replaced);
        $status= $request->status;

        Purchase::whereIN('id',$ids)
            ->where('reconciled','No')
            ->update(['reconciled'=>'Yes','pending_reconciled'=>'No']);

        return redirect()->back()->with('message',"Reconciled Selected items");
    }
}
