<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EventGenre;
use App\Models\TicketStatus;
use App\Models\TicketType;
use App\Models\ExpenseSubCategory;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.index');
    }

    /**
     * Save or update Genre.
     */

    public function updateEventGenre(Request $request ,$id = null){

        $this->validate($request,[
           'genre' => 'required | unique:event_genres'
        ]);
        if($request->active == 'on'){
            $active = 1;
        }else{
            $active = 0;
        }
        if($id != null){
            $genres =  EventGenre::find($id);
            $genres->genre =  $request->genre;
            $genres->active = $active;
            $genres->update();

        }else{
            $genres =  new EventGenre();
            $genres->genre =  $request->genre;
            $genres->active =  1;
            $genres->save();
        }

        return redirect()->route('settings.index')->with('message', 'Genre saved');

    }
    /**
     * Remove Genre.
     */

    public function removeEventGenre($id){



        EventGenre::find($id)->delete();
        return redirect()->route('settings.index')->with('message', 'Genre removed');

    }

    /**
     * update the Event Genre.
     */
    public function showEventGenre($id)
    {
        $data['genre']  = EventGenre::find($id);

       return view('settings.genre.show',$data);
    }


   ////TICKET TYPE



    /**
     * Save or update Genre.
     */

    public function updateTicketTypes(Request $request ,$id = null){

        $this->validate($request,[
            'tickettype' => 'required',
            'slug'       => 'required',
        ]);

        if($request->active == 'on'){
            $active = 1;
        }else{
            $active = 0;
        }


        if($id != null){
            $genres =  TicketType::find($id);
            $genres->tickettype =  $request->tickettype;
            $genres->slug =  $request->slug;
            $genres->active =  $active;
            $genres->update();

        }else{
            $genres =  new TicketType();
            $genres->tickettype =  $request->tickettype;
            $genres->slug =  $request->slug;
            $genres->active =  1;
            $genres->save();
        }

        return redirect()->route('settings.index')->with('message', 'Ticket Type saved');

    }
    /**
     * Remove Genre.
     */

    public function removeTicketTypes($id){
        TicketType::find($id)->delete();
        return redirect()->route('settings.index')->with('message', 'Ticket Type removed');

    }

    /**
     * update the Event Genre.
     */
    public function showTicketTypes($id)
    {
        $data['ticket']  = TicketType::find($id);

        return view('settings.ticket-type.show',$data);
    }

    /**
     * Save or update Genre.
     */

    public function updateTicketStatus(Request $request ,$id = null){

        $this->validate($request,[
            'ticketStatus' => 'required | unique:ticket_status'
        ]);
        if($request->active == 'on'){
            $active = 1;
        }else{
            $active = 0;
        }
        if($id != null){
            $genres =  TicketStatus::find($id);
            $genres->ticketStatus =  $request->ticketStatus;
            $genres->active = $active;
            $genres->update();

        }else{
            $genres =  new TicketStatus();
            $genres->ticketStatus =  $request->ticketStatus;
            $genres->active =  1;
            $genres->save();
        }

        return redirect()->route('settings.index')->with('message', 'Ticket Status saved');

    }
    /**
     * Remove Genre.
     */

    public function removeTicketStatus($id){

        TicketStatus::find($id)->delete();
        return redirect()->route('settings.index')->with('message', 'Status removed');

    }

    /**
     * update the Event Genre.
     */
    public function showTicketStatus($id)
    {
        $data['status']  = TicketStatus::find($id);

        return view('settings.ticket-status.show',$data);
    }

    //// Expense Subcategories

    // Update / Create expense subcategory
    /**
     * Save or update Genre.
     */

    public function updateSubcategories(Request $request, $id = null)
    {
        
        $this->validate($request, [
            'name' => ['required', 'string']
        ]);

        if ($id != null) {
            $subcategory = ExpenseSubCategory::find($id);
            $subcategory->name = $request->name;
            $subcategory->update();

            return redirect()->route('settings.index')->with('message', 'Expense subcategory updated');
        } else {
            $subcategory = new ExpenseSubCategory();
            $subcategory->name = $request->name;
            $subcategory->save();

            return redirect()->route('settings.index')->with('message', 'Expense subcategory created');
        }
    }

    /**
     * Show and update the Expense subcategory.
     */
    public function showSubcategory($id)
    {
        $data['subCategory'] = ExpenseSubCategory::find($id);

        return view('settings.expense-subcategory.show', $data);
    }

    /**
     * Remove Genre.
     */

    public function removeSubcategory($id)
    {
        ExpenseSubCategory::find($id)->delete();
        return redirect()->route('settings.index')->with('message', 'Expense subcategory removed');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
