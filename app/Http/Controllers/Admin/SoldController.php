<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\InventoryRepository;
use App\Facades\InventoryQuantityRepository;
use App\Facades\SoldTicketService;
use App\Facades\SoldTicketRepository;
use App\DatatableResources\InventoryQuantityCollection;
use App\Models\Inventory;
use App\Models\InventoryQuantity;
use App\Models\SoldTicket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class SoldController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sold.index');
        /*return view('sold.index', [
            'inventory_quantities' => InventoryQuantityRepository::getSold(),
        ]);*/
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('sold.show', [
            'inventory_quantity' => InventoryQuantityRepository::findById($id)->load([
                'inventory',
                'soldTicket'
            ]),
        ]);
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function sellTicket($id)
    {
        return view('modals.sale-modal-content', [
            'inventory' => InventoryRepository::findById($id)->load([
                'purchase',
            ]),
        ]);
    }

    /**
     * @param integer $id
     * @return \App\Models\InventoryQauntity|null
     */
    public function sell($id)
    {
        return InventoryQuantityRepository::findById($id)->load([
            'inventory',
        ]);
    }

    /**
     * @param integer $id
     */
    public function refund($id)
    {
        return SoldTicketService::refund(SoldTicketRepository::findById($id));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JSONResponse
     */
    public function store(Request $request)
    {
        $soldTickets = SoldTicketService::createCollection($request->all());
        return Response()->json($soldTickets->pluck('inventory_quantity_id'), 200);
    }

    /**
     * @param integer $id
     * @return \App\Models\SoldTicket
     */
    public function getSoldTicket($id)
    {
        return SoldTicket::findOrFail($id);
    }

    /**
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function soldTicket($id)
    {
        return view('modals.sold-modal-content', [
            'inventory_quantity' => InventoryQuantityRepository::findById($id)->load([
                'inventory',
                'soldTicket'
            ]),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\DatatableResources\InventoryQuantityCollection
     */
    public function datatable(Request $request)
    {
        $filters = InventoryQuantityCollection::repositoryFilters($request);

        return (new InventoryQuantityCollection(InventoryQuantityRepository::getSold($filters)))
            ->withTotals(InventoryQuantityRepository::getSoldCount(collect($filters)->except(['start', 'length'])->toArray()));
    }
}
