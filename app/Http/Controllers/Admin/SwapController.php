<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use App\Models\InventoryQuantity;
use App\Models\Swap;
use App\Models\TicketStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SwapController extends Controller
{
    public function availableForSwap($id)
    {
        $inventory = Inventory::findOrFail($id);
        $tickets = $inventory->inventoryQuantities;

        $inventory->ticket_status_id = Inventory::AVAILABLE_FOR_SWAP;
        $inventory->save();

        foreach($tickets as $ticket) {
            $ticket->ticket_status_id = Inventory::AVAILABLE_FOR_SWAP;
            $ticket->save();
        }
        
        return back()->with('message', 'Moved to "Available for swap" section');
    }

    public function returnToAvailable($id)
    {
        $inventory = Inventory::findOrFail($id);
        $tickets = $inventory->inventoryQuantities;

        $inventory->ticket_status_id = Inventory::AVAILABLE;
        $inventory->save();

        foreach($tickets as $ticket) {
            $ticket->ticket_status_id = Inventory::AVAILABLE;
            $ticket->save();
        }
        
        return back()->with('message', 'Returned to "Available" section');
    }

    public function available(Request $request, $id)
    {
        $this->validate($request, [
            'transfer_price' => ['numeric', 'required'],
            'owner_code' => ['string', 'required'],
            'reconciled' => ['in:on,off'],
        ]);

        $inventory = Inventory::findOrFail($id);
        $tickets = $inventory->inventoryQuantities;

        $old_buyer = $inventory->buyer;

        $inventory->transfer_price = $request->input('transfer_price');
        $inventory->buyer = $request->input('owner_code');
        $inventory->reconciled = $request->input('reconciled') == 'on' ? 1 : 0;
        $inventory->ticket_status_id = Inventory::AVAILABLE;
        $inventory->save();

        foreach($tickets as $ticket) {
            $ticket->ticket_status_id = Inventory::AVAILABLE;
            $ticket->save();
        }
        
        // create swap
        if ($inventory->buyer != $old_buyer) {
            $swap = Swap::create();
            $swap->inventory0_id = null;
            $swap->inventory1_id = $inventory->id;
            $swap->buyer0 = $inventory->buyer;
            $swap->buyer1 = $old_buyer;
            $swap->save();            
        }

        return back()->with('message', 'Moved to "Inventory" section');
    }

    public function change(Request $request, $id)
    {
        $this->validate($request, [
            'transfer_price' => ['numeric', 'required'],
            'owner_code' => ['string', 'required'],
            'reconciled' => ['in:on,off'],
        ]);

        $inventory = Inventory::findOrFail($id);
        $tickets = $inventory->inventoryQuantities;

        $old_buyer = $inventory->buyer;

        $inventory->buyer = $request->input('owner_code');
        $inventory->transfer_price = $request->input('transfer_price');
        $inventory->reconciled = $request->input('reconciled') == 'on' ? 1 : 0;
        $inventory->save();

        // create swap
        if ($inventory->buyer != $old_buyer) {
            $swap = Swap::create();
            $swap->inventory0_id = null;
            $swap->inventory1_id = $inventory->id;
            $swap->buyer0 = $inventory->buyer;
            $swap->buyer1 = $old_buyer;
            $swap->save();
        }
        
        return back()->with('message', 'User code changed');
    }

    public function swap(Request $request)
    {
        $inventory0 = Inventory::where('purchase_number', $request->input('purchase_number0'))->firstOrFail();
        $inventory1 = Inventory::where('purchase_number', $request->input('purchase_number1'))->firstOrFail();
            
        // create swap
        $swap = Swap::create();
        $swap->inventory0_id = $inventory0->id;
        $swap->inventory1_id = $inventory1->id;
        $swap->buyer0 = $inventory0->buyer;
        $swap->buyer1 = $inventory1->buyer;
        $swap->save();

        // swap inventories
        $transit_buyer = $inventory0->buyer;

        if ($inventory0) {
            $inventory0->ticket_status_id = TicketStatus::SWAPPED;
            $inventory0->buyer = $inventory1->buyer;
            $inventory0->save();            
        }

        if ($inventory1) {
            $inventory1->ticket_status_id = TicketStatus::SWAPPED;
            $inventory1->buyer = $transit_buyer;       
            $inventory1->save();            
        }

        // change inventory_quantity status
        foreach([$inventory0, $inventory1] as $inventory) {
            foreach($inventory->inventoryQuantities as $quantity) {
                $quantity->ticket_status_id = TicketStatus::SWAPPED;
                $quantity->save();
            }
        }

        return back()->with('message', "{$inventory0->purchase_number} and {$inventory1->purchase_number} are swapped successfully");
    }
}
