<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Tax;
use Carbon\Carbon;

class TaxesController extends Controller
{
    public function index()
    {
    	$taxes = Tax::all();
        return view('finances.taxes.index', [
        	'taxes' => $taxes,
        ]);
    }

    /**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		$tax = Tax::findOrFail($id);

		return view('finances.taxes.show', [
            'tax' => $tax,
        ]);
    }

    /**
     * @return View
     */
    public function add()
    {
		$tax = new Tax([
			'date' => Carbon::today(),
		]);
		
		return view('finances.taxes.show', [
            'tax' => $tax,
        ]);
    }

	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
    	$tax = Tax::findOrFail($id);
    	$data = $request->all();

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	$tax->update($data);

		return redirect()->route('finances.taxes')->with('message', 'Tax updated');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
    	$data = $request->all();
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	Tax::create($data);

		return redirect()->route('finances.taxes')->with('message', 'Tax created');
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
		$tax = Tax::findOrFail($id);
		$tax->delete();

        return redirect()->route('finances.taxes')->with('message', 'Tax deleted');
    }

    /**
     * @param array $data
     * @return Validator
     */
    public function validator($data)
    {
    	return Validator::make($data, [
            'notes' => ['nullable', 'string'],
            'amount_incl_vat' => ['required', 'numeric'],
            'date' => ['date'],
        ]);
    }
}
