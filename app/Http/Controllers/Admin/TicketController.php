<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TicketHandlingStatus;
use App\Facades\InventoryRepository;
use App\Facades\InventoryService;
use App\Facades\InventoryQuantityRepository;
use App\Facades\InventoryQuantityService;
use App\Facades\SoldTicketService;
use App\Facades\SoldTicketRepository;
use App\DatatableResources\InventoryCollection;
use App\DatatableResources\InventoryQuantityCollection;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Exception;

class TicketController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function ticketsAll()
    {
        return view('tickets.all');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\DatatableResources\InventoryCollection
     */
    public function ticketsAllDatatable(Request $request)
    {
        $filters = InventoryQuantityCollection::repositoryFilters($request);

        return (new InventoryQuantityCollection(InventoryQuantityRepository::getAllInventory($filters)))
            ->withTotals(InventoryQuantityRepository::getAllInventoryCount(collect($filters)->except(['start', 'length'])->toArray()));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param integer $inventory_quantity_id
     * @return \Illuminate\Http\Response
     */
    public function updateBatch(Request $request, $inventory_quantity_id)
    {
        try {
            InventoryQuantityService::update(
                InventoryQuantityRepository::findById($inventory_quantity_id),
                $request->all()
            );
            return back()->with('message','Batch have been updated');
        } catch (ValidationException $e) {
            return back()->withInputs($request->all())->withErrors($e->errors());
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param integer $inventory_quantity_id
     * @return \Illuminate\Http\Response
     */
    public function splitBatch(Request $request, $inventory_quantity_id)
    {
        try {
            $inventoryQuantities = InventoryQuantityService::split(
                InventoryQuantityRepository::findById($inventory_quantity_id),
                $request->all()
            );
            return $request->api ? $inventoryQuantities->toJson() : back()->with('message','Tickets have been split');

        } catch (ValidationException $e) {
            if ($request->api) {
                throw $e;
            } else {
                return back()->withInputs($request->all())->withErrors($e->errors());    
            }
            
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param integer $inventory_quantity_id
     * @return \Illuminate\Http\Response
     */
    public function returnToStock(Request $request, $inventory_quantity_id)
    {
        try {
            InventoryQuantityService::returnBatchToStock(InventoryQuantityRepository::findById($inventory_quantity_id));
            return back()->with('message','Restocked ticket successfully');
        } catch (ValidationException $e) {
            return back()->withInputs($request->all())->withErrors($e->errors());
        } catch (Exception $e) {
            return back()->withInputs($request->all())->with('error', '!!!!!!!!!!!!!!!ISSUE WITH RESTOCKING TICKET !!!!!!!!!!!!!');
        }
    }

    /**
     * @param integer $inventory_id
     * @param integer $status
     * @return \App\Models\Inventory
     */
    public function inventoryInHand($inventory_id, $status)
    {
        return InventoryService::inHand(InventoryRepository::findById($inventory_id), $status);
    }

    /**
     * @param integer $inventory_quantity_id
     * @param integer $status
     * @return \App\Models\Inventory
     */
    public function ticketsInHand($inventory_quantity_id, $status)
    {
        return InventoryQuantityService::inHand(InventoryQuantityRepository::findById($inventory_quantity_id), $status);
    }

    /**
     * @param integer $inventory_quantity_id
     * @param integer $handling_status
     */
    public function handling($inventory_quantity_id, $handling_status)
    {
        return InventoryQuantityService::handling(InventoryQuantityRepository::findById($inventory_quantity_id), TicketHandlingStatus::find($handling_status), null);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     */
    public function refundTo(Request $request, $id)
    {
        $data_refund_to = $request->all();
        $default_states = ['delivered', 'sold', 'inventory', 'expired'];

        if(!empty($data_refund_to['refund_to'])){
            [$refund_to] = array_values($data_refund_to);
            $last_state_key = array_search($refund_to, $default_states);
            $refund_states = array_slice($default_states, 0, $last_state_key + 1, true);

            if(count($refund_states)){
                $inventory_quantity_id = $inventory_purchase_number = 0;

                if($refund_to != 'delivered'){
                    $sold_ticket = SoldTicketRepository::findById($id);
                    $inventory_quantity_id = $sold_ticket->inventory_quantity_id;
                    $inventory_purchase_number = $sold_ticket->purchase_number;
                }    

                foreach($refund_states as $state_name){
                    switch ($state_name) {
                        case 'delivered':
                            $results['to_delivered'] = SoldTicketService::refund(SoldTicketRepository::findById($id));
                            break;
                        case 'sold':
                            $inventory_quantity = InventoryQuantityRepository::findById($inventory_quantity_id);
                            $results['to_sold'] = InventoryQuantityService::handling($inventory_quantity, $inventory_quantity->ticketHandlingStatus, null);
                            break;
                        case 'inventory':
                            $results['to_inventory'] = InventoryQuantityService::returnBatchToStock(InventoryQuantityRepository::findById($inventory_quantity_id));
                            break;
                        case 'expired':
                            $results['to_expired'] = InventoryService::expire(InventoryRepository::findById($inventory_purchase_number));
                            break;
                    }    
                }

                return response()->json(['message'=> 'Ticked moved to "'. strtoupper($refund_to). '" stage.', 'toStage' => $refund_to]);
            } else {
                $results['errors'][] = 'Incorrect value "refund_to"';
            }
        } else {
            $results['errors'][] = 'Empty or Missing value "refund_to"';
        }

        return $results['errors']? response()->json(['message'=> implode('. ',$results['errors'])], 500): response()->json(['message'=> 'ERROR'], 500);     
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\DatatableResources\InventoryCollection
     */
    public function datatable(Request $request)
    {
        $filters = InventoryCollection::repositoryFilters($request);

        return (new InventoryCollection(InventoryRepository::getInventory($filters)))
            ->withTotals(InventoryRepository::getInventoryCount(collect($filters)->except(['start', 'length'])->toArray()));
    }
}
