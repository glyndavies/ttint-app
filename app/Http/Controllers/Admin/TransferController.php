<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facades\InventoryRepository;
use App\Facades\InventoryService;
use App\Models\PlatformUser;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transfer.index', [
        	'inventories' => InventoryRepository::getTransfer(),
            'platformUsers' => PlatformUser::all()
        ]);
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param integer $inventory_quantity_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateUserCode(Request $request, $inventory_id)
    {
        try {
            InventoryService::updateUserCode(InventoryRepository::findById($inventory_id), $request->all());
            return back()->with('message','User code & token updated');
        } catch (ValidationException $e) {
            return back()->withInputs($request->all())->withErrors($e->errors());
        }
    }
}