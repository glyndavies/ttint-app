<?php

namespace App\Http\Controllers\Admin;

use App\Imports\BusinessExpenseImport;
use Illuminate\Http\Request;
use Exception;

class UploadBusinessExpensesController extends UploadController
{
    /**
     * @return View
     */
    public function show()
    {
        return view('upload.business-expenses.index');
    }

    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, BusinessExpenseImport::class, 'business-expenses', 'USERUPLOADS');
    }

    /**
     *
     */
    public function process()
    {
        try {
            $count = BusinessExpenseImport::process();
        } catch(Exception $e) {
            return redirect()->route('upload.business-expenses')->withErrors([$e->getMessage()]);
        }        

        return redirect()
            ->route('upload.business-expenses')
            ->with('message', sprintf("%d business expense(s) have been imported. Move to <a href='%s'><b>Business Expenses</b><a>", $count, route('finances.business-expenses')));
    }
}
