<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TempDataTable;
use App\Models\TempTableLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Excel;
use DB;
use Storage;

class UploadController extends Controller
{  
    /**
     * @return Redirect
     */
    public function destroy()
    {
        DB::table('temp_data_table')->truncate();
        return redirect()->back()->with('message', 'Temp table now empty');
    }

    /**
     * @param  Request  $request
     * @return View
     */
    protected function storeClass(Request $request, $importClass, $viewName, $storeFolderTitle)
    {
        $validator = Validator::make($request->all(), [
            'fileupload' => ['required'],
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

        DB::table('temp_data_table')->truncate();
        DB::table('temp_table_logs')->truncate();

        Excel::import(new $importClass, Request()->file('fileupload'));
        Storage::disk('local')->put($storeFolderTitle.'/'.Carbon::now()->format('d-m-y H:i:s').'/',  Request()->file('fileupload'));

        $tem = TempDataTable::select('data')->get()->toArray();
        $error_logs =  TempTableLog::all();

        if ( !$error_logs->count() ) {
            return view('upload.' . $viewName . '.process', [
                'tem' => $tem,
                'error_logs' => $error_logs,
            ]);

        } else {

            $errors = $error_logs->map(function($item) {
                return $item->issue_reason;
            });

            return back()->withInput()->withErrors($errors);
        }

    }
}
