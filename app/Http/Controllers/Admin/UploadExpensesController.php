<?php

namespace App\Http\Controllers\Admin;

use App\Imports\ExpenseImport;
use Illuminate\Http\Request;
use Exception;

class UploadExpensesController extends UploadController
{
    /**
     * @return View
     */
    public function show()
    {
        return view('upload.expenses.index');
    }

    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, ExpenseImport::class, 'expenses', 'USERUPLOADS');
    }

    /**
     *
     */
    public function process()
    {
        try {
            $count = ExpenseImport::process();
        } catch(Exception $e) {
            return redirect()->route('upload.expenses')->withErrors([$e->getMessage()]);
        }        

        return redirect()
            ->route('upload.expenses')
            ->with('message', sprintf("%d expense(s) have been imported. Move to <a href='%s'><b>Expenses</b><a>", $count, route('finances.expenses')));
    }
}
