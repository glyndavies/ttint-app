<?php

namespace App\Http\Controllers\Admin;

use App\Imports\HistoricImport;
use Illuminate\Http\Request;
use Exception;

class UploadHistoricController extends UploadController
{
    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, HistoricImport::class, 'tickets', 'CSVUPLOADS');
    }

}
