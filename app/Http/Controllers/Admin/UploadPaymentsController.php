<?php

namespace App\Http\Controllers\Admin;

use App\Imports\PaymentImport;
use Illuminate\Http\Request;
use Exception;

class UploadPaymentsController extends UploadController
{
    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, PaymentImport::class, 'payments', 'PAYMENTS');
    }

    /**
     *
     */
    public function process()
    {
        try {
            $count = PaymentImport::process();
        } catch(Exception $e) {
            return redirect()->route('finances.payments')->withErrors([$e->getMessage()]);
        }  
        
        return redirect()->route('finances.payments')->with('message', sprintf("%d records matched and processed", $count));
    }
}