<?php

namespace App\Http\Controllers\Admin;

use App\Imports\ReconcileImport;
use Illuminate\Http\Request;
use Exception;

class UploadReconcileController extends UploadController
{
    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, ReconcileImport::class, 'reconcile', 'DELIVERED');
    }

    /**
     *
     */
    public function process()
    {
        try {
            $count = ReconcileImport::process();
        } catch(Exception $e) {
            return redirect('/admin/delivered')->withErrors([$e->getMessage()]);
        }  

        return redirect('/admin/delivered')
            ->with('message', sprintf("%d records matched and processed", $count));
    }

}
