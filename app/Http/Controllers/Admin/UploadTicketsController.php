<?php

namespace App\Http\Controllers\Admin;

use App\Imports\TicketImport;
use Illuminate\Http\Request;
use Exception;

class UploadTicketsController extends UploadController
{
    /**
     * @return View
     */
    public function show()
    {
        return view('upload.tickets.index');
    }

    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, TicketImport::class, 'tickets', 'CSVUPLOADS');
    }

    /*
     *
     */
    public function process()
    {
        try {
            $count = TicketImport::process();
        } catch(Exception $e) {
            return redirect()->route('upload.tickets')->withErrors([$e->getMessage()]);
        } 

        return redirect()->route('upload.tickets')
            ->with('message', sprintf("%d ticket(s) have been imported. Move to <a href='%s'><b>Inventory</b><a> or <a href='%s'><b>Transfer Area</b><a>", $count, route('tickets.index'), route('transfer')));
    }

}
