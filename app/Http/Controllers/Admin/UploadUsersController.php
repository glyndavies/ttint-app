<?php

namespace App\Http\Controllers\Admin;

use App\Imports\UserImport;
use Illuminate\Http\Request;
use Exception;

class UploadUsersController extends UploadController
{
    /**
     * @return View
     */
    public function show()
    {
        return view('upload.users.index');
    }

    /**
     * @param  Request  $request
     * @return View
     */
    public function store(Request $request)
    {
        return $this->storeClass($request, UserImport::class, 'users', 'USERUPLOADS');
    }

    /**
     *
     */
    public function process()
    {
        try {
            $count = UserImport::process();
        } catch(Exception $e) {
            return redirect()->route('upload.users')->withErrors([$e->getMessage()]);
        }        

        return redirect()
            ->route('upload.users')
            ->with('message', sprintf("%d user(s) have been imported. Move to <a href='%s'><b>Users</b><a>", $count, route('platform-users')));
    }
}
