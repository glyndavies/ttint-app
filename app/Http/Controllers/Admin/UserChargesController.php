<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UserCharge;
use App\Models\PlatformUser;
use Carbon\Carbon;

class UserChargesController extends Controller
{
    public function index()
    {
    	$userCharges = UserCharge::all();

        return view('finances.user-charges.index', [
        	'userCharges' => $userCharges,
        ]);
    }

    /**
     * @param integer $id
     * @return View
     */
    public function show($id)
    {
		$userCharge = UserCharge::findOrFail($id);
		$platformUsers = PlatformUser::all();

		return view('finances.user-charges.show', [
            'userCharge' => $userCharge,
            'platformUsers' => $platformUsers,
        ]);
    }

	/**
     * @return View
     */
    public function add()
    {
		$userCharge = new UserCharge([
			'date' => Carbon::today(),
		]);
		
		$platformUsers = PlatformUser::all();

		return view('finances.user-charges.show', [
            'userCharge' => $userCharge,
            'platformUsers' => $platformUsers,
        ]);
    }

	/**
	 * @param Request $request
     * @param integer $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
    	$userCharge = UserCharge::findOrFail($id);
    	$data = $request->all();

        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	$userCharge->update($data);

		return redirect()->route('finances.user-charges')->with('message', 'User charge updated');
    }

    /**
	 * @param Request $request
	 * @return Redirect
     */
    public function create(Request $request)
    {
    	$data = $request->all();
        $validator = $this->validator($data);
		if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

    	UserCharge::create($data);

		return redirect()->route('finances.user-charges')->with('message', 'User charge created');
    }

    /**
     * @param integer $id
     * @return Redirect
     */
    public function delete($id)
    {
		$userCharge = UserCharge::findOrFail($id);
		$userCharge->delete();

        return redirect()->route('finances.user-charges')->with('message', 'User charge deleted');
    }

    /**
     * @param array $data
     * @return Validator
     */
    public function validator($data)
    {
    	return Validator::make($data, [
            'user_id' => ['required', 'integer'],
            'description' => ['required', 'string'],
            'amount' => ['required', 'numeric'],
            'vat' => ['required', 'numeric'],
            'date' => ['date'],
        ]);
    }
}
