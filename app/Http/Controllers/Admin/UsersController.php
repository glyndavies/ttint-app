<?php

namespace  App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Models\Audit;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreUsersRequest;
USE App\Http\Requests\Admin\UpdateUsersRequest;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        $data['roles'] = Role::all();
        $data['users'] = User::all();
        return view('users.index', $data);
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        if (Gate::allows('manage administrators')) {
            $roles = Role::get()->pluck('name', 'name');
        } else {
            $roles = Role::where('name', '<>', 'super-admin')->get()->pluck('name', 'name');
        }
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        $user = User::create([
            'name' => $request->firstanme.' '.$request->surname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'firstname'=> $request->firstname,
            'surname'=> $request->surname,
            'dob'=> $request->DOB,
        ]);
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        return redirect()->back()->with('message','User added to system');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        if (Gate::allows('manage users')) {
            $roles = Role::get()->pluck('name', 'name');
        } else {
            $roles = Role::where('name', '<>', 'super-admin')->get()->pluck('name', 'name');
        }
        $user = User::findOrFail($id);
        $audits = Audit::where('user_id',$id)->get();


        return view('users.show', compact('user', 'roles','audits'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, $id)
    {
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        $user = User::findOrFail($id);


        $user->update([
            'name' =>  $request->firstanme.' '.$request->surname,
            'email' => $request->email,
            'firstname'=> $request->firstanme,
            'surname'=> $request->surname,
            'dob'=> $request->DOB,
            'houseNumber'=> $request->houseNumber,
            'Address1'=> $request->Address1,
            'Address2'=> $request->Address2,
            'town'=> $request->town,
            'city'=> $request->city,
            'postcode'=> $request->postcode
        ]);


        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
        }

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return redirect()->back()->with('message','User details updated');
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->back()->with('message','User removed');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('manage users')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = User::whereIn('id', $request->input('ids'))->get();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
