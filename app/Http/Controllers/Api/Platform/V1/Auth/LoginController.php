<?php

namespace App\Http\Controllers\Api\Platform\V1\Auth;

use App\Http\Controllers\Controller;
use App\Facades\PlatformUserService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        return [
            'access_token' => PlatformUserService::createToken($request->all())->plainTextToken
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function logout(Request $request)
    {
        return PlatformUserService::revokeCurrentToken($request->user());
    }
}