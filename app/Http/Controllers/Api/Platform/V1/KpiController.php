<?php

namespace App\Http\Controllers\Api\Platform\V1;

use App\Http\Controllers\Controller;
use App\Resources\KpiStateResource;
use App\Facades\PlatformUserRepository;
use Illuminate\Http\Request;

class KpiController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function getState(Request $request)
    {
        return new KpiStateResource(
            $request->user()
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function getStateByUserCode(Request $request)
    {
        if ( !($user = PlatformUserRepository::findByUserCode($request->input('user_code'))) ) {
            abort(404);
        }
        return new KpiStateResource($user);
    }
}