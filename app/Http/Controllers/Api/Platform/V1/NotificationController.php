<?php

namespace App\Http\Controllers\Api\Platform\V1;

use App\Http\Controllers\Controller;
use App\Facades\PlatformUserRepository;
use App\Resources\NotificationHistoryResource;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function history(Request $request)
    {
        return new NotificationHistoryResource($request->user());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function getHistoryByUserCode(Request $request)
    {
        if ( !($user = PlatformUserRepository::findByUserCode($request->input('user_code'))) ) {
            abort(404);
        }

        return new NotificationHistoryResource($user);
    }
}