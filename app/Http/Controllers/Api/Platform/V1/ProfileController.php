<?php

namespace App\Http\Controllers\Api\Platform\V1;

use App\Http\Controllers\Controller;
use App\Resources\PlatformProfileResource;
use App\Facades\PlatformUserRepository;
use App\Facades\PlatformUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformProfileResource
     */
    public function get(Request $request)
    {
        return new PlatformProfileResource(
            $request->user()
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformProfileResource
     */
    public function getByUserCode(Request $request)
    {
        if ( !($user = PlatformUserRepository::findByUserCode($request->input('user_code'))) ) {
            abort(404);
        }

        return new PlatformProfileResource(
            $user
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformProfileResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        return new PlatformProfileResource(
            PlatformUserService::updateProfile($request->user(), $request->all())
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function createFCMToken(Request $request)
    {
        return PlatformUserService::createFcmToken(Auth::user(), $request->all());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function deleteFCMToken(Request $request)
    {
        return PlatformUserService::deleteFcmToken(Auth::user(), $request->all());
    }

}