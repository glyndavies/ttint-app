<?php

namespace App\Http\Controllers\Api\Platform\V1;

use App\Http\Controllers\Controller;
use App\Facades\PlatformUserRepository;
use App\Facades\RebateRepository;
use App\Resources\RebateCollection;
use Illuminate\Http\Request;

class RebateController extends Controller
{
    /**
     * @param Illuminate\Http\Request $request
     * @return App\Resources\PlatformRebateCollection
     */
    public function get(Request $request)
    {
        $request->validate([
            'date_from' => ['required', 'date'],
            'date_to'   => ['required', 'date'],
        ]);

        $rebates = RebateRepository::find([
            'user_code'      => $request->user()->user_code,
            'sold_date_from' => $request->input('date_from'),
            'sold_date_to'   => $request->input('date_to'),
            'cancelled'      => false
        ]);

        return new RebateCollection($rebates);    
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function getByUserCode(Request $request)
    {
        if ( !count($request->except(['calculations'])) ) {
            abort(404);
        }

        $request->validate([
            'user_code'    => ['nullable'],
            'order_number' => ['nullable'],
            'deposit'      => ['nullable'],
            'date_from'    => ['nullable', 'date'],
            'date_to'      => ['nullable', 'date'],
        ]);

        $rebates = RebateRepository::find([
            'user_code'      => $request->input('user_code'),
            'order_number'   => $request->input('order_number'),
            'deposit'        => $request->input('deposit'),
            'sold_date_from' => $request->input('date_from'),
            'sold_date_to'   => $request->input('date_to'),
            'cancelled'      => false
        ]);

        return new RebateCollection($rebates);  
    }
}