<?php

namespace App\Http\Controllers\Api\Platform\V1;

use App\Http\Controllers\Controller;
use App\Models\PlatformUser;
use App\Facades\PlatformUserRepository;
use App\Resources\PlatformSummaryResource;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function get(Request $request)
    {
        return new PlatformSummaryResource(
            $request->user()
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function getAll(Request $request)
    {
        return PlatformUser::orderBy('user_code')->get()->mapWithKeys(function ($user, $index) {
            return [$user->user_code => new PlatformSummaryResource($user)];
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return App\Resources\PlatformSummaryResource
     */
    public function getByUserCode(Request $request)
    {
        if ( !($user = PlatformUserRepository::findByUserCode($request->input('user_code'))) ) {
            abort(404);
        }
        return new PlatformSummaryResource($user);
    }
}