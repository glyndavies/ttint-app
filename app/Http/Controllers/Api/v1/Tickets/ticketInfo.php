<?php

namespace App\Http\Controllers\Api\v1\Tickets;

use App\Http\Controllers\Controller;
use App\Facades\InventoryService;
use App\Models\ChasePic;
use App\Models\CompanyRefund;
use App\Models\Inventory;
use App\Models\InventoryQuantity;
use App\Models\SoldTicket;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ticketInfo extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function information($ticketID)
    {
        $data = Inventory::with(['purchase','inventoryQuantities'])->find($ticketID);
        if ($data) {
            return response($data,'200');
        } else {
            return response('','400');
        }

        return $data;
    }

    public function ReturnBatches($PN)
    {
        if($PN) {
            $data = InventoryQuantity::where('purchase_number',$PN)->get();
            if ($data) {
                return response($data,'200');
            } else {
                return response('','400');
            }
        }
    }

    public function IsPaid($ticketID, $status)
    {
        $data = SoldTicket::find($ticketID);

        $oaid = ($status == '1' ? 'N':'Y');
        if ($data) {
            $data->is_paid = $oaid;
            $data->paid_date = Carbon::now();
            $data->save();
            return response($data,'200');

        } else {
            return response('','400');
        }

        return $data;
    }

    public function SplitTickets($id)
    {
        $data =  InventoryQuantity::find($id);
        return response()->json($data,200);
    }


    public function IsSold($ticketID, $status)
    {
        $data = InventoryQuantity::find($ticketID);
        $sold =  ($status == '1' ? '1':'3');

        if ($sold == 1) {
            SoldTicket::where('inventory_quantity_id',$data->id)->where('cancelled','N')->update((['cancelled'=>'Y']));
        }

        if ($data) {
            $data->ticket_status_id = $sold;
            $data->save();
            return response($data,'200');

        } else {
            return response('','400');
        }

        return $data;
    }

    /*public function returnTicketToStock($id)
    {
        $ticket = InventoryQuantity::find($id);
        // change status to available
        if($ticket->soldTicket->cancelled == 'N') {
            //cancel the sales record
            SoldTicket::where('inventory_quantity_id',$id)->update(['cancelled' => 'Y']);
        }

        $ticket->ticket_status_id  = 1;
        $ticket->ticket_handling_id  = 1;
        $ticket->save();

        SoldTicket::where('inventory_quantity_id', $ticket->id)->delete();
    }*/

    public function companyRefund($id,Request $request)
    {
        $this->validate($request, [
           'refund'=>'required'
        ]);
        $data = Inventory::find($id)->update(['event_cancelled'=>'Y']);

        if($data) {
            $record = Inventory::find($id);
            CompanyRefund::insert([
                'inventory_id'=>$record->id,
                'refunded_amount'=> $request->refund,
                'created_at'=>Carbon::now(),
                'purchase_number'=>$record->purchase_number
            ]);
        }

        return response()->json($record ,'200');
    }

    public function chase($ticketID, $status)
    {
        $chaseFind = ChasePic::where('ticket_id', $ticketID)->first();
        if ($chaseFind) {
            $chaseFind->chase  = $status;
        } else {
            $chaseFind = new ChasePic();
            $chaseFind->ticket_id  = $ticketID;
            $chaseFind->chase  = $status;
        }
        $chaseFind->save();
        return response($chaseFind,'200');
 }

    public function pic($ticketID, $status)
    {
        $chaseFind = ChasePic::where('ticket_id', $ticketID)->first();
        if ($chaseFind) {
            $chaseFind->pic  = $status;
        } else {
            $chaseFind = new ChasePic();
            $chaseFind->ticket_id  = $ticketID;
            $chaseFind->pic  = $status;
        }
        $chaseFind->save();
        return response($chaseFind,'200');
    }

    public function inventoryNotesTicketUpdate($id, $notes)
    {
        $inventory = Inventory::find($id);
        if ($inventory) {
            $inventory->update(['notes' => $notes]);

            return response('update','200');
        } else {
            return response('Not found ','400');
        }

        return $inventory;
    }

    public function inventoryQuantityNotesTicketUpdate($id, $notes)
    {
        $inventoryQuantity = InventoryQuantity::find($id);
        if ($inventoryQuantity) {
            $inventoryQuantity->update(['notes' => $notes]);

            return response('update','200');
        } else {
            return response('Not found ','400');
        }

        return $inventoryQuantity;
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function cancelEvent($id)
    {
        InventoryService::cancel(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function restoreCancelledEvent($id)
    {
        InventoryService::restoreCancelled(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function pendingCancel($id)
    {
        InventoryService::pendingCancel(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function restorePendingCancelled($id)
    {
        InventoryService::restorePendingCancelled(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function listOnResale($id)
    {
        InventoryService::listOnResale(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function unlistOnResale($id)
    {
        InventoryService::unlistOnResale(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function expire($id)
    {
        InventoryService::expire(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }

    /**
     * @param integer $id
     * @return \Illumintate\Http\JSonResponse
     */
    public function restoreExpired($id)
    {
        InventoryService::restoreExpired(Inventory::findOrFail($id));
        return response()->json(true, '200');
    }
}
