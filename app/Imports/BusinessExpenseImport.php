<?php

namespace App\Imports;

use App\Models\BusinessExpense;
use App\Models\ExpenseCategory;
use App\Models\ExpenseSubCategory;
use App\Models\Card;

class BusinessExpenseImport extends DataImport
{
    /**
     *
     */
    protected function validationRules($rowData)
    {
        //$totalAmount = (floatval(preg_replace('/[^0-9-.]+/', '', $this->rowData['amount']) ?? 0) + floatval(preg_replace('/[^0-9-.]+/', '', $this->rowData['vat']) ?? 0));

        return [
            'id'           => ['nullable', 'integer'],
            'date'         => ['required', 'regex:/^\d{1,}$/'],
            'description'  => ['nullable'],
            'category'     => ['required', 'exists:expense_categories,name'],
            'subcategory'  => ['required', 'exists:expense_sub_categories,name'],
            'card'         => ['required', 'exists:cards,name'],
            'amount'       => ['required'], //'numeric'
            'vat'          => ['nullable'], //'numeric'
            //'total_amount' => ['required', 'numeric', 'in:'.$totalAmount],
        ];
    }

    /**
     *
     */
    protected function additionalFields()
    {

        return [
            'status' => $this->getUpdateCreateStatus(BusinessExpense::find($this->rowData['id'] ?? null)),
        ];
    }

    /**
     *
     */
    public static function process()
    {
        $data = self::getData();

        foreach ($data as $row) {
            $da = json_decode($row->data);

            BusinessExpense::updateOrCreate([
                'id' => $da->id ?? null,
            ],
            [
                'id' => $da->id ?? null,
                'date' => self::transformDate($da->date),
                'description' => $da->description,
                'category_id' => ExpenseCategory::getByName($da->category)->id,
                'sub_category_id' => !empty($da->subcategory)? ExpenseSubCategory::getByName($da->subcategory)->id: null,
                'card_id' => Card::getByName($da->card)->id,
                'amount' => preg_replace('/[^0-9-.]+/', '', $da->amount),
                'vat' => preg_replace('/[^0-9-.]+/', '', $da->vat),
            ]);
        }

        return $data->count();
    }
}
