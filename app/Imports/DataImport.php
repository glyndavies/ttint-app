<?php

namespace App\Imports;

use App\Models\TempDataTable;
use App\Models\TempTableLog;
use App\Models\TicketStatus;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Carbon\Carbon;
use ErrorException;

class DataImport implements ToModel, WithHeadingRow, WithCalculatedFormulas
{
    /**
     * 
     */
    protected $rowNum;

    /**
     * 
     */
    protected $issuesNum;

    /**
     * 
     */
    protected $rowData;

    /**
     * 
     */
    protected $importedData = [];

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $rowData)
    {
        $this->rowNum ++;
        $this->rowData = $rowData;

        $validator = Validator::make($rowData, $this->validationRules($rowData));

        if ($validator->fails()) {
            foreach($validator->errors()->all() as $error) {
                $this->issue($rowData, 'Row ' . $this->rowNum . ': ' . $error);
            }
        } else {
            $this->import();
        }

        $this->importedData[] = $rowData;

    }

    public function headingRow(): int
    {
        return 1;
    }

    /**
     *
     */
    public function import()
    {
        $this->rowData = array_merge($this->rowData, $this->additionalFields());

        TempDataTable::create([
            'User_id' => Auth::id(),
            'data' => json_encode($this->rowData)
        ]);
    }

    /**
     * @inheritDoc
     */
    public function issue($rowData, $reason)
    {
        $this->issuesNum ++;

        TempTableLog::create([
            'User_id' => Auth::id(),
            'data' => json_encode($rowData),
            'issue' => '1',
            'issue_reason' => $reason
        ]);
    }

    /**
     *
     */
    protected function validationRules($rowData)
    {
        return [];
    }

    /**
     *
     */
    protected function additionalFields()
    {
        return [];
    }

    /**
     *
     */
    protected static function transformDate($value, $format = 'd-m-Y')
    {
        if ( !$value ) {
            return null;
        }

        $dateTime = spreadsheetDate($value) ?? Carbon::createFromFormat($format, $value);
        return $dateTime->format('Y-m-d');
    }
    
    /**
     *
     */
    protected static function transformString($value)
    {
        if ( !trim($value) ) {
            return null;
        }

        return $value;
    }
    
    /**
     *
     */
    protected static function transformTime($value, $format = 'H:i')
    {
        if ( !$value ) {
            return null;
        }

        $dateTime = spreadsheetDate($value) ?? Carbon::createFromFormat($format, $value);
        return $dateTime->format('H:i');
    }

    /**
     *
     */
    protected static function transformYesNo($value)
    {
        if ($value) {
            if(mb_strtolower($value) == 'yes') {
                return 'Yes';
            } else {
                return 'No';
            }
        } else {
            return 'No';
        }
    }

    /**
     * @return string: allowed values separated by commas
     */
    protected static function booleanImportValues()
    {
        return "Yes,yes,No,no,Opt in,opt in,Opt out,opt out";
    }

    /**
     *
     */
    protected static function transformBoolean($value)
    {
        if ( !$value ) {
            return false;
        }

        if(mb_strtolower($value) == 'yes' || mb_strtolower($value) == 'opt in') {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    protected static function transformTicketStatusId($value)
    {
        if (mb_strtolower($value) == 'swap') {
            return TicketStatus::AVAILABLE_FOR_SWAP;
        //} else if (mb_strtolower($value) == 'sell') {
        //    return TicketStatus::SOLD;
        } else {
            return TicketStatus::AVAILABLE;
        }
    }

    /**
     *
     */
    protected static function transformLocation($value)
    {
        return explode(',', $value);
    }

    /**
     *
     */
    protected static function getData()
    {
        return TempDataTable::select('data')->get();
    }

    /**
     *
     */
    protected function getUpdateCreateStatus($model)
    {
        if ($model) {
            return 'update';
        } else {
            return 'create';
        }
    }
}
