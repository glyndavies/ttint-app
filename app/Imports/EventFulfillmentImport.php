<?php

namespace App\Imports;

use App\Facades\InventoryQuantityRepository;
use App\Facades\InventoryQuantityService;
use App\Imports\DataImport;
use App\Models\InventoryQuantity;
use App\Models\TicketHandlingStatus;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EventFulfillmentImport implements ToCollection, WithHeadingRow
{
    public $inventoryQuantityIds = [];

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $inventoryQuantity = InventoryQuantityRepository::findById((isset($row['id']) && $row['id']) ? $row['id'] : 0);

            $startValues = [$inventoryQuantity->comparable, $inventoryQuantity->fulfilled];

            if (!$inventoryQuantity) continue;

            if (isset($row['comparable'])) {
                $inventoryQuantity->update([
                    'comparable' => (strtolower($row['comparable']) == 'yes') ? 'Y' : 'N',
                ]);
            }

            if (isset($row['fulfilled']) && strtolower($row['fulfilled']) == 'yes') {
                $inventoryQuantity->update([
                    'fulfilled' => 'Y',
                    'comparable' => 'Y',
                ]);
            }

            if ($startValues != [$inventoryQuantity->comparable, $inventoryQuantity->fulfilled]) {
                $inventoryQuantity->update([
                    'in_progress_by_user_id' => null,
                ]);
            }

            if ($inventoryQuantity->isComparable() && $inventoryQuantity->isFulfilled()) {
                InventoryQuantityService::handling($inventoryQuantity, TicketHandlingStatus::find(3), null);
            }

            array_push($this->inventoryQuantityIds, $row['id']);
        }
    }

    public function headingRow(): int
    {
        return 1;
    }

}
