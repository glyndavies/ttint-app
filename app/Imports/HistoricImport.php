<?php

namespace App\Imports;

class HistoricImport extends DataImport
{
    /**
     *
     */
    protected function validationRules($rowData)
    {
        return [
            'transaction_number' => ['required', 'integer', 'unique:inventory,purchase_number'],
        ];
    }

    /**
     *
     */
    public static function process()
    {
        // processing as TicketImport
        return 0;
    }
}
