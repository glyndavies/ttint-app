<?php

namespace App\Imports;

use App\Models\PlatformUser;
use App\Models\Payment;
use App\Rules\SpreadsheetDateBeforeOrEqual;
use Carbon\Carbon;

class PaymentImport extends DataImport
{
    /**
     *
     */
    protected function validationRules($rowData)
    {
        return [
            'date' => [
                'required',
                'regex:/^\d{1,}$/',
                // date before or equal today
                new SpreadsheetDateBeforeOrEqual(Carbon::today()),
            ],
            'user_code' => [
                'required',
                'string',
                'exists:platform_users,user_code'
            ],
            'total_amount' => [
                'required', 
                'numeric'
            ],
        ];
    }

    /**
     * @return integer
     */
    public static function process()
    {
        $data = self::getData();

        foreach ($data as $row) {
            $da = json_decode($row->data);

            Payment::create([
                'user_id' => PlatformUser::getByUserCode($da->user_code)->id,
                'amount_incl_vat' => $da->total_amount,
                'date' => self::transformDate($da->date),
            ]);
        }

        return $data->count();
    }
}
