<?php

namespace App\Imports;

use App\Models\SoldTicket;
use App\Facades\InventoryQuantityRepository;
use App\Facades\SoldTicketRepository;
use App\Facades\SoldTicketService;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class ReconcileImport extends DataImport
{
    /**
     *
     */
    protected function validationRules($rowData)
    {
        $systemPrice = $this->getSystemPriceBySaleIdRef($this->rowData['sold_ref'] ?? null);

        return [
            'sold_ref'       => ['required', Rule::exists('sold_tickets', 'sale_id_ref')->where('cancelled','N')],
            'payment_amount' => ['required', 'in:' . $systemPrice],
        ];
    }

    /**
     *
     */
    protected function additionalFields()
    {
        return [
            'system_id_ref' => $this->getSystemSaleIdBySaleIdRef($this->rowData['sold_ref'] ?? null),
            'system_price' => $this->getSystemPriceBySaleIdRef($this->rowData['sold_ref'] ?? null),
            'system_site' => $this->getSystemSiteBySaleIdRef($this->rowData['sold_ref'] ?? null),
        ];
    }

    /**
     *
     */
    protected function getSystemPriceBySaleIdRef($saleIdRef)
    {
        if ($saleIdRef) {
            return SoldTicket::where('sale_id_ref', $saleIdRef)->where('cancelled','N')->sum('sell_price');
        } else {
            return null;
        }
    }

    /**
     *
     */
    protected function getSystemSaleIdBySaleIdRef($saleIdRef)
    {
        if ( !$saleIdRef ) {
            return null;
        }

        $soldTickets = SoldTicket::where('sale_id_ref', $saleIdRef)->where('cancelled','N')->get();
        return $soldTickets->count() ? $soldTickets->first()->sale_id_ref : null;
    }

    /**
     *
     */
    protected function getSystemSiteBySaleIdRef($saleIdRef)
    {
        if ( !$saleIdRef ) {
            return null;
        }

        $soldTickets = SoldTicket::where('sale_id_ref', $saleIdRef)->where('cancelled','N')->get();
        return $soldTickets->count() ? $soldTickets->first()->site_sold : null;
    }

    /**
     * @return integer
     */
    public static function process()
    {
        // sold sale_id_refs
        $sold_sale_id_refs = InventoryQuantityRepository::getSold()->map(function($iq) {
            return optional($iq->soldTicket)->sale_id_ref;
        });
        
        // delivered sale_id_refs
        $delivered_sale_id_refs = InventoryQuantityRepository::getDelivered()->map(function($iq) {
            return optional($iq->soldTicket)->sale_id_ref;
        });

        $data = self::getData();
        foreach ($data as $row) {
            $da = json_decode($row->data);

            $soldTickets = SoldTicketRepository::findBySaleIdRef($da->sold_ref);
            foreach($soldTickets as $soldTicket) {
                if ($sold_sale_id_refs->contains($da->sold_ref)) {
                    // sold -> delivered -> completed
                    SoldTicketService::paid($soldTicket);

                } elseif ($delivered_sale_id_refs->contains($da->sold_ref)) {
                    // delivered -> completed
                    SoldTicketService::paid($soldTicket);
                }
            }
        }

        return $data->count();
    }
}
