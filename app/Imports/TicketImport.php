<?php

namespace App\Imports;

use App\Models\Inventory;
use App\Models\InventoryQuantity;
use App\Models\Purchase;
use App\Models\TicketStatus;
use App\Rules\ValidateIf;
use App\Rules\SpreadsheetDateBeforeOrEqual;
use App\Rules\SpreadsheetDateAfterOrEqual;
use Illuminate\Validation\Rules\In;
use Carbon\Carbon;

class TicketImport extends DataImport
{
    /**
     *
     */
    protected function validationRules($rowData)
    {
        $prevRowData = $this->importedData[max(0, $this->rowNum - 2)] ?? [];

        return [
            'transaction_number' => [
                'required',
                'integer',
                'unique:inventory,purchase_number',
                // transaction_number == prev transaction_number + 1
                new ValidateIf('in:'.(($prevRowData['transaction_number'] ?? 0) + 1), function() {
                    return $this->rowNum > 1;
                }),
            ],
            'order_number' => [
                'required'
            ],
            'event_genre' => [
                'required',
                'exists:event_genres,genre'
            ],
            'event' => [
                'required'
            ],
            'arena' => [
                'required'
            ],
            'country' => [
                'required'
            ],
            'site_purchased' => [
                'required'
            ],
            'type_of_ticket' => [
                'required',
                'exists:ticket_types,tickettype'
            ],
            'purchase_date' => [
                'nullable',
                'regex:/^\d{1,}$/',
                // purchase date in range from today: [-30 days; +0 days]
                new SpreadsheetDateAfterOrEqual(Carbon::today()->addDays(-30)),
                new SpreadsheetDateBeforeOrEqual(Carbon::today()),
            ],
            'event_date' => [
                'required',
                'regex:/^\d{1,}$/',
                // event_date after or equal today
                new SpreadsheetDateAfterOrEqual(Carbon::today()),
            ],
            'number_of_tickets' => [
                'required',
                'integer'
            ],
            'user_code' => [
                'required',
                'exists:platform_users,user_code'
            ],
            // deposit null or 0.15
            'deposit' => [
                'nullable',
                new In([0.15]),
            ],
            'user_deposit' => [
                'required',
                // user_deposit == 0 if deposit is empty
                new ValidateIf('in:0', function() use ($rowData) {
                    return !($rowData['deposit'] ?? null);
                }),
                // user_deposit == 0 if ticket_status is SWAP
                new ValidateIf('in:0', function() use ($rowData) {
                    return self::transformTicketStatusId($rowData['ticket_status'] ?? null) == TicketStatus::AVAILABLE_FOR_SWAP;
                }),
                // user_deposit > 0 if ticket_status is SELL & deposit is not empty
                new ValidateIf('gt:0', function() use ($rowData) {
                    return self::transformTicketStatusId($rowData['ticket_status'] ?? null) == TicketStatus::AVAILABLE & !!($rowData['deposit'] ?? null);
                }),
            ]
        ];
    }

    /**
     *
     */
    public static function process()
    {
        $data = self::getData();

        foreach ($data as $row) {
            $da = json_decode($row->data);

            $inventory = Inventory::create([
                'purchase_number' => $da->transaction_number,
                'order_number'    => $da->order_number,
                'event_genre'     => $da->event_genre,
                'event'           => $da->event,
                'arena'           => $da->arena,
                'country'         => $da->country,
                'site_purchased'  => $da->site_purchased,
                //'purchase_date'   => self::transformDate($da->purchase_date),
                'type_of_ticket'  => $da->type_of_ticket,
                'event_date'      => self::transformDate($da->event_date),
                'time_of_event'   => self::transformTime($da->time_of_event),
                'restrictions'    => $da->restrictions,
                'listed'          => $da->listed,
                'referral_1'      => self::transformString($da->referral_1),
                'referral_2'      => self::transformString($da->referral_2),
                'buyer'           => $da->user_code,
                'selling_rate'    => $da->selling,
                'transfer_price'  => $da->transfer_price,
                'paid_amount'     => $da->paid_amount,
                'origin_status'   => $da->ticket_status,
                'ticket_status_id'=> self::transformTicketStatusId($da->ticket_status),
                'notes'           => $da->notes ?? null,
                'broken_limit'    => $da->broken_limit ? 'Y' : null,
            ]);

            InventoryQuantity::create([
                'inventory_id'      => $inventory->id,
                'purchase_number'   => $da->transaction_number,
                'number_of_tickets' => $da->number_of_tickets,
                'seats'             => $da->seats,
                'ticket_status_id'  => $inventory->ticket_status_id,
                'tickets_in_hand'   => self::transformYesNo($da->tickets_in_hand) == 'Yes' ? 2 : 1,
            ]);

            Purchase::create([
                'purchase_number'       => $da->transaction_number,
                'order_number'          => $da->order_number,
                'purchase_date'         => self::transformDate($da->purchase_date),
                'number_of_tickets'     => $da->number_of_tickets,
                'site_purchased'        => $da->site_purchased,
                'deposit_rate'          => $da->deposit,
                'buyer_investment'      => $da->user_deposit,
                'face_value_of_tickets' => $da->face_value_of_tickets,
                'buyers_fees'           => $da->buying_fees,
                'buyer'                 => $da->user_code,
                'card_used'             => $da->user_name,
                'VA'                    => $da->total_cost_per_ticket,
                'total_cost_per_ticket' => $da->total_cost_per_ticket,
                'total_cost'            => $da->face_value_of_tickets+$da->buying_fees,
                'imputer'               => $da->inputer,
                'wrong_filter'          => self::transformYesNo($da->tickets_in_hand),
                'broken_limit'          => self::transformYesNo($da->tickets_in_hand),
                'insurance'             => self::transformYesNo($da->tickets_in_hand),
                'status_id'             => 1,
            ]);
        }

        return $data->count();
    }
}
