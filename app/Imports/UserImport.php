<?php

namespace App\Imports;

use App\Models\PlatformUser;

class UserImport extends DataImport
{
	/**
	 *
	 */
    protected function validationRules($rowData)
    {
        return [
            'unique_id'       => ['nullable', 'integer'],
            'full_name_first' => ['required', 'string'],
            'full_name_last'  => ['required', 'string'],
            'email_address_1' => ['nullable', 'email'],
            'email_address_2' => ['nullable', 'email'],
            'date_of_birth'   => ['nullable', 'regex:/^\d{1,}$/'],
            '1_if_you_would_like_to_receive_presale_information_on_upcoming_events_opt_in_here'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '2_opt_in_here_if_you_would_like_to_use_a_dedicated_email_address_to_sign_up_to_the_events_alerts_service_ensuring_you_do_not_miss_out_on_your_events_of_choice'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '3_if_you_would_like_the_ability_to_to_pre_authorise_sale_and_swap_transfers_opt_in_to_the_express_transfer_service_here'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '4_if_you_would_like_to_share_your_account_data_and_receive_weekly_updates_on_the_highest_rebate_earners_on_the_platform_opt_in_here'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '5_opt_in_if_you_agree_to_receiving_weekly_portfolio_summary_of_your_account_status_and_performance'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '6_opt_in_if_you_agree_to_receive_broadcast_whatsapp_alerts_including_relevant_information_and_new_events'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '7_opt_in_if_you_agree_to_share_your_details_of_desired_events_with_resale_partners_to_increase_the_chance_of_getting_access_to_your_desired_event'
                => ['nullable', 'in:'.self::booleanImportValues()],
            '8_opt_in_if_you_agree_to_allowing_permission_to_contact_an_authorised_third_party_on_your_behalf_should_your_tickets_be_redirected_eg_when_using_a_virtualtemporary_address'
                => ['nullable', 'in:'.self::booleanImportValues()],
            'read_and_understand_the_terms_and_conditions_of_use_and_company_policies_as_outlined_in_your_welcome_email' => ['nullable', 'in:'.self::booleanImportValues()],
            'todays_date'     => ['nullable', 'regex:/^\d{1,}$/'],
            'ip_address'      => ['nullable', 'ip'],
            'location'        => ['nullable', 'regex:/^(-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)$/'],
        ];
    }

    /**
     *
     */
    protected static function getDesiredEvents($da)
    {
        $arr = [];
        for($i = 1; $i <= 5; $i++) {
            if ($da->{'desired_event_' . $i}) {
                $arr[] = $da->{'desired_event_' . $i};
            }
        }

        return $arr;
    }

    /**
     * @param array $data
     */
    public static function process()
    {
    	$data = self::getData();
    	foreach ($data as $row) {
    		$da = json_decode($row->data);
            
            $platformUser = PlatformUser::create([
                'unique_id' => $da->unique_id,
                'first_name' => $da->full_name_first,
                'last_name' => $da->full_name_last,
                'email_1' => $da->email_address_1,
                'email_2' => $da->email_address_2,
                'date_of_birth' => self::transformDate($da->date_of_birth),
                'address_line_1' => $da->full_address_address,
                'address_line_2' => $da->full_address_address2,
                'address_city' => $da->full_address_city,
                'address_state' => $da->full_address_state,
                'address_zip' => $da->full_address_zip,
                'country' => $da->country,
                'phone' => $da->phone,
                'referral_1' => $da->referral_1,
                'referral_2' => $da->referral_2,
                'desired_events' => self::getDesiredEvents($da),
                'presale_information' => self::transformBoolean($da
                    ->{'1_if_you_would_like_to_receive_presale_information_on_upcoming_events_opt_in_here'}),
                'events_alert_service' => self::transformBoolean($da
                    ->{'2_opt_in_here_if_you_would_like_to_use_a_dedicated_email_address_to_sign_up_to_the_events_alerts_service_ensuring_you_do_not_miss_out_on_your_events_of_choice'}),
                'express_transfer_service' => self::transformBoolean($da
                    ->{'3_if_you_would_like_the_ability_to_to_pre_authorise_sale_and_swap_transfers_opt_in_to_the_express_transfer_service_here'}),
                'weekly_updates' => self::transformBoolean($da
                    ->{'4_if_you_would_like_to_share_your_account_data_and_receive_weekly_updates_on_the_highest_rebate_earners_on_the_platform_opt_in_here'}),
                'weekly_portfolio' => self::transformBoolean($da
                    ->{'5_opt_in_if_you_agree_to_receiving_weekly_portfolio_summary_of_your_account_status_and_performance'}),
                'whatsapp_alerts' => self::transformBoolean($da
                    ->{'6_opt_in_if_you_agree_to_receive_broadcast_whatsapp_alerts_including_relevant_information_and_new_events'}),
                'share_desired_events' => self::transformBoolean($da
                    ->{'7_opt_in_if_you_agree_to_share_your_details_of_desired_events_with_resale_partners_to_increase_the_chance_of_getting_access_to_your_desired_event'}),
                'permission_to_contact' => self::transformBoolean($da
                    ->{'8_opt_in_if_you_agree_to_allowing_permission_to_contact_an_authorised_third_party_on_your_behalf_should_your_tickets_be_redirected_eg_when_using_a_virtualtemporary_address'}),
                'tcs_company_policies' => self::transformBoolean($da
                    ->{'read_and_understand_the_terms_and_conditions_of_use_and_company_policies_as_outlined_in_your_welcome_email'}),
                'filled_at' => self::transformDate($da->todays_date),            
                'browser' => $da->browser,
                'ip_address' => $da->ip_address,
                'location' => self::transformLocation($da->location),
                'user_code' => $da->user_code,
                'password' => PlatformUser::generatePassword(),
                
            ]);
        }

        return $data->count();
    }
}
