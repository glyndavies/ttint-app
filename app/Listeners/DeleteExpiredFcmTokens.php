<?php

namespace App\Listeners;

use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Support\Arr;

class DeleteExpiredFcmTokens
{
    public function handle(NotificationFailed $event): void
    {
        $report = Arr::get($event->data, 'report');

        $target = $report->target();

        \Illuminate\Support\Facades\Log::debug('FCM token has expired: ' . $target->value());
        \Illuminate\Support\Facades\Log::debug($report);
        /*$event->notifiable->notificationTokens()
            ->where('push_token', $target->value())
            ->delete();
        */
    }
}