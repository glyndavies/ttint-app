<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Card extends Model
{
    protected $table = 'cards';

    /**
     *
     */
    public static function getByName($name)
    {
    	return self::where('name', $name)->first();
    }
    
}
