<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyRefund extends Model
{
    protected $table = 'company_refund';

    /**
     * 
     */
    protected $fillable = [
        'refunded_amount',
        'inventory_id',
        'purchase_number',
    ];
}
