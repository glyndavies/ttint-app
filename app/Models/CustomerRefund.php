<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerRefund extends Model
{
	protected $table = 'customer_refunds';
	
    public function sales()
    {
        return $this->belongsTo(Sale::class,'id','sold_id');
    }
}
