<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryBatch extends Model
{
    protected $table = 'delivery_batches';

    /**
     * 
     */
    protected $fillable = [
        'ticket_id',
        'batch_id'
    ];
}

