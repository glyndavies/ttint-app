<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectorLoanEntry extends Model
{
    protected $table = 'director_loan_entries';

    protected $fillable = [
    	'amount_incl_vat',
        'notes',
    	'date',
    ];

    protected $dates = ['date'];

}
