<?php

namespace App\Models;

use App\Facades\InventoryRepository;
use Illuminate\Support\Collection;
use Jenssegers\Model\Model;

class EventFulfillment extends Model
{

    /**
     * @var string[]
     */
    protected $fillable = [
        'event',
        'event_date',
        'time_of_event',
        'arena',
        'inventories'
    ];

    /**
     * @return Collection|null
     */
    public function getInventoryQuantitiesAttribute()
    {
        return $this->inventories->flatMap(function ($inventory) {
            return $inventory->inventoryQuantities;
        });
    }

    /**
     * @return Collection<\App\Models\InventoryQuantity>
     */
    public function getSiteInventoryQuantitiesAttribute()
    {
        return $this->inventoryQuantities->filter(function ($invQ) {
            return !$invQ->isHandlingDelivered() && !$invQ->isFulfilled();
        });
    }

    /**
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    public function getSitePurchasesAttribute()
    {
        return self::getPurchasesBySitePurchased($this->siteInventoryQuantities);
    }

    /**
     * @return Collection<\App\Models\InventoryQuantity>
     */
    public function getComparableInventoryQuantitiesAttribute()
    {
        return $this->inventoryQuantities->filter(function ($invQ) {
            return ($invQ->ifStatusSold() || (!$invQ->ifStatusSold() && $invQ->inventory->type_of_ticket == 'Paper'))
                && !$invQ->isHandlingDelivered() && !$invQ->isComparable() && !$invQ->isFulfilled();
        });
    }

    /**
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    public function getComparablePurchasesAttribute()
    {
        return self::getPurchasesBySitePurchased($this->comparableInventoryQuantities)->filter(function ($eventFulfillmentPurchase) {
            return $eventFulfillmentPurchase->is_tickets_in_hand;
        });
    }

    /**
     * @return Collection<\App\Models\InventoryQuantity>
     */
    public function getToBeFulfilledInventoryQuantitiesAttribute()
    {
        return $this->inventoryQuantities->filter(function ($invQ) {
            return ($invQ->ifStatusSold() || (!$invQ->ifStatusSold() && $invQ->inventory->type_of_ticket == 'Paper'))
                && !$invQ->isHandlingDelivered() && $invQ->isComparable() && !$invQ->isFulfilled();
        });
    }

    /**
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    public function getToBeFulfilledPurchasesAttribute()
    {
        return self::getPurchasesBySitePurchased($this->toBeFulfilledInventoryQuantities)->filter(function ($eventFulfillmentPurchase) {
            return $eventFulfillmentPurchase->is_tickets_in_hand;
        });
    }

    /**
     * @return Collection<\App\Models\InventoryQuantity>
     */
    public function getFulfillmentSummaryInventoryQuantitiesAttribute()
    {
        return $this->inventoryQuantities->filter(function ($invQ) {
            return ($invQ->ifStatusSold() || (!$invQ->ifStatusSold() && $invQ->inventory->type_of_ticket == 'Paper'))
                && !$invQ->isHandlingDelivered() && !$invQ->isFulfilled();
        });
    }

    /**
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    public function getFulfillmentSummaryPurchasesAttribute()
    {
        return self::getPurchasesBySitePurchased($this->fulfillmentSummaryInventoryQuantities);
    }

    /**
     * @return Collection<\App\Models\InventoryQuantity>
     */
    public function getSiteSoldInventoryQuantitiesAttribute()
    {
        return $this->inventoryQuantities->filter(function ($invQ) {
            return ($invQ->ifStatusSold() || (!$invQ->ifStatusSold() && $invQ->inventory->type_of_ticket == 'Paper'))
                && !$invQ->isHandlingDelivered() && !$invQ->isFulfilled();
        });
    }

    /**
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    public function getSiteSoldPurchasesAttribute()
    {
        return $this->getPurchasesBySiteSold($this->siteSoldInventoryQuantities);
    }

    /**
     * @param Collection<\App\Models\InventoryQuantity>
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    static private function getPurchasesBySitePurchased(Collection $inventoryQuantities)
    {
        return $inventoryQuantities->groupBy(function($item) {
                $sitePurchased = trim($item->inventory->site_purchased);
                $typeOfTicket = $item->inventory->type_of_ticket == 'Paper' ? ' - Paper' : '';
                return $sitePurchased . $typeOfTicket;
            })
            ->map(function ($inventoryQuantities, $sitePurchased) {
                return new EventFulfillmentPurchase([
                    'site_purchased' => $sitePurchased,
                    'number_of_items' => $inventoryQuantities->count(),
                    'is_tickets_in_hand' => $inventoryQuantities->every(function ($inventoryQuantity) {
                        return $inventoryQuantity->isTicketsInHand();
                    }),
                    'in_progress_by_user' => optional($inventoryQuantities->first(function ($invQ) {
                        return !is_null($invQ->in_progress_by_user_id);
                    }))->inProgressByUser,
                    'inventory_quantities' => $inventoryQuantities
                ]);
            })->sortByDesc('number_of_items');
    }

    /**
     * @param Collection<\App\Models\InventoryQuantity>
     * @return Collection<\App\Models\EventFulfillmentPurchase>
     */
    static private function getPurchasesBySiteSold(Collection $inventoryQuantities)
    {
        $groupedInventoryQuantities = $inventoryQuantities->groupBy(function ($invQ) {
            $siteSold = trim(optional($invQ->soldTicket)->site_sold);
            return !in_array($siteSold, ['Viagogo', 'Stubhub', 'Gigsberg']) ? 'Other' : $siteSold;
        });

        $groupedInventoryQ = $groupedInventoryQuantities->pull('Other');
        if ($groupedInventoryQ) {
            $groupedInventoryQuantities->put('Other', $groupedInventoryQ);
        }

        return $groupedInventoryQuantities->map(function ($inventoryQuantities, $siteSold) {
                return new EventFulfillmentPurchase([
                    'site_sold' => $siteSold,
                    'number_of_items' => $inventoryQuantities->count(),
                    'is_tickets_in_hand' => $inventoryQuantities->every(function ($inventoryQuantity) {
                        return $inventoryQuantity->isTicketsInHand();
                    }),
                    'inventory_quantities' => $inventoryQuantities
                ]);
            });
    }

    /**
     * @param $exportType
     * @param $inventoryQuantity
     *
     * @return string
     */
    static public function getExportNameFromInvQuantId($exportType, $inventoryQuantityId)
    {
        $inventoryQuantity = InventoryQuantity::find($inventoryQuantityId);
        $inventory = $inventoryQuantity->inventory;
        return substr($exportType, 0, 3) .
            '-' . str_replace(' ', '-', $inventory->event) .
            '-' . str_replace([' ', ','], ['-', ''], $inventory->arena) .
            '-' . $inventory->event_date->format('jS-m') .
            '.xlsx';
    }
}
