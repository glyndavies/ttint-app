<?php

namespace App\Models;

use Jenssegers\Model\Model;

class EventFulfillmentPurchase extends Model
{
    protected $fillable = [
        'site_purchased',
        'site_sold',
        'number_of_items',
        'is_tickets_in_hand',
        'inventory_quantities',
        'in_progress_by_user'
    ];
}
