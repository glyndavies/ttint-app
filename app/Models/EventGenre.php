<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class EventGenre extends Model implements  Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'event_genres';

    /**
     *
     */
    public function scopeActive($query)
    {
    	return $query->where('active', 1);
    }
}
