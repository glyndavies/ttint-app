<?php

namespace App\Models;

use App\Models\ExpenseSubCategory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table = 'expenses';

    protected $fillable = [
        'id',
    	'category_id',
        'sub_category_id',
    	'card_id',
    	'description',
    	'amount',
    	'vat',
    	'date',
    ];

    protected $dates = ['date'];

    /**
     *
     */
    public function category()
    {
    	return $this->belongsTo(ExpenseCategory::class, 'category_id');
    }

    /**
     *
     */
    public function subcategory()
    {
        return $this->belongsTo(ExpenseSubCategory::class, 'sub_category_id');
    }

    /**
     *
     */
    public function card()
    {
    	return $this->belongsTo(Card::class, 'card_id');
    }

    /**
     *
     */
    public function getAmountInclVatAttribute()
    {
    	return $this->amount + $this->vat;
    }

    /**
     * @param integer $id
     */
    public static function getByCategoryId($id)
    {
        return self::where('category_id', $id)->get();
    }
}
