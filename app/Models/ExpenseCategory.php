<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ExpenseCategory extends Model
{
    protected $table = 'expense_categories';

    /**
     *
     */
    public static function getByName($name)
    {
    	return self::where('name', $name)->first();
    }
}
