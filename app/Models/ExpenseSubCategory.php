<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ExpenseSubCategory extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    /**
     * 
     */
    protected $table = 'expense_sub_categories';

    /**
     *
     */
    public static function getByName($name)
    {
        return self::where('name', $name)->first();
    }
}
