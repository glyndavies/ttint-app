<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FcmClientToken extends Model
{
    protected $fillable = [
    	'platform_user_id',
    	'token',
    	'device_name'
    ];
}
