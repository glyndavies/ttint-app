<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ImportIssue extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table = 'import_issues';
}