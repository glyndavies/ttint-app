<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cmgmyr\Messenger\Models\Thread;
use OwenIt\Auditing\Contracts\Auditable;
use DB;

use function foo\func;

class Inventory extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'inventory';

    protected $with = [
        //'inventoryQuantities'
    ];

    public $dates = [
        'event_date',
        'ticketStatus',
        //'purchase_date'
    ];

    public $fillable = [
        'purchase_number',
        'order_number',
        'event_genre',
        'event',
        'arena',
        'country',
        'site_purchased',
        'type_of_ticket',
        //'purchase_date',
        'event_date',
        'time_of_event',
        'day_of_event',
        'restrictions',
        'buyer_group',
        'buyer',
        'notes',
        'broken_limit',
        'origin_status',
        'ticket_status_id',
        'event_cancelled',
        'pending_cancelled',
        'listed_on_resale',
        'expired',
        'listed',
        'referral_1',
        'referral_2',
        'selling_rate',
        'transfer_price',
        'paid_amount',
        'buyer',
        'new_buyer',
        'token',
    ];

    public const AVAILABLE          = 1;
    public const SPLIT              = 2;
    public const AVAILABLE_FOR_SWAP = 11;
    public const SWAPPED            = 12;

    public function inventoryQuantities()
    {
      return $this->hasMany(InventoryQuantity::class, 'inventory_id', 'id');
    }

    public function purchase()
    {
        return $this->hasOne(Purchase::class, 'purchase_number', 'purchase_number');
    }

    public function ticketStatus()
    {
        return $this->hasOne(TicketStatus::class, 'id', 'ticket_status_id');
    }

    /*public function soldTickets()
    {
        return $this->hasOne(SoldTicket::class, 'inventory_quantity_id', 'id');
    }*/

    public function chase()
    {
        return $this->hasOne(ChasePic::class, 'ticket_id', 'id');
    }

    public function pic()
    {
        return $this->hasOne(ChasePic::class, 'ticket_id', 'id');
    }

    public function swaps0()
    {
        return $this->hasMany(Swap::class, 'inventory0_id', 'id');
    }

    public function swaps1()
    {
        return $this->hasMany(Swap::class, 'inventory1_id', 'id');
    }

    public function ifStatusAvailable()
    {
        return $this->ticket_status_id == 1;
    }

    public function ifStatusSplit()
    {
        return $this->ticket_status_id == 2;
    }

    public function ifStatusForSale()
    {
        return in_array($this->ticket_status_id, [1, 2, 12]);
    }

    public function ifStatusSold()
    {
        return $this->ticket_status_id == 3;
    }

    public function ifStatusAvailableForSwap()
    {
        return $this->ticket_status_id == 11;
    }

    public function ifStatusSwapped()
    {
        return $this->ticket_status_id == 12;
    }

    public function ifCancelled()
    {
        return $this->event_cancelled == 'Y';
    }

    public function ifPendingCancelled()
    {
        return $this->pending_cancelled == 'Y';
    }

    public function ifListedOnResale()
    {
        return $this->listed_on_resale == 'Y';
    }

    public function getRealDepositRateAttribute()
    {
        if (strtolower($this->origin_status) != 'swap' && $this->purchase->total_cost) {
            // return $this->purchase->buyer_investment / $this->purchase->total_cost;
            return optional($this->purchase)->deposit_rate;
        } else {
            return 0;
        }
    }

    // scopes
    public function scopeHasQuantityForSale($query)
    {
        return $query->whereHas('inventoryQuantities', function($q) {
            $q->statusForSale();
        });
    }

    public function scopeHasQuantityAvailableForSwap($query)
    {
        return $query->whereHas('inventoryQuantities', function($q) {
            $q->statusAvailableForSwap();
        });
    }

    public function scopeHasQuantitySwapped($query)
    {
        return $query->whereHas('inventoryQuantities', function($q) {
            $q->statusSwapped();
        });
    }

    public function scopeHasQuantityForTransfer($query)
    {
        return $query->whereHas('inventoryQuantities', function($q) {
            $q->statusForTransfer();
        });
    }

    public function scopeCancelled($query)
    {
        return $query->where('event_cancelled', 'Y');
    }

    public function scopeNotCancelled($query)
    {
        return $query->where('event_cancelled', '!=', 'Y');
    }

    public function scopePendingCancelled($query)
    {
        return $query->where('pending_cancelled', 'Y');
    }

    public function scopeNotPendingCancelled($query)
    {
        return $query->where('pending_cancelled', '!=', 'Y');
    }

    public function scopeListedOnResale($query)
    {
        return $query->where('listed_on_resale', 'Y');
    }

    public function scopeNotListedOnResale($query)
    {
        return $query->where('listed_on_resale', '!=', 'Y');
    }

    public function scopeExpired($query)
    {
        return $query->where('expired', 1);
    }

    public function scopeNotExpired($query)
    {
        return $query->where('expired', '!=', 1);
    }

    public function scopeWithinPeriod($query, $date1, $date2)
    {
        return $query->whereBetween('event_date', [$date1, $date2]);
    }

    public function scopeWithinSessionPeriod($query)
    {
        $period = getSessionPeriod();
        return $this->scopeWithinPeriod($query, $period['date1'], $period['date2']);
    }

    public function scopeReferral1($query, $referral)
    {
        return $query->whereRaw('lower(referral_1) like "%'.strtolower($referral).'%"');
    }

    public function scopeUnsoldOrUndeliveredQuantities($query)
    {
        return $query->whereHas('inventoryQuantities', function ($q) {
            $q->statusNotSold()->orWhere(function ($qu) {
                $qu->handlingNotDelivered();
            });
        });
    }

    /**
     * @param array $filters
     *   partly => [
     *     transaction_number
     *     order_number
     *     event
     *     arena
     *     site_purchased
     *     type_of_ticket
     *     restrictions
     *     seats
     *     user_code
     *   ]
     *   start
     *   length
     */
    public function scopeFilters($query, $filters)
    {
        $partly = $filters['partly'] ?? [];

        return $query
            // search
            ->when(isset($filters['search']), function ($q) use ($filters) {
                return $q->whereHas('inventoryQuantities', function($q) use ($filters) {
                    return $q->whereRaw('lower(inventory_quantity.seats) like "%'.strtolower($filters['search']).'%"');
                })
                ->orWhereRaw('lower(order_number) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(purchase_number) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(event) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(arena) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(site_purchased) like "%'.strtolower($filters['search']).'%"');
            })
            // partly: event_details
            ->when(isset($partly['event_details']), function ($q) use ($partly) {
                return $q->where(function ($query) use ($partly) {
                    $eventDetails = strtolower($partly['event_details']);

                    $query->whereRaw('lower(event) like ?', ['%' . $eventDetails . '%'])
                        ->orWhereRaw('DATE_FORMAT(event_date, "%a %d %b %Y") like ?', ['%' . $eventDetails . '%'])
                        ->orWhereRaw('DATE_FORMAT(CONCAT(event_date, " ", time_of_event), "%a %d %b %Y %H:%i") like ?', ['%' . $eventDetails . '%'])
                        ->orWhereRaw('lower(arena) like ?', ['%' . $eventDetails . '%']);
                });
            })
            // partly: to_be_fulfilled - checkbox
            ->when(($partly['to_be_fulfilled'] ?? 'unchecked') != 'unchecked', function ($q) {
                return $q->whereHas('inventoryQuantities', function ($query) {
                    $query->where(function($subQuery) {
                            $subQuery->statusSold()
                                ->orWhere(function ($innerQuery) {
                                    $innerQuery->statusNotSold()
                                        ->paperTicket();
                                });
                        })
                        ->handlingNotDelivered()
                        ->ticketsInHand()
                        ->comparable()
                        ->notFulfilled();
                });
            })

            // partly: to_be_fulfilled - search
            ->when(!in_array($partly['to_be_fulfilled'] ?? 'unchecked', ['checked', 'unchecked']), function ($q) use ($partly) {
                return $q->whereRaw('lower(site_purchased) like ?', ['%' . strtolower($partly['to_be_fulfilled']) . '%']);
            })

            // partly: paper
            ->when(isset($partly['paper']) && $partly['paper'] == 'checked', function ($q) use ($partly) {
                return $q->whereRaw('lower(type_of_ticket) like "%paper%"')
                    ->whereHas('inventoryQuantities', function($query) {
                        $query->where('ticket_status_id', '=', 1)
                            ->orWhere(function($query) {
                                $query->statusSold()
                                    ->handlingNotDelivered();
                            });
                    });
            })
            // partly: unsold
            ->when(isset($partly['unsold']) && $partly['unsold'] == 'checked', function ($q) use ($partly) {
                return $q->whereHas('inventoryQuantities', function($query) {
                        $query->where('ticket_status_id', '=', 1);
                    });
            })
            // partly: comparable
            ->when(isset($partly['comparable']) && $partly['comparable'] == 'checked', function ($q) use ($partly) {
                return $q->whereHas('inventoryQuantities', function($query) {
                        $query->where(function($query) {
                                $query->where(function($subQuery) {
                                        $subQuery->statusSold()
                                            ->orWhere(function ($innerQuery) {
                                                $innerQuery->statusNotSold()
                                                    ->paperTicket();
                                            });
                                    })
                                    ->handlingNotDelivered()
                                    ->ticketsInHand()
                                    ->notComparable()
                                    ->notFulfilled();
                            });
                    });
            })
            // partly: fulfillment_summary
            ->when(isset($partly['fulfillment_summary']), function ($q) use ($partly) {
                return $q->whereRaw('lower(site_purchased) like ?', ['%' . strtolower($partly['fulfillment_summary']) . '%'])
                    ->whereHas('inventoryQuantities', function ($query) {
                        $query->handlingNotDelivered();
                    });
            })
            // partly: transaction_number
            ->when(isset($partly['transaction_number']), function ($q) use ($partly) {
                return $q->whereRaw('lower(purchase_number) like "%'.strtolower($partly['transaction_number']).'%"');
            })
            // partly: order_number
            ->when(isset($partly['order_number']), function ($q) use ($partly) {
                return $q->whereRaw('lower(order_number) like "%'.strtolower($partly['order_number']).'%"');
            })
            // partly: event
            ->when(isset($partly['event']), function ($q) use ($partly) {
                return $q->whereRaw('lower(event) like "%'.strtolower($partly['event']).'%"');
            })
            // partly: arena
            ->when(isset($partly['arena']), function ($q) use ($partly) {
                return $q->whereRaw('lower(arena) like "%'.strtolower($partly['arena']).'%"');
            })
            // partly: site_purchased
            ->when(isset($partly['site_purchased']), function ($q) use ($partly) {
                return $q->whereRaw('lower(site_purchased) like "%'.strtolower($partly['site_purchased']).'%"');
            })
            // partly: type_of_ticket
            ->when(isset($partly['type_of_ticket']), function ($q) use ($partly) {
                return $q->whereRaw('lower(type_of_ticket) like "%'.strtolower($partly['type_of_ticket']).'%"');
            })
            // partly: event_date
            ->when(isset($partly['event_date']), function ($q) use ($partly) {
                return $q->whereDate('event_date', '=', $partly['event_date']);
            })
            // partly: event_date_from
            ->when(isset($partly['event_date_from']), function ($q) use ($partly) {
                return $q->whereDate('event_date', '>=', $partly['event_date_from']);
            })
            // partly: restrictions
            ->when(isset($partly['restrictions']), function ($q) use ($partly) {
                return $q->whereRaw('lower(restrictions) like "%'.strtolower($partly['restrictions']).'%"');
            })
            // partly: seats
            ->when(isset($partly['seats']), function ($q) use ($partly) {
                return $q->whereHas('inventoryQuantities', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory_quantity.seats) like "%'.strtolower($partly['seats']).'%"');
                });
            })
            // partly: delivered
            ->when(isset($partly['delivered']), function ($q) use ($partly) {
                return $q->whereHas('inventoryQuantities', function($q) use ($partly) {
                    return $partly['delivered'] == 'Y' ? $q->handlingDelivered() : $q->handlingNotDelivered();
                });
            })
            // partly: user_code
            ->when(isset($partly['user_code']), function ($q) use ($partly) {
                return $q->whereRaw('lower(buyer) like "%'.strtolower($partly['user_code']).'%"');
            })
            // partly: referral_1
            ->when(isset($partly['referral_1']), function ($q) use ($partly) {
                return $q->referral1($partly['referral_1']);
            })
            // partly: pending_cancelled
            ->when(isset($partly['pending_cancelled']) && $partly['pending_cancelled'], function ($q) use ($partly) {
                return $partly['pending_cancelled'] == 'Y' ? $q->pendingCancelled() : $q->notPendingCancelled();
            })
            // partly: listed_on_resale
            ->when(isset($partly['listed_on_resale']) && $partly['listed_on_resale'], function ($q) use ($partly) {
                return $partly['listed_on_resale'] == 'Y' ? $q->listedOnResale() : $q->NotListedOnResale();
            })
            // start -> offset SQL
            ->when(isset($filters['start']), function ($q) use ($filters) {
                return $q->offset($filters['start']);
            })
            // length -> limit SQL
            ->when(isset($filters['length']), function ($q) use ($filters) {
                return $q->take($filters['length']);
            });
    }

    /**
     * @return array
     */
    public function getSwapHistoryAttribute()
    {

        $swaps0 = $this->swaps0->map(function($swap) {
            return [
                'created_at' => $swap->created_at,
                'old_buyer'      => $swap->buyer0,
                'new_buyer'      => $swap->buyer1,
            ];
        });

        $swaps1 = $this->swaps1->map(function($swap) {
            return [
                'created_at' => $swap->created_at,
                'old_buyer'      => $swap->buyer1,
                'new_buyer'      => $swap->buyer0,
            ];
        });

        return collect()
            ->merge($swaps0)
            ->merge($swaps1)
            ->sortBy('created_at');
    }

    /**
     * @return Thread
     */
    public function getThreadAttribute()
    {
        return Thread::find($this->purchase_number);
    }
}
