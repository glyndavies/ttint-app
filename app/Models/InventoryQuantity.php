<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class InventoryQuantity extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'inventory_quantity';
    //protected $with = ['ticketStatus'];

    public $fillable = [
        'inventory_id',
        'purchase_number',
        'number_of_tickets',
        'seats',
        'sell_price',
        'sold_date',
        'ticket_status_id',
        'tickets_in_hand',
        'ticket_handling_id',
        'comparable',
        'fulfilled',
        'notes',
        'in_progress_by_user_id'
    ];

    public $dates = [
        'sold_date',
        'delivered_date',
    ];

    public function ticketStatus()
    {
        return $this->hasOne(TicketStatus::class,'id','ticket_status_id');
    }

    public function ticketHandlingStatus()
    {
        return $this->hasOne(TicketHandlingStatus::class,'id','ticket_handling_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class,'inventory_id','id')->orderBy('event_date');
    }

    public function soldTicket()
    {
        return $this->hasOne(SoldTicket::class,'inventory_quantity_id','id');
    }

    public function customerRefund()
    {
        return $this->hasOne(CustomerRefund::class,'inventory_id','id');
    }

    public function rebates()
    {
        return $this->hasMany(Rebate::class);
    }

    // used for calculation of gross profit on split tickets
    /*public function TotalTickSum($pn)
    {
        return  InventoryQuantity::where('purchase_number',$pn)->sum('number_of_tickets');
    }*/

    public function deliveryBatch()
    {
        return $this->hasOne(DeliveryBatch::class,'ticket_id','id');
    }

    public function chasePic()
    {
        return $this->hasOne(ChasePic::class,'ticket_id','id');
    }

    public function inProgressByUser()
    {
        return $this->belongsTo(User::class, 'in_progress_by_user_id');
    }

    public function ifStatusAvailable()
    {
        return $this->ticket_status_id == 1;
    }

    public function ifStatusSplit()
    {
        return $this->ticket_status_id == 2;
    }

    public function ifStatusForSale()
    {
        return in_array($this->ticket_status_id, [1, 2, 12]);
    }

    public function ifStatusForTransfer()
    {
        return in_array($this->ticket_status_id, [11, 12]);
    }

    public function ifStatusSold()
    {
        return $this->ticket_status_id == 3;
    }

    public function ifStatusAvailableForSwap()
    {
        return $this->ticket_status_id == 11;
    }

    public function ifStatusSwapped()
    {
        return $this->ticket_status_id == 12;
    }

    public function ifHandlingStarted()
    {
        return $this->ticket_handling_id > 1;
    }

    public function isHandlingDelivered()
    {
        return in_array($this->ticket_handling_id, [2, 3, 5]);
    }

    public function isTicketsInHand()
    {
        return $this->tickets_in_hand == 2;
    }

    public function isComparable()
    {
        return $this->comparable == 'Y';
    }

    public function isFulfilled()
    {
        return $this->fulfilled == 'Y';
    }

    public function getExpiredAttribute()
    {
        return $this->inventory->expired && !optional($this->soldTicket)->sold_date;
    }

    public function getCostAttribute()
    {
        return round($this->cost_per_ticket * $this->number_of_tickets, 2);
    }

    public function getCostPerTicketAttribute()
    {
        $purchase = $this->inventory->purchase;
        return round(optional($purchase)->number_of_tickets ? ($purchase->face_value_of_tickets + $purchase->buyers_fees) / $purchase->number_of_tickets : 0, 2);
    }

    public function getContributionAttribute()
    {

        return round($this->cost * $this->inventory->realDepositRate, 2);
    }

    public function getRepaidAttribute()
    {
        return round($this->contribution ? $this->cost - $this->contribution : 0, 2);
    }

    public function getProfitAttribute()
    {
        if(optional($this->inventory->purchase)->companyRefund) {
            return round($this->inventory->purchase->companyRefund->refunded_amount - $this->cost, 2);
        } else {
            return round($this->soldTicket ? $this->soldTicket->sell_price - $this->cost : 0, 2);
        }
    }

    public function getRawTradingProfitAttribute()
    {
        return round($this->profit < 0 ? $this->profit : $this->profit * 0.75, 2);
    }

    public function getTrustRebateAttribute()
    {
        if ($this->inventory->event_cancelled == 'Y' || $this->expired) {
            return 0;
        }

        return round($this->rawTradingProfit * (optional($this->inventory->purchase)->deposit_rate ? 0.25 : 0.1), 2);
    }

    public function getReferralRebateAttribute()
    {
        if ($this->inventory->event_cancelled == 'Y' || $this->expired) {
            return 0;
        }

        if ($this->inventory->referral_1) {
            return round($this->rawTradingProfit * 0.15, 2);
        } else {
            return 0;
        }

    }

    public function getTotalRebateAttribute()
    {
        return $this->trustRebate + $this->referralRebate;
    }

    /**
     * get InventoryQuantity table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return (new self())->getTable();
    }

    // scopes
    public function scopeOrderByEventDate($query)
    {
        return $query->selectRaw('inventory_quantity.*, inventory.event_date as event_date_order')
            ->join('inventory' , 'inventory.id', '=', 'inventory_quantity.inventory_id')
            ->orderBy('event_date_order');
    }

    public function scopeWithStateTicketAll($query)
    {
        $table_name_as = 'inv0';
        $purchase = new Purchase;

        return $query->select($table_name_as . '.*')
        ->from($this->getTableName() . ' as ' . $table_name_as)
        ->selectRaw($purchase->withStateTicketInventory($table_name_as))
        ->selectRaw($purchase->withStateTicketSold($table_name_as))
        ->selectRaw($purchase->withStateTicketDelivered($table_name_as))
        ->selectRaw($purchase->withStateTicketCompleted($table_name_as));
    }

    public function scopePaperTicket($query)
    {
        return $query->whereHas('inventory', function($q) {
            return $q->where('type_of_ticket', 'Paper');
        });
    }

    public function scopeStatusInSells($query)
    {
        return $query->whereIn('ticket_status_id', [1, 2, 3]);
    }

    public function scopeStatusForSale($query)
    {
        return $query->whereIn('ticket_status_id', [1, 2, 12]);
    }

    public function scopeStatusSold($query)
    {
        return $query->where('inventory_quantity.ticket_status_id', '=', 3);
    }

    public function scopeStatusNotSold($query)
    {
        return $query->where('inventory_quantity.ticket_status_id', '!=', 3);
    }

    public function scopeStatusAvailableForSwap($query)
    {
        return $query->where('ticket_status_id', '=', 11);
    }

    public function scopeStatusSwapped($query)
    {
        return $query->where('ticket_status_id', '=', 12);
    }

    public function scopeStatusForTransfer($query)
    {
        return $query->whereIn('ticket_status_id', [11, 12]);
    }

    public function scopeHandlingDelivered($query)
    {
        return $query->whereIn('ticket_handling_id', [2, 3, 5]);
    }

    public function scopeHandlingNotDelivered($query)
    {
        return $query->whereNotIn('inventory_quantity.ticket_handling_id', [2, 3, 5]);
    }

    public function scopeWithinPeriod($query, $date1, $date2)
    {
        return $query->whereHas('inventory', function($q) use ($date1, $date2) {
            return $q->whereBetween('event_date', [$date1, $date2]);
        });
    }

    public function scopeWithinSessionPeriod($query)
    {
        $period = getSessionPeriod();
        return $this->scopeWithinPeriod($query, $period['date1'], $period['date2']);
    }

    public function scopeUserCode($query, $user_code)
    {
        return $query->whereHas('inventory', function($q) use ($user_code) {
            return $q->where('buyer', $user_code);
        });
    }

    public function scopeReferral($query, $referral)
    {
        return $query->whereHas('inventory', function($q) use ($referral) {
            return $q->referral1($referral);
        });
    }

    public function scopeHasSaleIdRef($query)
    {
        return $query->whereHas('soldTicket', function($q) {
            $q->whereNotNull('sale_id_ref');
        });
    }

    public function scopeDepositRate($query, $deposit_rate)
    {
        return $query->whereHas('inventory', function($q) use ($deposit_rate) {
            return $q->whereHas('purchase', function($q2) use ($deposit_rate) {
                $q2->where('deposit_rate', $deposit_rate);
            });
        });
    }

    public function scopeTicketsInHand($query)
    {
        return $query->where('tickets_in_hand', 2);
    }

    public function scopeComparable($query)
    {
        return $query->where('comparable', 'Y');
    }

    public function scopeNotComparable($query)
    {
        return $query->where('comparable', 'N');
    }

    public function scopeFulfilled($query)
    {
        return $query->where('fulfilled', 'Y');
    }

    public function scopeNotFulfilled($query)
    {
        return $query->where('fulfilled', 'N');
    }

    /**
     * @param array $filters
     *   user_code
     *   referral
     *   sold_ticket
     *   sale_id_ref
     *   deposit_rate
     *   sold_date_from
     *   sold_date_to
     *   search
     *   partly => [
     *     transaction_number
     *     order_number
     *     event
     *     arena
     *     site_purchased
     *     type_of_ticket
     *     restrictions
     *     seats
     *     user_code
     *     site_sold
     *     sale_id_ref
     *   ]
     *   start
     *   length
     */
    public function scopeFilters($query, $filters)
    {
        $partly = $filters['partly'] ?? [];

        return $query
            // user_code
            ->when(isset($filters['user_code']), function ($q) use ($filters) {
                return $q->userCode($filters['user_code']);
            })
            // referral
            ->when(isset($filters['referral']), function ($q) use ($filters) {
                return $q->referral($filters['referral']);
            })
            // sold_ticket
            ->when(isset($filters['sold_ticket']) && $filters['sold_ticket'], function ($q) {
                return $q->has('soldTicket');
            })
            // sale_id_ref
            ->when(isset($filters['sale_id_ref']) && $filters['sale_id_ref'], function ($q) {
                return $q->hasSaleIdRef();
            })
            // deposit_rate
            ->when(isset($filters['deposit_rate']), function ($q) use ($filters) {
                return $q->depositRate($filters['deposit_rate']);
            })
            // sold_date_from
            ->when(isset($filters['sold_date_from']), function ($q) use ($filters) {
                return $q->whereHas('soldTicket', function($q2) use ($filters) {
                    return $q2->whereDate('sold_date', '>=', $filters['sold_date_from']);
                });
            })
            // sold_date_to
            ->when(isset($filters['sold_date_to']), function ($q) use ($filters) {
                return $q->whereHas('soldTicket', function($q2) use ($filters) {
                    return $q2->whereDate('sold_date', '<=', $filters['sold_date_to']);
                });
            })
            // search
            ->when(isset($filters['search']), function ($q) use ($filters) {
                return $q->whereHas('inventory', function($q) use ($filters) {
                    return $q
                        ->whereRaw('lower(inventory.order_number) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.purchase_number) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.event) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.arena) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.site_purchased) like "%'.strtolower($filters['search']).'%"');
                })->orWhereHas('soldTicket', function($q) use ($filters) {
                    return $q
                        ->whereRaw('lower(sold_tickets.site_sold) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(sold_tickets.sale_id_ref) like "%'.strtolower($filters['search']).'%"');
                })->orWhereRaw('lower(seats) like "%'.strtolower($filters['search']).'%"');
            })
            // partly: transaction_number
            ->when(isset($partly['transaction_number']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.purchase_number) like "%'.strtolower($partly['transaction_number']).'%"');
                });
            })
            // partly: order_number
            ->when(isset($partly['order_number']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.order_number) like "%'.strtolower($partly['order_number']).'%"');
                });
            })
            // partly: event
            ->when(isset($partly['event']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.event) like "%'.strtolower($partly['event']).'%"');
                });
            })
            // partly: arena
            ->when(isset($partly['arena']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.arena) like "%'.strtolower($partly['arena']).'%"');
                });
            })
            // partly: site_purchased
            ->when(isset($partly['site_purchased']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.site_purchased) like "%'.strtolower($partly['site_purchased']).'%"');
                });
            })
            // partly: type_of_ticket
            ->when(isset($partly['type_of_ticket']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.type_of_ticket) like "%'.strtolower($partly['type_of_ticket']).'%"');
                });
            })
            // partly: event_date
            ->when(isset($partly['event_date']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereDate('event_date', '=', $partly['event_date']);
                });
            })
            // partly: number_of_tickets
            ->when(isset($partly['number_of_tickets_d']), function ($q) use ($partly) {
                return $q->where('number_of_tickets', $partly['number_of_tickets_d']);
            })
            // partly: restrictions
            ->when(isset($partly['restrictions']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.restrictions) like "%'.strtolower($partly['restrictions']).'%"');
                });
            })
            // partly: seats
            ->when(isset($partly['seats']), function ($q) use ($partly) {
                return $q->whereRaw('lower(seats) like "%'.strtolower($partly['seats']).'%"');
            })
            // delivered
            ->when(isset($partly['delivered']) && $partly['delivered'], function ($q) use ($partly) {
                return $partly['delivered'] == 'Y' ? $q->handlingDelivered() : $q->handlingNotDelivered();
            })
            // partly: user_code
            ->when(isset($partly['user_code']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.buyer) like "%'.strtolower($partly['user_code']).'%"');
                });
            })
            // partly: site_sold
            ->when(isset($partly['site_sold']), function ($q) use ($partly) {
                return $q->whereHas('soldTicket', function($q) use ($partly) {
                    return $q->whereRaw('lower(sold_tickets.site_sold) like "%'.strtolower($partly['site_sold']).'%"');
                });
            })
            // partly: sale_id_ref
            ->when(isset($partly['sale_id_ref']), function ($q) use ($partly) {
                return $q->whereHas('soldTicket', function($q) use ($partly) {
                    return $q->whereRaw('lower(sold_tickets.sale_id_ref) like "%'.strtolower($partly['sale_id_ref']).'%"');
                });
            })
            // partly: referral_1
            ->when(isset($partly['referral_1']), function ($q) use ($partly) {
                return $q->referral($partly['referral_1']);
            })
            // start -> offset SQL
            ->when(isset($filters['start']), function ($q) use ($filters) {
                return $q->offset($filters['start']);
            })
            // length -> limit SQL
            ->when(isset($filters['length']), function ($q) use ($filters) {
                return $q->take($filters['length']);
            });
    }
}
