<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kpi extends Model
{
    // Basic Summary KPI
    public const CONTRIBUTION             = 'contribution';
    public const COST_WITH_DEPOSIT        = 'cost_with_deposit';
    public const REPAID_WITH_DEPOSIT      = 'repaid_with_deposit';
    public const TRUST_REBATE             = 'trust_rebate';
    public const REFERRAL_REBATE          = 'referral_rebate';
    public const TOTAL_REBATE             = 'total_rebate';
    public const WITHDRAWN                = 'withdrawn';
    public const NET_CONTRIBUTION         = 'net_contribution';
    public const GROSS_PORTFOLIO_VALUE    = 'gross_portfolio_value';
    public const ROI                      = 'roi';
    public const COMBINE_ROI              = 'combine_roi';
    public const TOP_TRUST_REBATE         = 'top_trust_rebate';
    public const TOP_REFERRAL_REBATE      = 'top_referral_rebate';

    // Additional Summary KPI
    public const TOP_DAY_TOTAL_REBATE     = 'top_day_total_rebate';
    public const TOP_WEEK_TOTAL_REBATE    = 'top_week_total_rebate';
    public const TOP_MONTH_TOTAL_REBATE   = 'top_month_total_rebate';
    public const TOP_QUARTER_TOTAL_REBATE = 'top_quarter_total_rebate';
    
    // KPI for the period
    public const CUR_WEEK_TOTAL_REBATE    = 'cur_week_total_rebate';
    public const CUR_MONTH_TOTAL_REBATE   = 'cur_month_total_rebate';
  
    /**
     * 
     */
    protected $fillable = [
        'buyer',
        'date',
        'kpi_name',
        'value',
        'rank',
    ];

    /**
     * 
     */
    protected $casts = [
        'date' => 'date',
    ];

    /**
     * @return string[]
     */
    public static function allKpiNames() {
        return [
            self::CONTRIBUTION,
            self::COST_WITH_DEPOSIT,
            self::REPAID_WITH_DEPOSIT,
            self::TRUST_REBATE,
            self::REFERRAL_REBATE,
            self::TOTAL_REBATE,
            self::WITHDRAWN,
            self::NET_CONTRIBUTION,
            self::GROSS_PORTFOLIO_VALUE,
            self::ROI,
            self::COMBINE_ROI,
            self::TOP_TRUST_REBATE,
            self::TOP_REFERRAL_REBATE,
        
            self::TOP_DAY_TOTAL_REBATE,
            self::TOP_WEEK_TOTAL_REBATE,
            self::TOP_MONTH_TOTAL_REBATE,
            self::TOP_QUARTER_TOTAL_REBATE,
            
            self::CUR_WEEK_TOTAL_REBATE,
            self::CUR_MONTH_TOTAL_REBATE,
        ];
    }

    /**
     * @return self|null
     */
    public function getPreviousAttribute()
    {
        return self::where('kpi_name', $this->kpi_name)->where('buyer', $this->buyer)->whereDate('date', '<', $this->date)->orderByDesc('date')->first();
    }
}
