<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanEntry extends Model
{
    protected $table = 'loan_entries';

    protected $fillable = [
    	'amount_incl_vat',
        'notes',
    	'date',
    ];

    protected $dates = ['date'];

}
