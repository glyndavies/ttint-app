<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
    	'user_id',
    	'notes',
    	'amount_incl_vat',
    	'date',
    ];

    protected $dates = [
        'date'
    ];

    /**
     *
     */
    public function user()
    {
    	return $this->belongsTo(PlatformUser::class, 'user_id');
    }
}
