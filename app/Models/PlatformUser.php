<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

class PlatformUser extends Authenticatable
{
	use HasApiTokens, Notifiable;
	
    protected $table = 'platform_users';

    protected $fillable = [
    	'unique_id',
        'first_name',
        'last_name',
        'email_1',
        'email_2',
        'date_of_birth',
        'address_line_1',
        'address_line_2',
        'address_city',
        'address_state',
        'address_zip',
        'country',
        'phone',
        'referral_1',
        'referral_2',
        'desired_events',
		'presale_information',
        'events_alert_service',
        'express_transfer_service',
        'weekly_updates',
        'weekly_portfolio',
        'whatsapp_alerts',
        'share_desired_events',
        'permission_to_contact',
        'tcs_company_policies',
        'portfolio_needed',
        'browser',
        'ip_address',
        'location',
        'user_code',
		'blocked',
		'registered_at',
        'filled_at',
		'left_at',
		'password',
	];

	protected $hidden = [
		'password'
	];

	protected $casts = [
		'date_of_birth'  => 'date',
		'desired_events' => 'array',
		'location'       => 'array',
		'registered_at'  => 'datetime',
		'filled_at'      => 'date',
		'left_at'        => 'date',
		'portfolio_needed'=> 'boolean',
	];

	/**
	 * @return string
	 */
	public static function generatePassword()
	{
		return Str::random(8);
	}

	/**
	 * 
	 */
	public function payments()
	{
		return $this->hasMany(Payment::class, 'user_id');
	}

	/**
	 * 
	 */
	public function rebates()
	{
		return $this->hasMany(Rebate::class, 'buyer', 'user_code');
	}

	/**
	 * 
	 */
	public function rebatesWithoutNotifications()
	{
		return $this->rebates()->whereNull('notified_at')->orderBy('sold_date', 'desc');
	}

	/**
	 *
	 */
	public function getChunkedRebatesWithoutNotifications()
	{
		$chunks = collect();
		$this->rebatesWithoutNotifications->each(function($rebate) use (&$chunks) {
			if ($chunks->count() == 0 || $chunks->last()->sum('total_rebate') >= config('tt.low_rebate_notify_limit')) {
				$chunks->push(collect());
			}
			$chunks->last()->push($rebate);
		});

		return $chunks;
	}

	/**
	 * 
	 */
	public function fcmClientTokens()
	{
		return $this->hasMany(FcmClientToken::class);
	}

	/**
	 *
	 */
	public function getFullNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	/**
	 *
	 */
	public function getFullNameWithCodeAttribute()
	{
		return $this->full_name . ' (' . $this->user_code . ')';
	}

	/**
	 *
	 */
	public function getEmailsAttribute()
	{
		$emails = collect();
		if ($this->email_1) {
			$emails->push($this->email_1);
		}
		if ($this->email_2) {
			$emails->push($this->email_2);
		}
		return $emails;
	}

	/**
	 *
	 */
	public function getFullAddressAttribute()
	{
		$address = collect();
		if ($this->address_line_1) {
			$address->push(trim($this->address_line_1));
		}
		if ($this->address_line_2) {
			$address->push(trim($this->address_line_2));
		}
		if ($this->address_city) {
			$address->push(trim($this->address_city));
		}
		if ($this->address_state) {
			$address->push(trim($this->address_state));
		}
		if ($this->address_zip) {
			$address->push(trim($this->address_zip));
		}
		if ($this->country) {
			$address->push(trim($this->country));
		}

		return $address->implode(', ');
	}

	/**
	 *
	 */
	public function getDesiredEventsRawAttribute()
	{
		return implode(', ', $this->desired_events);	
	}

	/**
	 *
	 */
	public function setDesiredEventsRawAttribute($value)
	{
		return $this->desired_events = explode(',', $value);
	}

	/**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array|string
     */
    public function routeNotificationForMail($notification)
    {
        // Return email address and name...
        return [$this->email_1 => $this->name];
    }

	/**
     * Specifies the user's FCM tokens
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
		return $this->fcmClientTokens->pluck('token')->toArray();
    }

	/**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'platform-user.' . $this->user_code;
    }

	/**
     * @param string $user_code
     */
	public static function getByUserCode($value)
    {
    	return self::where('user_code', $value)->first();
    }
}
