<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Facades\InventoryRepository;
use App\Facades\InventoryQuantityRepository;
use App\Models\InventoryQuantity;
use DB;

class Purchase extends Model implements  Auditable
{
    use \OwenIt\Auditing\Auditable;
  
    protected $table ='purchases';

    protected $fillable = [
      'purchase_number',
      'order_number',
      'purchase_date',
      'number_of_tickets',
      'site_purchased',
      'deposit_rate',
      'buyer_investment',
      'face_value_of_tickets',
      'buyers_group',
      'buyers_fees',
      'buyer',
      'card_used',
      'VA',
      'total_cost_per_ticket',
      'total_cost',
      'imputer',
      'wrong_filter',
      'broken_limit',
      'insurance',
      'status_id',
    ];

    public $dates = [
        'purchase_date',
    ];

    public const STATE_TICKET_INVENTORY = 'state_ticket_I';
    public const STATE_TICKET_SOLD      = 'state_ticket_S';
    public const STATE_TICKET_DELIVERED = 'state_ticket_D';
    public const STATE_TICKET_COMPLETED = 'state_ticket_C';

    public function inventory()
    {
        return $this->hasOne(Inventory::class,'purchase_number','purchase_number');
    }
    
    public function inventoryQuantities()
    {
        return $this->hasMany(InventoryQuantity::class,'purchase_number','purchase_number');
    }

    public function companyRefund()
    {
        return $this->hasOne(CompanyRefund::class,'purchase_number','purchase_number');
    }

    public function getFaceValuePerTicketAttribute()
    {
        return $this->number_of_tickets ? $this->face_value_of_tickets / $this->number_of_tickets : 0;
    }

    public function getCostAttribute()
    {
        return $this->face_value_of_tickets + $this->buyers_fees;
    }

    public function getCostPerTicketAttribute()
    {
        return $this->number_of_tickets ? $this->cost / $this->number_of_tickets : 0;
    }

    /** 
     * get Purchase table name
     * 
     * @return string
     */
    public static function getTableName()
    {
        return (new self())->getTable();
    }

    /** 
     * get Inventory table name
     * 
     * @return string
     */
    public static function getTableNameInventory()
    {
        return (new Inventory)->getTable();
    }

    /** 
     * get Inventory Quantity table name
     * 
     * @return string
     */
    public static function getTableNameInventoryQuantity()
    {
        return (new InventoryQuantity)->getTable();
    }
     
    /** 
     * bild SQL for States Ticket
     * 
     * @param string $ticket_state 
     * @return string
     */
    public function bildStateTicketWhere($ticket_state, $table_name)
    {
        $table_name_state = ($ticket_state == Purchase::STATE_TICKET_INVENTORY)? $this->getTableNameInventory(): $this->getTableNameInventoryQuantity();
        switch ($ticket_state) {
            case Purchase::STATE_TICKET_INVENTORY:
                $query_state = InventoryRepository::onInventory();
                break;
            case Purchase::STATE_TICKET_SOLD:
                $query_state = InventoryQuantityRepository::onSold();
                break;
            case Purchase::STATE_TICKET_DELIVERED:
                $query_state = InventoryQuantityRepository::onDelivered();                
                break;
            case Purchase::STATE_TICKET_COMPLETED:
                $query_state = InventoryQuantityRepository::onCompleted();
                break;
        }

        $query_state->whereColumn($table_name.'.purchase_number', $table_name_state.'.purchase_number');

        return vsprintf(str_replace("?", '"%s"', $query_state->toSql()), $query_state->getBindings());
    }

    public function withStateTicketInventory($table_name)
    {
        return 'EXISTS('.$this->bildStateTicketWhere(Purchase::STATE_TICKET_INVENTORY, $table_name).') as '.Purchase::STATE_TICKET_INVENTORY;      
    }

    public function withStateTicketSold($table_name)
    {
        return 'EXISTS('.$this->bildStateTicketWhere(Purchase::STATE_TICKET_SOLD, $table_name).') as '.Purchase::STATE_TICKET_SOLD;    
    }

    public function withStateTicketDelivered($table_name)
    {
        return 'EXISTS('.$this->bildStateTicketWhere(Purchase::STATE_TICKET_DELIVERED, $table_name).') as '.Purchase::STATE_TICKET_DELIVERED;    
    }

    public function withStateTicketCompleted($table_name)
    {
        return 'EXISTS('.$this->bildStateTicketWhere(Purchase::STATE_TICKET_COMPLETED, $table_name).') as '.Purchase::STATE_TICKET_COMPLETED;    
    }

    // scopes
    public function scopeWithStateTicket($query)
    {
        $table_name = $this->getTableName();

        return $query
        ->select(['*'])
        ->selectRaw($this->withStateTicketInventory($table_name))
        ->selectRaw($this->withStateTicketSold($table_name))
        ->selectRaw($this->withStateTicketDelivered($table_name))
        ->selectRaw($this->withStateTicketCompleted($table_name));
    }

    /**
     * @param array $filters
     *   partly => [
     *     transaction_number
     *     order_number
     *     number_of_tickets
     *     user_code
     *     user_name
     *     broken_limit
     *     insurance
     *     inputer
     *     pending_reconciled
     *     reconciled
     *     // inventory->
     *        event_genre
     *        event
     *        event_date
     *        arena
     *        country
     *        site_purchase
     *        type_of_ticket
     *        restrictions
     *     // inventoryQuantities(inventory_quantity)->
     *        seats
     *   ]
     *   start
     *   length
     */
    public function scopeFilters($query, $filters)
    {
        $partly = $filters['partly'] ?? [];

        return $query
            // search
            ->when(isset($filters['search']), function ($q) use ($filters) {
                return $q->whereHas('inventory', function($q) use ($filters) {
                    return $q
                        ->whereRaw('lower(inventory.event_genre) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.event) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.arena) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.country) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.site_purchased) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.type_of_ticket) like "%'.strtolower($filters['search']).'%"')
                        ->orWhereRaw('lower(inventory.restrictions) like "%'.strtolower($filters['search']).'%"');
                })
                ->orWhereHas('inventoryQuantities', function($q) use ($filters) {
                    return $q
                        ->whereRaw('lower(inventory_quantity.seats) like "%'.strtolower($filters['search']).'%"');
                })
                ->orWhereRaw('lower(purchase_number) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(order_number) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(number_of_tickets) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(buyer) like "%'.strtolower($filters['search']).'%"')
                ->orWhereRaw('lower(card_used) like "%'.strtolower($filters['search']).'%"');
            })
            // partly: transaction_number (purchase_number)
            ->when(isset($partly['transaction_number']), function ($q) use ($partly) {
                return $q->whereRaw('lower(purchase_number) like "%'.strtolower($partly['transaction_number']).'%"');
            })
            // partly: order_number
            ->when(isset($partly['order_number']), function ($q) use ($partly) {
                return $q->whereRaw('lower(order_number) like "%'.strtolower($partly['order_number']).'%"');
            })
            // partly: number_of_tickets
            ->when(isset($partly['number_of_tickets']), function ($q) use ($partly) {
                return $q->whereRaw('lower(number_of_tickets) like "%'.strtolower($partly['number_of_tickets']).'%"');
            })
            // partly: user_code (buyer)
            ->when(isset($partly['user_code']), function ($q) use ($partly) {
                return $q->whereRaw('lower(buyer) like "%'.strtolower($partly['user_code']).'%"');
            })
            // partly: user_name (card_used)
            ->when(isset($partly['user_name']), function ($q) use ($partly) {
                return $q->whereRaw('lower(card_used) like "%'.strtolower($partly['user_name']).'%"');
            })
            // partly: broken_limit
            ->when(isset($partly['broken_limit']), function ($q) use ($partly) {
                return $q->whereRaw('lower(broken_limit) like "%'.strtolower($partly['broken_limit']).'%"');
            })
            // partly: insurance
            ->when(isset($partly['insurance']), function ($q) use ($partly) {
                return $q->whereRaw('lower(insurance) like "%'.strtolower($partly['insurance']).'%"');
            })
            // partly: inputer (Imputer)
            ->when(isset($partly['inputer']), function ($q) use ($partly) {
                return $q->whereRaw('lower(Imputer) like "%'.strtolower($partly['inputer']).'%"');
            })
            // partly: pending_reconciled
            ->when(isset($partly['pending_reconciled']), function ($q) use ($partly) {
                return $q->whereRaw('lower(pending_reconciled) like "%'.strtolower($partly['pending_reconciled']).'%"');
            })
            // partly: reconciled
            ->when(isset($partly['reconciled']), function ($q) use ($partly) {
                return $q->whereRaw('lower(reconciled) like "%'.strtolower($partly['reconciled']).'%"');
            })
            // partly: inventory->event_genre
            ->when(isset($partly['event_genre']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.event_genre) like "%'.strtolower($partly['event_genre']).'%"');
                });
            })
            // partly: inventory->event
            ->when(isset($partly['event']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.event) like "%'.strtolower($partly['event']).'%"');
                });
            })
            // partly: inventory->event_date
            ->when(isset($partly['event_date']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereDate('inventory.event_date', '=', $partly['event_date']);
                });                
            })
            // partly: inventory->arena
            ->when(isset($partly['arena']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.arena) like "%'.strtolower($partly['arena']).'%"');
                });
            })
            // partly: inventory->country
            ->when(isset($partly['country']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.country) like "%'.strtolower($partly['country']).'%"');
                });
            })
            // partly: inventory->site_purchase (inventory->site_purchased)
            ->when(isset($partly['site_purchase']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.site_purchased) like "%'.strtolower($partly['site_purchase']).'%"');
                });
            })
            // partly: inventory->type_of_ticket
            ->when(isset($partly['type_of_ticket']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.type_of_ticket) like "%'.strtolower($partly['type_of_ticket']).'%"');
                });
            })
            // partly: inventory->restrictions
            ->when(isset($partly['restrictions']), function ($q) use ($partly) {
                return $q->whereHas('inventory', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory.restrictions) like "%'.strtolower($partly['restrictions']).'%"');
                });
            })
            // partly: inventoryQuantities(inventory_quantity)->seats
            ->when(isset($partly['seats']), function ($q) use ($partly) {
                return $q->whereHas('inventoryQuantities', function($q) use ($partly) {
                    return $q->whereRaw('lower(inventory_quantity.seats) like "%'.strtolower($partly['seats']).'%"');
                });
            })
            // start -> offset SQL
            ->when(isset($filters['start']), function ($q) use ($filters) {
                return $q->offset($filters['start']);
            })
            // length -> limit SQL
            ->when(isset($filters['length']), function ($q) use ($filters) {
                return $q->take($filters['length']);
            });
    }
}
