<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rebate extends Model
{
    /**
     * 
     */
    protected $fillable = [
        'buyer',
        'inventory_quantity_id',
        'trust_rebate',
        'referral_rebate',
        'sold_date',
        'notified_at',
    ];

    /**
     * 
     */
    protected $casts = [
        'sold_date' => 'date',
        'notified_at' => 'datetime',
        'cancelled_at' => 'datetime'
    ];

    /**
     * 
     */
    public function inventoryQuantity()
    {
        return $this->belongsTo(InventoryQuantity::class);
    }

    /**
     * @return float
     */
    public function getTotalRebateAttribute()
    {
        return $this->trust_rebate + $this->referral_rebate;
    }

    /**
     * 
     */
    public function scopeNotCancelled($query)
    {
        return $query->whereNull('cancelled_at');
    }

    /**
     * @param $filters
     *   'user_code'
     *   'order_number'
     *   'deposit'
     *   'sold_date_from'
     *   'sold_date_to'
     *   'cancelled'
     */
    public function scopeFilters($query, $filters)
    {
        return $query
            // user_code
            ->when(isset($filters['user_code']), function ($q) use ($filters) {
                return $q->where('buyer', $filters['user_code']);
            })
            // order_number
            ->when(isset($filters['order_number']), function ($q) use ($filters) {
                return $q->whereHas('inventoryQuantity.inventory', function ($q) use ($filters) {
                    return $q->where('order_number', $filters['order_number']);
                });
            })
            // deposit
            ->when(isset($filters['deposit']), function ($q) use ($filters) {
                return $q->whereHas('inventoryQuantity.inventory.purchase', function ($q) use ($filters) {
                    return $filters['deposit'] == 0
                        ? $q->whereNull('deposit_rate')->orWhere('deposit_rate', 0)
                        : $q->whereNotNull('deposit_rate')->where('deposit_rate', '!=', 0);
                });
            })
            // sold_date_from
            ->when(isset($filters['sold_date_from']), function ($q) use ($filters) {
                return $q->whereDate('sold_date', '>=', $filters['sold_date_from']);
            })
            // sold_date_to
            ->when(isset($filters['sold_date_to']), function ($q) use ($filters) {
                return $q->whereDate('sold_date', '<=', $filters['sold_date_to']);
            })
            // cancelled true
            ->when(isset($filters['cancelled']) && $filters['cancelled'], function ($q) use ($filters) {
                return $q->whereNotNull('cancelled_at');
            })
            // cancelled false
            ->when(isset($filters['cancelled']) && !$filters['cancelled'], function ($q) use ($filters) {
                return $q->whereNull('cancelled_at');
            });
    }
}