<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SoldTicket extends Model  implements Auditable
{
    use \OwenIt\Auditing\Auditable;

	protected $table = 'sold_tickets';

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'inventory_quantity_id',
        'purchase_number',
        'sold_date',
        'site_sold',
        'sale_id_ref',
        'sell_price',
        'ticket_qty',
        'seats',
        'is_paid',
        'paid_date',
    ];

    protected $dates = [
        'sold_date',
        'paid_date',
    ];

    //protected $with = ['customer_refund'];

    public function inventoryQuantity()
    {
        return $this->belongsTo(InventoryQuantity::class);
    }

    // scopes
    public function scopePaid($query)
    {
        return $query->where('is_paid', 'Y');
    }

    public function scopeNotPaid($query)
    {
        return $query->where('is_paid', '!=', 'Y');
    }
}
