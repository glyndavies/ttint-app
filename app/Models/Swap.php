<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Swap extends Model implements  Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table ='swaps';

    public function inventory0()
    {
        return $this->belongsTo(Inventory::class, 'inventory0_id');
    }

    public function inventory1()
    {
        return $this->belongsTo(Inventory::class, 'inventory1_id');
    }
}
