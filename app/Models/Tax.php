<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table = 'taxes';

    protected $fillable = [
    	'amount_incl_vat',
        'notes',
    	'date',
    ];

    protected $dates = ['date'];

}
