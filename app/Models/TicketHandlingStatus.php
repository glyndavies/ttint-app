<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketHandlingStatus extends Model
{
    /**
     * 
     */
  	protected $table = 'ticket_handling_status';

    /**
     * 
     */
    public function inventoryBatch()
    {
        return $this->belongsTo(InventoryQuantity::class,'ticket_handling_id','id');
    }

    /**
     * @return boolean
     */
    public function notStarted() {
        return $this->id == 1;
    }

    /**
     * @return boolean
     */
    public function uploaded() {
        return $this->id == 2;
    }

    /**
     * @return boolean
     */
    public function transfered() {
        return $this->id == 3;
    }

    /**
     * @return boolean
     */
    public function collected() {
        return $this->id == 4;
    }

    /**
     * @return boolean
     */
    public function sent() {
        return $this->id == 5;
    }

}
