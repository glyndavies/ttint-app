<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class TicketStatus extends Model implements Auditable
{
    use AuditableTrait;
    
    protected $table = 'ticket_status';

	public const AVAILABLE          = 1;
    public const SPLIT              = 2;
	public const SOLD               = 3;
    public const AVAILABLE_FOR_SWAP = 11;
    public const SWAPPED            = 12;
}
