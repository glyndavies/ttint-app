<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class TicketType extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    /**
     * 
     */
    protected $table ='ticket_types';

    /**
     * @return boolean
     */
    public function paper()
    {
        return $this->id == 1;
    }

    /**
     * @return boolean
     */
    public function eticket()
    {
        return $this->id == 2;
    }

    /**
     * @return boolean
     */
    public function flash()
    {
        return $this->id == 3;
    }

    /**
     * @return boolean
     */
    public function transfer()
    {
        return $this->id == 4;
    }

    /**
     * @return boolean
     */
    public function qrcode()
    {
        return $this->id == 5;
    }

}
