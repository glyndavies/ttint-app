<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use Auth;

class User extends Authenticatable  implements Auditable
{
    use Notifiable;
    use HasRoles;
    use Messagable;
    use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','firstname', 'surname', 'dob',
        'houseNumber',
        'Address1',
        'Address2',
        'town',
        'city',
        'postcode',
        'lastLogin',
        'last_LogOut',
        'color'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['dob','lastLogin'];

    /**
     *
     */
    public function scopeWhereNotCurrent($query)
    {
        return $query->where('id', '!=', Auth::id());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inventoryQuantities()
    {
        return $this->hasMany(InventoryQuantity::class, 'in_progress_by_user_id', 'id');
    }

    public function getColorHexAttribute()
    {
        return '#' . ($this->color ? $this->color : '50ff50');
    }
}
