<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCharge extends Model
{
    protected $table = 'user_charges';

    protected $fillable = [
    	'user_id',
    	'description',
    	'amount',
    	'vat',
    	'date',
    ];

    protected $dates = ['date'];

    /**
     *
     */
    public function user()
    {
    	return $this->belongsTo(PlatformUser::class, 'user_id');
    }

    /**
     *
     */
    public function getAmountInclVatAttribute()
    {
    	return $this->amount + $this->vat;
    }
}
