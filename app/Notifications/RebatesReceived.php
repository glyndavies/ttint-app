<?php
 
namespace App\Notifications;

use App\Models\Rebate;
use App\Resources\RebateResource;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;

class RebatesReceived extends Notification implements ShouldQueue
{
    use Queueable;
 
    /**
     * @var integer[]
     */
    public $rebate_ids;

    /**
     * @param array $rebate_ids
     * @return void
     */
    public function __construct($rebate_ids)
    {
        $this->rebate_ids = $rebate_ids;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->fcmClientTokens->count() ? ['mail', FcmChannel::class] : ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $rebates = Rebate::whereIn('id', $this->rebate_ids)->get();
        return (new MailMessage)
            ->subject($notifiable->user_code . ': ' . moneyformat($rebates->sum('total_rebate')) . ' Rebate received')
            ->markdown('emails.rebates-received', [
                'platformUser' => $notifiable,
                'rebates' => $rebates,
            ]);
    }

    /**
     * Get theFirebase Cloud Messaging representation of the notification.
     * @param  mixed $notifiable
     * @return \NotificationChannels\Fcm\FcmMessage
     */
    public function toFcm($notifiable)
    {
        $rebates = Rebate::whereIn('id', $this->rebate_ids)->get();

        return FcmMessage::create()
            ->setData([
                'rebates' => $rebates->map(function ($rebate) {
                    return new RebateResource($rebate);
                })->toJson(),
            ])
            ->setNotification(FcmNotification::create()
                ->setTitle('TTAnalytics')
                ->setBody(moneyformat($rebates->sum('total_rebate')) . ' Rebate received'))
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setNotification(AndroidNotification::create()->setColor('#0A0A0A')))
            ->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios')))
            ;
    }
}
