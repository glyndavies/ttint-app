<?php

namespace App\Providers;

use App\Facades\InventoryQuantityRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBladeDirectives();
        $this->registerFacades();
    }

    /**
     * @return void
     */
    public function registerBladeDirectives()
    {
        Blade::directive('money', function ($expression) {
            return "<?php echo '£' . number_format(($expression), 2); ?>";
        });
    }

    /**
     * @return void
     */
    public function registerFacades()
    {
        $this->app->singleton('inventory-repository', \App\Repositories\InventoryRepository::class);
        $this->app->singleton('inventory-service', \App\Services\InventoryService::class);
        $this->app->singleton('inventory-quantity-repository', \App\Repositories\InventoryQuantityRepository::class);
        $this->app->singleton('inventory-quantity-service', \App\Services\InventoryQuantityService::class);
        $this->app->singleton('rebate-repository', \App\Repositories\RebateRepository::class);
        $this->app->singleton('rebate-service', \App\Services\RebateService::class);
        $this->app->singleton('sold-ticket-repository', \App\Repositories\SoldTicketRepository::class);
        $this->app->singleton('sold-ticket-service', \App\Services\SoldTicketService::class);
        $this->app->singleton('purchase-repository', \App\Repositories\PurchaseRepository::class);
        $this->app->singleton('purchase-service', \App\Services\PurchaseService::class);
        $this->app->singleton('ticket-type-repository', \App\Repositories\TicketTypeRepository::class);
        $this->app->singleton('ticket-status-repository', \App\Repositories\TicketStatusRepository::class);
        $this->app->singleton('ticket-handling-status-repository', \App\Repositories\TicketHandlingStatusRepository::class);
        $this->app->singleton('event-genre-repository', \App\Repositories\EventGenreRepository::class);
        $this->app->singleton('event-fulfillment-repository', \App\Repositories\EventFulfillmentRepository::class);
        $this->app->singleton('platform-user-repository', \App\Repositories\PlatformUserRepository::class);
        $this->app->singleton('platform-user-service', \App\Services\PlatformUserService::class);
        $this->app->singleton('payment-repository', \App\Repositories\PaymentRepository::class);
        $this->app->singleton('payment-service', \App\Services\PaymentService::class);
        $this->app->singleton('delivery-batch-service', \App\Services\DeliveryBatchService::class);
        $this->app->singleton('kpi-repository', \App\Repositories\KpiRepository::class);
        $this->app->singleton('kpi-service', \App\Services\KpiService::class);
        $this->app->singleton('charge-service', \App\Services\ChargeService::class);
        $this->app->singleton('expenses-sub-category-repository', \App\Repositories\ExpenseSubCategoryRepository::class);
        //
    }
}
