<?php

namespace App\Repositories;

use App\Facades\InventoryRepository;
use App\Models\EventFulfillment;
use App\Models\EventGenre;
use Illuminate\Support\Collection;

class EventFulfillmentRepository
{
    /**
     * @param array $filters
     * @param array $inventoryFilters
     *
     * @return Collection
     */
    public function find($filters = [], $inventoryFilters = [])
    {
        $inventoryData = InventoryRepository::getFulfillmentBoard($inventoryFilters);

        $groupedData = [];

        foreach ($inventoryData as $item) {
            $key = trim($item->event) . '|' . $item->event_date . '|' . $item->time_of_event . '|' . trim($item->arena);
            if (!isset($groupedData[$key])) {
                $groupedData[$key] = [
                    'event' => trim($item->event),
                    'event_date' => $item->event_date,
                    'time_of_event' => $item->time_of_event,
                    'arena' => trim($item->arena),
                    'inventories' => collect()
                ];
            }
            $groupedData[$key]['inventories']->push($item);
        }

        $eventFulfillments = new Collection();

        foreach ($groupedData as $group) {
            $eventFulfillments->push(new EventFulfillment($group));
        }
        if (!isset($filters['start']) && !isset($filters['length'])) {
            return $eventFulfillments;
        }

        $page = intdiv($filters['start'] ?? 0, $filters['length'] ?? -1) + 1;
        return $eventFulfillments->forPage($page, $filters['length'] ?? -1);
    }

    /**
     * @param array $filters
     * @param array $inventoryFilters
     *
     * @return int
     */
    public function getEventFulfillmentsCount($filters = [], $inventoryFilters = [])
    {
        return $this->find(collect($filters)->except(['start', 'length']), $inventoryFilters)->count();
    }
}
