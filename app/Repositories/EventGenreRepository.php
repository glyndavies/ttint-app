<?php

namespace App\Repositories;

use App\Models\EventGenre;

class EventGenreRepository
{
    /**
     * @return \App\Models\EventGenre[]
     */
    public function findAll()
    {
        return EventGenre::all();
    }

    /**
     * @param string $id
     * @return \App\Models\EventGenre|null
     */
    public function findById($id)
    {
        return EventGenre::where('id', $id)->first();
    }

    /**
     * @return \App\Models\EventGenre[]
     */
    public function getActive()
    {
        return EventGenre::where('active', 1)->get();
    }
}