<?php

namespace App\Repositories;

use App\Models\ExpenseSubCategory;

class ExpenseSubCategoryRepository
{
    /**
     * @return \App\Models\ExpenseSubCategory[]
     */
    public function findAll()
    {
        return ExpenseSubCategory::all();
    }

    /**
     * @param string $id
     * @return \App\Models\ExpenseSubCategory|null
     */
    public function findById($id)
    {
        return ExpenseSubCategory::where('id', $id)->first();
    }

    /**
     * @param string $slug
     * @return \App\Models\ExpenseSubCategory|null
     */
    public function findBySlug($slug)
    {
        return ExpenseSubCategory::where('slug', $slug)->first();
    }

    /**
     * @return \App\Models\ExpenseSubCategory[]
     */
    public function getActive()
    {
        return ExpenseSubCategory::where('active', 1)->get();
    }
}