<?php

namespace App\Repositories;

use App\Models\Inventory;
use App\Models\InventoryQuantity;
use App\Models\Purchase;
use App\Models\TicketType;
use App\Facades\TicketTypeRepository;
use App\Facades\InventoryRepository;
use Illuminate\Support\Facades\DB;

class InventoryQuantityRepository
{
    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function findAll($filters = [])
    {
        return InventoryQuantity::filters($filters)->get();
    }

    /**
     * @param string $id
     * @return \App\Models\InventoryQuantity|null
     */
    public function findById($id)
    {
        return InventoryQuantity::where('id', $id)->first();
    }

    /**
     * @param string $ids
     * @return \App\Models\InventoryQuantity[]
     */
    public function findByIds($ids)
    {
        return InventoryQuantity::whereIn('id', $ids)->get();
    }

    /**
     * Get one merged collection of ticketInventories from several sections
     * @param $sections
     * @param $filters
     */
    public function getSections($sections, $filters = [])
    {
        return collect($sections)->reduce(function($carry, $section) use ($filters) {
            $method = "get" . ucfirst($section);
            return $carry->concat($this->$method($filters));
        }, collect());
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getTransfer($filters = [])
    {
        return InventoryQuantity::with([
            'inventory.ticketStatus',
            'inventory.purchase',
            ])
            ->statusForTransfer()
            ->whereHas('inventory', function($q) {
                $q->notCancelled()->notExpired();
            })
            ->filters($filters)
            ->get()
            ->sortBy('event_date');
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getAllInventory($filters = [])
    {
        return InventoryQuantity::with([
            'inventory',
            'inventory.purchase',
            'inventory.purchase.companyRefund',
            'soldTicket',
            'deliveryBatch',
            'ticketStatus',
        ])
        ->withStateTicketAll()
        ->filters($filters)
        ->orderBy('purchase_number', 'DESC')
        ->get();
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getAllInventoryCount($filters = [])
    {
        return InventoryQuantity::with([
            'inventory',
            'inventory.purchase',
            'inventory.purchase.companyRefund',
            'soldTicket',
            'deliveryBatch',
            'ticketStatus',
        ])
        ->withStateTicketAll()
        ->filters($filters)
        ->count();
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getInventory($filters = [])
    {
        return InventoryQuantity::with([
            'inventory.ticketStatus',
            'inventory.purchase',
        ])
        ->statusForSale()
        ->whereHas('inventory', function($q) {
            $q->notCancelled()->notExpired();
        })
        ->filters($filters)
        ->get()
        ->sortBy('event_date');
    }
    
    /**
     * @return integer
     */
    public function getInventoryCount($filters = [])
    {
        return InventoryRepository::getInventoryCount($filters);
    }

    /**
     * 
    */
    public function onSold()
    {
        return InventoryQuantity::with([
            'inventory',
            'inventory.purchase',
            'inventory.purchase.companyRefund',
            'soldTicket',
            'ticketStatus',
            'ticketHandlingStatus',
        ])
        ->statusSold()
        ->handlingNotDelivered()
        // --- experimental changes --- //
        ->whereHas('soldTicket', function($q) {
            $q->notPaid();
        })
        // ---------------------------- //
        ->whereHas('inventory', function($q) {
            $q->notCancelled();
        });
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getSold($filters = [])
    {
        return $this->onSold()
            ->filters($filters)
            ->orderByEventDate()
            ->get();
    }

    /**
     * @return integer
     */
    public function getSoldCount($filters = [])
    {
        return InventoryQuantity::whereHas('inventory', function($q) {
                $q->notCancelled();
            })
            ->filters($filters)
            ->statusSold()
            ->handlingNotDelivered()
            // --- experimental changes --- //
            ->whereHas('soldTicket', function($q) {
                $q->notPaid();
            })
            // ---------------------------- //
            ->count();
    }

    /**
     * @return \App\Models\InventoryQuantity[]
    */
    public function onDelivered()
    {
        return InventoryQuantity::with([
            'inventory',
            'inventory.purchase',
            'inventory.purchase.companyRefund',
            'soldTicket',
            'deliveryBatch',
            'ticketStatus',
        ])
        ->statusSold()
        ->handlingDelivered()
        ->where('tickets_in_hand', 2)
        ->whereHas('soldTicket', function($q) {
            $q->notPaid();
        })
        ->whereHas('inventory', function($q) {
            $q->notCancelled();
        });
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getDelivered($filters = [])
    {
        return $this->onDelivered()
            ->filters($filters)
            ->orderByEventDate()
            ->get();
    }

    /**
     * @return integer
     */
    public function getDeliveredCount($filters = [])
    {
        return InventoryQuantity::whereHas('inventory', function($q) {
                $q->notCancelled();
            })
            ->filters($filters)
            ->statusSold()
            ->handlingDelivered()
            ->where('tickets_in_hand', 2)
            // --- experimental changes --- //
            ->whereHas('soldTicket', function($q) {
                $q->notPaid();
            })
            // ---------------------------- //
            ->count();
    }

    /**
     * @return \App\Models\InventoryQuantity[]
    */
    public function onCompleted()
    {
        return InventoryQuantity::with([
            'inventory',
            'inventory.purchase',
            'inventory.purchase.companyRefund',
            'soldTicket',
            'ticketStatus',
        ])
        ->statusSold()
        // --- experimental changes --- //
        //->handlingDelivered()
        //->where('tickets_in_hand', 2)
        // --------------------------- //
        ->whereHas('soldTicket', function($q) {
            $q->paid();
        })
        ->whereHas('inventory', function($q) {
            $q->notCancelled();
        });
        // --- experimental changes --- //
        //->whereDoesntHave('soldTicket.customerRefund')
        // ---------------------------- //
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getCompleted($filters = [])
    {
        return $this->onCompleted()
            ->filters($filters)
            ->orderByEventDate()
            ->get();
    }

    /**
     * @return integer
     */
    public function getCompletedCount($filters = [])
    {
        return InventoryQuantity::whereHas('inventory', function($q) {
                $q->notCancelled();
            })
            ->filters($filters)
            ->statusSold()
            ->whereHas('soldTicket', function($q) {
                $q->paid();
            })
            // ---------------------------- //
            ->count();
    }

    /**
     * @param \App\Models\TicketType $ticketType
     * @return \App\Models\InventoryQuantity[]
     */
    public function getNotReceived(TicketType $ticketType)
    {
        return InventoryQuantity::with([
                'inventory',
                'inventory.purchase',
                'soldTicket',
                'chasePic',
                'ticketStatus',
                'ticketHandlingStatus',
            ])
            ->statusInSells()
            ->where('tickets_in_hand', 1)
            ->whereHas('inventory', function($q) use ($ticketType) {
                $q->where('type_of_ticket', $ticketType->tickettype)->where('event_cancelled', 'N');
            })
            ->get()
            ->sort(function($a, $b) {
                return strcmp($a->inventory->purchase->buyer, $b->inventory->purchase->buyer)
                    ?: strcmp($a->inventory->event, $b->inventory->event)
                    ?: strcmp(optional($a->inventory->event_date)->timestamp, optional($b->inventory->event_date)->timestamp);
            });
    }

    /**
     * @param \App\Models\TicketType $ticketType
     * @return \App\Models\InventoryQuantity[]
     */
    public function getFulfillment(TicketType $ticketType)
    {
        return InventoryQuantity::with([
                'inventory',
                'inventory.purchase',
                'soldTicket',
                'deliveryBatch',
                'ticketHandlingStatus',
            ])
            ->statusSold()
            ->handlingNotDelivered()
            ->where('tickets_in_hand', 2)
            ->whereHas('inventory', function($q) use ($ticketType) {
                $q->where('type_of_ticket', $ticketType->tickettype)->where('event_cancelled', 'N');
            })
            ->get()
            ->sort(function($a, $b) {
                return strcmp($a->inventory->purchase->buyer, $b->inventory->purchase->buyer)
                    ?: strcmp($a->inventory->event, $b->inventory->event)
                    ?: strcmp(optional($a->inventory->event_date)->timestamp, optional($b->inventory->event_date)->timestamp);
            });
    }

    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getCancelled($filters = [])
    {
        return InventoryQuantity::with([
                'inventory',
            ])
            ->whereHas('inventory', function($q){
                $q->cancelled();
            })
            ->filters($filters)
            ->get();
    }
    
    /**
     * @param array $filters
     * @return \App\Models\InventoryQuantity[]
     */
    public function getExpired($filters = [])
    {
        return InventoryQuantity::whereHas('inventory', function($q) {
                $q->expired();
            })
            ->statusNotSold()
            ->filters($filters)
            ->get();
    }
}