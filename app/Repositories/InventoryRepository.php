<?php

namespace App\Repositories;

use App\Models\Inventory;

class InventoryRepository
{
    /**
     * @return \App\Models\Inventory[]
     */
    public function findAll()
    {
        return Inventory::all();
    }

    /**
     * @param string $id
     * @return \App\Models\Inventory|null
     */
    public function findById($id)
    {
        return Inventory::where('id', $id)->first();
    }

    public function onInventory()
    {
        return Inventory::with([
                'ticketStatus',
                'purchase',
                'inventoryQuantities',
                'inventoryQuantities.ticketStatus',
            ])
            ->hasQuantityForSale()
            ->notCancelled()
            ->notExpired();
    }

    /**
     * @return \App\Models\Inventory[]
     */
    public function getInventory($filters = [])
    {
        return $this->onInventory()
            ->filters($filters)
            ->orderBy('event_date')
            ->orderBy('time_of_event')
            ->get();
    }

    public function getInventoryCount($filters = []){

        return Inventory::hasQuantityForSale()
        ->filters($filters)
        ->notCancelled()
        ->notExpired()
        // ---------------------------- //
        ->count();
    }

    /**
     * @return \App\Models\Inventory[]
     */
    public function getTransfer()
    {
        return Inventory::with([
            'ticketStatus',
            'purchase',
            'inventoryQuantities',
            'inventoryQuantities.ticketStatus',
        ])
            ->hasQuantityForTransfer()
            ->notCancelled()
            ->notExpired()
            ->get()
            ->sortBy('event_date');
    }

    /**
     * @return \App\Models\Inventory[]
     */
    public function getExpired()
    {
        return Inventory::expired()->get();
    }

    /**
     * @return \App\Models\Inventory[]
     */
    public function onFulfillmentBoard()
    {
        return Inventory::with([
                'ticketStatus',
                'purchase',
                'inventoryQuantities',
                'inventoryQuantities.ticketStatus',
            ])
            ->unsoldOrUndeliveredQuantities()
            ->notCancelled()
            ->notExpired();
    }

    /**
     * @param array $filters
     *
     * @return \App\Models\Inventory[]
     */
    public function getFulfillmentBoard($filters = [])
    {

        return $this->onFulfillmentBoard()
            ->filters($filters)
            ->orderBy('event_date')
            ->orderBy('time_of_event')
            ->get();
    }
}
