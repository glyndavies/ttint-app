<?php

namespace App\Repositories;

use App\Models\Kpi;

class KpiRepository
{
    /**
     * @return \App\Models\Kpi[]
     */
    public function topRanks($kpi_name, $limit = null)
    {
        $query = Kpi::where('kpi_name', $kpi_name)->whereDate('date', now())->orderBy('rank');
        if ($limit) {
            $query = $query->take($limit);
        }
        return $query->get();
    }
}