<?php

namespace App\Repositories;

use App\Models\Payment;
use App\Models\PlatformUser;

class PaymentRepository
{
    /**
     * @return \App\Models\Payment[]
     */
    public function findAll()
    {
        return Payment::with([
            'user',            
        ])->get();
    }

    /**
     * @param integer $id
     * @return \App\Models\Payment
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findOrFail($id)
    {
        return Payment::findOrFail($id);
    }

    /**
     * @param \App\Models\PlatformUser $user
     * @return \App\Models\Payment[]
     */
    public function getByUser(PlatformUser $user)
    {
        return Payment::where('user_id', $user->id);
    }
}