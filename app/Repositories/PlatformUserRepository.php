<?php

namespace App\Repositories;

use App\Models\PlatformUser;
use Illuminate\Support\Facades\Hash;

class PlatformUserRepository
{
    /**
     * @return \App\Models\PlatformUser[]
     */
    public function findAll()
    {
        return PlatformUser::all();
    }

    /**
     * @param integer $id
     * @return \App\Models\PlatformUser
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findOrFail($id)
    {
        return PlatformUser::findOrFail($id);
    }

    /**
     * @param string $user_code
     * @param string $password
     * @return \App\Models\PlatformUser|null
     */
    public function findByCredentials($user_code, $password)
    {
        $platformUser = PlatformUser::where('user_code', $user_code)->first();
        
        //if ( !$platformUser || !Hash::check($password, $platformUser->password)) {
        if ( !$platformUser || $password != $platformUser->password) {
            return null;
        }

        return $platformUser;
    }

    /**
     * @param string $user_code
     * @return \App\Models\PlatformUser|null
     */
    public function findByUserCode($user_code)
    {
        return $platformUser = PlatformUser::where('user_code', $user_code)->first();
    }
}