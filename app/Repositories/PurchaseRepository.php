<?php

namespace App\Repositories;

use App\Models\Purchase;
use DB;

class PurchaseRepository
{
    /**
     * @return \App\Models\Putchase[]
     */
    public function findAll()
    {
        return Purchase::all();
    }

    /**
     * @param string $id
     * @return \App\Models\Purchase|null
     */
    public function findById($id)
    {
        return Purchase::where('id', $id)->first();
    }

    /**
     * @param array $filters
     * @return \App\Models\Purchase[]
     */
    public function getPurchases($filters = [])
    {
        return Purchase::with([
                'inventory',
                'inventoryQuantities',
                'companyRefund',
            ])
            ->withStateTicket()
            ->whereHas('inventory', function($q) {
                $q->where('event_cancelled', 'N');
            })
            ->filters($filters)
            ->get();
    }
    
    /**
     * @param array $filters
     * @return integer
     */
    public function getPurchasesCount($filters = [])
    {
        return Purchase::whereHas('inventory', function($q) {
                $q->where('event_cancelled', 'N');
            })
            ->withStateTicket()
            ->filters($filters)
            // ---------------------------- //
            ->count();
    }

    /**
     * @return \App\Models\Purchase[]
     */
    public function getCancelled()
    {
        return Purchase::with([
                'inventory',
                'inventoryQuantities',
                'companyRefund',
            ])
            ->whereHas('inventory', function($q){
                $q->where('event_cancelled', 'Y');
            })
            ->get();
    }

    /**
     * @return float
     */
    public function getPurchasesTotalCost($filters = []){
        return Purchase::whereHas('inventory', function($q) {
            $q->where('event_cancelled', 'N');
        })
        ->withStateTicket()
        ->filters($filters)
        // ---------------------------- //
        ->sum('total_cost');
    }
}