<?php

namespace App\Repositories;

use App\Models\Rebate;

class RebateRepository
{
    /**
     * @param array $filters
     * @return \App\Models\Rebate[]
     */
    public function find($filters = [])
    {
        return Rebate::filters($filters)->orderBy('sold_date', 'desc')->get();
    }
}