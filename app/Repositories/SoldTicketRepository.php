<?php

namespace App\Repositories;

use App\Models\SoldTicket;

class SoldTicketRepository
{
    /**
     * @return \App\Models\SoldTicket[]
     */
    public function findAll()
    {
        return SoldTicket::all();
    }

    /**
     * @param string $id
     * @return \App\Models\SoldTicket|null
     */
    public function findById($id)
    {
        return SoldTicket::where('id', $id)->first();
    }

    /**
     * @param string $sale_id_ref
     * @return \App\Models\SoldTicket[]
     */
    public function findBySaleIdRef($sale_id_ref)
    {
        return SoldTicket::where('sale_id_ref', $sale_id_ref)->get();
    }
}