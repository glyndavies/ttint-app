<?php

namespace App\Repositories;

use App\Models\TicketHandlingStatus;

class TicketHandlingStatusRepository
{
    /**
     * @return \App\Models\TicketHandlingStatus[]
     */
    public function findAll()
    {
        return TicketHandlingStatus::all();
    }

    /**
     * @param string $id
     * @return \App\Models\TicketHandlingStatus|null
     */
    public function findById($id)
    {
        return TicketHandlingStatus::where('id', $id)->first();
    }

    /**
     * @return \App\Models\TicketHandlingStatus[]
     */
    public function getActive()
    {
        return TicketHandlingStatus::where('active', 1)->get();
    }
}