<?php

namespace App\Repositories;

use App\Models\TicketStatus;

class TicketStatusRepository
{
    /**
     * @return \App\Models\TicketStatus[]
     */
    public function findAll()
    {
        return TicketStatus::all();
    }

    /**
     * @param string $id
     * @return \App\Models\TicketStatus|null
     */
    public function findById($id)
    {
        return TicketStatus::where('id', $id)->first();
    }

    /**
     * @return \App\Models\TicketStatus[]
     */
    public function getActive()
    {
        return TicketStatus::where('active', 1)->get();
    }
}