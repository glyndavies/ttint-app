<?php

namespace App\Repositories;

use App\Models\TicketType;

class TicketTypeRepository
{
    /**
     * @return \App\Models\TicketType[]
     */
    public function findAll()
    {
        return TicketType::all();
    }

    /**
     * @param string $id
     * @return \App\Models\TicketType|null
     */
    public function findById($id)
    {
        return TicketType::where('id', $id)->first();
    }

    /**
     * @param string $slug
     * @return \App\Models\TicketType|null
     */
    public function findBySlug($slug)
    {
        return TicketType::where('slug', $slug)->first();
    }

    /**
     * @return \App\Models\TicketType[]
     */
    public function getActive()
    {
        return TicketType::where('active', 1)->get();
    }
}