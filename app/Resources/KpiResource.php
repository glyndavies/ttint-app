<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Kpi;

class KpiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'user_code' => $this->buyer,
            'date'      => $this->date->toDateString(),
            'kpi_name'  => $this->kpi_name,
            'value'     => $this->value,
            'rank'      => $this->rank,
        ];
   }
}