<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Kpi;
use App\Resources\KpiResource;

class KpiStateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return collect(Kpi::allKpiNames())->mapWithKeys(function($kpi_name, $index) {
            $kpis = Kpi::where('buyer', $this->user_code)->where('kpi_name', $kpi_name)->orderBy('date', 'desc')->take(2)->get();
            return [
                $kpi_name => [
                    'current'  => new KpiResource($kpis->get(0)),
                    'previous' => new KpiResource($kpis->get(1))
                ]
            ];
        });
   }
}