<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \App\Models\Rebate;

class NotificationHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $limit = $request->input('limit', 10);
        
        $dates = Rebate::where('buyer', $this->user_code)
            ->whereNotNull('notified_at')
            ->select('notified_at')
            ->orderBy('notified_at', 'desc')
            ->groupBy('notified_at')
            ->limit($limit)
            ->get()
            ->pluck('notified_at');

        return [
            'params' => [
                'limit' => $limit,
            ],
            'data' => $dates->map(function ($notified_at) {
                return [
                    'notified_at' => $notified_at,
                    'rebates' => Rebate::where('buyer', $this->user_code)->where('notified_at', $notified_at)->get()->map(function ($rebate) {
                        return new RebateResource($rebate);
                    })
                ];
            })
        ];
    }
}