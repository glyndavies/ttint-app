<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlatformProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                       => $this->id,
            'user_code'                => $this->user_code,
            'unique_id'                => $this->unique_id,
            'first_name'               => $this->first_name,
            'last_name'                => $this->last_name,
            'email_1'                  => $this->email_1,
            'email_2'                  => $this->email_2,
            'date_of_birth'            => optional($this->date_of_birth)->toDateString(),
            'address_line_1'           => $this->address_line_1,
            'address_line_2'           => $this->address_line_2,
            'address_city'             => $this->address_city,
            'address_state'            => $this->address_state,
            'address_zip'              => $this->address_zip,
            'country'                  => $this->country,
            'phone'                    => $this->phone,
            'referral_1'               => $this->referral_1,
            'referral_2'               => $this->referral_2,
            'desired_events'           => $this->desired_events,
            'presale_information'      => $this->presale_information,
            'events_alert_service'     => $this->events_alert_service,
            'express_transfer_service' => $this->express_transfer_service,
            'weekly_portfolio'         => $this->weekly_portfolio,
            'whatsapp_alerts'          => $this->whatsapp_alerts,
            'share_desired_events'     => $this->share_desired_events,
            'permission_to_contact'    => $this->permission_to_contact,
            'tcs_company_policies'     => $this->tcs_company_policies,
            'registered_at'            => optional($this->registered_at)->toDatetimeString(),
        ];
    }
}