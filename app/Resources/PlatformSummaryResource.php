<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Facades\KpiService;

class PlatformSummaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return KpiService::calculateSummary($this->user_code, now(), false, $request->input('calculations'))->toArray();
   }
}