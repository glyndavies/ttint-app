<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RebateCollection extends ResourceCollection
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function toArray($request)
    {
        return [
            'params' => [
                'date_from' => $request->input('date_from'),
                'date_to'   => $request->input('date_to'),
            ],
            'totals' => [
                'trust_rebate'     => round($this->collection->sum('trust_rebate'), 2),
                'referral_rebate'  => round($this->collection->sum('referral_rebate'), 2),
                'total_rebate'     => round($this->collection->sum('total_rebate'), 2),
            ],
            'data' => $this->collection->map(function($rebate) {
                return new RebateResource($rebate);
            })
        ];
    }
}