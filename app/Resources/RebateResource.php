<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Facades\RebateService;

class RebateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function toArray($request = null)
    {
        return array_merge([
            'id'              => $this->id,
            'buyer'           => $this->buyer,
            'sold_date'       => optional($this->sold_date)->toDateString(),
            'event'           => optional($this->inventoryQuantity->inventory)->event,
            'trust_rebate'    => round($this->trust_rebate, 2),
            'referral_rebate' => round($this->referral_rebate, 2),
            'total_rebate'    => round($this->total_rebate, 2),
        ],
        optional($request)->input('calculations') ? [
            'calculations' => RebateService::calculations($this->inventoryQuantity, $this->trust_rebate != 0, $this->referral_rebate != 0)
        ] : []
        );
    }
}