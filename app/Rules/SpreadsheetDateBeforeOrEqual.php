<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use PhpOffice\PhpSpreadsheet\Shared\Date as SpreadsheetDate;
use Carbon\Carbon;

class SpreadsheetDateBeforeOrEqual implements Rule
{
    /**
     * @var \Carbon\Caron
     */
    public $date;

    /**
     * @param \Carbon\Carbon $date
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
    }

	/**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value <= SpreadsheetDate::dateTimeToExcel($this->date);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be before or equal ' . $this->date->format('Y-m-d');
    }
}