<?php

namespace App\Rules;

use InvalidArgumentException;

class ValidateIf
{
    /**
     * @var string
     */
    public $rule;

    /**
     * The condition that validates the attribute.
     *
     * @var callable|bool
     */
    public $condition;

    /**
     * Create a new required validation rule based on a condition.
     *
     * @param string $rule
     * @param  callable|bool  $condition
     * @return void
     */
    public function __construct($rule, $condition)
    {
        $this->rule = $rule;

        if (! is_string($condition)) {
            $this->condition = $condition;
        } else {
            throw new InvalidArgumentException('The provided condition must be a callable or boolean.');
        }
    }

    /**
     * Convert the rule to a validation string.
     *
     * @return string
     */
    public function __toString()
    {
        if (is_callable($this->condition)) {
            return call_user_func($this->condition) ? $this->rule : '';
        }

        return $this->condition ? $rule : '';
    }
}