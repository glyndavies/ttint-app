<?php

namespace App\Services;

use App\Models\Charge;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ChargeService
{
    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'date'            => ['date'],
            'amount_incl_vat' => ['required', 'numeric'],
            'sale_id_ref'     => ['nullable', 'string'],
            'site_sold'       => ['nullable', 'string'],
            'event'           => ['nullable', 'string'],
            'reason'          => ['nullable', 'string'],
            'staff_code'      => ['nullable', 'string'],
            'notes'           => ['nullable', 'string'],
        ]);
    }

    /**
     * @param array $data
     * @return \App\Models\Charge
     */
    public function create($data)
    {
        $validator = $this->validator($data);

		if ($validator->fails()) {
            throw new ValidationException($validator);
        }

    	return Charge::create($validator->validated());
    }

    /**
     * @param \App\Models\Charge $charge
     * @param array $data
     * @return \App\Models\Charge
     */
    public function update($charge, $data)
    {
        $validator = $this->validator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $charge->fill($validator->validated())->save();

        return $charge;
    }

    /**
     * @param \App\Models\Charge $charge
     * @return \App\Models\Charge
     */
    public function delete(Charge $charge)
    {
        $charge->delete();
    }
}