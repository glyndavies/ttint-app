<?php

namespace App\Services;

use App\Models\DeliveryBatch;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class DeliveryBatchService
{
    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorCreate(array $data)
    {
        return Validator::make($data, [
            'ticket_id' => ['required', 'integer'],
            'batch_id'  => ['required', 'integer']
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdate(array $data)
    {
        return Validator::make($data, [
            'batch_id'  => ['required', 'integer']
        ]);
    }

    /**
     * @param array $data
     * @return \App\Models\DeliveryBatch
     */
    public function create($data)
    {
        $validator = $this->validatorCreate($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return DeliveryBatch::create($validator->validated());
    }

    /**
     * @param \App\Models\DeliveryBatch $deliveryBatch
     * @param array $data
     * @return \App\Models\DeliveryBatch
     */
    public function update($deliveryBatch, $data)
    {
        $validator = $this->validatorUpdate($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $deliveryBatch->fill($validator->validated())->save();

        return $deliveryBatch;
    }
}