<?php

namespace App\Services;

use App\Models\InventoryQuantity;
use App\Models\Inventory;
use App\Facades\DeliveryBatchService;
use App\Facades\RebateService;
use App\Models\SoldTicket;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;

class InventoryQuantityService
{
	/**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdate(array $data)
    {
        return Validator::make($data, [
          	'number_of_tickets' => ['required', 'min:1'],
            'seats'             => ['required'],
            'sell_price'        => ['nullable', 'numeric'],
        ]);
    }

	/**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorSplit(array $data)
    {
        return Validator::make($data, [
          	'number_of_tickets'     => ['required', 'min:1'],
            'seats'                 => ['required'],
            'number_of_tickets_new' => ['required', 'min:1'],
            'seats_new'             => ['required']
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorMassInHand(array $data)
    {
        return Validator::make($data, [
            'inventory-quantity-ids' => ['required', 'array', 'min:1'],
            'status'                 => ['required', 'in:1,2']
        ]);
    }

    /**
     * @param Collection $inventoryQuantities
     *
     * @return Collection
     */
    public function checkIfSoldTickets(Collection $inventoryQuantities)
    {
        return $inventoryQuantities->filter(function ($inventoryQuantity) {
            return SoldTicket::where('inventory_quantity_id', $inventoryQuantity->id)->exists();
        })->isNotEmpty();
    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @param array $data
     * @return \App\Models\InventoryQuantity
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(InventoryQuantity $inventoryQuantity, array $data)
    {
    	$validator = $this->validatorUpdate($data);

 		if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $inventoryQuantity->fill($validator->validated())->save();

        // update related purchase
        $total = $inventoryQuantity->inventory->inventoryQuantities()->sum('number_of_tickets');
        $inventoryQuantity->inventory->purchase->fill([
            'number_of_tickets' => $total,
        ])->save();

        // sync rebates
        RebateService::sync($inventoryQuantity);

        return $inventoryQuantity;
    }

    /**
     * @param \App\Models\InventoryQuantity
     * @return App\Models\InventoryQuantity
     */
    public function syncWithSoldTicket(InventoryQuantity $inventoryQuantity)
    {
        $soldTicket = $inventoryQuantity->soldTicket;

        $inventoryQuantity->fill([
            'ticket_status_id'  => 3,
            'sell_price'        => $soldTicket->sell_price,
            'seats'             => $soldTicket->seats,
            'number_of_tickets' => $soldTicket->ticket_qty,
            'sold_date'         => $soldTicket->sold_date,
        ])->save();

        // sync rebates
        RebateService::sync($inventoryQuantity);

        return $inventoryQuantity;
    }

	/**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @param array $data
     * @return \App\Models\InventoryQuantity[]
     * @throws \Illuminate\Validation\ValidationException
     */
    public function split(InventoryQuantity $inventoryQuantity, array $data)
    {
    	$validator = $this->validatorSplit($data);

 		if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // validate current status
        if ( !in_array($inventoryQuantity->ticket_status_id, [Inventory::AVAILABLE, Inventory::SPLIT, Inventory::AVAILABLE_FOR_SWAP]) ) {
            fail_validation('number_of_tickets', 'Only tickets with the status "Available" or "Splitted" can be split');
        }

        // validate total number
        if ($data['number_of_tickets'] + $data['number_of_tickets_new'] !== $inventoryQuantity->number_of_tickets) {
            fail_validation('number_of_tickets', 'The total number of tickets must be ' . $inventoryQuantity->number_of_tickets);
        }

        // Update current batch
        $inventoryQuantity->update([
            'number_of_tickets' => $data['number_of_tickets'],
            'seats'             => $data['seats'],
            'sell_price'        => $data['sell_price'],
        ]);

        // Create New Batch
        $newInventoryQuantity = InventoryQuantity::create([
            'ticket_status_id'  => $inventoryQuantity->ticket_status_id,
            'inventory_id'      => $inventoryQuantity->inventory_id,
            'purchase_number'   => $inventoryQuantity->purchase_number,
            'number_of_tickets' => $data['number_of_tickets_new'],
            'seats'             => $data['seats_new'],
            'sell_price'        => $data['sell_price_new'],
        ]);

        $inventoryQuantity->inventory->update([
            'ticket_status_id' => 2,
        ]);

        // sync rebates
        RebateService::sync($inventoryQuantity);
        RebateService::sync($newInventoryQuantity);

        return $inventoryQuantity->inventory->inventoryQuantities;
    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @return \App\Models\InventoryQuantity $inventoryQuantity
     */
    public function returnBatchToStock(InventoryQuantity $inventoryQuantity)
    {
        $inventoryQuantity->ticket_status_id = 1;
        $inventoryQuantity->save();

        if ($inventoryQuantity->soldTicket) {
        	$inventoryQuantity->soldTicket->delete();
        }

        // sync rebates
        RebateService::sync($inventoryQuantity);

        return $inventoryQuantity;
    }

    public function massInHand($data)
    {
        $validator = $this->validatorMassInHand($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $inventoryQuantities = InventoryQuantity::whereIn('id', $data['inventory-quantity-ids'])->get();
        $status = $data['status'];

        // not allowed clear status for delivered tickets
        if ($inventoryQuantities->contains(function ($inventoryQuantity) {return $inventoryQuantity->isHandlingDelivered();}) && $status == 1) {
            fail_validation('status', 'Clearing InHand is not allowed for tickets with DELIVERED status');
        }

        foreach ($inventoryQuantities as $inventoryQuantity) {
            $inventoryQuantity->forceFill([
                'tickets_in_hand' => $status,
                'delivered_date'  => $status == 1 ? null : now(),
            ])->save();
        }

        return $inventoryQuantities;

    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @param integer $status
     * @return \App\Models\InventoryQuantity
     */
    public function inHand(InventoryQuantity $inventoryQuantity, $status)
    {
        // not allowed clear statis for delivered tickets
        if ($inventoryQuantity->isHandlingDelivered() && $status == 1) {
            fail_validation('status', 'Clearing InHand is not allowed for tickets with DELIVERED status ');
        }

        $inventoryQuantity->forceFill([
            'tickets_in_hand' => $status,
            'delivered_date'  => $status == 1 ? null : now(),
        ])->save();

        return $inventoryQuantity;
    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @param \App\Models\TicketHandlingStatus $ticketHandlingStatus
     * @param integer|null $batch_id
     * @return \App\Models\InventoryQuantity
     */
    public function handling(InventoryQuantity $inventoryQuantity, $ticketHandlingStatus, $batch_id)
    {
        $inventoryQuantity->forceFill([
            'ticket_handling_id' => $ticketHandlingStatus->id,
            // clear if status == 1
            'delivered_date'     => $ticketHandlingStatus->id == 1 ? null : Carbon::now(),
            'tickets_in_hand'    => $ticketHandlingStatus->id != 1 ? 2 : $inventoryQuantity->tickets_in_hand,
        ])->save();

        if ($batch_id) {
            if ($inventoryQuantity->deliveryBatch) {
                DeliveryBatchService::update($inventoryQuantity->deliveryBatch, [
                    'batch_id' => $batch_id
                ]);
            } else {
                DeliveryBatchService::create([
                    'ticket_id' => $inventoryQuantity->id,
                    'batch_id'  => $batch_id
                ]);
            }
        }

        // sync rebates
        RebateService::sync($inventoryQuantity);

        return $inventoryQuantity;
    }
}
