<?php

namespace App\Services;

use App\Models\Inventory;
use App\Facades\RebateService;
use App\Facades\InventoryQuantityService;
use App\Models\InventoryQuantity;
use App\Models\TicketHandlingStatus;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class InventoryService
{
    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdate(array $data)
    {
        return Validator::make($data, [
            'order_number'   => 'required',
            'event_genre'    => 'required',
            'event_date'     => 'required',
            'restrictions'   => 'nullable',
            'event'          => 'required',
            'arena'          => 'required',
            'country'        => 'required',
            'time_of_event'  => 'nullable',
            'type_of_ticket' => 'required',
            'site_purchased' => 'required',
            'origin_status'  => 'nullable',
            'listed'         => 'nullable',
            'referral_1'     => 'nullable',
            'referral_2'     => 'nullable',
            'selling_rate'   => 'nullable',
            'paid_amount'    => 'nullable',
            'notes'          => 'nullable',
        ]);
    }
    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdateMass(array $data)
    {
        return Validator::make($data, [
            'event_genre'              => ['string', 'min:1'],
            'event_date'               => 'date',
            'event'                    => ['string', 'min:1'],
            'arena'                    => ['string', 'min:1'],
            'country'                  => ['string', 'min:1'],
            'time_of_event'            => ['string', 'min:1'],
            'type_of_ticket_old_value' => ['nullable'],
            'type_of_ticket'           => ['string', 'min:1'],
            'site_purchased_old_value' => ['nullable'],
            'site_purchased'           => ['string', 'min:1'],
            'pending_cancelled'        => ['string']
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdateUserCode(array $data)
    {
        return Validator::make($data, [
          	'new_buyer' => ['nullable', 'string'],
            'token'     => ['nullable', 'integer']
        ]);
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @param integer $status
     * @return \App\Models\Inventory
     */
    public function inHand(Inventory $inventory, $status)
    {
        foreach($inventory->inventoryQuantities as $inventoryQuantity) {
            InventoryQuantityService::inHand($inventoryQuantity, $status);
        }

        return $inventory;
    }

    /**
     * @param Collection $inventories
     * @param array $data
     *
     * @throws ValidationException
     */
    public function updateMass($inventories, $data)
    {
        if (!$inventories->count()) {
            throw ValidationException::withMessages(['inventories' => 'no inventories to update']);
        }

        $validator = $this->validatorUpdateMass($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if (isset($data['site_purchased_old_value']) && $data['site_purchased_old_value']) {
            $inventories = $inventories->where('site_purchased', $data['site_purchased_old_value']);
        }

        if (isset($data['type_of_ticket_old_value']) && $data['type_of_ticket_old_value']) {
            $inventories = $inventories->where('type_of_ticket', $data['type_of_ticket_old_value']);
        }

        foreach ($inventories as $inventory) {
            $inventory->fill($validator->validated())->save();

            if (isset($data['pending_cancelled']) && $data['pending_cancelled'] == 'Y') {
                foreach($inventory->inventoryQuantities as $inventoryQuantity) {
                    InventoryQuantityService::handling($inventoryQuantity, TicketHandlingStatus::find(1), null);
                    InventoryQuantityService::inHand($inventoryQuantity, 1);
                    InventoryQuantityService::returnBatchToStock($inventoryQuantity);
                }

            }

            // sync rebates
            RebateService::syncCollection($inventory->inventoryQuantities);
        }

        return $inventories;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @param array $data
     * @return \App\Models\Inventory
     */
    public function update(Inventory $inventory, $data)
    {
        $validator = $this->validatorUpdate($data);

 		if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $inventory->fill($validator->validated())->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @param array $data
     * @return \App\Models\Inventory
     */
    public function updateUserCode(Inventory $inventory, $data)
    {
        $validator = $this->validatorUpdateUserCode($data);

 		if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $inventory->fill($validator->validated())->save();
        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function cancel(Inventory $inventory)
    {
        $inventory->fill([
            'event_cancelled' => 'Y'
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function restoreCancelled(Inventory $inventory)
    {
        $inventory->fill([
            'event_cancelled' => 'N'
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function pendingCancel(Inventory $inventory)
    {
        $inventory->fill([
            'pending_cancelled' => 'Y'
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function restorePendingCancelled(Inventory $inventory)
    {
        $inventory->fill([
            'pending_cancelled' => 'N'
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function listOnResale(Inventory $inventory)
    {
        $inventory->fill([
            'listed_on_resale' => 'Y'
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function unlistOnResale(Inventory $inventory)
    {
        $inventory->fill([
            'listed_on_resale' => 'N'
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function expire(Inventory $inventory)
    {
        $inventory->fill([
            'expired' => 1
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }

    /**
     * @param \App\Models\Inventory $inventory
     * @return \App\Models\Inventory
     */
    public function restoreExpired(Inventory $inventory)
    {
        $inventory->fill([
            'expired' => 0
        ])->save();

        // sync rebates
        RebateService::syncCollection($inventory->inventoryQuantities);

        return $inventory;
    }
}
