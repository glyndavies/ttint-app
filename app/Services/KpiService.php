<?php

namespace App\Services;

use App\Models\Kpi;
use App\Models\Payment;
use App\Models\PlatformUser;
use App\Facades\PlatformUserRepository;
use App\Facades\InventoryQuantityRepository;
use App\Facades\RebateRepository;
use Carbon\Carbon;

class KpiService
{
    /**
     * @param string $user_code
     * @param \Carbon\Carbon $datetime
     * @param boolean $with_groupped
     * @param boolean $with_calculations
     * @return \Illuminate\Support\Collection
     */
    public function calculateSummary($user_code, $datetime, $with_groupped = false, $with_calculations = false)
    {
        $user = PlatformUserRepository::findByUserCode($user_code);

        // tickets without cancelled, refunded
        $allWithDepositItems = InventoryQuantityRepository::getSections([
            'inventory',
            'sold',
            'delivered',
            'completed',
            'transfer',
            'expired',
        ], [
            'user_code'    => $user_code,
            // warning (!): we don't pass any date here
            // we can get only current data here
            'deposit_rate' => 0.15,
        ]);

        $rebates = RebateRepository::find([
            'user_code' => $user_code,
            'sold_date_to' => $datetime,
            'cancelled' => false
        ]);

        $payments = $user ? Payment::where('user_id', $user->id)->whereDate('date', '<=', $datetime) : collect();

        $costWithDeposit     = $allWithDepositItems->sum('cost');
        $repaidWithDeposit   = $allWithDepositItems->sum('repaid');
        $contribution        = $costWithDeposit - $repaidWithDeposit;
        $trustRebate         = $rebates->sum('trust_rebate');
        $referralRebate      = $rebates->sum('referral_rebate');
        $totalRebate         = $rebates->sum('total_rebate');
        $withdrawn           = $payments->sum('amount_incl_vat');
        $netContribution     = $contribution - $withdrawn; 
        $combineRoi          = $netContribution ? ($trustRebate + $referralRebate) / $netContribution : 0;
        $grossPortfolioValue = $contribution + $totalRebate - $withdrawn;
        $roi                 = $contribution ? $totalRebate / $contribution : 0;
        $topTrustRebate      = $rebates->max('trust_rebate');
        $topReferralRebate   = $rebates->max('referral_rebate');
        
        $summary = collect([
            Kpi::CONTRIBUTION          => round($contribution, 2),
            Kpi::COST_WITH_DEPOSIT     => round($costWithDeposit, 2),
            Kpi::REPAID_WITH_DEPOSIT   => round($repaidWithDeposit, 2),
            Kpi::TRUST_REBATE          => round($trustRebate, 2),
            Kpi::REFERRAL_REBATE       => round($referralRebate, 2),
            Kpi::TOTAL_REBATE          => round($totalRebate, 2),
            Kpi::WITHDRAWN             => round($withdrawn, 2),
            Kpi::NET_CONTRIBUTION      => round($netContribution, 2),
            Kpi::GROSS_PORTFOLIO_VALUE => round($grossPortfolioValue, 2),
            Kpi::ROI                   => round($roi, 4),
            Kpi::COMBINE_ROI           => round($combineRoi, 2),
            Kpi::TOP_TRUST_REBATE      => round($topTrustRebate, 2),
            Kpi::TOP_REFERRAL_REBATE   => round($topReferralRebate, 2),
        ]);

        if ($with_groupped) {
            $summary = $summary->merge($this->calculateGrouppedSummary($rebates));
        }

        if ($with_calculations) {
            $summary = $summary->merge(collect([
                'calculations' => [
                    'items_with_deposit' => $allWithDepositItems->map(function ($inventoryQuantity) {
                        return [
                            'inventory_quantity_id'  => $inventoryQuantity->id,
                            'order_number'           => $inventoryQuantity->inventory->order_number,
                            'seats'                  => $inventoryQuantity->seats,
                            'purchase_face_value'    => round(optional($inventoryQuantity->inventory->purchase)->face_value_of_tickets, 2),
                            'purchase_buyers_fees'   => round(optional($inventoryQuantity->inventory->purchase)->buyers_fees, 2),
                            'puchase_num_of_tickets' => optional($inventoryQuantity->inventory->purchase)->number_of_tickets,
                            'cost_per_ticket'        => $inventoryQuantity->cost_per_ticket,
                            'number_of_tickets'      => $inventoryQuantity->number_of_tickets,
                            'cost'                   => $inventoryQuantity->cost,
                            'contribution'           => $inventoryQuantity->contribution,
                            'repaid'                 => $inventoryQuantity->repaid,
                        ];
                    })
                ]
            ]));
        }

        return $summary;
    }

    /**
     * @param \Illuminate\Support\Collection of \App\Models\Rebate $rebates
     */
    private function calculateGrouppedSummary($rebates) {
        $topDayTotalRebate = $rebates->groupBy(function($rebate) {
            return $rebate->sold_date->startOfDay()->timestamp;
        })->map(function($rebates) {
            return $rebates->sum('total_rebate');
        })->max();

        $topWeekTotalRebate = $rebates->groupBy(function($rebate) {
            return $rebate->sold_date->startOfWeek(Carbon::SUNDAY)->timestamp;
        })->map(function($rebates) {
            return $rebates->sum('total_rebate');
        })->max();

        $topMonthTotalRebate = $rebates->groupBy(function($rebate) {
            return $rebate->sold_date->startOfMonth()->timestamp;
        })->map(function($rebates) {
            return $rebates->sum('total_rebate');
        })->max();

        $topQuarterTotalRebate = $rebates->groupBy(function($rebate) {
            return $rebate->sold_date->startOfQuarter()->timestamp;
        })->map(function($rebates) {
            return $rebates->sum('total_rebate');
        })->max();

        return collect([
            Kpi::TOP_DAY_TOTAL_REBATE     => round($topDayTotalRebate, 2),
            Kpi::TOP_WEEK_TOTAL_REBATE    => round($topWeekTotalRebate, 2),
            Kpi::TOP_MONTH_TOTAL_REBATE   => round($topMonthTotalRebate, 2),
            Kpi::TOP_QUARTER_TOTAL_REBATE => round($topQuarterTotalRebate, 2),
        ]);
    }

    /**
     * @param string $user_code
     * @param \Carbon\Carbon $datetime
     * @return \Illuminate\Support\Collection
     */
    public function calculateWeek($user_code, $datetime)
    {
        $curWeekTotalRebate = RebateRepository::find([
            'user_code'      => $user_code,
            'sold_date_from' => $datetime->clone()->startOfWeek(Carbon::SUNDAY)->startOfDay(),
            'sold_date_to'   => $datetime,
            'cancelled'      => false
        ])->sum('total_rebate');

        return collect([
            Kpi::CUR_WEEK_TOTAL_REBATE => round($curWeekTotalRebate, 2)
        ]);
    }

    /**
     * @param string $user_code
     * @param \Carbon\Carbon $datetime
     * @return \Illuminate\Support\Collection
     */
    public function calculateMonth($user_code, $datetime)
    {
        $curMonthTotalRebate = RebateRepository::find([
            'user_code'      => $user_code,
            'sold_date_from' => $datetime->clone()->startOfMonth()->startOfDay(),
            'sold_date_to'   => $datetime,
            'cancelled'      => false
        ])->sum('total_rebate');

        return collect([
            Kpi::CUR_MONTH_TOTAL_REBATE   => round($curMonthTotalRebate, 2)
        ]);
    }

    /**
     * @param string $user_code
     * @param \Carbon\Carbon $datetime
     * @param boolean $with_groupped
     * @return \Illuminate\Support\Collection
     */
    public function rebuildSummary($user_code, $datetime, $with_groupped = false)
    {
        $kpis = $this->calculateSummary($user_code, $datetime->startOfDay(), $with_groupped);
        return $this->updateKpis($user_code, $datetime, $kpis);
    }

    /**
     * @param string $user_code
     * @param \Carbon\Carbon $datetime
     * @return \Illuminate\Support\Collection
     */
    public function rebuildWeek($user_code, $datetime)
    {
        $kpis = $this->calculateWeek($user_code, $datetime->startOfDay());
        return $this->updateKpis($user_code, $datetime, $kpis);
    }

    /**
     * @param string $user_code
     * @param \Carbon\Carbon $datetime
     * @return \Illuminate\Support\Collection
     */
    public function rebuildMonth($user_code, $datetime)
    {
        $kpis = $this->calculateMonth($user_code, $datetime->startOfDay());
        return $this->updateKpis($user_code, $datetime, $kpis);
    }

    /**
     * @param \Carbon\Carbon $datetime
     * @return \Illuminate\Support\Collection
     * @return \Illuminate\Support\Collection
     */
    public function rebuildAndRankAll($datetime)
    {
        $platformUsers = PlatformUser::all();
        foreach($platformUsers as $platformUser) {
            KpiService::rebuildSummary($platformUser->user_code, $datetime, true);
            KpiService::rebuildWeek($platformUser->user_code, $datetime);
            KpiService::rebuildMonth($platformUser->user_code, $datetime);
        }

        foreach(Kpi::allKpiNames() as $kpi_name) {
            KpiService::rank($kpi_name, $datetime);
        }

        return $platformUsers;
    }

    /**
     * @param string $rank_name
     * @param \Carbon\Carbon $date
     * @return void
     */
    private function rank($kpi_name, $date)
    {
        $kpis = Kpi::where('kpi_name', $kpi_name)->whereDate('date', $date)->orderBy('value', 'desc')->get();

        $rank = 0;
        $prev_value = null;
        foreach($kpis as $kpi) {
            if (is_null($prev_value) || $kpi->value < $prev_value) {
                $rank++;
            }
            $kpi->rank = $rank;
            $kpi->save();
            $prev_value = $kpi->value;
        }
    }

    /**
     * @param array $data
     * @return \App\Models\Kpi
     */
    private function updateKpi($data)
    {
        return Kpi::updateOrCreate([
            'buyer'    => $data['buyer'],
            'date'     => $data['date']->startOfDay(),
            'kpi_name' => $data['kpi_name']
        ], $data);
    }

    /**
     * @param string $user_code
     * @param \Carbon\Carbon $date
     * @param \Illuminate\Support\Collection $kpis
     * @return \App\Models\Kpi
     */
    private function updateKpis($user_code, $date, $kpis)
    {
        return $kpis->map(function($value, $kpi_name) use ($user_code, $date) {
            return $this->updateKpi([
                'buyer'    => $user_code,
                'date'     => $date,
                'kpi_name' => $kpi_name,
                'value'    => $value,
            ]);
        });
    }
}