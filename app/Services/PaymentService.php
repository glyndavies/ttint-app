<?php

namespace App\Services;

use App\Models\Payment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PaymentService
{
    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'user_id'         => ['required', 'integer'],
            'notes'           => ['nullable', 'string'],
            'amount_incl_vat' => ['required', 'numeric'],
            'date'            => ['date'],
        ]);
    }

    /**
     * @param array $data
     * @return \App\Models\Payment
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(array $data)
    {
        $validator = $this->validator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return Payment::create($validator->validated());
    }

    /**
     * @param \App\Models\Payment $payment
     * @param array $data
     * @return \App\Models\Payment
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Payment $payment, array $data)
    {
        $validator = $this->validator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $payment->fill($validator->validated())->save();

        return $payment;
    }

    /**
     * @param \App\Models\Payment $payment
     * @return void
     */
    public function delete(Payment $payment)
    {
        $payment->delete();
    }
}