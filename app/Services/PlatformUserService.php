<?php

namespace App\Services;

use App\Models\PlatformUser;
use App\Models\FcmClientToken;
use App\Facades\PlatformUserRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;

class PlatformUserService
{
    /**
     * Get a validator for an incoming create request.
     *
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdate(PlatformUser $platformUser, array $data)
    {
        return Validator::make($data, [
            'unique_id'                => ['nullable', 'integer'],
            'first_name'               => ['string', 'max:255'],
            'last_name'                => ['string', 'max:255'],
            'password'                 => ['nullable', 'string', 'min:8', 'max:255'],
            'email_1'                  => ['nullable', 'email'],
            'email_2'                  => ['nullable', 'email'],
            'date_of_birth'            => ['nullable', 'date'],
            'address_line_1'           => ['nullable', 'string', 'max:255'],
            'address_line_2'           => ['nullable', 'string', 'max:255'],
            'address_city'             => ['nullable', 'string', 'max:255'],
            'address_state'            => ['nullable', 'string', 'max:255'],
            'address_zip'              => ['nullable', 'string', 'max:24'],
            'country'                  => ['nullable', 'string', 'max:255'],
            'phone'                    => ['nullable', 'string', 'max:15'],
            'referral_1'               => ['nullable', 'string', 'max:255'],
            'referral_2'               => ['nullable', 'string', 'max:255'],
            'presale_information'      => ['nullable', 'boolean'],
            'events_alert_service'     => ['nullable', 'boolean'],
            'express_transfer_service' => ['nullable', 'boolean'],
            'express_transfer_service' => ['nullable', 'boolean'],
            'weekly_portfolio'         => ['nullable', 'boolean'],
            'whatsapp_alerts'          => ['nullable', 'boolean'],
            'share_desired_events'     => ['nullable', 'boolean'],
            'permission_to_contact'    => ['nullable', 'boolean'],
            'tcs_company_policies'     => ['nullable', 'boolean'],
            'portfolio_needed'         => ['nullable', 'boolean'],
            'ip_address'               => ['nullable', 'ip'],
            'filled_at'                => ['nullable', 'date'],
            'left_at'                  => ['nullable', 'date'],
            'user_code'                => ['required', 'string'],
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdateProfile(PlatformUser $platformUser, array $data)
    {
        return Validator::make($data, [
            'first_name'               => ['string', 'max:255'],
            'last_name'                => ['string', 'max:255'],
            'email_1'                  => ['nullable', 'email'],
            'email_2'                  => ['nullable', 'email'],
            'date_of_birth'            => ['nullable', 'date'],
            'address_line_1'           => ['nullable', 'string', 'max:255'],
            'address_line_2'           => ['nullable', 'string', 'max:255'],
            'address_city'             => ['nullable', 'string', 'max:255'],
            'address_state'            => ['nullable', 'string', 'max:255'],
            'address_zip'              => ['nullable', 'string', 'max:24'],
            'country'                  => ['nullable', 'string', 'max:255'],
            'phone'                    => ['nullable', 'string', 'max:15'],
            'referral_1'               => ['nullable', 'string', 'max:255'],
            'referral_2'               => ['nullable', 'string', 'max:255'],
            'desired_events'           => ['nullable', 'array'],
            'presale_information'      => ['nullable', 'boolean'],
            'events_alert_service'     => ['nullable', 'boolean'],
            'express_transfer_service' => ['nullable', 'boolean'],
            'express_transfer_service' => ['nullable', 'boolean'],
            'weekly_portfolio'         => ['nullable', 'boolean'],
            'whatsapp_alerts'          => ['nullable', 'boolean'],
            'share_desired_events'     => ['nullable', 'boolean'],
            'permission_to_contact'    => ['nullable', 'boolean'],
            'tcs_company_policies'     => ['nullable', 'boolean'],
        ]);
    }

	/**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorCreateToken(array $data)
    {
        return Validator::make($data, [
            'user_code'   => ['required'],
            'password'    => ['required'],
            'device_name' => ['required'],
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorCreateFcmToken(array $data)
    {
        return Validator::make($data, [
            'token'       => ['required'],
            'device_name' => ['required'],
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorDeleteFcmToken(PlatformUser $platformUser, array $data)
    {
        return Validator::make($data, [
            'token' => [ 'required', Rule::exists('fcm_client_tokens', 'token')->where(function ($query) use ($platformUser) {
                return $query->where('platform_user_id', $platformUser->id);
            }) ]
        ]);
    }

    /**
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return \App\Models\PlatformUser
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(PlatformUser $platformUser, array $data)
    {
        $validator = $this->validatorUpdate($platformUser, $data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $platformUser->fill($validator->validated());

        // update modified fields
        $platformUser->location                 = [$data['location_0'] ?? null, $data['location_1'] ?? null];
        $platformUser->desired_events_raw       = $data['desired_events_raw'] ?? null;
        
        $platformUser->presale_information      = $data['presale_information'] ?? 0;
        $platformUser->events_alert_service     = $data['events_alert_service'] ?? 0;
        $platformUser->express_transfer_service = $data['express_transfer_service'] ?? 0;
        $platformUser->weekly_updates           = $data['weekly_updates'] ?? 0;
        $platformUser->weekly_portfolio         = $data['weekly_portfolio'] ?? 0;
        $platformUser->whatsapp_alerts          = $data['whatsapp_alerts'] ?? 0;
        $platformUser->share_desired_events     = $data['share_desired_events'] ?? 0;
        $platformUser->permission_to_contact    = $data['permission_to_contact'] ?? 0;
        $platformUser->tcs_company_policies     = $data['tcs_company_policies'] ?? 0;
        $platformUser->portfolio_needed         = $data['portfolio_needed'] ?? 0;        
        

        $platformUser->save();
        
        return $platformUser;
    }

    /**
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return \App\Models\PlatformUser
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateProfile(PlatformUser $platformUser, array $data)
    {
        $validator = $this->validatorUpdateProfile($platformUser, $data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $platformUser->fill($validator->validated())->save();

        return $platformUser;
    }

    /**
     * @param array $data
     * @return \Laravel\Sanctum\NewAccessToken
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createToken(array $data)
    {
        $validator = $this->validatorCreateToken($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $platformUser = PlatformUserRepository::findByCredentials($data['user_code'], $data['password']);
        if ( !$platformUser ) {
            throw ValidationException::withMessages([
                'user_code' => ['The provided credentials are incorrect.'],
            ]);
        }
 
        return $platformUser->createToken($data['device_name']);
    }
    
    /**
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createFcmToken(PlatformUser $platformUser, $data)
    {
        $validator = $this->validatorCreateFcmToken($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        FcmClientToken::updateOrCreate([
                'platform_user_id' => $platformUser->id,
                'token' => $data['token']
            ],[
                'platform_user_id' => $platformUser->id,
                'token' => $data['token'],
                'device_name' => $data['device_name']
            ]
        );
    }

    /**
     * @param \App\Models\PlatformUser $platformUser
     * @param array $data
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteFcmToken(PlatformUser $platformUser, $data)
    {
        $validator = $this->validatorDeleteFcmToken($platformUser, $data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $platformUser->fcmClientTokens()->where('token', $data['token'])->delete();
    }

    /**
     * @param \App\Models\PlatformUser $user
     * @return boolean
     */
    public function revokeCurrentToken(PlatformUser $platformUser)
    {
        $platformUser->currentAccessToken()->delete();
    }
    
    /**
     * @param \App\Models\PlatformUser $user
     * @return void
     */
    public function delete(PlatformUser $platformUser)
    {
        $platformUser->delete();
    }
}