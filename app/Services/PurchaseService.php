<?php

namespace App\Services;

use App\Models\Purchase;
use App\Facades\RebateService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PurchaseService
{
	/**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdate(array $data)
    {
        return Validator::make($data, [
            'purchase_date'         => ['required', 'date'],
            'face_value_of_tickets' => ['required'],
            'buyers_fees'           => ['required'],
            //'purchase_number'       => ['required'],
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdateBuyerInfo(array $data)
    {
        return Validator::make($data, [
            'buyer_investment' => ['required'],
            'deposit_rate'     => ['nullable'],
            'buyer_investment' => ['nullable'],
            'buyers_group'     => ['nullable'],
            'buyer'            => ['nullable'],
            'card_used'        => ['nullable']
        ]);
    }

    /**
     * @param \App\Models\Purchase $purchase
     * @param array $data
     * @return \App\Models\Purchase
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Purchase $purchase, array $data)
    {
        $validator = $this->validatorUpdate($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $purchase->fill($validator->validated())->save();

        // sync rebates
        RebateService::syncCollection($purchase->inventoryQuantities);

        return $purchase;
    }

    /**
     * @param \App\Models\Purchase $purchase
     * @param array $data
     * @return \App\Models\Purchase
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateBuyerInfo(Purchase $purchase, array $data)
    {
        $validator = $this->validatorUpdateBuyerInfo($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $purchase->fill($validator->validated())->save();

        // sync rebates
        RebateService::syncCollection($purchase->inventoryQuantities);

        return $purchase;
    }
}