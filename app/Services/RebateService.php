<?php

namespace App\Services;

use App\Models\InventoryQuantity;
use App\Models\PlatformUser;
use App\Models\Rebate;
use App\Notifications\RebatesReceived;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Exception;

class RebateService
{
    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @return Collection of \App\Models\Rebate
     */
    public function sync($inventoryQuantity)
    {
        return $this->syncCollectionForInventoryQuantity($inventoryQuantity);
    }

    /**
     * @param \App\Models\InventoryQuantity[] $inventoryQuantities
     * @return Collection of \App\Models\Rebate
     */
    public function syncCollection($inventoryQuantities)
    {
        $rebates = collect();

        foreach($inventoryQuantities as $inventoryQuantity) {
            $rebates = $rebates->concat($this->syncCollectionForInventoryQuantity($inventoryQuantity));
        }

        return $rebates;
    }

    /**
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @return Collection of \App\Models\Rebate
     */
    private function syncCollectionForInventoryQuantity(InventoryQuantity $inventoryQuantity)
    {
        $inventoryQuantity->refresh();

        $rebates = collect();
        $originRebates = $inventoryQuantity->rebates;

        if ( !optional($inventoryQuantity->soldTicket)->sold_date) {
            return $this->removeOldRebates($rebates, $originRebates);
        }

        $trust_buyer = $inventoryQuantity->inventory->buyer;
        $trust_rebate = $inventoryQuantity->trustRebate;

        $referral_buyer = $inventoryQuantity->inventory->referral_1;
        $referral_rebate = $inventoryQuantity->referralRebate;

        // same buyers
        if ($trust_buyer && $referral_buyer && $trust_buyer == $referral_buyer) {
            if ($trust_rebate || $referral_rebate) {
                $rebates->push(Rebate::updateOrCreate([
                    'buyer'                 => $trust_buyer,
                    'inventory_quantity_id' => $inventoryQuantity->id,
                ],
                [
                    'buyer'                 => $trust_buyer,
                    'inventory_quantity_id' => $inventoryQuantity->id,
                    'sold_date'             => $inventoryQuantity->soldTicket->sold_date,
                    'trust_rebate'          => $trust_rebate,
                    'referral_rebate'       => $referral_rebate,
                ]));
                $this->syncNotificationMultiplier($trust_buyer);
            }

        // different buyers
        } else {
            if ($trust_buyer && $trust_rebate) {
                $rebates->push(Rebate::updateOrCreate([
                    'buyer'                 => $trust_buyer,
                    'inventory_quantity_id' => $inventoryQuantity->id,
                ],
                [
                    'buyer'                 => $trust_buyer,
                    'inventory_quantity_id' => $inventoryQuantity->id,
                    'sold_date'             => $inventoryQuantity->soldTicket->sold_date,
                    'trust_rebate'          => $trust_rebate,
                ]));
                $this->syncNotificationMultiplier($trust_buyer);
            }
            if ($referral_buyer && $referral_rebate) {
                $rebates->push(Rebate::updateOrCreate([
                    'buyer'                 => $referral_buyer,
                    'inventory_quantity_id' => $inventoryQuantity->id,
                ],
                [
                    'buyer'                 => $referral_buyer,
                    'inventory_quantity_id' => $inventoryQuantity->id,
                    'sold_date'             => $inventoryQuantity->soldTicket->sold_date,
                    'referral_rebate'       => $referral_rebate,
                ]));
                $this->syncNotificationMultiplier($referral_buyer);
            }
        }

        return $this->removeOldRebates($rebates, $originRebates);
    }

    /**
     * It's used for checking of calculations
     * @param \App\Models\InventoryQuantity $inventoryQuantity
     * @return Collection
     */
    function calculations($inventoryQuantity, $include_trust_rebate, $include_referral_rebate)
    {
        $calculations = collect();

        $trust_buyer = $inventoryQuantity->inventory->buyer;
        $trust_rebate = $inventoryQuantity->trustRebate;

        $referral_buyer = $inventoryQuantity->inventory->referral_1;
        $referral_rebate = $inventoryQuantity->referralRebate;


        // same buyers
        if ($trust_buyer && $referral_buyer && $trust_buyer == $referral_buyer) {
            if ($trust_rebate || $referral_rebate) {
                $calculations->push([
                    'inventory_quantity_id'   => $inventoryQuantity->id,
                    'order_number'            => $inventoryQuantity->inventory->order_number,
                    'sell_price'              => round(optional($inventoryQuantity->soldTicket)->sell_price, 2),
                    'refund'                  => optional($inventoryQuantity->inventory->purchase)->companyRefund,
                    'deposit_rate'            => round(optional($inventoryQuantity->inventory->purchase)->deposit_rate, 2),
                    'user_deposit'            => round(optional($inventoryQuantity->inventory->purchase)->buyer_investment, 0),
                    'real_deposite_rate'      => $inventoryQuantity->inventory->real_deposit_rate,
                    'cost'                    => $inventoryQuantity->cost,
                    'contribution'            => $inventoryQuantity->contribution,
                    'repaid'                  => $inventoryQuantity->repaid,
                    'profit'                  => $inventoryQuantity->profit,
                    'raw_trading_profit_075'  => $inventoryQuantity->rawTradingProfit,
                    'calculated_deposit_rate' => round(optional($inventoryQuantity->inventory->purchase)->deposit_rate ? 0.25 : 0.1, 2),
                    'trust_rebate'            => $trust_rebate,
                    'referral_rebate'         => $referral_rebate,
                ]);
            }

        // different buyers
        } else {
            if ($trust_buyer && $trust_rebate) {
                $calculations->push([
                    'inventory_quantity_id'   => $inventoryQuantity->id,
                    'order_number'            => $inventoryQuantity->inventory->order_number,
                    'sell_price'              => round(optional($inventoryQuantity->soldTicket)->sell_price, 2),
                    'refund'                  => optional($inventoryQuantity->inventory->purchase)->companyRefund,
                    'deposit_rate'            => round(optional($inventoryQuantity->inventory->purchase)->deposit_rate, 2),
                    'user_deposit'            => round(optional($inventoryQuantity->inventory->purchase)->buyer_investment, 2),
                    'real_deposite_rate'      => $inventoryQuantity->inventory->real_deposit_rate,
                    'cost'                    => $inventoryQuantity->cost,
                    'contribution'            => $inventoryQuantity->contribution,
                    'repaid'                  => $inventoryQuantity->repaid,
                    'profit'                  => $inventoryQuantity->profit,
                    'raw_trading_profit_075'  => $inventoryQuantity->rawTradingProfit,
                    'calculated_deposit_rate' => round(optional($inventoryQuantity->inventory->purchase)->deposit_rate ? 0.25 : 0.1, 2),
                    'trust_rebate'            => $trust_rebate,
                ]);
            }
            if ($referral_buyer && $referral_rebate) {
                $calculations->push([
                    'inventory_quantity_id'      => $inventoryQuantity->id,
                    'order_number'               => $inventoryQuantity->inventory->order_number,
                    'sell_price'                 => round(optional($inventoryQuantity->soldTicket)->sell_price, 2),
                    'refund'                     => optional($inventoryQuantity->inventory->purchase)->companyRefund,
                    'deposit_rate'               => round(optional($inventoryQuantity->inventory->purchase)->deposit_rate, 2),
                    'user_deposit'               => round(optional($inventoryQuantity->inventory->purchase)->buyer_investment, 2),
                    'real_deposite_rate'         => $inventoryQuantity->inventory->real_deposit_rate,
                    'cost'                       => $inventoryQuantity->cost,
                    'contribution'               => $inventoryQuantity->contribution,
                    'repaid'                     => $inventoryQuantity->repaid,
                    'profit'                     => $inventoryQuantity->profit,
                    'raw_trading_profit_075'     => $inventoryQuantity->rawTradingProfit,
                    'calculated_deposit_rate'    => round(optional($inventoryQuantity->inventory->purchase)->deposit_rate ? 0.25 : 0.1, 2),
                    'calculated_referral_1_rate' => $inventoryQuantity->inventory->referral_1 ? 0.15 : 0,
                    'referral_rebate'            => $referral_rebate,
                ]);
            }
        }

        return $calculations
            ->filter(function ($calculation) use ($include_trust_rebate) {
                return !($calculation['trust_rebate'] ?? 0) || $include_trust_rebate;
            })
            ->filter(function($calculation) use ($include_referral_rebate) {
                return !($calculation['referral_rebate'] ?? 0) || $include_referral_rebate;
            })
            ->first();
    }
    
    /**
     * @param Collection of \App\Models\Rebate $rebates
     * @param Collection of \App\Models\Rebate $originRebates
     * @return Collection of \App\Models\Rebate
     */
    private function removeOldRebates($rebates, $originRebates)
    {
        $rebatesKeyed = $rebates->keyBy('id');
        $originRebatesKeyed = $originRebates->keyBy('id');
        $toRemove = $originRebatesKeyed->diffKeys($rebatesKeyed);
        $toRemove->each->delete();

        return $rebates;
    }
    
    /**
     * @param \App\Models\Rebate $rebate
     * @param Carbon\Cerbon|null $cancelled_at
     */
    public function cancel(Rebate $rebate, $cancelled_at = null)
    {
        $rebate->cancelled_at = $cancelled_at ? $cancelled_at : Carbon:: now();
        $rebate->save();
        return $rebate;
    }

    /**
     * @param \App\Models\PlatformUser $platformUser
     * @return boolean
     */
    public function notify($platformUser)
    {
        $allRebates = $platformUser->rebatesWithoutNotifications()->orderBy('created_at')->get();
        $rebates = collect();

        // take rebates for a total amount of at least 'low_rebate_notify_limit'
        foreach($allRebates as $rebate) {
            if ($rebates->sum('total_rebate') < config('tt.low_rebate_notify_limit')) {
                $rebates->push($rebate);
            } else {
                break;
            }
        }

        if ($rebates->sum('total_rebate') >= config('tt.low_rebate_notify_limit')) {
            // try to notify
            try {
                Notification::send(
                    [ $platformUser ],
                    new RebatesReceived($rebates->pluck('id'))
                );    
            } catch (Exception $e) {
                Log::error('Notification not sent. user_code ' . $platformUser->user_code . ': '. $e->getMessage());
            }

            $notified_at = Carbon::now();
            $rebates->each(function ($rebate) use ($notified_at) {
                $rebate->fill([
                    'notified_at' => $notified_at,
                ])->save();
            });
        
            return true;
        }

        return false;
    }

    /**
     * @param \App\Models\PlatformUser $platformUser
     * @param float $baseProbability
     * @return boolean
     */
    public function notifyWithRandomizer(PlatformUser $platformUser, $baseProbability = 1)
    {
        $sent = false;
        call_with_probability(function () use ($platformUser, &$sent) {
            if ($this->notify($platformUser)) {
                $sent = true;
            }
        }, $baseProbability * $platformUser->notification_multiplier);

        return $sent;
    }

    /**
     * @param string $user_code
     */
    public function syncNotificationMultiplier($user_code) {
        if ($platformUser = PlatformUser::where('user_code', $user_code)->first()) {
            $platformUser->notification_multiplier = min(config('tt.max_rebate_notify_per_day'), max(1, $platformUser->getChunkedRebatesWithoutNotifications()->count()));
            $platformUser->save();
        }
    }
}