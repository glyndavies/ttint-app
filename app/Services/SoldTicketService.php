<?php

namespace App\Services;

use App\Models\SoldTicket;
use App\Models\InventoryQuantity;
use App\Models\CompanyRefund;
use App\Facades\InventoryQuantityService;
use App\Facades\RebateService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;

class SoldTicketService
{
    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorCreateCollection(array $data)
    {
        return Validator::make($data, [
            'id'        => ['required'],
            'price'     => ['required'],
            'sold_date' => ['required'],
            'sold_site' => ['required'],
            'saleidref' => ['required'],
            'notes'     => ['nullable'],
        ]);
    }

    /**
     * Get a validator for an incoming create request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorUpdate(array $data)
    {
        return Validator::make($data, [
            'sale_id_ref' => ['required'],
            'site_sold'   => ['required'],
            'sold_date'   => ['required'],
            'ticket_qty'  => ['required'],
            'seats'       => ['required'],
            'sell_price'  => ['required'],
        ]);
    }

    /**
     * @param array $data
     * @return Collection of \App\Models\SoldTicket
     */
    public function createCollection($data)
    {
        $validator = $this->validatorCreateCollection($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // inventory quantities for update
        $inventoryQuantities = collect($data['id'])->map(function($id) {
            return InventoryQuantity::find($id);
        })
        ->filter(function ($inventoryQuantity) {
            return !$inventoryQuantity->ifStatusSold();
        })
        ->each(function ($inventoryQuantity) use ($data) {
            $inventoryQuantity->notes = $data['notes'] ?? null;
            $inventoryQuantity->save();
        });

        if (InventoryQuantityService::checkIfSoldTickets($inventoryQuantities)) {
            $validator->errors()->add('inventory_quantity_id', 'One or more inventory quantities are already associated with a sold ticket.');
            throw new ValidationException($validator);
        }

        $soldTickets = $inventoryQuantities->map(function ($inventoryQuantity) use ($data) {
            return tap(SoldTicket::create([
                'inventory_quantity_id' => $inventoryQuantity->id,
                'sold_date'             => $data['sold_date'],
                'site_sold'             => $data['sold_site'],
                'sale_id_ref'           => $data['saleidref'],
                'sell_price'            => $data['price'][$inventoryQuantity->id],
                'ticket_qty'            => $inventoryQuantity->number_of_tickets,
                'purchase_number'       => $inventoryQuantity->purchase_number,
                'seats'                 => $inventoryQuantity->seats
            ]), function ($soldTicket) use ($inventoryQuantity) {
                InventoryQuantityService::syncWithSoldTicket($inventoryQuantity);
            });
        });

        // sync rebates
        RebateService::syncCollection($inventoryQuantities);

        return $soldTickets;

    }

    /**
     * @param \App\Models\SoldTicket $soldTicket
     * @param array $data
     * @return \App\Models\SoldTicket
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(SoldTicket $soldTicket, array $data)
    {
        $validator = $this->validatorUpdate($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $soldTicket->fill($validator->validated())->save();
        InventoryQuantityService::syncWithSoldTicket($soldTicket->inventoryQuantity);

        // sync rebates
        RebateService::sync($soldTicket->inventoryQuantity);

        return $soldTicket;
    }

    /**
     * @param \App\Models\SoldTicket $soldTicket
     * @return \App\Models\SoldTicket
     */
    public function paid(SoldTicket $soldTicket)
    {
        $soldTicket->fill([
            'is_paid' => 'Y',
            'paid_date' => Carbon::now(),
        ])->save();

        return $soldTicket;
    }

    /**
     * @param \App\Models\SoldTicket $soldTicket
     * @param array $data
     * @return \App\Models\SoldTicket
     * @throws \Illuminate\Validation\ValidationException
     */
    public function refund($soldTicket)
    {
        $soldTicket->fill([
            'is_paid' => 'N',
            'paid_date' => null,
        ])->save();

        // sync rebates
        RebateService::sync($soldTicket->inventoryQuantity);

        return $soldTicket;
    }
}
