<?php

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use PhpOffice\PhpSpreadsheet\Shared\Date as SpreadsheetDate;
use Carbon\Carbon;

/**
 * @param $value
 */
function moneyformat($value)
{
	return '£' . number_format(($value), 2);
}

/**
 * @param $value
 */
function dateformat($value)
{
	return $value ? $value->format('d/m/Y') : '-';
}

/**
 * @param $value
 */
function innerDateformat($value)
{
	return $value ? $value->format('Y-m-d') : '-';
}

/**
 * @param $value
 * @return Carbon\Carbon
 */
function spreadsheetDate($value)
{
	try {
		return Carbon::instance(SpreadsheetDate::excelToDateTimeObject($value));
	} catch (\Exception $e) {
		return null;
	}

}

/**
 * Get current period from session
 * @return Caron\Carbon[]
 */
function getSessionPeriod()
{
	if (Session::has('period-date1') && Session::has('period-date2')) {
		return [
			'date1' => Carbon::parse(Session::get('period-date1')),
			'date2' => Carbon::parse(Session::get('period-date2')),
		];

	} else {
		// default period
		return [
			'date1' => Carbon::today(),
			'date2' => Carbon::today()->addMonths(6),
		];
	}
}

/**
 * Update current period in session from Request
 * @param Caron\Carbon[] $period
 * @return void
 */
function updateSessionPeriod()
{
	if (Request::input('period-date1') && Request::input('period-date2')) {
		Session::put('period-date1', Request::input('period-date1'));
		Session::put('period-date2', Request::input('period-date2'));
	}
}

/**
 * @param string $key
 * @param string $message
 * @throws \Illuminate\Validation\ValidationException
 */
function fail_validation(string $key, string $message)
{
    Validator::make([$key => 'some-value'], [
        $key => [function ($attribute, $value, $fail) use ($message) {
            $fail($message);
        }],
    ])->validate();
}

/**
 * @param callable $callback
 * @param float $probability [0 - 1]
 * @return boolean
 */
function call_with_probability($callback, $probability)
{
	if ($probability && $probability >= lcg_value()) {
		$callback();
		return true;
	}

	return false;
}

/**
 * @param string $_eventDate
 * @param string $eventTime
 *
 * @return string
 */
function formatDateDifference($_eventDate, $eventTime)
{
    $eventDate = Carbon::parse($_eventDate);
    list($hour, $minute, $second) = explode(':', $eventTime);
    $eventDate->setTime($hour, $minute, $second);

    $now = Carbon::now();

    $diffInDays = $now->diffInDays($eventDate);
    $diffInWeeks = $now->diffInWeeks($eventDate);
    $diffInMonths = $now->diffInMonths($eventDate);
    $diffInYears = $now->diffInYears($eventDate);

    if ($diffInDays < 1) {
        return '<24h';
    } elseif ($diffInDays < 7) {
        return $diffInDays . ' Day' . ($diffInDays > 1 ? 's' : '');
    } elseif ($diffInDays < 31) {
        return $diffInWeeks . ' Week' . ($diffInWeeks > 1 ? 's' : '');
    } elseif ($diffInMonths < 12) {
        return $diffInMonths . ' Month' . ($diffInMonths > 1 ? 's' : '');
    } else {
        return $diffInYears . ' Year' . ($diffInYears > 1 ? 's' : '');
    }
}
