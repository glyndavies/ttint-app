<?php

return [
    /**
     * Low limit for sending notifications of new rebates
     */
	'low_rebate_notify_limit' => 10,

    /**
     * 
     */
    'max_rebate_notify_per_day' => 10,

];