<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraDetailsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {


            $table->string('firstname')->nullable();
            $table->string('surname')->nullable();
            $table->date('dob')->nullable();
            $table->dateTime('lastLogin')->nullable();
            $table->string('houseNumber')->nullable();
            $table->string('Address1')->nullable();
            $table->string('Address2')->nullable();
            $table->string('town')->nullable();
            $table->string('city')->nullable();
            $table->string('postcode', 10)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['firstname', 'surname', 'dob','lastLogin', 'houseNumber', 'Address1','Address2', 'town', 'city','postcode']);
        });
    }
}
