<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempTableLogsTable extends Migration
{
    /**
     * Run the migrations.
     *   'User_id'=>Auth::id(),
    'data'=>json_encode($rowData),
    'issue'=>'1',
    'issue_reason'=>$reason
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('temp_table_logs')) {
            Schema::create('temp_table_logs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('User_id');
                $table->string('issue');
                $table->text('data');
                $table->text('issue_reason');
                $table->timestamps();
            });            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_table_logs');
    }
}
