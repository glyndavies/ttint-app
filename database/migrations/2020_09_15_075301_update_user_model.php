<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::update('update model_has_permissions set model_type = ? where model_type = ?', ['App\Models\User', 'App\User']);
        DB::update('update model_has_roles set model_type = ? where model_type = ?', ['App\Models\User', 'App\User']);
        DB::update('update audits set user_type = ? where user_type = ?', ['App\Models\User', 'App\User']);
        DB::update('update audits set auditable_type = ? where auditable_type = ?', ['App\Models\User', 'App\User']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::update('update model_has_permissions set model_type = ? where model_type = ?', ['App\User', 'App\Models\User']);
        DB::update('update model_has_roles set model_type = ? where model_type = ?', ['App\User', 'App\Models\User']);
        DB::update('update audits set user_type = ? where user_type = ?', ['App\User', 'App\Models\User']);
        DB::update('update audits set auditable_type = ? where auditable_type = ?', ['App\User', 'App\Models\User']);
    }
}
