<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlatformUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platform_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('unique_id')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email_1')->nullable();
            $table->string('email_2')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('address_line_1')->nullable();
            $table->string('address_line_2')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->string('address_zip')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('referral_1')->nullable();
            $table->string('referral_2')->nullable();
            $table->string('desired_events')->nullable();
            $table->string('browser')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('location')->nullable();
            $table->string('user_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_users');
    }
}
