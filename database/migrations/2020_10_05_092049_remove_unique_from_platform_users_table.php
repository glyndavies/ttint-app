<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveUniqueFromPlatformUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $indexes = collect(DB::select("show indexes from platform_users"))->pluck('Column_name');

        Schema::table('platform_users', function (Blueprint $table) use($indexes) {
            //
            if ($indexes->contains('unique_id')) {
                $table->dropUnique(['unique_id']);
            }
            $table->unsignedBigInteger('unique_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('platform_users', function (Blueprint $table) {
        });
    }
}
