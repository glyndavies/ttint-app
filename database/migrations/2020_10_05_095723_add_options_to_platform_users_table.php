<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOptionsToPlatformUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('platform_users', function (Blueprint $table) {
            //
            $table->boolean('presale_information')->default(false)->after('desired_events');
            $table->boolean('events_alert_service')->default(false)->after('presale_information');
            $table->boolean('express_transfer_service')->default(false)->after('events_alert_service');
            $table->boolean('weekly_updates')->default(false)->after('express_transfer_service');
            $table->boolean('weekly_portfolio')->default(false)->after('weekly_updates');
            $table->boolean('whatsapp_alerts')->default(false)->after('weekly_portfolio');
            $table->boolean('share_desired_events')->default(false)->after('whatsapp_alerts');
            $table->boolean('permission_to_contact')->default(false)->after('share_desired_events');
            $table->boolean('tcs_company_policies')->default(false)->after('permission_to_contact');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('platform_users', function (Blueprint $table) {
            $table->dropColumn('presale_information');
            $table->dropColumn('events_alert_service');
            $table->dropColumn('express_transfer_service');
            $table->dropColumn('weekly_updates');
            $table->dropColumn('weekly_portfolio');
            $table->dropColumn('whatsapp_alerts');
            $table->dropColumn('share_desired_events');
            $table->dropColumn('permission_to_contact');
            $table->dropColumn('tcs_company_policies');
            //
        });
    }
}
