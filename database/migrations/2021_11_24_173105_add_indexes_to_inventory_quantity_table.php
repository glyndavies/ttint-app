<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToInventoryQuantityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_quantity', function (Blueprint $table) {
            //
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade');
            $table->foreign('ticket_status_id')->references('id')->on('ticket_status')->onDelete('set null');
            $table->foreign('ticket_handling_id')->references('id')->on('ticket_handling_status')->onDelete('set null');

            $table->index('purchase_number');
            $table->index('tickets_in_hand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_quantity', function (Blueprint $table) {
            //
            $table->dropForeign(['inventory_id']);
            $table->dropForeign(['ticket_status_id']);
            $table->dropForeign(['ticket_handling_id']);

            $table->dropIndex(['purchase_number']);
            $table->dropIndex(['tickets_in_hand']);
        });
    }
}
