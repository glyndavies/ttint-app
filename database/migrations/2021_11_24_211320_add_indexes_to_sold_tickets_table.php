<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToSoldTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sold_tickets', function (Blueprint $table) {
            //
            $table->foreign('inventory_quantity_id')->references('id')->on('inventory_quantity')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sold_tickets', function (Blueprint $table) {
            //
            $table->dropForeign(['inventory_quantity_id']);
        });
    }
}
