<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToCustomerRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_refunds', function (Blueprint $table) {
            //
            $table->foreign('sold_id')->references('id')->on('sold_tickets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_refunds', function (Blueprint $table) {
            //
            $table->dropForeign(['sold_id']);
        });
    }
}
