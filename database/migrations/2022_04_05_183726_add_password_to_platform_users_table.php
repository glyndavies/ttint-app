<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPasswordToPlatformUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('platform_users', function (Blueprint $table) {
            //
            $table->string('password')->nullable()->after('user_code');
            $table->boolean('blocked')->nullable()->after('password');
            $table->timestamp('registered_at')->nullable()->after('created_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('platform_users', function (Blueprint $table) {
            //
            $table->dropColumn('password');
            $table->dropColumn('blocked');
            $table->dropColumn('registered_at');
        });
    }
}
