<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\InventoryQuantity;
use App\Facades\RebateService;

class CreateRebatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rebates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('buyer');
            $table->integer('inventory_quantity_id');
            $table->decimal('trust_rebate')->default(0);
            $table->decimal('referral_rebate')->default(0);
            $table->date('sold_date');
            $table->date('cancelled_at')->nullable();
            $table->timestamp('notified_at')->nullable();
            $table->timestamps();

            $table->index('buyer');
            $table->index('sold_date');
            $table->foreign(['inventory_quantity_id'])->references('id')->on('inventory_quantity')->onDelete('cascade');
        });

        InventoryQuantity::all()->each(function($inventoryQuantity) {
            RebateService::syncCollection([$inventoryQuantity]);
            // notified_at
            $inventoryQuantity->rebates->each(function($rebate) {
                $rebate->fill([ 'notified_at' => $rebate->sold_date ])->save();
            });
            // cancelled_at
            if ($inventoryQuantity->inventory->event_cancelled == 'Y') {
                $inventoryQuantity->rebates->each(function($rebate) {
                    RebateService::cancel($rebate, $rebate->inventoryQuantity->inventory->updated_at);
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rebates');
    }
}
