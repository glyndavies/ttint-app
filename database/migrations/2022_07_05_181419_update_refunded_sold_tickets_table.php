<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRefundedSoldTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('sold_tickets')->where('cancelled', 'Y')->update([
            'is_paid' => 'N',
            'paid_date' => null,
        ]);

        DB::table('inventory_quantity')
        ->leftJoin('sold_tickets', 'sold_tickets.inventory_quantity_id', '=', 'inventory_quantity.id')
        ->where('sold_tickets.cancelled', 'Y')->update([
            'ticket_status_id' => 3,
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
