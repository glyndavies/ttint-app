<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\PlatformUser;

class AddNotificationMultiplierToPlatformUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('platform_users', function (Blueprint $table) {
            //
            $table->integer('notification_multiplier')->default(1)->after('left_at');
        });

        PlatformUser::all()->each(function($platformUser) {
            $platformUser->notification_multiplier = $platformUser->rebatesWithoutNotifications()->count();
            $platformUser->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('platform_users', function (Blueprint $table) {
            //
            $table->dropColumn('notification_multiplier');
        });
    }
}
