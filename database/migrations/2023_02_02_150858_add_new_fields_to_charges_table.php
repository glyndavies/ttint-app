<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('charges', function (Blueprint $table) {
            //
            $table->string('sale_id_ref')->nullable()->after('date');
            $table->string('site_sold')->nullable()->after('sale_id_ref');
            $table->string('event')->nullable()->after('site_sold');
            $table->string('reason')->nullable()->after('event');
            $table->string('staff_code')->nullable()->after('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('charges', function (Blueprint $table) {
            //
            $table->dropColumn('sale_id_ref');
            $table->dropColumn('site_sold');
            $table->dropColumn('event');
            $table->dropColumn('reason');
            $table->dropColumn('staff_code');
        });
    }
}
