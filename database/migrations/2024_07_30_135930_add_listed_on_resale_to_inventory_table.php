<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddListedOnResaleToInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory', function (Blueprint $table) {
            $table->enum('listed_on_resale', ['N', 'Y'])->default('N')->after('event_cancelled');
            $table->enum('pending_cancelled', ['N', 'Y'])->default('N')->after('listed_on_resale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory', function (Blueprint $table) {
            $table->dropColumn('listed_on_resale');
            $table->dropColumn('pending_cancelled');
        });
    }
}
