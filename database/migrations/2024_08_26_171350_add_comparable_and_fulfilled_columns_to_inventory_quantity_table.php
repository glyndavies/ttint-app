<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddComparableAndFulfilledColumnsToInventoryQuantityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_quantity', function (Blueprint $table) {
            $table->enum('comparable', ['N', 'Y'])->default('N')->after('ticket_handling_id');
            $table->enum('fulfilled', ['N', 'Y'])->default('N')->after('comparable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_quantity', function (Blueprint $table) {
            $table->dropColumn('comparable');
            $table->dropColumn('fulfilled');
        });
    }
}
