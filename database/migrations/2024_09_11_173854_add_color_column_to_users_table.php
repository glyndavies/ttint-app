<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddColorColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('color')->nullable();
        });

        $colors = ['808cff', 'db8cff', 'ff8cf2', 'ff8c8c', 'ffe08c', 'c9ff8c', 'd400ff', '8cffc9', '8cffff', '8ccdff',
            'ffdfc7', 'ffc7c7', 'ffffc7', 'daffc7', 'c7ffe6', 'c7f5ff', 'c7d5ff', 'f2c7ff', 'f0f1ff', 'f0fff5'];

        $users = DB::table('users')->get();

        foreach ($users as $index => $user) {
            if (isset($colors[$index])) {
                DB::table('users')
                    ->where('id', $user->id)
                    ->update(['color' => $colors[$index]]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
