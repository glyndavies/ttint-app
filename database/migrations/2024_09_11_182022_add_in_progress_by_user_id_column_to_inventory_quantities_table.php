<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInProgressByUserIdColumnToInventoryQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_quantity', function (Blueprint $table) {
            $table->unsignedBigInteger('in_progress_by_user_id')->nullable();
            $table->foreign('in_progress_by_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_quantity', function (Blueprint $table) {
            $table->dropForeign(['in_progress_by_user_id']);
            $table->dropColumn('in_progress_by_user_id');
        });
    }
}
