# Using Platform API

All API requests must be made using the Bearer Token. Laravel Sanctum is used to manage tokens

## Making request to API

Every request to API must contain a header:
```
Accept: application/json
Authorization: Bearer <access-token>
```

## Login

Description:

Validation user credentials and creating a new personal access token for using with all next requests.
This token have a very long expiration time (years), but may be manually revoked by the user at anytime

Endpoint:

`/api/platform/v1/login`

Method: `POST`

Params:

- `user_code` - string, user code in uppercase.
- `password` - string, user password
- `device_name`  - string. Any value. Will be linked  with the issued personal access token

Response:

`access token` - This token must be used as Bearer Token in the header of every next requests to API


## Logout

Description:

Revoking personal access token

Endpoint:

`/api/platform/v1/logout`

Method: `POST`

Response:

[Boolean]


## Get Profile

Endpoint:

`/api/platform/v1/profile`

Method: `GET`

Response:

[PlatformProfileResource](#markdown-header-platformprofileresource)


## Update Profile

Endpoint:

`/api/platform/v1/profile`

Method: `POST`

Params (all params are optional):

`first_name` - string. User first name.
`last_name` - string. User last name.
`email_1` - string. Email.
`email_2` - string. Email.
`date_of_birth` - date in `yyyy-mm-dd` format
`address_line_1` - string.
`address_line_2` - string.
`address_city` - string.
`address_state` - string.
`address_zip` - string.
`country` - string.
`phone` - string.
`referral_1` - string.
`referral_2` - string.
`desired_events` - string[].
`presale_information` - number 0 or 1.
`events_alert_service` - number 0 or 1.
`express_transfer_service` - number 0 or 1.
`express_transfer_service` - number 0 or 1.
`weekly_portfolio` - number 0 or 1.
`whatsapp_alerts` - number 0 or 1.
`share_desired_events` - number 0 or 1.
`permission_to_contact` - number 0 or 1.
`tcs_company_policies` - number 0 or 1.

Response:

[PlatformProfileResource](#markdown-header-platformprofileresource)


## Create FCM client token

Description:

Creating a new Firebase Cloud Messaging (FCM) client token

Endpoint:

`/api/platform/v1/profile/fcm-client-token`

Method: `POST`

Params:

`token` - string. FCM client token
`device_name` - string. Any value. Will be linked  with the issued FCM client token

## Remove FCM client token

Description:

Revoking Firebase Cloud Messaging (FCM) client token

Endpoint:

`/api/platform/v1/profile/fcm-client-token`

Method: `DELETE`

Params:

`token` - string. FCM client token


## Get Summary

Endpoint:

`/api/platform/v1/summary`

Method: `GET`

Response:

[PlatformSummaryResource](#markdown-header-platformsummaryresource)


## Get Rebates

Endpoint:

`/api/platform/v1/rebates`

Method: `GET`

Params:
`date_from` => date in 'yyyy-mm-dd' format, start of requested period
`date_to`   => date in 'yyyy-mm-dd' format, end of requested period

Response:

[RebateCollection](#markdown-header-rebatecollection)


## Get KPI state

Endpoint:

`/api/platform/v1/kpi/state`

Method: `GET`

Params: no

Response:

[KpiStateResource](#markdown-header-kpistateresource)


## Get Notifications history

Endpoint:

`/api/platform/v1/notifications/history`

Method: `GET`

Params:
`limit` => integer, number of last sent notifications. Default - 10

Response:

[NotificationHistoryResource](#markdown-header-notificationhistoryresource)


## PlatformProfileResource

- `id` - integer, permanent internal Platform user ID in App.
- `user_code` - string, permanent user code
- `unique_id` - integer, unique id
- `first_name` - string, first name
- `last_name` - string, last name
- `email_1` - string, email
- `email_2` - string, email
- `date_of_birth` - date in 'yyyy-mm-dd' format, date of birth
- `address_line_1` - string, address line 1
- `address_line_2` - string, address line 2
- `address_city` - string, city
- `address_state` - string, state
- `address_zip` - string, post code
- `country` - string, country
- `phone` - string, phone
- `referral_1` - string, referral user code
- `referral_2` - string, referral user code
- `desired_events` - array of string, list of Plarform user desired event genres. Matches to `event_genres.genre` fields
- `presale_information` - integerer (0 / 1)
- `events_alert_service` - integerer (0 / 1)
- `express_transfer_service` - integerer (0 / 1)
- `weekly_portfolio` - integerer (0 / 1)
- `whatsapp_alerts` - integerer (0 / 1)
- `share_desired_events` - integerer (0 / 1)
- `permission_to_contact` - integerer (0 / 1)
- `tcs_company_policies` - integerer (0 / 1)
- `registered_at` - date in 'yyyy-mm-dd' format, date of user registration in App

Implementation in App:

[\App\resources\PlatformProfileResource](../app/Resources/PlatformProfileResource.php)


## PlatformSummaryResource

- `contribution` - float, total contribution
- `cost_with_deposit` - float, total cost with deposit 0.15
- `repaid_with_deposit` - float, total repaid with deposit 0.15
- `trust_rebate` - float, total trust rebate
- `referral_rebate` - float, total referral rebate
- `total_rebate` - float, total trust & referral rebate
- `withdrawn` - float, total withdrawn
- `gross_portfolio_value` - float, gross portfolio value
- `roi` - float, ROI
- `top_trust_rebate` - float, top trust rebate
- `top_referral_rebate` - float, top referral rebate

Implementation in App:

[\App\resources\PlatformSummaryResource](../app/Resources/PlatformSummaryResource.php)


## RebateCollection

- `params`
  - `date_from` - date in 'yyyy-mm-dd' format, requested start of rebates period
  - `date_to` - date in 'yyyy-mm-dd' format, requested end of rebates period
- `totals`
  - `trust_rebate` - float, total trust rebate for requested period
  - `referral_rebate` - float, total referral rebate for requested period
  - `total_rebate` - float, total trust & referral rebate for requested period
- `data` - array of [RebateResource](#markdown-header-rebateresource)

Implementation in App:

[\App\resources\RebateCollection](../app/Resources/RebateCollection.php)


## RebateResource

- `id` - integer, internal ID of rebate
- `buyer` - string, user code
- `sold_date` - date in 'yyyy-mm-dd' format, date of sold
- `event` - string, name of event
- `trust_rebate` - float, trust rebate
- `referral_rebate` - float referral rebate
- `total_rebate` - float, total trust & referral rebate

Implementation in App:

[\App\resources\RebateResource](../app/Resources/RebateResource.php)


## KpiStateResource

Object with KPI names as keys and KPIResource objects as values

All available KPI names: [All KPI names](#markdown-header-all-kpi-names)

Implementation in App:

[\App\resources\KpiStateResource](../app/Resources/KpiStateResource.php)


## KPIResource

- `id` - integer, internal ID of calculated KPI
- `user_code` - string, user code
- `date` - date in 'yyyy-mm-dd' format, KPI date
- `kpi_name` - string, [KPI name](#markdown-header-all-kpi-names)
- `value` - float, calculated KPI value
- `rank` - integer, calculated KPI rank

Implementation in App:

[\App\resources\KpiResource](../app/resources/KpiResource.php)


## All KPI names

See constants in [\App\Models\Kpi](../app/Models/Kpi.php)


## NotificationHistoryResource

- `params`
  - `limit` - integer, requested number of last notifications
- `data`
  - `notified_at` - datetime, ISO 8601 format
  - `rebates` - array of [RebateResource](#markdown-header-rebateresource)

Implementation in App:

[\App\resources\NotificationHistoryResource](../app/Resources/NotificationHistoryResource.php)


## Notifications

When new rebates are created, Laravel can send notifications to users about this. Laravel sends each rebate in a separate notification. Notifications are not sent instantly, but using some "randomizer".
`Laravel Notifications` are used to send notifications here.
One of the channels for sending notifications is `FCM` (Firebase Cloud Messaging)
