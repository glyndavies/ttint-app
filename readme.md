## Deploying

Recommended `php` version: 8.1

Recommended `npm` version: 12

Build assests for production enviroment:

```
npm run prod
```

Importing DB

```
mysql -uroot -p ttanalytics < ./tt_db.sql
```


## Using Platform API

[Platform API documentation](./documentation/platform-api.md)

## Deploying Scheduler:

/usr/local/bin/php /var/www/vhosts/ttanalytics.net/httpdocs/artisan schedule:run
