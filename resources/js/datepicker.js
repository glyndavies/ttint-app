import datepickerFactory from 'jquery-datepicker'

datepickerFactory($)

/* datepicker inputs */
$('.datepicker').datepicker({
     dateFormat: 'yy-mm-dd',
     autoHide: true,
});
