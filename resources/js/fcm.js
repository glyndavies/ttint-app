// https://firebase.google.com/docs/cloud-messaging/js/client?authuser=0&hl=en
// Import the functions you need from the SDKs you need
import Axios from "axios";
import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyA2ikx9RCWutxhkHRW7NaZkpsjoMRfwSMM",
  authDomain: "ttanalytics-fc237.firebaseapp.com",
  databaseURL: "https://ttanalytics-fc237-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "ttanalytics-fc237",
  storageBucket: "ttanalytics-fc237.appspot.com",
  messagingSenderId: "220960447839",
  appId: "1:220960447839:web:85edbf9694fe843973aaea",
  measurementId: "G-H9XZYD6SET"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Cloud Messaging and get a reference to the service
const messaging = getMessaging(app);

function requestPermission() {
  console.log('Requesting permission...');
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
    }
    getToken(messaging, { vapidKey: 'BKoT8ShZ9UmJ0CtlIrmLn_6iddEQbUj6Df4yxyvC0gHZoPT4dWBDOA-l-fss7qWi5lL3D7QhpO4wkZ0BDACt4fo' }).then((currentToken) => {
      console.log('created new FCM token', currentToken)
    })
  })
}

requestPermission()