<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inventory/Tickets  Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during ticket management for various
    | areas. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'purchase_order' => 'Purchase order',
    'order_number' => 'Order number',

];
