@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Adjustments</h1>

        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#cancelled">Cancelled Events<span class="badge badge-pill badge-danger ml-2">{{ $cancelled_purchases->count() }}</span> </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#expired">Expired Tickets<span class="badge badge-pill badge-danger ml-2">{{ $expired_inventories->count() }}</span> </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <!-- Cancelled -->
                <div class="tab-pane active py-4" id="cancelled">
                    @include('adjustment.partials.cancelled', [ 'cancelled_purchases' => $cancelled_purchases ])
                </div>

                <!-- Expired -->
                <div class="tab-pane py-4 fade" id="expired">
                    @include('adjustment.partials.expired', [ 'expired_inventories' => $expired_inventories ])
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    @include('modals.refund-modal')

@endsection

@section('page_level_js')
@endsection
