<div class="table-responsive">
    <table class="table table-bordered" id="dataTableTickets" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall">All</th>
            <th>Transaction Number</th>
            <th>Order Number</th>
            <th>Event Genre</th>
            <th>Event</th>
            <th>Arena</th>
            <th>Country</th>
            <th>Site Purchased</th>
            <th>Type of Ticket</th>
            <th>Purchased Date</th>
            <th>Event Date</th>
            <th>Time of Event</th>
            <th>Day of event</th>
            <th>Number of tickets</th>
            <th class="d-none">Notes</th>
            <th class="lg-width">Restrictions</th>
            <th>Facevalue Per Ticket</th>
            <th>Facevalue of tickets</th>
            <th>Seats</th>
            <th>Buyers Fees</th>
            <th>Total Cost per Ticket</th>
            <th>Total Cost</th>
            <th>Broken Limit</th>
            <th>Insurance</th>
            <th>Inputer</th>
            <th>Pending Reconciled</th>
            <th>Reconciled</th>
            <th>Refund</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cancelled_purchases as $cancelled_purchase)
            <tr id="row{{ $cancelled_purchase->inventory->id }}" class="@if($cancelled_purchase->inventory->event_cancelled == 'Y') bg-warning @endif small">
                <td><input class="selectCheck" value="{{ $cancelled_purchase->id }}" type="checkbox"></td>
                <td>{{ $cancelled_purchase->purchase_number }}</td>
                <td>{{ $cancelled_purchase->order_number }}</td>
                <td>{{ $cancelled_purchase->inventory->event_genre }}</td>
                <td>{{ $cancelled_purchase->inventory->event }}</td>
                <td>{{ $cancelled_purchase->inventory->arena }}</td>
                <td>{{ $cancelled_purchase->inventory->country }}</td>
                <td>{{ $cancelled_purchase->inventory->site_purchased }}</td>
                <td>{{ $cancelled_purchase->inventory->type_of_ticket }}</td>
                <td>{{ dateformat($cancelled_purchase->purchase_date) }}</td>
                <td>{{ dateformat($cancelled_purchase->inventory->event_date) }}</td>
                <td>{{ $cancelled_purchase->inventory->time_of_event }}</td>
                <td>{{ $cancelled_purchase->inventory->event_date ? substr($cancelled_purchase->inventory->event_date->format('l'), 0, 3) : '' }}</td>
                <td class="text-center">
                    @include('partials.badges.purchase-count', [ 'purchase' => $cancelled_purchase ])
                    @include('partials.badges.inventory-notes', [ 'inventory' => $cancelled_purchase->inventory ])
                </td>
                <td class="d-none">{{ $cancelled_purchase->inventory->notes }}</td>
                <td>{{ $cancelled_purchase->inventory->restrictions }} </td>
                <td>@money($cancelled_purchase->face_value_per_ticket)</td>
                <td>@money($cancelled_purchase->face_value_of_tickets)</td>
                <td>
                    @foreach ($cancelled_purchase->inventoryQuantities as $tick)
                        <span>{{ $tick->seats }}</span><br>
                    @endforeach
                </td>
                <td>@money($cancelled_purchase->buyers_fees)</td>
                <td>@money($cancelled_purchase->cost_per_ticket)</td>
                <td>@money($cancelled_purchase->cost)</td>
                <td>{{ $cancelled_purchase->broken_limit }}</td>
                <td>{{ $cancelled_purchase->insurance }}</td>
                <td>{{ $cancelled_purchase->Imputer }}</td>
                <td>{{ $cancelled_purchase->pending_reconciled }}</td>
                <td>{{ $cancelled_purchase->reconciled }}</td>
                <td><!-- Trigger the modal with a button -->
                    @if ($cancelled_purchase->companyRefund)
                        @money($cancelled_purchase->companyRefund->refunded_amount)
                    @else
                        <button type="button" class="company-refund btn btn-info btn-sm text-white" data-toggle="modal" data-target="#myModal" data-id="{{ $cancelled_purchase->inventory->id }}">Refund</button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr style="text-align:right; display: none;">
                <th colspan="21" style="text-align:right; display: none;">Total:</th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>
