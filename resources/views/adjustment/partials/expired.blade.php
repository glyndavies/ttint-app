<div class="table-responsive">
    <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>Transaction Number</th>
            <th>Order Number</th>
            <th>Event </th>
            <th>Arena</th>
            <th>Site purchased</th>
            <th>Type</th>
            <th>Event date</th>
            <th>Event Time</th>
            <th>Event Day</th>
            <th>In Hand</th>
            <th>Expired Qty</th>
            <th class="d-none">Notes</th>
            <th>Restrictions</th>
            <th>Facevalue Per Ticket</th>
            <th>Facevalue of tickets</th>
            <th>Seats</th>
            <th class="sm-width">New user</th>
            <th class="sm-width">Token</th>
            <th>TCP Ticket</th>
            <th>Total Cost</th>
            <th>Actions</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($expired_inventories as $expired_inventory)

            <tr class="@if($expired_inventory->broken_limit) bg-danger text-white @endif small">
                <td>{{ $expired_inventory->purchase_number }}</td>
                <td>{{ $expired_inventory->order_number }}</td>
                <td>{{ $expired_inventory->event }}</td>
                <td>{{ $expired_inventory->arena }}</td>
                <td>{{ $expired_inventory->site_purchased }}</td>
                <td>
                    {{ $expired_inventory->type_of_ticket }}
                    @foreach ($expired_inventory->inventoryQuantities as $tick)
                        @if ($tick->tickets_in_hand == 2)
                            <i class="fas fa-thumbs-up text-primary"></i>
                        @endif
                    @endforeach
                </td>
                <td>{{ dateformat($expired_inventory->event_date) }}</td>
                <td>{{ $expired_inventory->time_of_event }}</td>
                <td>{{ $expired_inventory->event_date ? substr($expired_inventory->event_date->format('l'), 0, 3) : '' }}</td>
                <td>
                    @if ($expired_inventory->inventoryQuantities->count() == 1 )
                        <span> @if($tick->tickets_in_hand == 2) yes @else no @endif</span>
                    @endif
                </td>
                <td>
                    @include('partials.badges.inventory-notsold-count', [ 'inventory' => $expired_inventory ])
                    @include('partials.badges.inventory-notes', [ 'inventory' => $expired_inventory ])
                </td>
                <td class="d-none">{{ $expired_inventory->notes }}</td>
                <td>{{ $expired_inventory->restrictions }}</td>
                <td>@money(optional($expired_inventory->purchase)->face_value_per_ticket)</td>
                <td>@money(optional($expired_inventory->purchase)->face_value_of_tickets)</td>
                <td>@foreach ($expired_inventory->inventoryQuantities as $tick)
                        @if ($tick->ifStatusAvailable())
                            <span>{{ $tick->seats }}</span><br>
                        @endif
                    @endforeach
                </td>
                <td>{{ $expired_inventory->new_buyer }}</td>
                <td>{{ $expired_inventory->token }}</td>
                <td>@money(optional($expired_inventory->purchase)->cost_per_ticket)</td>
                <td>@money(optional($expired_inventory->purchase)->cost)</td>
                <td style="display: flex; justify-content: center">
                    <button type="button" data-id="{{ $expired_inventory->id }}" title="restore" class="btn btn-success btn-xs restoreTicket"><i class="fas fa-clock"></i></button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
