@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Edit Permission</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{Route('permissions.index')}}">Permission</a></li>
            <li class="breadcrumb-item active">Permission: {{$permission->name}}</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
     {!! Form::model($permission, ['method' => 'PUT', 'route' => ['permissions.update', $permission->id]]) !!}
            <div class="row">
                <div class="col-12 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <a href="{{route('permissions.index')}}" class="btn btn-primary"> cancel</a>
                {!! Form::submit('Save Changes', ['class' => 'btn btn-success float-right']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

