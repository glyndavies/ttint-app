@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <h1 class="mt-4">Permissions</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{Route('permissions.index')}}">Permission</a></li>
            <li class="breadcrumb-item active">Permissions overview</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Permissions Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>



                        @if (count($permissions) > 0)
                            @foreach ($permissions as $permission)

                                <tr>
                                    <td>{{$permission->name}}</td>
                                    <td>
                                        @hasrole('Admin')

                                        @can('manage users')
                                            <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-primary">edit</a>
                                         @if($permission->id != 1)
                                            <form class="" action="{{ route('permissions.destroy', $permission->id) }}" method="POST" style="all: unset">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">delete</button>
                                            </form>
                                             @endif
                                        @endcan
                                        @else
                                            Please ask for correct permissions...
                                        @endhasrole
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6"> No permissions currently set up </td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Add New Permission
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form method="POST" action="{{Route('permissions.store')}}">
                                @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New Permission</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Permission Name', 'required' => '']) !!}
                                    </div>
                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>




            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
