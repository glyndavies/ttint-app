@extends('layouts.app')

@section('content')

        <div class="container-fluid">
            <h1 class="mt-4">Roles</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                <li class="breadcrumb-item active">Roles Edit</li>
            </ol>
            <div class="card mb-4">
                <div class="card-body">



         {!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update', $role->id]]) !!}



        <div class="row">
            <div class="col-12 form-group">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-12 form-group">
                {!! Form::label('permission', 'Permissions', ['class' => 'control-label']) !!}
                {!! Form::select('permission[]', $permissions, old('permission') ? old('permission') : $role->permissions()->pluck('name', 'name'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                <p class="help-block"></p>
                @if($errors->has('permission'))
                    <p class="help-block">
                        {{ $errors->first('permission') }}
                    </p>
                @endif

                <p>
                    current permissions:
                    @foreach($role->permissions as $premission)
                        <span class="badge badge-pill badge-primary">  {{$premission->name}}</span>


                    @endforeach
                </p>

            </div>
        </div>


                    <a href="{{route('users.index')}}" class="btn btn-primary"> back</a>

{!! Form::submit('Update Role', ['class' => 'btn btn-success float-right']) !!}
{!! Form::close() !!}
        </div>
            </div>
            </div>
@stop

