@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Completed</h1>

        <div>
            <table class="table table-bordered table-searchable small" id="completed-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="not-export noVis"><input type="checkbox" id="checkall"> All </th>
                        <th class="d-none noVis">Batch id</th>
                        <th>Transaction number</th>
                        <th>Order Number</th>
                        <th>Event</th>
                        <th>Arena</th>
                        <th>Site purchased</th>
                        <th>Type of Ticket</th>
                        <th>Purchase Date</th>
                        <th class="datepicker">Event Date</th>
                        <th>Time of Event</th>
                        <th>Day of Event</th>
                        <th class="not-export">Available</th>
                        <th class="d-none noVis">Available</th>
                        <th class="not-export">Number of tickets</th>
                        <th class="d-none noVis">Number of tickets</th>
                        <th class="d-none noVis">Notes</th>
                        <th class="lg-width">Restrictions</th>
                        <th>FV</th>
                        <th>Seats</th>
                        <th class="">Listed</th>
                        <th class="">Referral 1</th>
                        <th class="">Referral 2</th>
                        <th>User Code</th>
                        <th>User name</th>
                        <th>User Deposit</th>
                        <th class="d-none noVis">Sold</th>
                        <th>TCP Ticket</th>
                        <th>Total Cost</th>
                        <th class="d-none noVis">Tickets in Hand</th>
                        <th>Date Sold</th>
                        <th>Site Sold</th>
                        <th>Sale ID</th>
                        <th>Refund</th>
                        <th>TSP</th>
                        <th>Gross Profit</th>
                        <th>Delivered</th>
                        <th class="d-none noVis">Delivered Status</th>
                        <th>Delivered Date</th>
                        <th>Paid</th>
                        <th>Paid Date</th>
                        <th>Paid Amount</th>
                        <th class="not-export noVis">Cancel/Change</th>
                    </tr>
                </thead>                
            </table>
        </div>
    </div>

    <!-- Modal -->
    @include('modals.refund-modal')
    @include('modals.completed-modal')

@endsection

@section('page_level_js')
@endsection
