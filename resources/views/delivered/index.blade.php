@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2">Delivery</h1>

        <div class="row">
            <div class="col">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="{{ route('upload.reconcile.store') }}" method="post" enctype="multipart/form-data" style="all: unset">
                            @csrf
                            <div>
                                <input type="file" class="" id="fileupload" name="fileupload" value="upload file" required>
                                <button type="submit" class="btn btn-info text-white">Start upload</button>
                                <span><a class="mx-2" href="{{ asset('example_file_delivered.xlsx') }}">Example file</a></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <table class="table table-bordered table-searchable small" id="delivered-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="not-export noVis"><input type="checkbox" id="checkall"> All </th>
                        <th>Batch id</th>
                        <th>Transaction number</th>
                        <th>Order Number</th>
                        <th>Event</th>
                        <th>Arena</th>
                        <th>Site Purchased</th>
                        <th>Type of Ticket</th>
                        <th class="d-none noVis">Purchase Date</th>
                        <th class="datepicker">Event Date</th>
                        <th>Time of Event</th>
                        <th>Day of Event</th>
                        <th class="not-export">Available</th>
                        <th class="d-none noVis">Available</th>
                        <th class="not-export">Number of tickets</th>
                        <th class="d-none noVis">Number of tickets</th>
                        <th class="d-none noVis">Notes</th>
                        <th class="d-none noVis">Restrictions</th>
                        <th>FV</th>
                        <th>Seats</th>
                        <th class="">Listed</th>
                        <th class="">Referral 1</th>
                        <th class="">Referral 2</th>
                        <th>User Code</th>
                        <th>User name</th>
                        <th>User Deposit</th>
                        <th>Sold</th>
                        <th>TCP Ticket</th>
                        <th>Total Cost</th>
                        <th class="d-none noVis">Tickets in Hand </th>
                        <th class="d-none noVis">Date Sold</th>
                        <th>Site Sold</th>
                        <th>Sale ID</th>
                        <th class="d-none noVis">Refund</th>
                        <th>TSP</th>
                        <th>Gross Profit</th>
                        <th>Delivered</th>
                        <th class="d-none noVis">Delivered Status</th>
                        <th>Delivered Date</th>
                        <th>Paid</th>
                        <th>Paid Date</th>
                        <th class="d-none noVis">Paid Amount</th>
                        <th class="not-export noVis">Actions</th>
                    </tr>
                </thead>                
            </table>
        </div>
    </div>
    
    <!-- Modals -->
    @include('modals.delivered-modal')
    
@endsection

@section('page_level_js')
@endsection
