@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-12">
                <h1 class="mt-4">Ticket TN:{{$inventory_quantity->purchase_number}}  -  {{$inventory_quantity->inventory->event}} -  Event Date:  {{$inventory_quantity->inventory->event_date->isoformat('dddd')}} {{$inventory_quantity->inventory->event_date->format('d/m/Y')}}
                    <a  class="float-right" href="{{ route('delivered.index') }}" > <button class="float-right btn btn-danger" > Back</button> </a>
                    <span class="float-right" ></span>
                </h1>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <div class="row">

                    <!-- left column -->
                    <div class="col-md-8 col-sm-12">
                        <div class="row">

                            <!-- Ticket inventory details -->
                            <div class="col-sm-12 mb-3">
                                @include('partials.cards.forms.inventory_details', ['inventory' => $inventory_quantity->inventory])
                            </div>

                            <!-- Buyer information -->
                            <div class="col-md-4 col-sm-12 mb-3">
                                @include('partials.cards.forms.buyer_details', ['inventory' => $inventory_quantity->inventory])
                            </div>

                            <!-- Ticket value-->
                            <div class="col-md-4 col-sm-12 mb-4">
                                @include('partials.cards.forms.purchase', ['purchase' => $inventory_quantity->inventory->purchase])
                            </div>

                        </div>
                    </div>

                    <!-- right column -->
                    <div class="col-md-4 col-sm-12 mb-3">
                        @include('partials.cards.forms.sold_details', ['soldTicket' => $inventory_quantity->soldTicket])
                        <div class="mt-3">
                            @include('partials.inventory-quantity-notes', ['inventory_quantity' => $inventory_quantity])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
