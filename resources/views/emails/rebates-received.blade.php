@component('mail::message')
# Hi, {{ $platformUser->first_name }}!

@component('mail::table')
| You received {{ moneyformat($rebates->sum('total_rebate')) }} |       |
|:----------------|----------------------------------------------------:|
@foreach ($rebates as $rebate)
@if ($loop->index)
|                 |                                                     |
@endif
| Sold date       | {{ $rebate->sold_date->toDateString() }}            |
| Event           | {{ $rebate->inventoryQuantity->inventory->event }}  |
| Trust rebate    | {{ moneyformat($rebate->trust_rebate) }}            |
| Referral rebate | {{ moneyformat($rebate->referral_rebate) }}         |
| <b>Total rebate</b> | <b>{{ moneyformat($rebate->total_rebate) }}</b> |
@endforeach
@endcomponent

@endcomponent
