@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-8">
            <h1 class="mt-3">Business Expenses</h1>
        </div>
        <div class="col-4">
            <a href="{{ route('finances.business-expenses.add') }}" class="btn btn-primary mt-2 float-right">New Business Expense</a>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="">
                <table class="table table-bordered" id="dataTableFinance" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th id="column_date">Date</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Subcategory</th>
                            <th>Card</th>
                            <th>Amount</th>
                            <th>VAT</th>
                            <th>Total Amount</th>
                            <th>Actions</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th id="select_date"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($businessExpenses as $businessExpense)
                        <tr>
                            <td>{{ $businessExpense->id }}</td>
                            <td>{{ $businessExpense->date->format('Y-m-d') }}</td>
                            <td>{{ $businessExpense->description }}</td>
                            <td>{{ $businessExpense->category->name }}</td>
                            <td>{{ !empty($businessExpense->subcategory)? $businessExpense->subcategory->name : ''}}</td>
                            <td>{{ $businessExpense->card->name }}</td>
                            <td class="text-right">@money($businessExpense->amount)</td>
                            <td class="text-right">@money($businessExpense->vat)</td>
                            <td class="text-right">@money($businessExpense->amount_incl_vat)</td>
                            <td class="text-right">
                                <a href="{{ route('finances.business-expenses.show',$businessExpense->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                                <form method="POST" action="{{ route('finances.business-expenses.delete', ['id' => $businessExpense->id]) }}" class="d-inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <button type="button" class="btn btn-xs btn-danger submit-with-danger"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection