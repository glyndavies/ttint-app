@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">@if(!$businessExpense->id) New @endif Business Expense</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('finances.business-expenses') }}">Business Expenses</a></li>
                <li class="breadcrumb-item active">Business Expense</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.business_expense_details', [
        'businessExpense' => $businessExpense,
        'categories' => $categories,
        'subcategories' => $subcategories,
        'cards' => $cards
    ])

</div>
@endsection