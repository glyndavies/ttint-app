@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-8">
                <h1 class="mt-3">Charges</h1>
            </div>
            <div class="col-4">
                <a href="{{ route('finances.charges.add') }}" class="btn btn-primary mt-2 float-right">New Charge</a>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTableFinance" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th id="column_date">Date</th>
                                <th>Total Amount</th>
                                    <th>Sale ID</th>
                                    <th>Site Sold</th>
                                    <th>Event</th>
                                    <th>Reason</th>
                                    <th>Staff Code</th>
                                <th>Notes</th>
                                <th>Actions</th>
                            </tr>
                            <tr>
                                <th id="select_date"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($charges as $charge)
                            <tr>
                                <td>{{ $charge->date->format('Y-m-d') }}</td>
                                <td class="text-right">@money($charge->amount_incl_vat)</td>
                                <th>{{ $charge->sale_id_ref }}</th>
                                <th>{{ $charge->site_sold }}</th>
                                <th>{{ $charge->event }}</th>
                                <th>{{ $charge->reason }}</th>
                                <th>{{ $charge->staff_code }}</th>
                                <td>{{ $charge->notes }}</td>
                                <td class="text-right">
                                    <a href="{{ route('finances.charges.show', $charge->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                                    <form method="POST" action = "{{ route('finances.charges.delete', ['id' => $charge->id]) }}" class="d-inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button type="button" class="btn btn-xs btn-danger submit-with-danger"><i class="far fa-trash-alt"></i></button>
                                    </form>  
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

