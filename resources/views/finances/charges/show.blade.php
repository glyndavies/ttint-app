@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">@if(!$charge->id) New @endif Charge</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('finances.charges') }}">Charges</a></li>
                <li class="breadcrumb-item active">Charge</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.charge_details', [
        'charge' => $charge,
    ])

</div>
@endsection