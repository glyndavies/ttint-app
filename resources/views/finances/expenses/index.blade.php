@extends('layouts.app')

@section('content')

@php
$class_category_col = isset($hidden_columns) && is_array($hidden_columns) && in_array("category_name", $hidden_columns)? "d-none": "";
$title = ' Expenses';
$href_url_new = route('finances.expenses.add');
$route_data_show = ['route' => 'finances.expenses.show', 'params' => []];
$route_data_delete = ['route' => 'finances.expenses.delete', 'params' => []];
$redirect_to = route('finances.expenses');
if(isset($category) && is_object($category)){
$title = $category->name . $title;
$category_name = strtolower($category->name);
$href_url_new = route('finances.category-expenses.add', $category_name);
$route_data_show['route'] = 'finances.category-expenses.show';
$route_data_show['params']['name_category'] = $category_name;
$route_data_delete['route'] = 'finances.category-expenses.delete';
$route_data_delete['params']['name_category'] = $category_name;
$redirect_to = route('finances.category-expenses', ['name_category' => strtolower($category_name)]);
}
@endphp
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-8">
            <h1 class="mt-3"> {{ $title }}</h1>
        </div>
        <div class="col-4">
            <a href="{{ $href_url_new }}" class="btn btn-primary mt-2 float-right">New Expense</a>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="">
                <table class="table table-bordered" id="dataTableFinance" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th id="column_date">Date</th>
                            <th>Description</th>
                            <th class="{{ $class_category_col }}">Category</th>
                            <th>Subcategory</th>
                            <th>Card</th>
                            <th>Amount</th>
                            <th>VAT</th>
                            <th>Total Amount</th>
                            <th>Actions</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th id="select_date"></th>
                            <th></th>
                            <th class="{{ $class_category_col }}"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($expenses as $expense)
                        <tr>
                            <td>{{ $expense->id }}</td>
                            <td>{{ $expense->date->format('Y-m-d') }}</td>
                            <td>{{ $expense->description }}</td>
                            <td class="{{ $class_category_col }}">{{ $expense->category->name }}</td>
                            <td>{{ !empty($expense->subcategory)? $expense->subcategory->name : ''}}</td>
                            <td>{{ $expense->card->name }}</td>
                            <td class="text-right">@money($expense->amount)</td>
                            <td class="text-right">@money($expense->vat)</td>
                            <td class="text-right">@money($expense->amount_incl_vat)</td>
                            <td class="text-right">
                                @php
                                $route_data_show['params']['id'] = $expense->id;
                                @endphp
                                <a href="{{ route($route_data_show['route'], $route_data_show['params']) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                                <form method="POST" action="{{ route($route_data_delete['route'], array_merge(['id' => $expense->id], $route_data_delete['params'])) }}" class="d-inline-block" data-redirect="{{$redirect_to}}">
                                    @csrf
                                    <input type="hidden" name="redirect_to" value="{{ $redirect_to }}">
                                    @method('DELETE')
                                    <button type="button" class="btn btn-xs btn-danger submit-with-danger"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection