@extends('layouts.app')

@section('content')
@php
    $is_new = (!$expense->id)? 'New': '';
    $title = 'Expense';
    $href_url = route('finances.expenses');
    if($categories && count($categories) == 1){
        $category = $categories[0];
        $title = $category->name . ' ' .$title;
        $href_url = route('finances.category-expenses', strtolower($category->name));
    }
@endphp
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">{{ $is_new }} {{ $title }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ $href_url }}">{{ $title }}</a></li>
                <li class="breadcrumb-item active">Expense</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.expense_details', [
        'expense' => $expense,
        'categories' => $categories,
        'subcategories' => $subcategories,
        'cards' => $cards
    ])

</div>
@endsection