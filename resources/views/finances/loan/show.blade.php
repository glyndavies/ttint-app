@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">@if(!$entry->id) New @endif Entry</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('finances.loan') }}">Loan</a></li>
                <li class="breadcrumb-item active">Entry</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.loan_entry_details', [
        'entry' => $entry,
    ])

</div>
@endsection