@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-8">
                <h1 class="mt-3">Payments</h1>
            </div>
            <div class="col-4">
                <a href="{{ route('finances.payments.add') }}" class="btn btn-primary mt-2 float-right">New Payment</a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="{{ route('upload.payments.store') }}" method="post" enctype="multipart/form-data" style="all: unset">
                            @csrf
                            <div>
                                <input type="file" class="" id="fileupload" name="fileupload" value="upload file" required>
                                <button type="submit" class="btn btn-info text-white">Start upload</button>
                                <span><a class="mx-2" href="{{ asset('example_file_payments.xls') }}">Example file</a></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <table class="table table-bordered" id="dataTableFinance" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th id="column_date">Date</th>
                        <th>User Code</th>
                        <th>User Name</th>
                        <th>Total Amount</th>
                        <th>Notes</th>
                        <th>Actions</th>
                    </tr>
                    <tr>
                        <th id="select_date"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{ $payment->date->format('Y-m-d') }}</td>
                        <td>{{ $payment->user->user_code }}</td>
                        <td>{{ $payment->user->full_name }}</td>
                        <td class="text-right">@money($payment->amount_incl_vat)</td>
                        <td>{{ $payment->notes }}</td>
                        <td class="text-right">
                            <a href="{{ route('finances.payments.show',$payment->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                            <form method="POST" action = "{{ route('finances.payments.delete', ['id' => $payment->id]) }}" class="d-inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="button" class="btn btn-xs btn-danger submit-with-danger"><i class="far fa-trash-alt"></i></button>
                            </form>  
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

