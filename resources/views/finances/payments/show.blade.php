@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">@if(!$payment->id) New @endif Payment</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('finances.payments') }}">Payments</a></li>
                <li class="breadcrumb-item active">Payment</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.payment_details', [
        'payment' => $payment,
        'platformUsers' => $platformUsers,
    ])

</div>
@endsection