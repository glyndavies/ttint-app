@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-8">
                <h1 class="mt-3">Taxes</h1>
            </div>
            <div class="col-4">
                <a href="{{ route('finances.taxes.add') }}" class="btn btn-primary mt-2 float-right">New Tax</a>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTableFinance" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th id="column_date">Date</th>
                                <th>Total Amount</th>
                                <th>Notes</th>
                                <th>Actions</th>
                            </tr>
                            <tr>
                                <th id="select_date"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($taxes as $tax)
                            <tr>
                                <td>{{ $tax->date->format('Y-m-d') }}</td>
                                <td class="text-right">@money($tax->amount_incl_vat)</td>
                                <td>{{ $tax->notes }}</td>
                                <td class="text-right">
                                    <a href="{{ route('finances.taxes.show',$tax->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                                    <form method="POST" action = "{{ route('finances.taxes.delete', ['id' => $tax->id]) }}" class="d-inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button type="button" class="btn btn-xs btn-danger submit-with-danger"><i class="far fa-trash-alt"></i></button>
                                    </form>  
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

