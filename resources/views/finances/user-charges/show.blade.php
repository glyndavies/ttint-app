@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">@if(!$userCharge->id) New @endif User charge</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('finances.user-charges') }}">User charges</a></li>
                <li class="breadcrumb-item active">User charge</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.user_charge_details', [
        'userCharge' => $userCharge,
        'platformUsers' => $platformUsers,
    ])

</div>
@endsection