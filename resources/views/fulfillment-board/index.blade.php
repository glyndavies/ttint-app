@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }

        table td.td-no-padding {
            padding: 0!important;
        }

        table th.search-and-checkbox-filters input[type="text"] {
            width: 80%;
        }

        table th.search-and-checkbox-filters input[type="checkbox"] {
            width: 20%;
        }

        table tr {
            transition: background-color 1s ease;
        }

        table tr.highlight {
            background-color: yellow!important;
        }

        input.event-fulfillment-comparable-checkbox:checked + label,
        input.event-fulfillment-fulfilled-checkbox:checked + label {
            background-color: #50ff50!important;
        }

        input.event-fulfillment-comparable-checkbox:checked + label>span,
        input.event-fulfillment-fulfilled-checkbox:checked + label>span {
            background-color: transparent!important;
            color: #000000!important;
        }

    </style>
@endsection

@section('content')

    <div class="container-fluid">
        <h1 class="mt-4">Fulfillment Upload</h1>
        <div class="card mb-4">
            <div class="card-body">

                <div class="row mb-2">
                    <div class="col-12">
                        <form action="{{route('fulfillment-board.import')}}" method="post" enctype="multipart/form-data" style="all: unset">
                            @csrf
                            <div>
                                <input type="file" class="" id="fileupload" name="fileupload" value="upload file" required>
                                <button type="submit" class="btn btn-info text-white">Start upload</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Fulfillment board</h1>

        <div>
            <table class="table table-bordered table-searchable small table-striped" id="fulfillment-board-table" width="100%" cellspacing="0">
                <thead>
                <tr>
{{--                    <th class="not-export noVis"><input type="checkbox" id="checkall"> All </th>--}}
                    <th class="text-center" style="width: 185px;">Event details</th>
                    <th class="text-center lg-width">Site purchased</th>
                    <th class="text-center md-width" style="width: 86px;">Paper</th>
                    <th class="text-center">Unsold</th>
                    <th class="text-center lg-width">Comparable</th>
                    <th class="text-center lg-width search-and-checkbox-filters d-flex flex-wrap justify-content-center">To be Fulfilled</th>
                    <th class="text-center lg-width">Fulfillment Summary</th>
                    <th>Total</th>
                    <th class="text-center align-bottom">Deadline</th>
                    <th class="text-center lg-width">Site sold</th>
                    <th class="not-export noVis align-bottom">Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modals -->
    @include('modals.event-fulfillment-mass-update-modal')
    @include('modals.event-fulfillment-inventories-modal')

@endsection

@push('extra_js')
    <script>

        function addOptionToSelect(select, optionValue, optionText) {
            var opt = document.createElement('option');
            opt.value = optionValue;
            opt.innerHTML = optionText;
            $(select).append(opt);
        }

        $(document).on('click','.mass-update-modal-button', function () {
            var btn = $(this),
                btnDataValues = ['inventory-event', 'inventory-arena', 'inventory-event-date', 'inventory-time-of-event']

            btnDataValues.forEach(function (dataValue) {
                $('#massUpdateModal').data(dataValue, $(btn).data(dataValue))
            })

            $('#massUpdateModalLabelTitle').text($(btn).data('event-fulfillment-title'))

            var massUpdateFields = ['event', 'event_genre', 'arena', 'type_of_ticket', 'country', 'site_purchased', 'event_date', 'time_of_event'];

            massUpdateFields.forEach(function (field) {
                $('#mass_update_' + field + '_values').text('Current value: ' + $(btn).data('inventory-' + field + '-values'))
                var oldValuesSelect = $('#mass_update_' + field + '_old_values')

                if (oldValuesSelect.length) {
                    oldValuesSelect.empty()

                    var oldValuesArray = $(btn).data('inventory-' + field + '-values').split(', ');
                    addOptionToSelect(oldValuesSelect, '', 'All')

                    oldValuesArray.forEach(function (oldValue) {
                        addOptionToSelect(oldValuesSelect, oldValue, oldValue)
                    })
                }
            })
        })
    </script>
@endpush
