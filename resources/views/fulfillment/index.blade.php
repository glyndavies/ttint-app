@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Tickets Type : {{ $ticketType->tickettype }}</h1>

        <div>
            <table class="table table-bordered" id="dataTableTickets" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkall"> All </th>
                    <th>Batch ID</th>
                    <th>Transaction number</th>
                    <th>Order Number</th>
                    <th>Event </th>
                    <th>Event Arena</th>
                    <th>Site Purchased</th>
                    <th>Event Date</th>
                    <th>Time of Event</th>
                    <th>Day of event</th>
                    <th>Number of tickets</th>
                    <th class="d-none">Notes</th>
                    <th class="lg-width">Restrictions</th>
                    <th>Seats</th>
                    <th>User Code</th>
                    <th>Tickets in Hand </th>
                    <th>Date Sold</th>
                    <th>Site Sold</th>
                    <th>Sale ID</th>
                    <th>TSP</th>
                    <th>Delivered Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($inventory_quantities as $inventory_quantity)
                    <tr  class="@if($inventory_quantity->deliveryBatch)  batch-{{ $inventory_quantity->deliveryBatch->batch_id }} @endif small">
                        <td><input class="selectCheck" value="{{ $inventory_quantity->id }}" type="checkbox" ></td>
                        <td>@if ($inventory_quantity->deliveryBatch) {{ $inventory_quantity->deliveryBatch->batch_id }} @endif</td>
                        <td>{{ $inventory_quantity->inventory->purchase_number }}</td>
                        <td>{{ $inventory_quantity->inventory->order_number }}</td>
                        <td>{{ $inventory_quantity->inventory->event }}</td>
                        <td>{{ $inventory_quantity->inventory->arena }}</td>
                        <td>{{ $inventory_quantity->inventory->site_purchased }}</td>
                        <td>{{ dateformat($inventory_quantity->inventory->event_date) }}</td>
                        <td>{{ $inventory_quantity->inventory->time_of_event }}</td>
                        <td>{{ $inventory_quantity->inventory->event_date ? substr($inventory_quantity->inventory->event_date->format('l'), 0, 3) : '' }}</td>
                        <td class="text-center">
                            <span>{{ $inventory_quantity->number_of_tickets }}</span>
                            @include('partials.badges.inventory-notes', [ 'inventory' => $inventory_quantity->inventory ])
                        </td>
                        <td class="d-none">{{ $inventory_quantity->inventory->notes }}</td>
                        <td>{{ $inventory_quantity->inventory->restrictions }}</td>
                        <td>{{ $inventory_quantity->seats }}</td>
                        <td>{{ $inventory_quantity->inventory->buyer }}</td>
                        <td>
                            @include('partials.toggles.inventory-quantity-inhands', [ 'inventory_quantity' => $inventory_quantity ])
                        </td>
                        <td>{{ dateformat($inventory_quantity->soldTicket->sold_date) }}</td>
                        <td>{{ $inventory_quantity->soldTicket->site_sold }}</td>
                        <td>{{ $inventory_quantity->soldTicket->sale_id_ref }}  </td>
                        <td>@money($inventory_quantity->sell_price)</td>
                        <td>
                            @include('partials.badges.inventory-quantity-handling', [ 'inventory_quantity' => $inventory_quantity ])
                        </td>
                        <td>
                            <button class="cancel_sale btn btn-danger btn-sm" title="Return to stock" data-id="{{ $inventory_quantity->id }}">Cancel</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col my-2">
                <button type="button" class="btn btn-sm btn-dark" id="massChangeBtn" data-toggle="modal" data-target="#saleModal">Mass Change</button>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModal" aria-hidden="true" >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ route('tickets.handlingMass') }}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Mass Change <span class="text-uppercase">{{ $ticketType->tickettype }}</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @include('fulfillment.partials.paper-mass', ['ticketType' => $ticketType ])
                        <div class="form-group">
                            <select name="batchSelection" class="form-control">
                                <option value="">Select batch name</option>
                                <option value="1">Batch 1</option>
                                <option value="2">Batch 2</option>
                                <option value="3">Batch 3</option>
                                <option value="4">Batch 4</option>
                                <option value="5">Batch 5</option>
                                <option value="6">Batch 6</option>
                                <option value="7">Batch 7</option>
                                <option value="8">Batch 8</option>
                                <option value="9">Batch 9</option>
                                <option value="10">Batch 10</option>
                            </select>
                        </div>
                        <textarea style="display: none;" id="massSelection" name="batchID"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-dark">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page_level_js')
    <script type="text/javascript">
        $('.ticketsInHand').change(function() {
            var id = $(this).data('id');
            if ($(this).is(":checked")) {
            } else if ($(this).is(":not(:checked)")) {
                $(this).closest('tr').remove();
            }
        });
    </script>
@endsection
