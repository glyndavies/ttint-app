<div class="form-group">
    <label for="exampleFormControlSelect1">Status</label>
    <select class="form-control" id="exampleFormControlSelect1" name="status">
        @foreach(TicketHandlingStatusRepository::getActive() as $handlingStatus)
            @if ($ticketType->paper())

                @if ($handlingStatus->notStarted() || $handlingStatus->sent() || $handlingStatus->collected())
                    <option value="{{ $handlingStatus->id }}">{{ $handlingStatus->ticketHandlingName }}</option>
                @endif

            @elseif ($ticketType->eticket())

                @if ($handlingStatus->notStarted() || $handlingStatus->uploaded())
                    <option value="{{ $handlingStatus->id }}">{{ $handlingStatus->ticketHandlingName }}</option>
                @endif

            @elseif ($ticketType->flash())

                @if ($handlingStatus->notStarted() || $handlingStatus->transfered())
                    <option value="{{ $handlingStatus->id }}">{{ $handlingStatus->ticketHandlingName }}</option>
                @endif

            @elseif ($ticketType->transfer())

                @if ($handlingStatus->notStarted() || $handlingStatus->transfered())
                    <option value="{{ $handlingStatus->id }}">{{ $handlingStatus->ticketHandlingName }}</option>
                @endif

            @elseif($ticketType->qrcode())
                @if($handlingStatus->notStarted() || $handlingStatus->transfered())
                    <option value="{{ $handlingStatus->id }}">{{ $handlingStatus->ticketHandlingName }}</option>
                @endif

            @endif
        @endforeach
    </select>
</div>


