@extends('layouts.app')

@section('content')

<div class="container my-3">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-right">
                    <h1 class="card-title pricing-card-title">{{$events_year}} </h1>
                    <small class="text-muted small text-uppercase"> events this year</small>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-right">
                    <h1 class="card-title pricing-card-title">{{$events_30}} </h1>
                    <small class="text-muted small text-uppercase"> events in next 30 days</small>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-right">
                    <h1 class="card-title pricing-card-title">{{$ticket_total}} </h1>
                    <small class="text-muted small text-uppercase"> TICKETS AVAILABLE </small>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-right">
                    <h1 class="card-title pricing-card-title">{{$ticket_sold}} </h1>
                    <small class="text-muted small text-uppercase"> TICKETS sold </small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid my-5">
    <div class="row justify-content-center">
        <div class="col-sm-6 col-md-4 col-xl-2 text-center">
            <h5 class="sticky-top bg-white border-bottom py-3 text-muted text-uppercase" style="top: 50px;">Total rebates</h5>
            @include('partials.top-kpi-list', [ 'kpis' => $kpis_total_rebate])
        </div>
        <div class="col-sm-6 col-md-4 col-xl-2 text-center">
            <h5 class="sticky-top bg-white border-bottom py-3 text-muted text-uppercase" style="top: 50px;">Trust rebates</h5>
            @include('partials.top-kpi-list', [ 'kpis' => $kpis_trust_rebate])
        </div>
        <div class="col-sm-6 col-md-4 col-xl-2 text-center">
            <h5 class="sticky-top bg-white border-bottom py-3 text-muted text-uppercase" style="top: 50px;">Referral rebates</h5>
            @include('partials.top-kpi-list', [ 'kpis' => $kpis_referral_rebate])
        </div>
        <div class="col-sm-6 col-md-4 col-xl-2 text-center">
            <h5 class="sticky-top bg-white border-bottom py-3 text-muted text-uppercase" style="top: 50px;">This week rebates</h5>
            @include('partials.top-kpi-list', [ 'kpis' => $kpis_cur_week_total_rebate])
        </div>
        <div class="col-sm-6 col-md-4 col-xl-2 text-center">
            <h5 class="sticky-top bg-white border-bottom py-3 text-muted text-uppercase" style="top: 50px;">This month rebates</h5>
            @include('partials.top-kpi-list', [ 'kpis' => $kpis_cur_month_total_rebate])
        </div>

    </div>
</div>

@endsection
