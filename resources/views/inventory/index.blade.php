@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
        /*thead input[type="date"]::-webkit-datetime-edit,
        thead input[type="date"]::-webkit-inner-spin-button,
        thead input[type="date"]::-webkit-clear-button {
            color: #fff;
            position: relative;
        }

        thead input[type="date"]::-webkit-datetime-edit-year-field{
            position: absolute !important;
            border-left:1px solid #8c8c8c;
            padding: 2px;
            color:#000;
            left: 56px;
        }

        thead input[type="date"]::-webkit-datetime-edit-month-field{
            position: absolute !important;
            border-left:1px solid #8c8c8c;
            padding: 2px;
            color:#000;
            left: 26px;
        }

        thead input[type="date"]::-webkit-datetime-edit-day-field{
            position: absolute !important;
            color:#000;
            padding: 2px;
            left: 4px;
        }
        input[type="date"] ::-webkit-calendar-picker-indicator {
            display: none;
            -webkit-appearance: none;
        }*/
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Tickets Inventory</h1>

        <div>
            <table class="table table-bordered table-searchable small" id="inventory-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="not-export d-none noVis">All</th>
                        <th class="d-none noVis">Batch id</th>
                        <th class="md-width">Transaction number</th>
                        <th>Order Number</th>
                        <th>Event</th>
                        <th>Arena</th>
                        <th>Site purchased</th>
                        <th>Type of Ticket</th>
                        <th class="d-none noVis">Purchase Date</th>
                        <th class="datepicker">Event Date</th>
                        <th>Time of Event</th>
                        <th>Day of Event</th>
                        <th class="not-export">Available</th>
                        <th class="d-none noVis">Available</th>
                        <th class="not-export">Number of tickets</th>
                        <th class="1d-none noVis">Number of tickets</th>
                        <th class="d-none noVis">Notes</th>
                        <th>Pending Cancelled</th>
                        <th>Listed on resale</th>
                        <th class="lg-width">Restrictions</th>
                        <th>FV</th>
                        <th class="lg-width">Seats</th>
                        <th>Listed</th>
                        <th>Referral 1</th>
                        <th>Referral 2</th>
                        <th class="sm-width">User Code</th>
                        <th>User name</th>
                        <th>User Deposit</th>
                        <th class="d-none noVis">sold</th>
                        <th>TCP Ticket</th>
                        <th>Total Cost</th>
                        <th>Tickets in Hand</th>
                        <th class="d-none noVis">Date Sold</th>
                        <th class="d-none noVis">Site Sold</th>
                        <th class="d-none noVis">Sale ID</th>
                        <th class="d-none noVis">Refund</th>
                        <th class="d-none noVis">TSP</th>
                        <th class="d-none noVis">Gross Profit</th>
                        <th class="d-none noVis">Delivered</th>
                        <th class="d-none noVis">Delivered Status</th>
                        <th class="d-none noVis">Delivered Date</th>
                        <th class="d-none noVis">Paid</th>
                        <th class="d-none noVis">Paid Date</th>
                        <th class="d-none noVis">Paid Amount</th>
                        <th class="not-export noVis">Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modals -->
    @include('modals.sale-modal')
    @include('modals.details-modal')

@endsection

@section('page_level_js')
@endsection
