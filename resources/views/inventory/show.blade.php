@extends('layouts.app')

@section('page_level_css')

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-12">
                <h1 class="mt-4">Ticket TN:{{ $inventory->purchase_number }}  -  {{ $inventory->event }} -  Event Date:  {{ $inventory->event_date->isoformat('dddd') }} {{ $inventory->event_date->format('d/m/Y') }}
                    <a class="float-right" href="{{ route('tickets.index') }}"> <button class="float-right btn btn-danger" > Back</button> </a>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="{{ route('tickets.index') }}">Tickets</a></li>
                    <li class="breadcrumb-item active">Ticket</li>
                </ol>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <div class="row">

                    <!-- left column -->
                    <div class="col-md-8 col-sm-12">
                        <div class="row">

                            <!-- Ticket inventory details -->
                            <div class="col-sm-12 mb-3">
                                @include('partials.cards.forms.inventory_details', [ 'inventory' => $inventory ])
                            </div>

                            <!-- Buyer information -->
                            <div class="col-md-4 col-sm-12 mb-3">
                                @include('partials.cards.forms.buyer_details', [ 'inventory' => $inventory ])
                            </div>

                            <!-- Ticket value-->
                            <div class="col-md-4 col-sm-12 mb-4">
                                @include('partials.cards.forms.purchase', [ 'purchase' => $inventory->purchase ])
                            </div>

                            <!-- Swap -->
                            <div class="col-md-4 col-sm-12 mb-3">
                                @include('partials.cards.forms.swap_details', [ 'inventory' => $inventory ])
                           </div>

                        </div>
                    </div>

                    <!-- right column -->
                    <div class="col-md-4 col-sm-12 mb-3">
                        <div class="batches"></div>
                        @include('partials.cards.forms.batches_details', [ 'inventory' => $inventory ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
