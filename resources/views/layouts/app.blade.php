<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('page_level_css')

</head>
<body>
    <div class="sb-nav-fixed">
        <div id="app">
            @include('layouts.navbar')
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    @include('layouts.sidebar')
                </div>
                <div id="layoutSidenav_content" class="pt-2">
                    <main>
                        @include('partials.errors')
                        @include('partials.error')
                        @include('partials.success')
                        @yield('content')
                    </main>
                </div>
            </div>
        </div>

        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; 2020</div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" ></script>

    @yield('page_level_js')
    @stack('extra_js')
</body>
</html>
<!--å-->
