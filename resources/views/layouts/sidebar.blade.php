<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">

            <div class="sb-sidenav-menu-heading">Navigation</div>

            <a class="nav-link" href="{{ route('dashboard') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-tachometer-alt"></i>
                </div>
                Dashboard
            </a>

            @hasanyrole('Staff|Admin')
            <div class="sb-sidenav-menu-heading">Upload</div>

            <a class="nav-link" href="{{ route('upload.users') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-angle-up"></i>
                </div>
                User upload
            </a>

            <a class="nav-link" href="{{ route('upload.tickets') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-angle-double-up"></i>
                </div>
                Ticket upload
            </a>

            <a class="nav-link" href="{{ route('upload.expenses') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-angle-double-up"></i>
                </div>
                Expense upload
            </a>

            <a class="nav-link" href="{{ route('upload.business-expenses') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-angle-double-up"></i>
                </div>
                Business Expense upload
            </a>
            @endrole

            <div class="sb-sidenav-menu-heading">Management</div>

            @hasanyrole('Staff|Admin')
            <a class="nav-link" href="{{ route('platform-users') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-user-friends"></i>
                </div>
                Users
            </a>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link" href="{{ route('tickets.all') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-solid fa-list"></i>
                </div>
                All Tickets
            </a>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link" href="{{ route('transfer') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-exchange-alt"></i>
                </div>
                Transfer Area
            </a>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link  @if(Route::is('tickets.index') | Route::is('sold.index')  | Route::is('delivered.index') | Route::is('completed.index')) collapsed @endif " href="#" data-toggle="collapse" data-target="#collapseTickets" aria-expanded="false" aria-controls="collapseTickets">
                <div class="sb-nav-link-icon"><i class="fas fa-ticket-alt"></i></div>
                Ticket Management
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse @if(Route::is('tickets.index') | Route::is('sold.index')  | Route::is('delivered.index') | Route::is('completed.index')) show @endif" id="collapseTickets" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ route('tickets.index') }}">Inventory<i class="ml-2 badge badge-warning float-right">{{ InventoryQuantityRepository::getInventoryCount() }}</i></a>
                    <a class="nav-link" href="{{ route('sold.index') }}">Sold<i class="ml-2 badge badge-warning float-right">{{ InventoryQuantityRepository::getSoldCount() }}</i></a>
                    <a class="nav-link" href="{{ route('delivered.index') }}">Delivered<i class="ml-2 badge badge-warning float-right">{{ InventoryQuantityRepository::getDeliveredCount() }}</i></a>
                    <a class="nav-link" href="{{ route('completed.index') }}">Completed<i class="ml-2 badge badge-warning float-right">{{ InventoryQuantityRepository::getCompletedCount() }}</i></a>
                </nav>
            </div>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link @if(Route::is('ticket-not-received.*')) collapsed @endif " href="#" data-toggle="collapse" data-target="#collapseNotReceived" aria-expanded="false" aria-controls="collapseNotReceived">
                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                Ticket Not Received
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse @if(Route::is('ticket-not-received.*')) show @endif" id="collapseNotReceived" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    @foreach(TicketTypeRepository::getActive() as $ticketType)
                    <a class="nav-link" href="{{ route('ticket-not-received.index', $ticketType->slug) }}">{{ $ticketType->tickettype }}</a>
                    @endforeach
                </nav>
            </div>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link" href="{{ route('fulfillment-board.index') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-layer-group"></i></div>
                Fulfillment Board
            </a>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link @if(Route::is('fulfillment.*') ) collapsed @endif" href="#" data-toggle="collapse" data-target="#collapseType" aria-expanded="false" aria-controls="collapseType">
                <div class="sb-nav-link-icon"><i class="fas fa-check-double"></i></div>
                Fulfillment
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse @if(Route::is('fulfillment.*')) show @endif" id="collapseType" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    @foreach(TicketTypeRepository::getActive() as $ticketType)
                    <a class="nav-link" href="{{ route('fulfillment.index', $ticketType->slug) }}">{{ $ticketType->tickettype }}</a>
                    @endforeach
                </nav>
            </div>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link" href="{{ route('purchases.index') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-shopping-cart"></i>
                </div>
                Purchases
            </a>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link" href="{{ route('adjustments.index') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-remove-format"></i>
                </div>
                Adjustments
            </a>
            @endrole

            @hasanyrole('Staff|Admin')
            <a class="nav-link @if(Route::is('finances.*')) collapsed @endif" href="#" data-toggle="collapse" data-target="#collapseFinances" aria-expanded="false" aria-controls="collapseFinances">
                <div class="sb-nav-link-icon"><i class="fas fa-comments-dollar"></i></div>
                Finances
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse @if(Route::is('finances.*')) show @endif" id="collapseFinances" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    @hasanyrole('Admin')
                    <a class="nav-link" href="{{ route('finances.business-expenses') }}">Business Expenses</a>
                    @endrole
                    <a class="nav-link" href="{{ route('finances.category-expenses', 'business') }}">Expenses</a>
                    @hasanyrole('Admin')
                    <a class="nav-link" href="{{ route('finances.category-expenses', 'personal') }}">Personal Expenses</a>
                    <a class="nav-link" href="{{ route('finances.category-expenses', 'entertainment') }}">Entertainment Expenses</a>
                    @endrole
                    <a class="nav-link" href="{{ route('finances.user-charges') }}">User Charges</a>
                    <a class="nav-link" href="{{ route('finances.payments') }}">Payments</a>
                    @hasanyrole('Admin')
                    <a class="nav-link" href="{{ route('finances.director-loan') }}">Director Loan</a>
                    <a class="nav-link" href="{{ route('finances.loan') }}">Loan</a>
                    <a class="nav-link" href="{{ route('finances.taxes') }}">Taxes</a>
                    @endrole
                    <a class="nav-link" href="{{ route('finances.charges') }}">Charges & Credits</a>
                </nav>
            </div>
            @endrole

            @hasanyrole('Admin')
            <a class="nav-link" href="{{ route('settings.index') }}">
                <div class="sb-nav-link-icon">
                    <i class="fas fa-cogs"></i>
                </div>
                App Settings
            </a>
            @endrole

        </div>
    </div>

    <div class="sb-sidenav-footer">
        <div class="small">
            Version 1.1
        </div>
    </div>

</nav>
