@extends('layouts.app')

@section('content')
    @include('messenger.partials.flash')

        <div class="card mb-4">
            <div class="card-body">

                <div class="email-app mb-4">
                    <nav>
                        <a href="page-inbox-compose.html" class="btn btn-danger btn-block">New Message</a>
                        <ul class="nav">
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#"><i class="fa fa-inbox"></i> Inbox <span class="badge badge-danger">4</span></a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#"><i class="fa fa-star"></i> Stared</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#"><i class="fa fa-rocket"></i> Sent</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#"><i class="fa fa-trash-o"></i> Trash</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#"><i class="fa fa-bookmark"></i> Important</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#"><i class="fa fa-inbox"></i> Inbox <span class="badge badge-danger">4</span></a>--}}
{{--                                    </li>--}}
                        </ul>
                    </nav>
                    <main class="inbox">
{{--                                <div class="toolbar">--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-envelope"></span>--}}
{{--                                        </button>--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-star"></span>--}}
{{--                                        </button>--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-star-o"></span>--}}
{{--                                        </button>--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-bookmark-o"></span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-mail-reply"></span>--}}
{{--                                        </button>--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-mail-reply-all"></span>--}}
{{--                                        </button>--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-mail-forward"></span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                    <button type="button" class="btn btn-light">--}}
{{--                                        <span class="fa fa-trash-o"></span>--}}
{{--                                    </button>--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">--}}
{{--                                            <span class="fa fa-tags"></span>--}}
{{--                                            <span class="caret"></span>--}}
{{--                                        </button>--}}
{{--                                        <div class="dropdown-menu">--}}
{{--                                            <a class="dropdown-item" href="#">add label <span class="badge badge-danger"> Home</span></a>--}}
{{--                                            <a class="dropdown-item" href="#">add label <span class="badge badge-info"> Job</span></a>--}}
{{--                                            <a class="dropdown-item" href="#">add label <span class="badge badge-success"> Clients</span></a>--}}
{{--                                            <a class="dropdown-item" href="#">add label <span class="badge badge-warning"> News</span></a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="btn-group float-right">--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-chevron-left"></span>--}}
{{--                                        </button>--}}
{{--                                        <button type="button" class="btn btn-light">--}}
{{--                                            <span class="fa fa-chevron-right"></span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                        <ul class="messages">

                            @foreach($messages as $thread)

                                <?php $class = $thread->isUnread(Auth::id()) ? 'unread alert-info' : ''; ?>

                                    <li class="message {{ $class }}">
                                        <a href="{{ route('messages.show', $thread->id) }}">
                                            <div class="actions">
                                                <span class="action"><i class="fa fa-square-o"></i></span>
                                                <span class="action"><i class="fa fa-star-o"></i></span>
                                            </div>
                                            <div class="header">
                                                <span class="from"> From :{{ $thread->creator()->name ?? '?' }}</span>
                                                <span class="date">
                                                 <span class="fa fa-paper-clip"></span> {{ $thread->updated_at  }}</span>
                                            </div>
                                            <div class="title">
                                                {{ $thread->subject }}
                                            </div>
                                            <div class="description">
                                                {{ isset($thread->latestMessage->body) ? $thread->latestMessage->body : 'content  ref to : purchase number '.$thread->id }}    </div>
                                        </a>
                                    </li>

                            @endforeach

                        </ul>
                            </main>
                        </div>




{{--                        @each('messenger.partials.thread', $messages, 'thread', 'messenger.partials.no-threads')--}}

{{--                        --}}


            </div>
        </div>
    </div>


@stop
