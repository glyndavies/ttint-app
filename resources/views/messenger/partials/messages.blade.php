<div class="media">
    <div class="media-body">
        <div class="@if($message->user_id == Auth::id()) outgoing_msg @else incoming_msg @endif ">

            <div class="@if($message->user_id == Auth::id()) outgoing_msg_img @else incoming_msg_img @endif mb-3">
                @if($message->user_id != Auth::id()) <img src="//www.gravatar.com/avatar/{{ md5($message->user->email) }} ?s=64" alt="{{ $message->user->name }}" class="img-circle">@endif
            </div>

            <div class="@if($message->user_id == Auth::id()) sent_msg @else received_msg @endif ">

                <div class=" @if($message->user_id == Auth::id()) sent_withd_msg @else received_withd_msg @endif  received_withd_msg">

                    <p>{{ $message->body }} </p>
                    <span class="time_date"> {{ $message->user->name }} | Posted {{ $message->created_at->diffForHumans() }}</span></div>
            </div>
        </div>
    </div>
</div>
