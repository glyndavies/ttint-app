<?php $count = Auth::user()->newThreadsCount(); ?>



<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" id="noticeDropdown" href="{{route('messages.index')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-bell fa-fw"> </i>
        @if($count > 0)
            <span class="badge badge-danger blinking">{{ $count }}</span>
        @endif
    </a>


</li>
