<div class="modal fade" id="completedModal" tabindex="-1" role="dialog" aria-labelledby="completedModal" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body m-0">
                <div class="completed-modal-content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
<script>
    /**
     * Init Completed Modal
     */
    $('#completedModal').on('show.bs.modal', function (event) {
        let $button = $(event.relatedTarget); // Button that triggered the modal
        let inventoryQuantityId = $button.data('inventory-quantity-id');

        completedTrans(inventoryQuantityId);
    });

    /**
     *
     */
    function completedTrans(id) {
        $.get('completed/toCompleted/'+id,function(response){
            $('.completed-modal-content').html(response);
        });
    }

</script>
@endpush