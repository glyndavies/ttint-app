<div class="modal-header">
    <h6 class="modal-title" id="exampleModalLabelDelivered">
        <b>Ticket TN:</b> {{$inventory_quantity->purchase_number}}  -  {{$inventory_quantity->inventory->event}} -  Event Date:  {{$inventory_quantity->inventory->event_date->isoformat('dddd')}} {{$inventory_quantity->inventory->event_date->format('d/m/Y')}}
    </h6>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="card-body-modal p-0">
        <div class="row">
            <!-- left column -->
            <div class="col-md-9 col-sm-12 px-0">
                <div class="row">
                    <!-- Ticket inventory details -->
                    <div class="col-sm-12 mb-3 pr-0">
                        @include('partials.cards.forms.inventory_details', ['inventory' => $inventory_quantity->inventory])
                    </div>

                    <!-- Buyer information -->
                    <div class="col-md-4 col-sm-12 mb-3 pr-0">
                        @include('partials.cards.forms.buyer_details', ['inventory' => $inventory_quantity->inventory])
                    </div>

                    <!-- Ticket value-->
                    <div class="col-md-4 col-sm-12 mb-4 pr-0">
                        @include('partials.cards.forms.purchase', ['purchase' => $inventory_quantity->inventory->purchase])
                    </div>

                </div>
            </div>

            <!-- right column -->
            <div class="col-md-3 col-sm-12 p-0 pl-3">
                <div class="pl-3">
                    @include('partials.cards.forms.sold_details', ['soldTicket' => $inventory_quantity->soldTicket])
                    <div class="mt-3">
                        @include('partials.inventory-quantity-notes', ['inventory_quantity' => $inventory_quantity])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
