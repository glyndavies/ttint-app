<div class="modal fade" id="deliveredModal" tabindex="-1" role="dialog" aria-labelledby="deliveredModal" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body m-0">
                <div class="delivered-modal-content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
<script>
    /**
     * Init Delivered Modal
     */
    $('#deliveredModal').on('show.bs.modal', function (event) {
        let $button = $(event.relatedTarget); // Button that triggered the modal
        let inventoryQuantityId = $button.data('inventory-quantity-id');

        deliveredTrans(inventoryQuantityId);
    });

    /**
     *
     */
    function deliveredTrans(id) {
        $.get('delivered/toDelivered/'+id, function(response){
            $('.delivered-modal-content').html(response);
        });
    }

</script>
@endpush