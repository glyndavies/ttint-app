<div class="modal-header">
    <h6 class="modal-title" id="exampleModalLabelDetails">
        <b>Ticket TN:</b> {{ $inventory->purchase_number }}  -  {{ $inventory->event }} -  Event Date:  {{ $inventory->event_date->isoformat('dddd') }} {{ $inventory->event_date->format('d/m/Y') }}
    </h6>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="card-body-modal p-0">
        <div class="row">
            <!-- left column -->
            <div class="col-md-9 col-sm-12 px-0">
                <div class="row">
                    <!-- Ticket inventory details -->
                    <div class="col-sm-12 mb-3 pr-0">
                        @include('partials.cards.forms.inventory_details', [ 'inventory' => $inventory ])
                    </div>

                    <!-- Buyer information -->
                    <div class="col-md-4 col-sm-12 mb-3 pr-0">
                        @include('partials.cards.forms.buyer_details', [ 'inventory' => $inventory ])
                    </div>

                    <!-- Ticket value-->
                    <div class="col-md-4 col-sm-12 mb-4 pr-0">
                        @include('partials.cards.forms.purchase', [ 'purchase' => $inventory->purchase ])
                    </div>

                    <!-- Swap -->
                    <div class="col-md-4 col-sm-12 mb-3 pr-0">
                        @include('partials.cards.forms.swap_details', [ 'inventory' => $inventory ])
                    </div>

                </div>
            </div>
            <!-- right column -->
            <div class="col-md-3 col-sm-12 p-0 pl-3">
                <div class="pl-3">
                    <div class="batches"></div>
                    @include('partials.cards.forms.batches_details', [ 'inventory' => $inventory ])
                </div>
            </div>
        </div>
    </div> 
</div>
