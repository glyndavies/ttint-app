<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModal" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body m-0">
                <div class="details-modal-content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
<script>
    /**
     * Init Details Modal
     */
    $('#detailsModal').on('show.bs.modal', function (event) {
        let $button = $(event.relatedTarget); // Button that triggered the modal
        let inventory = $button.data('inventory');

        detailsTrans(inventory);
    });

    /**
     *
     */
    function detailsTrans(id) {
        $.get('tickets/toDetails/'+id,function(response){
            $('.details-modal-content').html(response);
        });
    }

</script>
@endpush