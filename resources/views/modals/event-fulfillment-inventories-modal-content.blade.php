<div class="modal-body">
    <div class="card-body-modal p-0">
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th scope="col">Order Number</th>
                    <th scope="col">Number of Tickets</th>
                    <th scope="col">Seats</th>
                </tr>
                </thead>
                <tbody>
                @foreach($inventoryQuantities as $i => $inventoryQuantity)
                    <tr>
                        <th scope="row">
                            {{ $i + 1 }}
                        </th>
                        <td>
                            {{ $inventoryQuantity->inventory->order_number }}
                        </td>
                        <td>
                            @include('partials.badges.inventory-quantity-status', [ 'inventory_quantity' => $inventoryQuantity])
                        </td>
                        <td>
                            {{ $inventoryQuantity->seats }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
