<div class="modal fade" id="eventFulfillmentInventoriesModal" tabindex="-1" role="dialog" aria-labelledby="eventFulfillmentInventoriesModal" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="event-fulfillment-inventories-modal-header"></div>
            </div>
            <div class="modal-body m-0">
                <div class="event-fulfillment-inventories-modal-content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
    <script>
        /**
         * Init Details Modal
         */
        $('#eventFulfillmentInventoriesModal').on('show.bs.modal', function (event) {
            let $button = $(event.relatedTarget); // Button that triggered the modal
            let inventoryQuantitiesIds = $button.data('inventory-quantities-ids');

            $('.event-fulfillment-inventories-modal-header').text($button.data('event-fulfillment-title'))

            if(inventoryQuantitiesIds !== undefined){
                inventoryList(inventoryQuantitiesIds);
            }
        });

        /**
         *
         */
        function inventoryList(ids) {
            $.get('fulfillment-board/inventory-quantities/'+ids,function(response){
                $('.event-fulfillment-inventories-modal-content').html(response);
            });
        }

    </script>
@endpush
