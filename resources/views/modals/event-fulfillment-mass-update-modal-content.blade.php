<div class="mb-2">
    <label>Select field you want to update</label>
    <select class="form-control mass-update-form-selector">
        <option value="event" selected>Title</option>
        <option value="event_genre">Genre</option>
        <option value="arena">Arena</option>
        <option value="type_of_ticket">Type of Ticket</option>
        <option value="country">Country</option>
        <option value="site_purchased">Site Purchased</option>
        <option value="event_date">Event Date</option>
        <option value="time_of_event">Time of Event</option>
        <option value="pending_cancelled">Pending Cancelled</option>
    </select>
</div>
<div class="mass-update-forms">
    <form class="mass-update-form" id="mass_update_event" action="{{ route('fulfillment-board.updateMass') }}" method="POST">
        @csrf
        <small id="mass_update_event_values" class="d-block mb-4"></small>
        <label for="event">Event title</label>
        <input id="event" class="form-control" name="event" type="text" required />
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_event_genre" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_event_genre_values" class="d-block mb-4"></small>
        <label for="event_genre">Genre</label>
        <input id="event_genre" class="form-control" name="event_genre" type="text" required />
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_arena" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_arena_values" class="d-block mb-4"></small>
        <label for="arena">Arena</label>
        <input id="arena" class="form-control" name="arena" type="text" required />
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_type_of_ticket" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_type_of_ticket_values" class="d-block"></small>
        <label for="mass_update_type_of_ticket_old_values" class="mt-4">Type of ticket (old value)</label>
        <select id="mass_update_type_of_ticket_old_values" class="form-control" name="type_of_ticket_old_value" type="text" required>
            <option value="">All</option>
        </select>
        <label for="type_of_ticket" class="mt-4">Type of ticket (new value)</label>
        <select id="type_of_ticket" class="form-control" name="type_of_ticket" type="text" required >
            @foreach(TicketTypeRepository::getActive() as $ticket_type)
                <option  value="{{$ticket_type->tickettype}}">{{$ticket_type->tickettype}}</option>
            @endforeach
        </select>
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_country" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_country_values" class="d-block mb-4"></small>
        <label for="country">Country</label>
        <input id="country" class="form-control" name="country" type="text" required />
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_site_purchased" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_site_purchased_values" class="d-block"></small>
        <label for="mass_update_site_purchased_old_values" class="mt-4">Site Purchased (old value)</label>
        <select id="mass_update_site_purchased_old_values" class="form-control" name="site_purchased_old_value" type="text">
            <option value="">All</option>
        </select>
        <label for="site_purchased" class="mt-4">Site Purchased (new value)</label>
        <input id="site_purchased" class="form-control" name="site_purchased" type="text" required />
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_event_date" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_event_date_values" class="d-block mb-4"></small>
        <label for="event_date">Event date</label>
        <input id="event_date" class="form-control" name="event_date" type="date" required>
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_time_of_event" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <small id="mass_update_time_of_event_values" class="d-block mb-4"></small>
        <label for="time_of_event">Time of Event</label>
        <input id="time_of_event" class="form-control" name="time_of_event" type="time" required>
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
    <form class="mass-update-form" id="mass_update_pending_cancelled" action="{{ route('fulfillment-board.updateMass') }}" method="POST" style="display: none;">
        @csrf
        <p class="d-block my-3">This action will make all tickets pending cancelled and return them back to inventory</p>
        <input type="hidden" name="pending_cancelled" value="Y" />
        <button type="submit" class="btn btn-danger mt-2">Cancel</button>
    </form>
</div>

@push('extra_js')
<script>
    $('.mass-update-form-selector').on('change', function () {
        $('.mass-update-form').hide()
        $('#mass_update_' + $(this).val()).first().show()
    })
</script>
@endpush
