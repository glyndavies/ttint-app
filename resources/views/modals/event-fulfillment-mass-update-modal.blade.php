<div class="modal fade" id="massUpdateModal" tabindex="-1" role="dialog" aria-labelledby="massUpdateModal" aria-hidden="true" data-inventory-event="" data-inventory-arena="" data-inventory-event-date="" data-inventory-time-of-event="">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body m-0">
                <div class="details-modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title" id="massUpdateModalLabel">
                            Event Fulfillment: <span id="massUpdateModalLabelTitle"></span>
                        </h6>
                    </div>
                    <div class="modal-body">
                        <div class="card-body-modal p-0">
                            @include('modals.event-fulfillment-mass-update-modal-content')
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
