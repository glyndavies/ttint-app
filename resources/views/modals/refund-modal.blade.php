
<!-- Modal -->
<div id="refundModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Customer Refund</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>This action will cancel all payment information</p>
                <div class="form-group">
                    <label class="info">info</label>
                </div>
                <div>
                    @include('partials/select/refund_ticket_state')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary refund-action" data-dismiss="modal">Refund</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
<script type="text/javascript">

$('#refundModal').on('show.bs.modal', function (event) {
    let $buttonCancel = $(event.relatedTarget); // Button that triggered the modal
    const id = $buttonCancel.data('id');
    $('#refundModal .refund-action').attr('data-id', id);

    axios.get('/admin/sold/get/' + id)
        .then((response) => {
            $('#refundModal .info').html('<p>Sale ID: ' + response.data.sale_id_ref + '</p><p>Ticket quantity: ' + response.data.ticket_qty + '</p><p>Amount: ' + response.data.sell_price + '</p>')
        });
})

$('#refundModal').on('hide.bs.modal', function (event) {
    $("#moveTicketToState").prop("selectedIndex", 0);
});

$('#refundModal .refund-action').click(function () {
    const id = $(this).data('id');
    const refundTo = $('#moveTicketToState').val();

    return axios({
        method: 'post',
        url: '/admin/ticket/refund-to/' + id,
        data: {'refund_to': refundTo},
    }).then((response) => {
        $('#row'+id).addClass('bg-info');
        Swal.fire( 'Refunded!', 'The payment information has been cancelled.', 'success')
        setTimeout(() => { window.location.href = '/admin/completed' }, 2000)
    }).catch((response) => {
        //handle error
        console.log(response);
    });
});
</script>

@endpush
