<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Refunded Amount for <span class="poname"></span></label>
                    <input class="form-control refundValue" value="" required>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success save" data-dismiss="modal">Save</button>
                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
    <script type="text/javascript">
    $(document).on('click', '.company-refund', function () {

    let id = $(this).data('id');
    var refund  = $(this).data('price');
    $('.refundValue').val(refund);

    const ipAPI = '/api/ticketInfo/companyRefund/'+id;

    $('.save').click(function () {
        Swal.fire({
            title: 'Are you sure ?  ',
            text: "You won't be able to revert this refund of £"+$('.refundValue').val()+"",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Cancel it!'
        }).then((result) => {

            if (result.value) {
                return axios
                    .post(ipAPI,{'refund':$('.refundValue').val()})
                    .then((response) => {
                        $('#row'+id).addClass('bg-warning');
                            Swal.fire(
                                'Cancelled!',
                                'The Ticket  has been Cancelled.',
                                'success'
                            )
                        $('.refundValue').val('');
                        setTimeout(function () {
                            window.location.href = '/admin/purchases';
                        }, 2000)
                        return false;
                    })
            }
        });
    });
 });
</script>

@endpush
