<div class="modal-header">
    <h6 class="modal-title" id="exampleModalLabelSell">
        <b>Sell Ticket:</b>  Event:  {{ $inventory->event }}  Arena: {{ $inventory->arena }} | Event_Date: {{ $inventory->event_date->format('D d/m//y') }}   {{ $inventory->time_of_event }}
    </h6>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-8 split-info" style="display:;">

            <form action="{{ route('sold.store') }}" method="POST" id="saveSale">
                @csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-5">
                            <label for="sold_date">Sold Date</label>
                            <input type="date" class="form-control" id="sold_date" name="sold_date"  required="yes"   value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                        </div>
                        <div class="col-sm-7">
                            <label for="sold_site">Site sold</label>
                            <input type="text" class="form-control mb-1" id="sold_site" name="sold_site"  value="" required="yes">

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" onclick="selectedSite('Viagogo')" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                <label class="form-check-label" for="inlineRadio1">Viagogo</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input "  onclick="selectedSite('Stubhub')" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                <label class="form-check-label" for="inlineRadio2">Stubhub</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input "  onclick="selectedSite('Gigsberg')" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                <label class="form-check-label" for="inlineRadio3">Gigsberg</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input "  onclick="selectedSite('Private')" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
                                <label class="form-check-label" for="inlineRadio3">Private </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="saleidref">Sale ID Ref</label>
                            <input type="text" class="form-control" id="saleidref" name="saleidref"  value="" required="yes">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="notes">Notes</label>
                            <input type="text" class="form-control" id="notes" name="notes"  value="">
                        </div>
                    </div>
                </div>

                <table class="table" id="myTable">
                    <thead>
                    <tr>
                        <td></td>
                        <td>Ticket QTY</td>
                        <td>SEAT</td>
                        <td>PRICE</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody class="sellList">
                    </tbody>

                </table>

                <button type="submit"  class="btn btn-primary">Complete Sale</button>
                <div class="error-block">
                </div>
            </form>
        </div>

        <div class="col-md-8 split-info" style="display: none;">
            <form id="formSplitonSale" method="POST">
                @csrf
                <input type="hidden" id="inventory_id" value="{{ $inventory->id }}">
                <input type="hidden" id="number_of_tickets_original" value="">
                <input type="hidden" name="selected_id" id="selected_id" value="">
                <input type="hidden" name="api" id="api" value="1">

                <div class="row">
                    <div class="col border-right">
                        <h4>Current Batch</h4>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="number_of_tickets">Number of tickets</label>
                                    <input type="number" class="form-control" id="number_of_tickets"  name="number_of_tickets" value="" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="seats">Seats</label>
                                    <input  class="form-control" id="seats" name="seats"  value="" >
                                </div>
                                <div class="col">
                                    <label for="sell_price">Sell Price</label>
                                    <input  class="form-control" id="sell_price" name="sell_price"  value="" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <h4>New Split Batch</h4>
                        <div class="batch-wrapper">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="number_of_tickets">Number of tickets</label>
                                        <input type="number" min="1" max="" class="form-control " id="number_of_tickets_new" name="number_of_tickets_new" value="" required="yes">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="seats">Seats</label>
                                        <input  class="form-control" id="seats_new" name="seats_new"  value="" required="yes">
                                    </div>
                                    <div class="col">
                                        <label for="sell_price">Sell Price</label>
                                        <input  class="form-control" id="sell_price_new" name="sell_price_new"  value="">
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <button class="btn btn-primary" onclick="splitTicketSale()" type="button">Save Split</button>
                        <button class="btn btn-danger" onclick="salesTrans({{ $inventory->id }})" type="button">
                            Cancel Split
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-4 ">

            <div class="card bg-light mb-3 w-100 float-right">
                <div class="card-header text-white bg-success">
                    Ticket Stock  <span class="badge-pill rounded-pill badge-dark float-right"> Batches {{ count($inventory->inventoryQuantities) }} </span>
                </div>
                <div class="card-body">
                    @foreach($inventory->inventoryQuantities as $stock)
                        <div id="tickets{{$stock->id}}">
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col">
                                        <label for="number_of_tickets">Number of tickets  {{ $stock->ifStatusSold() ?   'SOLD': ''}}</label>
                                        <input type="number" class="form-control" id="number_of_tickets"  name="number_of_tickets" value="{{$stock->number_of_tickets}}" readonly="yes">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="seats">Seats</label>
                                        <input  class="form-control" id="seats" name="seats"  value="{{$stock->seats}}"  {{ $stock->ifStatusSold() ? 'readonly="yes"': ''}}>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="sell_price">Sell Price Currently: <span id="sell_price"> </span></label>
                                        <input  class="form-control" id="sell_price{{$stock->id}}" name="sell_price"  value="" {{ $stock->ifStatusSold() ? 'readonly="yes"': '' }}>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div  id="sellme{{$stock->id}}"  class="row">
                                    <div class="col float-right">
                                        @if($stock->ifStatusForSale() && $stock->number_of_tickets > 1)
                                            <button class="btn btn-primary float-right"
                                                onclick="splitBatches({{ $stock->id }})">split this batch
                                            </button>
                                        @endif

                                        @if($stock->ifStatusSold())
                                            <button class="btn btn-danger float-left">
                                                SOLD
                                            </button>
                                        @else
                                            <button class="btn btn-success float-left" onclick="addToSale({{$stock->id}})">
                                                Sell
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr/>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $( "#saveSale" ).submit(function( event ) {
        event.preventDefault();
        var form =$( "#saveSale" );
        var errorBlock = $(form).find('.error-block')
        var url = form.attr('action');

        $(errorBlock).html('')

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                $.each(data, function(k, v) {
                    $('#'+v).remove();
                });
                $('#saleModal').modal('hide');
            },
            error: function (data)
            {
                if (data && data.responseJSON &&data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function(key, error) { $(errorBlock).html('<div class="alert alert-danger mt-2">Error with ' + key + ': ' + error + '</div>')})
                }
            }
        });
    });
</script>

@section('page_level_js')
    <script type="text/javascript">
        $('.siteSelected').on('click',function() {
            $('#sold_site').val($(this).val());
        });
    </script>
@endsection
