<div class="modal fade" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModal" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body m-0">
                <div class="sale-modal-content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
<script>

/**
 * Init Sale Modal
 */
 const $detailsModal = $('#detailsModal');

$('#saleModal').on('show.bs.modal', function (event) {
    let $button = $(event.relatedTarget); // Button that triggered the modal
    let inventory = $button.data('inventory');
    if($detailsModal && $detailsModal.css('display') == 'block'){
        $detailsModal.toggleClass('modal-down');
    } 

    salesTrans(inventory);
});

/**
 * Hidden Sale Modal
 */
$('#saleModal').on('hidden.bs.modal', function (event) {
    if($detailsModal && $detailsModal.hasClass('modal-down')){
        $detailsModal.toggleClass('modal-down');
        $('body').toggleClass('modal-open');
    }
});

/**
 *
 */
function salesTrans(id) {
    $.get('/admin/tickets/toSell/'+id,function(response){
        $('.sale-modal-content').html(response);
    });
}

/**
 *
 */
function splitBatches(id) {

    $('.split-info').toggle();
    $('#tickets'+id).toggle();

    //get stock from batch
    $.get('/api/ticketInfo/splitTickets/'+id,function (response) {
         //set  values to inputs
         $formSplitonSale = $('#formSplitonSale');
         $formSplitonSale.find('#number_of_tickets_original').val(response.number_of_tickets);
         $formSplitonSale.find('#number_of_tickets').val(response.number_of_tickets-1);
         $formSplitonSale.find('#number_of_tickets_new').val(1);
         $formSplitonSale.find('#seats').val(response.seats);
         $formSplitonSale.find('#seats_new').val(response.seats);
         $formSplitonSale.find('#sell_price').val(response.sell_price);
         $formSplitonSale.find('#sell_price_new').val(response.sell_price);
         $formSplitonSale.find('#selected_id').val(response.id);
     });

};

function selectedSite(id) {
    $('#sold_site').val(id);
}

function saveSale() {
    $('#saveSale').submit();
}

function reMoveSale(id){

   $('#'+id).remove();
   $('#sellme'+id).show();
}


function addToSale(id){

    if($('#sell_price'+id).val() != '' ){
        $.get('/admin/tickets/sell/'+id,function(response){
            $('#myTable > tbody:last-child').append('<tr id="'+id+'"><td><input type="hidden" name="id[]" value="'+response.id+'"> </td><td>'+response.number_of_tickets+' </td><td> '+response.seats+'  </td><td><input type="hidden" name="price['+id+']" value="'+$('#sell_price'+id).val()+'"> '+$('#sell_price'+id).val()+'</td><td><button onclick="reMoveSale('+id+')">x</button></td></tr>');
            $('#sell_price'+id).removeClass('border border-danger');
         $('#sellme'+id).hide();
        });
    }else{
        $('#sell_price'+id).addClass('border border-danger');
    }
}

function splitTicketSale () {

    var str = $('#formSplitonSale').serialize();
    var inventory_id =  $('#inventory_id').val();
    var id =  $('#selected_id').val();
    $.ajax({
        type: "POST",
        url: "/admin/tickets/splitbatch/"+id+"",
        data: str,
        dataType: "json",
        success: function(data) {
            //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
            // do what ever you want with the server response
            alert('Splited')
            salesTrans(inventory_id);
        },
        error: function() {
            alert('error handling here');
        }
    });
}

/**
 * Select on focus number_of_tickets
 */
$("#saleModal").on('focus', '#number_of_tickets, #number_of_tickets_new', function() {
    this.select()
});

/**
 * On change number_of_tickets when split
 */
$("#saleModal").on('input', '#formSplitonSale #number_of_tickets', function() {
    let original_number = +$('#formSplitonSale #number_of_tickets_original').val();
    let number_new =  +$('#formSplitonSale #number_of_tickets_new').val();

    let number = +$(this).val();
    number = Math.max(number, 1);
    number = Math.min(number, original_number-1);
    number_new = original_number - number;

    $('#formSplitonSale #number_of_tickets').val(number);
    $('#formSplitonSale #number_of_tickets_new').val(number_new);
});

$("#saleModal").on('input', '#formSplitonSale #number_of_tickets_new', function() {
    let original_number = +$('#formSplitonSale #number_of_tickets_original').val();
    let number =  +$('#formSplitonSale #number_of_tickets').val();

    let number_new = +$(this).val();
    number_new = Math.max(number_new, 1);
    number_new = Math.min(number_new, original_number-1);
    number = original_number - number_new;

    $('#formSplitonSale #number_of_tickets').val(number);
    $('#formSplitonSale #number_of_tickets_new').val(number_new);
});

</script>
@endpush