<div class="modal fade" id="soldModal" tabindex="-1" role="dialog" aria-labelledby="soldModal" aria-hidden="true" >
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body m-0">
                <div class="sold-modal-content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('extra_js')
<script>
    /**
     * Init Sold Modal
     */
    $('#soldModal').on('show.bs.modal', function (event) {
        let $button = $(event.relatedTarget); // Button that triggered the modal
        let inventoryQquantityId = $button.data('inventory-quantity-id');

        soldTrans(inventoryQquantityId);
    });

    /**
     *
     */
    function soldTrans(id) {
        $.get('sold/toSold/'+id,function(response){
            $('.sold-modal-content').html(response);
        });
    }

</script>
@endpush