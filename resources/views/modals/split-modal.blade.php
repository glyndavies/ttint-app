<div class="modal fade" id="splitModal" tabindex="-1" role="dialog" aria-labelledby="splitModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form method="post" data-action="{{ route('tickets.split_batch','') }}">
                @csrf
                <input type="hidden" id="number_of_tickets_original">

                <div class="modal-header">
                    <h5 class="modal-title" id="splitModalLabel">Ticket Splitting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col border-right">
                            <h4>Current Batch</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="number_of_tickets">Number of tickets</label>
                                        <input type="number" class="form-control" id="number_of_tickets" name="number_of_tickets">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="seats">Seats</label>
                                        <input  class="form-control" id="seats" name="seats">
                                    </div>
                                    <div class="col">
                                        <label for="sell_price">Sell Price</label>
                                        <input  class="form-control" id="sell_price" name="sell_price" value="" >
                                    </div>                                        
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <h4>New Split Batch</h4>
                            <div class="batch-wrapper">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="number_of_tickets">Number of tickets</label>
                                            <input type="number" class="form-control number_of_split_tickets" id="number_of_tickets_new"  name="number_of_tickets_new" required="yes">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="seats">Seats</label>
                                            <input  class="form-control" id="seats" name="seats_new"  value="" required="yes">
                                        </div>
                                        <div class="col">
                                            <label for="sell_price">Sell Price</label>
                                            <input  class="form-control" id="sell_price" name="sell_price_new"  value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@push('extra_js')
<script>

/**
 * Init Split Modal
 */

$('#splitModal').on('show.bs.modal', function (event) {
    let $button = $(event.relatedTarget); // Button that triggered the modal
    let stock = $button.data('stock');
    let tickets = $button.data('tickets');
    let seats = $button.data('seats');
    let price = $button.data('price');

    let $modal = $(this);

    $modal.find('#number_of_tickets_original').val(tickets);
    $modal.find('#number_of_tickets').val(tickets-1);
    $modal.find('#number_of_tickets_new').val(1);
    $modal.find('#seats').val(seats);
    $modal.find('#seats_new').val(seats);
    $modal.find('#sell_price').val(price);
    $modal.find('#sell_price_new').val(price);

    let $form = $modal.find('form');
    let action = $form.data('action');
    $modal.find('form').attr('action', action + '/' + stock);
});

/**
 * Select on focus number_of_tickets
 */
$("#splitModal").on('focus', '#number_of_tickets, #number_of_tickets_new', function() {
    this.select()
});

/**
 * On change number_of_tickets when split
 */
$("#splitModal").on('input', '#number_of_tickets', function() {
    let original_number = +$('#splitModal #number_of_tickets_original').val();
    let number_new =  +$('#splitModal #number_of_tickets_new').val();

    let number = +$(this).val();
    number = Math.max(number, 1);
    number = Math.min(number, original_number-1);
    number_new = original_number - number;

    $('#splitModal #number_of_tickets').val(number);
    $('#splitModal #number_of_tickets_new').val(number_new);
});

$("#splitModal").on('input', '#number_of_tickets_new', function() {
    let original_number = +$('#splitModal #number_of_tickets_original').val();
    let number =  +$('#splitModal #number_of_tickets').val();

    let number_new = +$(this).val();
    number_new = Math.max(number_new, 1);
    number_new = Math.min(number_new, original_number-1);
    number = original_number - number_new;

    $('#splitModal #number_of_tickets').val(number);
    $('#splitModal #number_of_tickets_new').val(number_new);
});

</script>
@endpush