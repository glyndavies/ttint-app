<!-- Modal -->
<div class="modal fade" id="tokenModal" tabindex="-1" role="dialog" aria-labelledby="tokenModal" aria-hidden="true" >
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <form method="POST" data-action="{{ route('transfer.updateUserCode', '') }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Token<span class="text-uppercase"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>New Buyer Code</label>
                        <select class="form-control platform-user-code" name="new_buyer">
                            @foreach($platformUsers as $platformUser)
                                <option value="{{ $platformUser->user_code }}">
                                    {{ $platformUser->user_code }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Token value</label>
                        <input type="number" name="token" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('extra_js')
    <script type="text/javascript">

    $('#tokenModal').on('show.bs.modal', function (event) {
        let $modal = $(this);
        let $form = $modal.find('form');
        let $button = $(event.relatedTarget); // Button that triggered the modal

        let action = $form.data('action');
        $form.attr('action', action + '/' + $button.data('inventory'));
        $('[name="new_buyer"]').selectize()[0].selectize.setValue($button.data('user_code'))
        $('[name="token"]').val($button.data('token'))
    });
    </script>
@endpush
