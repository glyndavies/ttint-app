<div class="text-nowrap text-right">
    <button data-inventory-quantity-id="{{ $inventory_quantity_id }}" data-toggle="modal" data-target="#completedModal" title="details" type="button" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></button>                                
    <button type="button" class="btn btn-danger btn-sm text-white" data-toggle="modal" data-target="#refundModal" data-id="{{ $sold_ticket_id }}">Cancel</button>
</div>