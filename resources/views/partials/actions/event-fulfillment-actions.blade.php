<div class="text-nowrap text-right d-flex flex-column">

    <button
        data-toggle="modal" data-target="#massUpdateModal"
        title="Update"
        type="button"
        class="btn btn-primary btn-xs rounded-0 mass-update-modal-button p-1"
        data-event-fulfillment-title="{{ $event_fulfillment->event . ' - ' . $event_fulfillment->event_date->format('D d M Y') . ' ' . \Carbon\Carbon::parse($event_fulfillment->time_of_event)->format('H:i') . ' - ' . $event_fulfillment->arena }}"
        data-inventory-event="{{ $event_fulfillment->event }}"
        data-inventory-arena="{{ $event_fulfillment->arena }}"
        data-inventory-event-date="{{ $event_fulfillment->event_date }}"
        data-inventory-time-of-event="{{ $event_fulfillment->time_of_event }}"
        data-inventory-event-values="{{ $event_fulfillment->inventories->pluck('event')->unique()->implode(', ') }}"
        data-inventory-event_genre-values="{{ $event_fulfillment->inventories->pluck('event_genre')->unique()->implode(', ') }}"
        data-inventory-arena-values="{{ $event_fulfillment->inventories->pluck('arena')->unique()->implode(', ') }}"
        data-inventory-type_of_ticket-values="{{ $event_fulfillment->inventories->pluck('type_of_ticket')->unique()->implode(', ') }}"
        data-inventory-country-values="{{ $event_fulfillment->inventories->pluck('country')->unique()->implode(', ') }}"
        data-inventory-site_purchased-values="{{ $event_fulfillment->inventories->pluck('site_purchased')->unique()->implode(', ') }}"
        data-inventory-event_date-values="{{ $event_fulfillment->event_date->format('D d M Y') }}"
        data-inventory-time_of_event-values="{{ $event_fulfillment->time_of_event }}"
    >
        <i class="fas fa-pen fa-xs"></i>
    </button>

    <form class="event-fulfillment-export-form" action="{{ route('fulfillment-board.export') }}" method="POST">
        @csrf
        <button class="btn btn-warning btn-xs rounded-0 w-100 p-1" type="submit" disabled>
            <i class="fas fa-file-export fa-xs"></i>
        </button>
    </form>
</div>
