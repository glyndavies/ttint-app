<div class="text-nowrap text-right">
    <button data-inventory="{{ $inventory->id }}" data-toggle="modal" data-target="#detailsModal" title="details" type="button" class="btn btn-primary btn-xs"><i class="fas fa-ticket-alt" aria-hidden="true"></i></button>
    <button data-inventory="{{ $inventory->id }}" data-toggle="modal" data-target="#saleModal" title="sale" type="button" class="btn btn-info btn-xs text-white"><i class="fa fa-credit-card" aria-hidden="true"></i></button>
    @if ($inventory->expired == 0)
        <button type="button" data-id="{{ $inventory->id }}" title="expire" class="btn btn-danger btn-xs expiredTickets"><i class="fas fa-clock"></i></button>
    @endif
    @if ($inventory->ifStatusAvailable() or $inventory->ifStatusSwapped())
        <button onclick="availableForSwap({{ $inventory->id }}, this)" title="move to transfer area" type="button" data-id="{{ $inventory->id }}" class="btn btn-info btn-xs text-white"><i class="fa fa-random"></i></button>
    @endif
</div>
