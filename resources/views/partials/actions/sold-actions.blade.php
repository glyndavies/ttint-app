<div class="text-nowrap text-right">
    <button data-inventory-quantity-id="{{ $inventory_quantity_id }}" data-toggle="modal" data-target="#soldModal" title="details" type="button" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></button>
    <button class="cancel_sale btn btn-danger btn-sm" title="return to stock" data-id="{{ $inventory_quantity_id }}">Cancel</button>
</div>