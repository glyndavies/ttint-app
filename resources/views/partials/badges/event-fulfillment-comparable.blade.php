<div class="container p-0" style="display: grid; grid-template-columns: 1fr 1fr; gap: 5px">
    @foreach($event_fulfillment->comparable_purchases->chunk(3) as $siteChunks)
        <div class="p-0 d-flex flex-column" style="gap: 5px">
            @foreach($siteChunks as $siteChunk)
                <input
                    id="{{'comparable_' . $siteChunk->site_purchased . '_' . $event_fulfillment->event . '_' . $event_fulfillment->event_date . '_' . $event_fulfillment->time_of_event . '_' . $event_fulfillment->arena }}"
                    class="event-fulfillment-comparable-checkbox"
                    type="checkbox"
                    value="{{ $siteChunk->inventory_quantities->pluck('id') }}" style="width: 0; height: 0; opacity: 0; position: absolute"
                />
                <label
                    title="{{ optional($siteChunk->in_progress_by_user)->name }}"
                    @if($siteChunk->in_progress_by_user) style="background-color: {{ optional($siteChunk->in_progress_by_user)->color_hex }}; color: #000000;" @endif
                    class="d-flex justify-content-between align-items-center p-0 m-0"
                    for="{{'comparable_' . $siteChunk->site_purchased . '_' . $event_fulfillment->event . '_' . $event_fulfillment->event_date . '_' . $event_fulfillment->time_of_event . '_' . $event_fulfillment->arena }}"
                >
                    <span>{{ $siteChunk->site_purchased }}</span>
                    <span
                        @if($siteChunk->in_progress_by_user)
                            class="text-color"
                            style="line-height: 100%; height: fit-content; width: 27.5px; text-align: center; padding: 1px 3px;"
                        @else
                            class="font-weight-bold rounded"
                            style="background-color: #3490dc; color: #ffffff; line-height: 100%; height: fit-content; width: 27.5px; text-align: center; padding: 1px 3px;"
                        @endif
                    >
                        {{ $siteChunk->number_of_items }}
                    </span>
                </label>
            @endforeach
        </div>
    @endforeach
</div>
