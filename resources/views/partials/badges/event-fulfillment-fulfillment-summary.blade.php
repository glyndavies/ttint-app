<div class="container p-0" style="display: grid; grid-template-columns: 1fr 1fr; gap: 5px">
    @foreach($event_fulfillment->fulfillment_summary_purchases->chunk(3) as $siteChunks)
        <div class="p-0 d-flex flex-column" style="gap: 5px">
            @foreach($siteChunks as $siteChunk)
                <div class="d-flex justify-content-between align-items-center p-0">
                    <span>{{ $siteChunk->site_purchased }}</span>
                    <span class="font-weight-bold rounded" style="background-color: #3490dc; color: #ffffff; line-height: 100%; height: fit-content; width: 27.5px; text-align: center; padding: 1px 3px;">{{ $siteChunk->number_of_items }}</span>
                </div>
            @endforeach
        </div>
    @endforeach
</div>
