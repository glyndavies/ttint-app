<div class="font-weight-bold" style="font-size: 120%; text-decoration: underline; cursor: pointer;"
     data-event-fulfillment-title="{{ $event_fulfillment->event . ' - ' . $event_fulfillment->event_date->format('D d M Y') . ' ' . \Carbon\Carbon::parse($event_fulfillment->time_of_event)->format('H:i') . ' - ' . $event_fulfillment->arena }}"
     data-toggle="modal"
     data-target="#eventFulfillmentInventoriesModal"
     data-inventory-quantities-ids="{{ $event_fulfillment->fulfillment_summary_purchases->flatMap(function($fulfillment_summary_item) {return $fulfillment_summary_item->inventory_quantities;})->pluck('id') }}"
>
    {{ $event_fulfillment->fulfillment_summary_purchases->sum('number_of_items') }}
</div>
<div>Orders</div>
