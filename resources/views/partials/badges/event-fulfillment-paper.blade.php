@php
$inventoryQuantities = $event_fulfillment['inventories']
    ->where('type_of_ticket', 'Paper')
    ->flatMap(function ($inv) { return $inv->inventoryQuantities;});

$unsoldNum =$inventoryQuantities
    ->filter(function ($invQ) { return $invQ->ifStatusAvailable();})
    ->count();

$soldNum = $inventoryQuantities
    ->filter(function ($invQ) { return $invQ->ifStatusSold() && !$invQ->isHandlingDelivered();})
    ->count();
@endphp
<div>
    @if($unsoldNum)
        <span class="font-weight-bold" style="font-size: 120%; text-decoration: underline;">{{ $unsoldNum }}</span>
        <span>Unsold</span>
    @endif
</div>
<div>
    <div>
        @if($soldNum)
            <span class="font-weight-bold" style="font-size: 120%; text-decoration: underline;">{{ $soldNum }}</span>
            <span>Sold</span>
        @else
            <br/>
        @endif
    </div>
    @if($unsoldNum || $soldNum)
        <div>Paper</div>
    @endif
</div>
