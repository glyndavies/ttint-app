<div class="container p-0" style="display: grid; grid-template-columns: 1fr 1fr; gap: 5px">
@foreach($event_fulfillment->site_purchases->chunk(3) as $siteChunks)
    <div class="p-0 d-flex flex-column" style="gap: 5px">
        @foreach($siteChunks as $siteChunk)
            <div class="d-flex justify-content-between p-0">
                <span>{{ $siteChunk->site_purchased }}</span>
                <input
                    class="site-tickets-in-hand-checkbox"
                    type="checkbox"
                    @if($siteChunk->is_tickets_in_hand) checked @endif
                    data-inventory-quantity-ids="{{ $siteChunk->inventory_quantities->pluck('id') }}"
                />
            </div>
        @endforeach
    </div>
@endforeach
</div>
