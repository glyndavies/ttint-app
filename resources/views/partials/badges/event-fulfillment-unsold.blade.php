@php
    $unsoldNum = $event_fulfillment->inventoryQuantities
        ->filter(function ($invQ) { return !$invQ->ifStatusSold();})
        ->sum('number_of_tickets');
@endphp
@if($unsoldNum)
    <span class="font-weight-bold" style="font-size: 120%; text-decoration: underline;">{{ $unsoldNum }}</span>
    <br/>
    Unsold
@endif
