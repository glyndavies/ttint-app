@php
    $inventory_quantity_list = [];
    if($inventory->ifStatusSplit()){
        foreach($inventory->inventoryQuantities as $inventory_quantity){
            $inventory_quantity_list[] = $inventory_quantity->number_of_tickets;
        }
    } else{
        $inventory_quantity[] = $inventory->inventoryQuantities->first()->number_of_tickets;
    }
@endphp
{{ implode('  ', $inventory_quantity_list) }}  