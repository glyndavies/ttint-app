@php
    $filtered_quantities = $inventory->inventoryQuantities->filter(function($inventory_quantity) {
        return $inventory_quantity->ifStatusForTransfer();
    });
@endphp

@if($inventory->inventoryQuantities->count() > 1)
    <span class="badge badge-{{ $inventory->ticketStatus->ticketStatus }}">
        {{ $filtered_quantities->sum('number_of_tickets') }}
    </span>
@else
    <span class="m-1">{{ $filtered_quantities->sum('number_of_tickets') }}</span>
@endif