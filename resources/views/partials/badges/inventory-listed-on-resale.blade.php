@if ($inventory->ifListedOnResale())
    <span class="badge badge-pill badge-warning" title="listed on resale">
        <small>Resale</small>
    </span>
@endif
