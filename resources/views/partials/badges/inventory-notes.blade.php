@if ($inventory->notes)
    <span class="badge badge-pill badge-danger" title="{{ $inventory->notes }}">
        <small>{{ Str::limit($inventory->notes, 5) }}</small>
    </span>
@endif
