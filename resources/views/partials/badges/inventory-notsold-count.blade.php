@php
    $filtered_quantities = $inventory->inventoryQuantities->filter(function($inventory_quantity) {
        return !$inventory_quantity->ifStatusSold();
    });
@endphp

@if($inventory->inventoryQuantities->count() > 1)
    <span class="badge badge-{{ $inventory->ticketStatus->ticketStatus }}">
        {{ $filtered_quantities->sum('number_of_tickets') }}
    </span>
@else
    {{ $filtered_quantities->sum('number_of_tickets') }}
@endif

