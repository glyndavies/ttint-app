<span class="badge badge-pill @if($inventory_quantity->ticket_handling_id == 1) badge-secondary @else badge-info @endif badge-secondary">
    {{ $inventory_quantity->ticketHandlingStatus->ticketHandlingName }}
</span>
