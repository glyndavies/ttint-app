@if ($inventory_quantity->notes)
    <span class="badge badge-pill badge-success" title="{{ $inventory_quantity->notes }}">
        <small>{{ Str::limit($inventory_quantity->notes, 5) }}</small>
    </span>
@endif
