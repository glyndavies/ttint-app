<span class="badge badge-pill font-100 m-1 badge-{{ $inventory_quantity->ticketStatus->ticketStatus }}">
	{{ $inventory_quantity->number_of_tickets }}{{ strtoupper(substr($inventory_quantity->ticketStatus->ticketStatus, 0, 1)) }}
</span>
