@if($inventory->ifStatusSplit())
    @foreach($inventory->inventoryQuantities as $inventory_quantity)
        @include('partials.badges.inventory-quantity-status', [ 'inventory_quantity' => $inventory_quantity ])
    @endforeach
@else
    @include('partials.badges.inventory-quantity-status', [ 'inventory_quantity' => $inventory->inventoryQuantities->first() ])
@endif
