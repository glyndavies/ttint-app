@if ($purchase->inventoryQuantities->count() > 1)
    <span class="badge badge-secondary">{{ $purchase->number_of_tickets }}</span>
@else
    {{ $purchase->number_of_tickets }}
@endif
