@if ($purchase->companyRefund)
    @money($purchase->companyRefund->refunded_amount)
@else
    <button type="button" class="company-refund btn btn-info btn-sm text-white" data-toggle="modal" data-target="#myModal" data-id="{{ $purchase->inventory->id }}"  data-price="{{ round($purchase->cost, 2) }}">@money($purchase->cost)</button>
@endif