<div class="card bg-light w-100">
    <div class="card-header text-white bg-success p-2">Ticket Stock  <span class="badge-pill rounded-pill badge-dark float-right"> Batches {{ count($inventory->inventoryQuantities) }} </span>
    </div>

    <div class="card-body">
        @foreach($inventory->inventoryQuantities as $inventory_quantity)
            <form method="POST" action="{{ route('tickets.update_batch', $inventory_quantity->id) }}">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="number_of_tickets">Number of tickets {{ $inventory_quantity->ifStatusSold() ? ' (SOLD)': '' }}</label>
                            <input type="number" class="form-control" id="number_of_tickets"  name="number_of_tickets" value="{{ $inventory_quantity->number_of_tickets }}" {{ $inventory_quantity->ifStatusSold() ? 'readonly="yes"': '' }}>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="seats">Seats</label>
                            <input  class="form-control" id="seats" name="seats"  value="{{ $inventory_quantity->seats }}"  {{ $inventory_quantity->ifStatusSold() ? 'readonly="yes"': '' }}>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="sell_price">Sell Price</label>
                            <input  class="form-control" id="sell_price" value="{{ $inventory_quantity->sell_price }}" name="sell_price" {{ $inventory_quantity->ifStatusSold() ? 'readonly="yes"': '' }}>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <button data-toggle="modal" data-target="#saleModal" data-inventory="{{ $inventory->id }}" type="button" class="btn btn-info btn-sm text-white">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="mb-2  btn-block btn btn-primary float-right">update batch</button>
                            @if( ($inventory_quantity->number_of_tickets > 1) && ($inventory_quantity->ifStatusAvailable() || $inventory_quantity->ifStatusSwapped() || $inventory_quantity->ifStatusAvailableForSwap()))
                                <button class="mb-2 mt-0 btn-block btn btn-secondary float-right" type="button" data-toggle="modal" data-target="#splitModal"
                                    data-stock="{{ $inventory_quantity->id }}" data-tickets="{{ $inventory_quantity->number_of_tickets }}" data-seats="{{ $inventory_quantity->seats }}" data-price="{{ $inventory_quantity->sell_price }}">
                                    split batch
                                </button>
                            @endif
                        </div>                        
                    </div>
                </div>                
            </form>
            <hr>
        @endforeach
    </div>
</div>

<!-- Modal -->
@include('modals.split-modal')

<!-- Modal -->
@include('modals.sale-modal')
