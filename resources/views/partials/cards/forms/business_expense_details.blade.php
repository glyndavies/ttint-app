<div class="card w-100">
    <div class="card-header">Business Expense details</div>
    <div class="card-body">
        @if ($businessExpense->id)
        <form id="business_expense_details_form" method="post" action="{{ route('finances.business-expenses.update', ['id' => $businessExpense->id]) }}" autocomplete="off">
            @method('PUT')
        @else
        <form id="business_expense_details_form" method="post" action="{{ route('finances.business-expenses.create') }}" autocomplete="off">
            @endif
            @csrf
            <div class="row">
                <div class="form-group col-sm-2">
                    <label for="amout">Amount</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount" name="amount" value="{{ old('amount', $businessExpense->amount) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="vat">VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="vat" name="vat" value="{{ old('vat', $businessExpense->vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="vat">Amount including VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount_incl_vat" name="amount_incl_vat" disabled value="{{ old('amount_incl_vat', $businessExpense->amount_incl_vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-3">
                    <label for="category_id">Category</label>
                    <select class="form-control" id="category_id" name="category_id">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" @if($category->id == $businessExpense->category_id) selected @endif>
                            {{ $category->name }}
                        </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-3">
                    <label for="sub_category_id">Subcategory</label>
                    <select class="form-control" id="sub_category_id" name="sub_category_id">
                        <option value="" @if($businessExpense->sub_category_id === null) selected @endif></option>
                        @foreach($subcategories as $subcategory)
                            <option value="{{ $subcategory->id }}" @if($subcategory->id == $businessExpense->sub_category_id) selected @endif>
                                {{ $subcategory->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-3">
                    <label for="card_id">Card</label>
                    <select class="form-control" id="card_id" name="card_id">
                        @foreach($cards as $card)
                            <option value="{{ $card->id }}" @if($card->id == $businessExpense->card_id) selected @endif>
                                {{ $card->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-2">
                    <label for="date">Date</label>
                    <input class="form-control datepicker" id="date" name="date" value="{{ old('date', $businessExpense->date->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-7">
                    <label for="description">Description</label>
                    <input class="form-control" id="description" name="description" value="{{ old('description', $businessExpense->description) }}">
                </div>

            </div>

            <button type="submit" class="btn btn-primary float-right">
                @if($businessExpense->id) Update @else Create @endif
            </button>
        </form>
    </div>
</div>