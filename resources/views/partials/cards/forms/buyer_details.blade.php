<div class="card bg-light w-100">
    <div class="card-header text-white bg-info p-2">Buyers information</div>
    <div class="card-body">
        <form method="post" action="{{route('purchases.updateBuyerInfo', $inventory->purchase_number)}}">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="buyer_investment">Deposit rate </label>
                        <input type="" class="form-control" id="deposit_rate" name="deposit_rate" value="{{ optional($inventory->purchase)->deposit_rate }}" >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="buyer_investment">User Deposit </label>
                        <input type="" class="form-control" id="buyer_investment" name="buyer_investment" value="{{ optional($inventory->purchase)->buyer_investment }}" >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="buyer">Buyer</label>
                        <input type="" class="form-control" id="buyer" name="buyer"  value="{{ optional($inventory->purchase)->buyer}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="buyer">User name</label>
                        <input type="" class="form-control" id="card_used" name="card_used"  value="{{ optional($inventory->purchase)->card_used }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-primary btn-sm px-4 float-right align-bottom">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>