<div class="card w-100">
    <div class="card-header">Charge details</div>
    <div class="card-body">
        @if ($charge->id)
            <form method="post" action="{{ route('finances.charges.update', ['id' => $charge->id]) }}" autocomplete="off">
                @method('PUT')
        @else
            <form method="post" action="{{ route('finances.charges.create') }}" autocomplete="off">
        @endif
            @csrf
            <div class="row">

                <div class="form-group col-sm-2">
                    <label for="vat">Amount including VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount_incl_vat" name="amount_incl_vat"
                            value="{{ old('amount_incl_vat', $charge->amount_incl_vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="date">Date</label>
                    <input class="form-control datepicker" id="date" name="date"
                        value="{{ old('date', $charge->date->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-8">
                    <label for="sale_id_ref">Sale ID</label>
                    <input class="form-control" id="sale_id_ref" name="sale_id_ref"
                        value="{{ old('sale_id_ref', $charge->sale_id_ref) }}">
                </div>

                <div class="form-group col-sm-4">
                    <label for="site_sold">Site sold</label>
                    <input class="form-control" id="site_sold" name="site_sold"
                        value="{{ old('site_sold', $charge->site_sold) }}">
                </div>

                <div class="form-group col-sm-8">
                    <label for="event">Event</label>
                    <input class="form-control" id="event" name="event"
                        value="{{ old('event', $charge->event) }}">
                </div>
                
                <div class="form-group col-sm-4">
                    <label for="staff_code">Staff Code</label>
                    <input class="form-control" id="staff_code" name="staff_code"
                        value="{{ old('staff_code', $charge->staff_code) }}">
                </div>

                <div class="form-group col-sm-8">
                    <label for="reason">Reason</label>
                    <input class="form-control" id="reason" name="reason"
                        value="{{ old('reason', $charge->reason) }}">
                </div>

                <div class="form-group col-sm-12">
                    <label for="notes">Notes</label>
                    <input class="form-control" id="notes" name="notes"
                        value="{{ old('notes', $charge->notes) }}">
                </div>

            </div>

            <button type="submit" class="btn btn-primary float-right">
                @if($charge->id) Update @else Create @endif
            </button>
        </form>
   </div>
</div>
