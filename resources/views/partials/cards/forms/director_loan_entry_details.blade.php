<div class="card w-100">
    <div class="card-header">Director Loan Entry details</div>
    <div class="card-body">
        @if ($entry->id)
            <form method="post" action="{{ route('finances.director-loan.update', ['id' => $entry->id]) }}" autocomplete="off">
                @method('PUT')
        @else
            <form method="post" action="{{ route('finances.director-loan.create') }}" autocomplete="off">
        @endif
            @csrf
            <div class="row">

                <div class="form-group col-sm-2">
                    <label for="vat">Amount including VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount_incl_vat" name="amount_incl_vat"
                            value="{{ old('amount_incl_vat', $entry->amount_incl_vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="date">Date</label>
                    <input class="form-control datepicker" id="date" name="date"
                        value="{{ old('date', $entry->date->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-8">
                    <label for="notes">Notes</label>
                    <input class="form-control" id="notes" name="notes"
                        value="{{ old('notes', $entry->notes) }}">
                </div>

            </div>

            <button type="submit" class="btn btn-primary float-right">
                @if($entry->id) Update @else Create @endif
            </button>
        </form>
   </div>
</div>
