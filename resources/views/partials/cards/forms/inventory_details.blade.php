<div class="card bg-light w-100">
    <div class="card-header text-white bg-primary d-flex justify-content-between align-items-center p-1">
        <div>Ticket inventory details</div>
        {{--<button data-toggle="modal" data-target="#exampleModalLong" class="float-right btn btn-dark btn-sm">Notes: @if(!$inventory->thread) No @else Yes @endif</button>--}}
        @if ($inventory->notes)
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#noteModal">{{ 'Note: ' . $inventory->notes }}</button>
        @else
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#noteModal">No notes</button>
        @endif

    </div>
    <div class="card-body">
        <form method="POST" action="{{route('tickets.update',$inventory->id)}}">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" id="notes" name="notes" value="{{ $inventory->notes }}">
            @csrf

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="purchase_number">Transaction number</label>
                        <input type="text" class="form-control" id="purchase_number"  value="{{$inventory->purchase_number}}" readonly="yes">
                    </div>
                    <div class="col">
                        <label for="order_number">Order number</label>
                        <input type="text" class="form-control" id="order_number" name="order_number"  value="{{$inventory->order_number}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="event">Event title</label>
                        <input type="text" class="form-control" id="event" name="event"  value="{{$inventory->event}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="event_genre">Event Genre</label>
                                <select class="form-control" id="event_genre" name="event_genre">
                                    <option selected   value="{{$inventory->event_genre}}">{{$inventory->event_genre}}</option>
                                    @foreach(EventGenreRepository::getActive() as $genre)
                                        @if($inventory->event_genre != $genre->genre)<option value="{{$genre->genre}}">{{$genre->genre}}</option> @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <label for="arena">Arena</label>
                                <input type="text" class="form-control" id="arena" name="arena"  value="{{$inventory->arena}}" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="type_of_ticket">Type of ticket</label>

                        <select class="form-control" id="type_of_ticket" name="type_of_ticket">
                            <option  value="{{$inventory->type_of_ticket}}">{{$inventory->type_of_ticket}}</option>

                            @foreach(TicketTypeRepository::getActive() as $ticket_type)

                                @if($inventory->type_of_ticket != $ticket_type->tickettype)
                                    <option  value="{{$ticket_type->tickettype}}">{{$ticket_type->tickettype}}</option>
                                @endif

                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <label for="country">Country</label>
                        <input type="text" class="form-control" id="country" name="country"  value="{{$inventory->country}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="time_of_event">Site Purchased</label>
                                <input  class="form-control" id="site_purchased"  name="site_purchased" value="{{$inventory->site_purchased}}" >
                            </div>
                            <div class="col">
                                <label for="restrictions">Restrictions</label>
                                <input  class="form-control" id="restrictions"  name="restrictions" value="{{$inventory->restrictions ?? 'N/A'}}" >
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="event_date">Event date: {{Carbon\Carbon::parse($inventory->event_date)->isoFormat('ddd')}} {{$inventory->event_date->format('d/m/Y')}}</label>
                                <input type="date" class="form-control col-12" id="event_date" name="event_date"  value="{{$inventory->event_date->format('Y-m-d')}}">
                            </div>
                            <div class="col">
                                <label for="time_of_event">Start Time</label>
                                <input type="time" class="form-control col-12" id="time_of_event"  name="time_of_event" value="{{$inventory->time_of_event}}" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="referral_1">Referral 1</label>
                        <input class="form-control" id="referral_1"  name="referral_1" value="{{ $inventory->referral_1 }}">
                    </div>
                    <div class="col">
                        <label for="referral_2">Referral 2</label>
                        <input class="form-control" id="referral_2"  name="referral_2" value="{{ $inventory->referral_2 }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="listed">Listed</label>
                        <input class="form-control" id="listed"  name="listed" value="{{ $inventory->listed }}">
                    </div>
                    <div class="col">
                        <label for="origin_status">Origin status</label>
                        <input class="form-control" id="origin_status"  name="origin_status" value="{{ $inventory->origin_status }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="selling_rate">Selling rate</label>
                        <input class="form-control" id="selling_rate"  name="selling_rate" value="{{ $inventory->selling_rate }}">
                    </div>
                    <div class="col">
                        <label for="paid_amount">Paid amount</label>
                        <input class="form-control" id="paid_amount"  name="paid_amount" value="{{ $inventory->paid_amount }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row align-items-end">
                    <div class="col">
                        <button type="submit" class="btn px-4 btn-primary float-right">update details</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-down" id="noteModal" tabindex="-1" role="dialog" aria-labelledby="noteModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="noteModalTitle">Notes  - {{$inventory->purchase_number}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <label for="modal_notes">Ticket Note</label>
                        <input class="form-control" id="modal_notes" value="{{ $inventory->notes }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="closeNoteModal()">Close</button>
                <button type="button" class="btn btn-primary" onclick="applyNoteModal({{$inventory->id}})">Apply</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#noteModal').on('shown.bs.modal', () => {
        // set focus
        $('#modal_notes').focus()
        toggleDetailModal();
    });

    $('#noteModal').on('hidden.bs.modal', function () {
        toggleDetailModal();
    });

    function applyNoteModal(inventory_id){
        var notesVal = $('#modal_notes').val()
        // change note
        $('#notes').val($('#modal_notes').val())
        $('button[data-target="#noteModal"]')
            .text('Note: ' + notesVal)
            .addClass('btn-danger')
        $('#noteModal').modal('hide')

        $.get('/api/ticketInfo/inventoryNotesTicketUpdate/'+inventory_id+'/'+notesVal).then((response) =>{
        })
    };

    function closeNoteModal(){
        $('#noteModal').modal('hide');
    }

    function toggleDetailModal(){
        $('#detailsModal').toggleClass('modal-down');
        $('body').toggleClass('modal-open');
    }
</script>
