<div class="card w-100">
    <div class="card-header">Payment details</div>
    <div class="card-body">
        @if ($payment->id)
            <form method="post" action="{{ route('finances.payments.update', ['id' => $payment->id]) }}" autocomplete="off">
                @method('PUT')
        @else
            <form method="post" action="{{ route('finances.payments.create') }}" autocomplete="off">
        @endif
            @csrf
            <div class="row">

                <div class="form-group col-sm-2">
                    <label for="vat">Amount including VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount_incl_vat" name="amount_incl_vat"
                            value="{{ old('amount_incl_vat', $payment->amount_incl_vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-8">
                    <label for="user_id">User</label>
                    <select class="form-control platform-user-id" id="user_id" name="user_id">
                        @foreach($platformUsers as $platformUser)
                            <option value="{{ $platformUser->id }}"
                                @if($platformUser->id == $payment->user_id) selected @endif>
                                {{ $platformUser->full_name_with_code }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-2">
                    <label for="date">Date</label>
                    <input class="form-control datepicker" id="date" name="date"
                        value="{{ old('date', $payment->date->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-12">
                    <label for="notes">Notes</label>
                    <input class="form-control" id="notes" name="notes"
                        value="{{ old('notes', $payment->notes) }}">
                </div>

            </div>

            <button type="submit" class="btn btn-primary float-right">
                @if($payment->id) Update @else Create @endif
            </button>
        </form>
   </div>
</div>
