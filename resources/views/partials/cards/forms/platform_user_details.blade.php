<div class="card w-100">
    <div class="card-header">User details</div>
    <div class="card-body">
        <form method="post" action="{{ route('platform-users.update', ['id' => $platformUser->id]) }}" autocomplete="off">
            @csrf
            @method('PUT')

            <div class="row">

                <div class="form-group col-sm-2">
                    <label for="unique_id">Unique ID</label>
                    <input class="form-control" id="unique_id" name="unique_id" value="{{ old('unique_id', $platformUser->unique_id) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="first_name">First Name</label>
                    <input class="form-control" id="first_name" name="first_name" value="{{ old('first_name', $platformUser->first_name) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="last_name">Last Name</label>
                    <input class="form-control" id="last_name" name="last_name" value="{{ old('last_name', $platformUser->last_name) }}">
                </div>

                <div class="form-group col-sm-1">
                    <label for="user_code">User Code</label>
                    <input class="form-control" id="user_code" name="user_code" value="{{ old('user_code', $platformUser->user_code) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="password">Password</label>
                    <input class="form-control" id="password" name="password" value="{{ old('password', $platformUser->password) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label for="desired_events_raw">Desired events</label>
                    <input type="text" class="form-control desired-events" id="desired_events_raw" name="desired_events_raw" value="{{ old('desired_events_raw', $platformUser->desired_events_raw) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label for="email_1">Email 1</label>
                    <input class="form-control" id="email_1" name="email_1" value="{{ old('email_1', $platformUser->email_1) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label for="email_2">Email 2</label>
                    <input class="form-control" id="email_2" name="email_2" value="{{ old('email_2', $platformUser->email_2) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label for="phone">Phone</label>
                    <input class="form-control" id="phone" name="phone" value="{{ old('phone', $platformUser->phone) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label for="date_of_birth">Date of birth</label>
                    <input class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="{{ old('date_of_birth', optional($platformUser->date_of_birth)->format('Y-m-d')) }}">
                </div>

            </div>

            <div class="row">
                <div class="form-group col-sm-2">
                    <label for="address_line_1">Address line 1</label>
                    <input class="form-control" id="address_line_1" name="address_line_1" value="{{ old('address_line_1', $platformUser->address_line_1) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="address_line_2">Address line 2</label>
                    <input class="form-control" id="address_line_2" name="address_line_2" value="{{ old('address_line_2', $platformUser->address_line_2) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="address_city">City</label>
                    <input class="form-control" id="address_city" name="address_city" value="{{ old('address_city', $platformUser->address_city) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="address_state">State</label>
                    <input class="form-control" id="address_state" name="address_state" value="{{ old('address_state', $platformUser->address_state) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="address_zip">Zip</label>
                    <input class="form-control" id="address_zip" name="address_zip" value="{{ old('address_zip', $platformUser->address_zip) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="country">Country</label>
                    <input class="form-control" id="country" name="country" value="{{ old('country', $platformUser->country) }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-2">
                    <label for="referral_1">Referral 1</label>
                    <input class="form-control" id="referral_1" name="referral_1" value="{{ old('referral_1', $platformUser->referral_1) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="referral_2">Referral 2</label>
                    <input class="form-control" id="referral_2" name="referral_2" value="{{ old('referral_2', $platformUser->referral_2) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="ip_address">IP address</label>
                    <input class="form-control" id="ip_address" name="ip_address" value="{{ old('ip_address', $platformUser->ip_address) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label for="browser">Browser</label>
                    <input class="form-control" id="browser" name="browser" value="{{ old('browser', $platformUser->browser) }}">
                </div>

                <div class="form-group col-sm-3">
                    <label>Location</label>
                    <div class = "row">
                        <div class="col pr-0">
                            <input class="form-control" id="location_0" name="location_0" value="{{ old('location_0', $platformUser->location[0] ?? '') }}">
                        </div>
                        <div class="col pl-0">
                            <input class="form-control" id="location_1" name="location_1" value="{{ old('location_1', $platformUser->location[1] ?? '') }}">
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="filled_at">Filled at</label>
                    <input class="form-control datepicker" id="filled_at" name="filled_at" value="{{ old('filled_at', optional($platformUser->filled_at)->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-2">
                    <label for="left_at">Left at</label>
                    <input class="form-control datepicker" id="left_at" name="left_at" value="{{ old('left_at', optional($platformUser->left_at)->format('Y-m-d')) }}">
                </div>

            </div>

            <hr>

            <div class="row">
                @php
                    $checkboxes = [
                        'presale_information' => 'Presale Information',
                        'events_alert_service' => 'Events Alert Service',
                        'express_transfer_service' => 'Express Transfer Service',
                        'weekly_updates' => 'Weekly Updates',
                        'weekly_portfolio' => 'Weekly Portfolio',
                        'whatsapp_alerts' => 'Whatsapp Alerts',
                        'share_desired_events' => 'Share desired events details',
                        'permission_to_contact' => 'Permission to contact authorised third party',
                        'tcs_company_policies' => 'T&Cs & Company Policies',
                        'portfolio_needed' => 'Portfolio Needed',                        
                    ];
                @endphp
                @foreach($checkboxes as $field => $title)
                    <div class="form-group col-sm-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{ $field }}" name="{{ $field }}" value="1" @if(old($field, $platformUser->$field)) checked @endif>
                            <label class="custom-control-label" for="{{ $field }}">
                                {{ $title }}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>

            <hr>

            <button type="submit" class="btn btn-primary float-right">Update</button>
        </form>
   </div>
</div>