
@if ($purchase)
    <div class="card bg-light w-100">
        <div class="card-header p-2">Ticket Value</div>
        <div class="card-body">
        <form method="post" action="{{ route('purchases.update', $purchase->id) }}">
            @csrf
            <input type="hidden" class="form-control" name="purchase_number" id="purchase_number"  value="{{ $purchase->purchase_number }}" readonly="yes">

                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="purchase_date">Purchase date</label>
                            <input type="date" class="form-control" id="purchase_date" name="purchase_date" value="{{ innerDateFormat($purchase->purchase_date) }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="buyers_fees">Buyers fees</label>
                            <input type="" class="form-control" id="buyers_fees" name="buyers_fees"  value="{{ $purchase->buyers_fees }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="face_value_of_tickets">Face value of tickets</label>
                            <input type="" class="form-control" id="face_value_of_tickets" name="face_value_of_tickets"  value="{{ $purchase->face_value_of_tickets }}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="face_value_per_ticket">Face value per ticket</label>
                            <h3>@money($purchase->face_value_per_ticket)</h3>
                        </div>
                        <div class="col">
                            <label for="total_cost_per_ticket"> Total cost per ticket</label>
                            <h3>@money($purchase->cost_per_ticket)</h3>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <div class="row align-items-end" >
                        <div class="col">
                            <label for="total_cost"><h3>Total cost </h3></label>
                            <h3>@money($purchase->cost)</h3>
                        </div>
                        <div class="col align-bottom">
                        <button type="submit" class="btn btn-primary btn-sm px-4 float-right align-bottom">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@else
<div class="card bg-light w-100">
    <div class="card-header p-2">Ticket Value !!!</div>
    <div class="card-body">Error in data</div>
</div>
@endif