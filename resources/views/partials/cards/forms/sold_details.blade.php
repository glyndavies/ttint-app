<div class="card bg-light w-100">

    <div class="card-header   {{ $soldTicket->is_paid == 'Y' ? 'bg-success':'bg-danger' }} ">
        <h6 class="text-white">Sold information : <span class="float-right"> Paid ? =  {{ $soldTicket->is_paid == 'Y' ? 'YES':'NO' }}  </span> </h6>
    </div>

    <div class="card-body">
        <form method="POST" action="{{route('delivered.update',$soldTicket->id)}}">
            <input type="hidden" name="_method" value="PUT">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="purchase_number">Sold Ref</label>
                        <input type="text" class="form-control" id="sale_id_ref" name="sale_id_ref" value="{{$soldTicket->sale_id_ref}}">
                    </div>                    
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="ordernumber">Site  Sold</label>
                        <input type="text" class="form-control" id="" name="site_sold" value="{{$soldTicket->site_sold}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="event">Sold Date</label>
                        <input type="text" class="form-control" id="event" name="sold_date" value="{{$soldTicket->sold_date}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="seats">Seats</label>
                                <input type="text" class="form-control" id="seats" name="seats"  value="{{$soldTicket->seats}}" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <label for="Arena">Ticket Qty Price</label>
                                <input type="text" class="form-control" id="Arena" name="ticket_qty"  value="{{$soldTicket->ticket_qty}}" >
                            </div>
                            <div class="col">
                                <label for="Arena">Sell Price</label>
                                <input type="text" class="form-control" id="Arena" name="sell_price"  value="{{$soldTicket->sell_price}}" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary float-right">update details</button>
        </form>
    </div>
</div>