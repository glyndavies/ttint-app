<div class="card w-100">
    <div class="card-header p-2">Swap information</div>
    <div class="card-body">
        <table class="table">
            <tr>
                <th>Change date</th>
                <th>Old user code</th>
                <th>New user code</th>
            </tr>
            @foreach($inventory->swap_history as $swap)
            <tr class="small">
                <td>{{ $swap['created_at']->format('Y-m-d') }}</td>
                <td>{{ $swap['old_buyer'] }}</td>
                <td>{{ $swap['new_buyer'] }}</td>
            </tr>
            @endforeach
        </table>

        @php
            if ($inventory->ifStatusAvailableForSwap()) {
                $button = "Mark as Sell";
                $formAction = route('swap.available', ['id' => $inventory->id]);
            } else {
                $button = "Update";
                $formAction = route('swap.change', ['id' => $inventory->id]);
            }
        @endphp

        <form method="post" action="{{ $formAction }}">
            @csrf
            <div class="form-group row">
                <div class="col">
                    <label for="transfer_price">Transfer price</label>
                    <input class="form-control" id="transfer_price" name="transfer_price" value="{{ old('transfer_price', $inventory->transfer_price) }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col d-flex justify-content-between align-items-center">
                    <label for="reconciled">Reconciled</label>
                    <input class="form-control" id="reconciled" name="reconciled" type="checkbox" data-onstyle="info" data-on="yes" data-off="no" @if (old('reconciled', $inventory->reconciled)) checked @endif>
                    <span style="display: none" ></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col">
                    <label for="owner_code">Owner code</label>
                    <input class="form-control" id="owner_code" name="owner_code" value="{{ old('owner_code', $inventory->buyer) }}">
                </div>
            </div>

            @if ($button)
                <button type="submit" class="btn btn-sm px-4 btn-primary float-right">
                    {{ $button }}
                </button>
            @endif
        </form>
   </div>
</div>
<script>

    $('#reconciled').bootstrapToggle({
        on: 'yes',
        off: 'no',
        onstyle: 'info',
        width: '65px'
    });

</script>