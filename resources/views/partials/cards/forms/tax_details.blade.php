<div class="card w-100">
    <div class="card-header">Tax details</div>
    <div class="card-body">
        @if ($tax->id)
            <form method="post" action="{{ route('finances.taxes.update', ['id' => $tax->id]) }}" autocomplete="off">
                @method('PUT')
        @else
            <form method="post" action="{{ route('finances.taxes.create') }}" autocomplete="off">
        @endif
            @csrf
            <div class="row">

                <div class="form-group col-sm-2">
                    <label for="vat">Amount including VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount_incl_vat" name="amount_incl_vat"
                            value="{{ old('amount_incl_vat', $tax->amount_incl_vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="date">Date</label>
                    <input class="form-control datepicker" id="date" name="date"
                        value="{{ old('date', $tax->date->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-8">
                    <label for="notes">Notes</label>
                    <input class="form-control" id="notes" name="notes"
                        value="{{ old('notes', $tax->notes) }}">
                </div>

            </div>

            <button type="submit" class="btn btn-primary float-right">
                @if($tax->id) Update @else Create @endif
            </button>
        </form>
   </div>
</div>
