<div class="card w-100">
    <div class="card-header">User charge details</div>
    <div class="card-body">
        @if ($userCharge->id)
            <form method="post" action="{{ route('finances.user-charges.update', ['id' => $userCharge->id]) }}" autocomplete="off">
                @method('PUT')
        @else
            <form method="post" action="{{ route('finances.user-charges.create') }}" autocomplete="off">
        @endif
            @csrf
            <div class="row">

                <div class="form-group col-sm-2">
                    <label for="amout">Amount</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount" name="amount"
                            value="{{ old('amount', $userCharge->amount) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="vat">VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="vat" name="vat"
                            value="{{ old('vat', $userCharge->vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-2">
                    <label for="vat">Amount including VAT</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">£</div>
                        </div>
                        <input type="number" step="0.01" class="form-control" id="amount_incl_vat"
                            name="amount_incl_vat" disabled
                            value="{{ old('amount_incl_vat', $userCharge->amount_incl_vat) }}">
                    </div>
                </div>

                <div class="form-group col-sm-4">
                    <label for="user_id">User</label>
                    <select class="form-control platform-user-id" id="user_id" name="user_id">
                        @foreach($platformUsers as $platformUser)
                            <option value="{{ $platformUser->id }}"
                                @if($platformUser->id == $userCharge->user_id) selected @endif>
                                {{ $platformUser->full_name_with_code }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-2">
                    <label for="date">Date</label>
                    <input class="form-control datepicker" id="date" name="date"
                        value="{{ old('date', $userCharge->date->format('Y-m-d')) }}">
                </div>

                <div class="form-group col-sm-12">
                    <label for="description">Description</label>
                    <input class="form-control" id="description" name="description"
                        value="{{ old('description', $userCharge->description) }}">
                </div>

            </div>

            <button type="submit" class="btn btn-primary float-right">
                @if($userCharge->id) Update @else Create @endif
            </button>
        </form>
   </div>
</div>
