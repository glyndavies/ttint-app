@php
	$period = getSessionPeriod();
@endphp

<form class="form-inline">
    <input class="form-control datepicker" style="width: 7rem;" id="period-date1" name="period-date1" value="{{ old('$period["date1"]', $period["date1"]->format('Y-m-d')) }}" autocomplete="off">
    <span class="text-sm ml-1">-</span>
    <input class="form-control datepicker ml-1" style="width: 7rem;" id="period-date2" name="period-date2" value="{{ old('$period["date2"]', $period["date2"]->format('Y-m-d')) }}" autocomplete="off">
    <button class="btn btn-sm btn-light text-primary ml-1" type="submit"><i class="fas fa-sync-alt"></i></button>
</form>