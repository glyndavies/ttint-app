@if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible mx-3 mt-2 mb-0 fade show" role="alert">
        <p class="mb-0">{!! Session::get('message') !!}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
