@if ($errors->any())
    <div class="alert alert-danger alert-dismissible mx-3 mt-2 mb-0 fade show" role="alert">
        <ol class="my-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>        
    </div>
@endif
