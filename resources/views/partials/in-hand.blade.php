<form method="POST" action="{{ route('tickets.changeInHandMass') }}">
    @csrf
    <textarea style="display: none;" id="massSelection" name="batchID"></textarea>
    <button class="btn btn-primary" type="submit"> Change in hand </button>
</form>
