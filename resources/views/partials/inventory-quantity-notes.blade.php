@if ($inventory_quantity->notes)
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inventoryQuantNoteModal">{{ 'Note: ' . $inventory_quantity->notes }}</button>
@else
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#inventoryQuantNoteModal">No notes</button>
@endif

<!-- Modal -->
<div class="modal fade modal-down" id="inventoryQuantNoteModal" tabindex="-1" role="dialog" aria-labelledby="inventoryQuantNoteModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inventoryQuantNoteModalTitle">Notes  - {{$inventory_quantity->purchase_number}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <label for="inventory_quant_modal_notes">Ticket Note</label>
                        <input class="form-control" id="inventory_quant_modal_notes" value="{{ $inventory_quantity->notes }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="closeInventoryQuantNoteModal()">Close</button>
                <button type="button" class="btn btn-primary" onclick="applyInventoryQuantNoteModal({{$inventory_quantity->id}})">Apply</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#inventoryQuantNoteModal').on('shown.bs.modal', () => {
        // set focus
        $('#modal_notes').focus()
        toggleDetailModal();
    });

    $('#inventoryQuantNoteModal').on('hidden.bs.modal', function () {
        toggleDetailModal();
    });

    function applyInventoryQuantNoteModal(inventory_quantity_id){
        var notesVal = $('#inventory_quant_modal_notes').val()
        $('button[data-target="#inventoryQuantNoteModal"]')
            .text('Note: ' + notesVal)
            .addClass('btn-success')
        $('#inventoryQuantNoteModal').modal('hide')

        $.get('/api/ticketInfo/inventoryQuantityNotesTicketUpdate/'+inventory_quantity_id+'/'+notesVal).then((response) =>{
        })
    };

    function closeInventoryQuantNoteModal(){
        $('#inventoryQuantNoteModal').modal('hide');
    }

    function toggleDetailModal(){
        $('#detailsModal').toggleClass('modal-down');
        $('body').toggleClass('modal-open');
    }
</script>
