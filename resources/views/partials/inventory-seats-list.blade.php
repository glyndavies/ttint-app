@foreach($inventory_quantities as $available)
    @if($available->ifStatusAvailable())
        <span>{{ $available->seats }}</span><br>
    @endif
@endforeach