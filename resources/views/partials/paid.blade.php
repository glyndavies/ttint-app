<form method="POST" action="/">
    @csrf

    <textarea style="display: none;" id="massSelection" name="batchID"></textarea>



    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ispaid" id="inlineRadio1" value="0" required>
        <label class="form-check-label" for="inlineRadio1">Unpaid</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ispaid" id="inlineRadio2" value="1" required>
        <label class="form-check-label" for="inlineRadio2">Paid</label>
    </div>


    <button class="btn btn-primary" type="submit"> save </button>

</form>
