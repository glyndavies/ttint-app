<p>Select <b>stage</b> want to move the ticket to:</p>
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <label class="btn btn-danger" for="moveTicketToState">Refund to:</label>
  </div>
  <select class="custom-select" id="moveTicketToState">
    <option selected value="delivered">Delivered</option>
    <option value="sold">Sold</option>
    <option value="inventory">Inventory</option>
    <option value="expired">Expired</option>
  </select>
</div>