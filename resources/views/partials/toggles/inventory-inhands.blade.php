@php 
	$checked = $inventory->inventoryQuantities->where('tickets_in_hand', 2)->count() == $inventory->inventoryQuantities->count();
@endphp
<input type="checkbox" class="inventoryInHand" data-toggle="_toggle" data-onstyle="info" data-on="yes" data-off="no" id="inhand{{ $inventory->id }}" data-id="{{ $inventory->id }}"
	@if ($checked) checked @endif>
@if ($checked)
    <span class="d-none">Yes</span>
@else
    <span class="d-none">No</span>
@endif
