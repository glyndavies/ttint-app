<button class="btn @if ($inventory_quantity->ifListedOnResale()) btn-warning unlistOnResale @else btn-sm listOnResale @endif" data-toggle="_toggle" data-onstyle="info" data-on="yes" data-off="no" id="event_listed_on_resale_{{ $inventory_quantity->id }}" data-id="{{ $inventory_quantity->id }}">
    @if($inventory_quantity->ifListedOnResale())
        YES
    @else
        NO
    @endif
</button>
