<button class="btn @if ($inventory_quantity->ifPendingCancelled()) btn-danger restorePendingCancelled @else btn-sm pendingCancel @endif" data-toggle="_toggle" data-onstyle="info" data-on="yes" data-off="no" id="event_cancelled_{{ $inventory_quantity->id }}" data-id="{{ $inventory_quantity->id }}">
    @if($inventory_quantity->ifPendingCancelled())
        YES
    @else
        NO
    @endif
</button>
