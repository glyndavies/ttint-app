<input type="checkbox" class="ticketHandlingRevers toggle" data-toggle="_toggle" data-onstyle="info" data-on="Y" data-off="N" data-id="{{ $inventory_quantity->id }}" data-hide="{{ $hide }}" @if ($inventory_quantity->ifHandlingStarted()) checked @endif>
@if ($inventory_quantity->ifHandlingStarted())
    <span class="d-none">Yes</span>
@else
    <span class="d-none">No</span>
@endif