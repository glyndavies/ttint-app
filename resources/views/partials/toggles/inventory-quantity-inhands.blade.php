<input type="checkbox" class="ticketsInHand" data-toggle="_toggle" data-onstyle="info" data-on="yes" data-off="no" id="inhand{{ $inventory_quantity->id }}" data-id="{{ $inventory_quantity->id }}" @if ($inventory_quantity->tickets_in_hand == 2) checked @endif>
@if ($inventory_quantity->tickets_in_hand == 2)
    <span class="d-none">Yes</span>
@else
    <span class="d-none">No</span>
@endif
