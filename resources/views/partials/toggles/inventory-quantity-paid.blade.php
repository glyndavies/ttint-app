<input type="checkbox" class="isPaid" data-toggle="_toggle" data-onstyle="info" data-on="Y" data-off="N" data-id="{{ $inventory_quantity->soldTicket->id }}" @if ($inventory_quantity->soldTicket->is_paid== 'Y') checked @endif>
@if ($inventory_quantity->soldTicket->is_paid== 'Y')
    <span class="d-none">Yes</span>
@else
    <span class="d-none">No</span>
@endif