<div class="list-group">
    @foreach($kpis as $kpi)
    <div class="list-group-item list-group-item-action">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $kpi->buyer }}</h5>
            <div class="d-flex align-items-center">
                <span class="text-right">{{ moneyformat($kpi->value) }}</span>
                <span class="text-right ml-3">
                    <small class="text-muted">{{ $kpi->rank }}</small>
                    {{--@if ($kpi->previous && $kpi->previous->rank < $kpi->rank)
                        <span class="text-success position-absolute">&uarr;</span>
                    @elseif ($kpi->previous && $kpi->previous->rank > $kpi->rank)
                        <span class="text-danger position-absolute">&darr;</span>
                    @endif--}}
                </span>
            </div>
        </div>
    </div>
    @endforeach
</div>
