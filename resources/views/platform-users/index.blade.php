@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>

@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Platform Users</h1>

        <div>
            <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Unique ID</th>
                        <th>Full Name</th>
                        <th>Emails</th>
                        <th>Date Of Birth</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Desired Events</th>
                        <th>User Code</th>
                        <th>Left at</th>
                        <th>Portfolio Needed</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($platformUsers as $platformUser)
                    <tr class="small">
                        <td>{{ $platformUser->unique_id }}</td>
                        <td>{{ $platformUser->full_name }}</td>
                        <td>{{ $platformUser->emails->implode(', ') }}</td>
                        <td class="text-nowrap">{{ optional($platformUser->date_of_birth)->format('Y-d-m') }}</td>
                        <td>{{ $platformUser->full_address }}</td>
                        <td>{{ $platformUser->phone }}</td>
                        <td>{{ implode(', ', $platformUser->desired_events) }}</td>
                        <td>{{ $platformUser->user_code }}</td>
                        <td class="text-nowrap">{{ optional($platformUser->left_at)->format('Y-m-d') }}</td>
                        <td>{{ $platformUser->portfolio_needed ? 'yes' : 'no' }}</td>
                        <td class="text-nowrap text-right">
                            <a href="{{ route('platform-users.show',$platformUser->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>
                            <form method="POST" action = "{{ route('platform-users.delete', ['id' => $platformUser->id]) }}" class="d-inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="button" id="deletePlatformUser" class="btn btn-xs btn-danger submit-with-danger"><i class="far fa-trash-alt"></i></button>
                            </form>                                    
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('page_level_js')
@endsection

