@extends('layouts.app')

@section('page_level_css')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-12">
            <h1 class="mt-3">Platform User</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('platform-users') }}">Users</a></li>
                <li class="breadcrumb-item active">User</li>
            </ol>
        </div>
    </div>

    @include('partials.cards.forms.platform_user_details', ['platformUser' => $platformUser])
</div>
@endsection