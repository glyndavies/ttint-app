@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Purchases</h1>

        <div class="row justify-content-center mb-1">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body text-right">
                        <h4>
                        <span class="text-muted small text-uppercase">Total cost &#163;</span>
                        <span class="card-title pricing-card-title total-cost">{{ number_format($totalCost, 2, '.', ',') }}</span>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body text-right">
                        <h4>
                        <span class="text-muted small text-uppercase">Filtered total cost &#163;</span>
                        <span class="card-title pricing-card-title filtered-cost">{{ number_format($totalCost, 2, '.', ',') }}</span>
                        </h4>
                        <small class="text-small showing-status"></small>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <table class="table table-bordered table-searchable small" id="purchases-table" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkall"> All </th>
                    <th>Transaction Number</th>
                    <th>Order Number</th>
                    <th>Event Genre</th>
                    <th>Event</th>
                    <th>Arena</th>
                    <th>Country</th>
                    <th>Site Purchased</th>
                    <th>Type of Ticket</th>
                    <th>Purchased Date</th>
                    <th class="datepicker">Event Date</th>
                    <th>Time of Event</th>
                    <th>Day of event</th>
                    <th>Number of tickets</th>
                    <th class="lg-width">Restrictions</th>
                    <th>Seats</th>
                    <th>Facevalue Per Ticket</th>
                    <th>Facevalue of tickets</th>
                    <th>Buyers Fees</th>
                    <th>User Code</th>
                    <th>User Deposit</th>
                    <th>User Name</th>
                    <th>Total Cost per Ticket</th>
                    <th>Total Cost</th>
                    <th class="lg-width">Tickets in Hand</th>
                    <th>Broken Limit</th>
                    <th>Insurance</th>
                    <th>Inputer</th>
                    <th>Pending Reconciled</th>
                    <th>Reconciled</th>
                    <th>States Ticket</th>
                    <th>Refund</th>          
                    <th>Paid Amount</th>          
                </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

            <div class="row">
                <div class="col my-2">
                    <button type="button" class="btn btn-sm btn-dark" id="massChangeBtn" data-toggle="modal" data-target="#saleModal">Mass Reconcile</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModal" aria-hidden="true" >
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-0">
                    <form method="POST" action="{{ route('purchases.reconcileMass') }}">
                        @csrf
                        <textarea style="display: none;" id="massSelection" name="batchID"></textarea>
                        <button type="submit" class="btn btn-dark">Reconcile</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_level_js')
    @include('modals.refund-purchase-modal')
@endsection
