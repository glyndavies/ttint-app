@extends('layouts.app')

@section('page_level_css')

@endsection

@section('content')

    <div class="container-fluid">
        <h1 class="mt-4"> PN:{{$purchase->purchase_number}}  -  {{$purchase->event}} -  Event Date:  {{$purchase->inventory->event_date->isoformat('dddd')}} {{$purchase->inventory->event_date->format('d/m/Y')}}  </h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Tickets</a></li>
            <li class="breadcrumb-item active">Ticket</li>
        </ol>


        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="card bg-light mb-3 w-100">
                            <div class="card-header text-white bg-primary">Ticket inventory details </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="purchase_number">Purchase number</label>
                                            <input type="text" class="form-control" id="purchase_number"  value="{{$purchase->purchase_number}}" readonly="yes">
                                        </div>
                                        <div class="col">
                                            <label for="ordernumber">Order number</label>
                                            <input type="text" class="form-control" id="ordernumber"  value="{{$purchase->order_number}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                    <label for="Event_Genre">Event Genre</label>
                                    <select class="form-control" id="Event_Genre">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                        </div>
                                        <div class="col">
                                            <label for="event">Event</label>
                                            <input  class="form-control" id="event" value="{{$purchase->event}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="Arena">Arena</label>
                                            <input type="text" class="form-control" id="Arena"  value="{{$purchase->arena}}" >
                                        </div>
                                        <div class="col">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" id="country"  value="{{$purchase->country}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="type_of_ticket">Type of ticket</label>
                                            <input type="text" class="form-control" id="type_of_ticket"  value="{{$purchase->type_of_ticket}}" >
                                        </div>
                                        <div class="col">
                                            <label for="event_date">Event date</label>
                                            <input type="text" class="form-control" id="event_date"  value="{{$purchase->event_date->format('d/m/Y')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="time_of_event">Start Time</label>
                                            <input type="time" class="form-control" id="time_of_event"  value="{{$purchase->time_of_event}}" >
                                        </div>
                                        <div class="col">


                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="card bg-light mb-3 w-100">
                                    <div class="card-header text-white bg-info">Buyers information</div>
                                    <div class="card-body">
                                        <form method="post" action="{{route('purchases.updateBuyerInfo',$purchase->purchase_number)}}">
                                            @csrf
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="buyer_investment">Buyers Investment </label>
                                                    <input type="" class="form-control" id="buyer_investment" name="buyer_investment" value="{{$purchase->purchase->buyer_investment}}" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="buyers_group">Buyers Group</label>
                                                    <input type="" class="form-control" id="buyers_group" name="buyers_group"  value="{{ $purchase->purchase->buyers_group }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="buyer">Buyer</label>
                                                    <input type="" class="form-control" id="buyer" name="buyer"  value="{{ optional($purchase->purchase)->buyer }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <button type="submit" class="btn btn-primary btn-sm float-right align-bottom">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                        <div class="card bg-light mb-3 w-100">
                            <div class="card-header">Ticket Value</div>
                            <div class="card-body">
                               <form method="post" action="{{route('purchases.update', $purchase->id)}}">
                                   @csrf
                                   <input type="hidden" class="form-control" name="purchase_number" id="purchase_number"  value="{{$purchase->purchase_number}}" readonly="yes">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="purchase_date">Purchase date</label>
                                            <input type="" class="form-control" id="purchase_date" name="purchase_date" value="{{$purchase->purchase_date}}" readonly="yes">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="buyers_fees">buyers fees</label>
                                            <input type="" class="form-control" id="buyers_fees" name="buyers_fees"  value="{{$purchase->buyers_fees}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="face_value_of_tickets">Face value of tickets</label>
                                            <input type="" class="form-control" id="face_value_of_tickets" name="face_value_of_tickets"  value="{{$purchase->face_value_of_tickets}}">
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="face_value_per_ticket">Face value per ticket</label>

                                            <h3> &pound;{{number_format($purchase->face_value_of_tickets/$purchase->number_of_tickets ,2)}} </h3>


                                        </div>
                                        <div class="col">
                                            <label for="total_cost_per_ticket"> Total cost per ticket</label>
                                            @php  $total = $purchase->face_value_of_tickets+$purchase->buyers_fees;  @endphp
                                            <h3>  {{number_format($total/$purchase->number_of_tickets,2)}}</h3>
                                        </div>
                                    </div>

                                </div>

                                <hr>
                                <div class="form-group">
                                    <div class="row align-items-end" >
                                        <div class="col">
                                            <label for="total_cost"><h3> Total cost </h3></label>
                                            <h3> &pound;{{number_format($purchase->face_value_of_tickets+$purchase->buyers_fees,2)}} </h3>
                                        </div>
                                        <div class="col align-bottom">
                                           <button type="submit" class="btn btn-primary btn-sm float-right align-bottom"> update</button>
                                        </div>
                                    </div>

                                </div>
                            </form>

                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card bg-light mb-3 w-100">
                            <div class="card-header text-white bg-success">Ticket Stock  <span class="badge-pill rounded-pill badge-dark float-right"> Batches {{count($purchase->inventoryQuantities)}} </span> </div>
                            <div class="card-body">

                                @foreach($purchase->inventoryQuantities as $inventory_quantity)
                                    <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="number_of_tickets">Number of tickets</label>
                                                    <input type="number" class="form-control" id="number_of_tickets"  name="number_of_tickets" value="{{$inventory_quantity->number_of_tickets}}" readonly="yes">
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="seats">Seats</label>
                                                    <input  class="form-control" id="seats" name="seats"  value="{{$inventory_quantity->seats}}" >
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col">
                                                <label for="sell_price">Sell Price</label>
                                                <input  class="form-control" id="sell_price" name="sell_price"  value="{{$inventory_quantity->sell_price}}" >
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col float-right">
                                                <button class="btn btn-primary float-right " data-toggle="modal" data-target="#exampleModal">split this batch</button></div>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('page_level_js')

@endsection
