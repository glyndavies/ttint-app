@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
    <link href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css" rel="stylesheet" crossorigin="anonymous" />

@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Settings</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Settings</a></li>
        </ol>
    </div>




    <div class="card mb-4">
        <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 ">

                            <form class="" method="POST" action="{{route('settings.updateEventGenre',$genre->id)}}">
                                @csrf
                                <div class="form-group">
                                    <div class="row align-content-center">
                                        <div class="col-8 col-sm-12">
                                            <div class="form-group">
                                                <label for="GenresName w-100">Genre Name</label>
                                                <input id="GenresName" placeholder="Genre Name" value="{{$genre->genre}}"  name="genre" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="GenresName w-100">Active
                                                <input type="checkbox" value="{{$genre->active}}"  @if($genre->active == 1)  checked  @endif name="active" class="form-control">
                                                </label>

                                            </div>

                                            <button type="submit" class="btn btn-primary mt-3"> save </button>
                                        </div>

                                    </div>
                                </div>

                            </form>

                        </div>

                    </div>





                </div>
            </div>

@endsection


@section('page_level_js')
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js" crossorigin="anonymous"></script>

@endsection
