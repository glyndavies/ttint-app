@extends('layouts.app')

@section('page_level_css')
<style>
    thead input {
        width: 100%;
    }
</style>
<link href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css" rel="stylesheet" crossorigin="anonymous" />

@endsection

@section('content')
<div class="container-fluid">
    <h1 class="mt-4">Settings</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Settings</a></li>
    </ol>
</div>

<div class="card mb-4">
    <div class="card-body">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Ticket Type</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="Genres-tab" data-toggle="tab" href="#Genres" role="tab" aria-controls="profile" aria-selected="false">Event Genres</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Ticket Status</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="subcategories-tab" data-toggle="tab" href="#subcategories" role="tab" aria-controls="subcategories" aria-selected="true">Expense Subcategories</a>
            </li>
        </ul>

        <div class="tab-content pt-2" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row">
                    <div class="col-md-4 col-sm-12 ">
                        <form class="" method="POST" action="{{ route('settings.updateTicketTypes') }}">
                            @csrf
                            <div class="form-group">
                                <div class="row align-content-center">
                                    <div class="col-8 col-sm-12">
                                        <label for="GenresName w-100">Ticket Type name</label>
                                        <input id="GenresName" placeholder="Ticket Types name" value="" name="tickettype" class="form-control">
                                        <button type="submit" class="btn btn-primary mt-3">Save</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 col-sm-12 ">
                        <div class="table-responsive mt-2">
                            <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Active</th>
                                        <th>Ticket Name</th>
                                        <th>Slug</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (TicketTypeRepository::findAll() as $types)
                                    <tr>
                                        <td>@if ($types->active == 1) Active @else inactive @endif</td>
                                        <td>{{ $types->tickettype }}</td>
                                        <td>{{ $types->slug }}</td>
                                        <td>
                                            <a href="{{ route('settings.showTicketTypes',$types->id) }}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></a>
                                            <div class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-exclamation-triangle"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <a class="dropdown-item" href="{{ route('settings.removeTicketTypes',$types->id) }}">Remove Genre</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="Genres" role="tabpanel" aria-labelledby="Genrese-tab">
                <div class="row">
                    <div class="col-md-4 col-sm-12 ">
                        <form class="" method="POST" action="{{ route('settings.updateEventGenre') }}">
                            @csrf
                            <div class="form-group">
                                <div class="row align-content-center">
                                    <div class="col-8 col-sm-12">
                                        <label for="GenresName w-100">Genre Name</label>
                                        <input id="GenresName" placeholder="Genre Name" value="" name="genre" class="form-control">
                                        <button type="submit" class="btn btn-primary mt-3">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 col-sm-12 ">
                        <div class="table-responsive mt-2">
                            <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Active</th>
                                        <th>Genres Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (EventGenreRepository::findAll() as $genres)
                                    <tr>
                                        <td>@if ($genres->active == 1) Active @else inactive @endif</td>
                                        <td>{{ $genres->genre }}</td>
                                        <td>

                                            <a href="{{ route('settings.showEventGenre',$genres->id) }}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></a>
                                            <div class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-exclamation-triangle"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <a class="dropdown-item" href="{{ route('settings.removeEventGenre',$genres->id) }}">Remove Genre</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <form class="" method="POST" action="{{ route('settings.updateTicketStatus') }}">
                            @csrf
                            <div class="form-group">
                                <div class="row align-content-center">
                                    <div class="col-8 col-sm-12">
                                        <label for="GenresName w-100">Ticket Status</label>
                                        <input id="GenresName" placeholder="Ticket Status name" value="" name="ticketStatus" class="form-control">
                                        <button type="submit" class="btn btn-primary mt-3">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 col-sm-12 ">
                        <div class="table-responsive mt-2">
                            <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Active</th>
                                        <th>Ticket Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (TicketStatusRepository::findAll() as $status)
                                    <tr>
                                        <td>@if ($status->active == 1) Active @else inactive @endif</td>
                                        <td>{{ $status->ticketStatus }}</td>
                                        <td>
                                            <a href="{{ route('settings.showTicketStatus',$status->id) }}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></a>
                                            <div class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-exclamation-triangle"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <a class="dropdown-item" href="{{ route('settings.removeTicketStatus',$status->id) }}">Remove Genre</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="subcategories" role="tabpanel" aria-labelledby="subcategories-tab">
                <div class="row">
                    <div class="col-md-4 col-sm-12 ">
                        <form class="" method="POST" action="{{ route('settings.updateSubcategories') }}">
                            @csrf
                            <div class="form-group">
                                <div class="row align-content-center">
                                    <div class="col-8 col-sm-12">
                                        <label for="subcategory-name" class="w-100">Name</label>
                                        <input id="subcategory-name" placeholder="Subcategory name" value="" name="name" class="form-control">
                                        <button type="submit" class="btn btn-primary mt-3">Create</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8 col-sm-12 ">
                        <div class="table-responsive mt-2">
                            <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (ExpenseSubCategoryRepository::findAll() as $subcategory)
                                    <tr>
                                        <td>{{ $subcategory->id }}</td>
                                        <td>{{ $subcategory->name }}</td>
                                        <td>
                                            <a href="{{ route('settings.showSubcategory',$subcategory->id) }}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i></a>
                                            <div class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-exclamation-triangle"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <a class="dropdown-item" href="{{ route('settings.removeSubcategory',$subcategory->id) }}">Remove Subcategory</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('page_level_js')
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js" crossorigin="anonymous"></script>
    @endsection