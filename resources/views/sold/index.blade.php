@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Tickets Sold</h1>

        <div>
            <table class="table table-bordered table-searchable small" id="sold-table" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th class="not-export"><input type="checkbox" id="checkall"> All </th>
                    <th class="d-none noVis">Batch id</th>
                    <th>Transaction number</th>
                    <th>Order Number</th>
                    <th>Event</th>
                    <th>Arena</th>
                    <th>Site purchased</th>
                    <th>Type of Ticket</th>
                    <th class="d-none noVis">Purchase Date</th>
                    <th class="datepicker">Event Date</th>
                    <th>Time of Event</th>
                    <th>Day of Event</th>
                    <th class="not-export">Available</th>
                    <th class="d-none noVis">Available</th>
                    <th class="not-export">Number of tickets</th>
                    <th class="d-none noVis">Number of tickets</th>
                    <th class="d-none noVis">Notes</th>
                    <th class="lg-width">Restrictions</th>
                    <th>FV</th>
                    <th>Seats</th>
                    <th class="">Listed</th>
                    <th class="">Referral 1</th>
                    <th class="">Referral 2</th>
                    <th>User Code</th>
                    <th>User name</th>
                    <th>User Deposit</th>
                    <th class="d-none noVis">sold</th>
                    <th>TCP Ticket</th>
                    <th>Total Cost</th>
                    <th>Tickets in Hand</th>
                    <th>Date Sold</th>
                    <th>Site Sold</th>
                    <th>Sale ID</th>
                    <th class="d-none noVis">Refund</th>
                    <th>TSP</th>
                    <th>Gross Profit</th>
                    <th>Delivered</th>
                    <th>Delivered Status</th>
                    <th class="d-none noVis">Delivered Date</th>
                    <th class="d-none noVis">Paid</th>
                    <th class="d-none noVis">Paid Date</th>
                    <th class="d-none noVis">Paid Amount</th>
                    <th class="not-export noVis">Actions</th>
                </tr>
                </thead>                
            </table>
        </div>

        <div class="row">
            <div class="col my-2">
                <button type="button" class="btn btn-sm btn-dark" id="massChangeBtn" data-toggle="modal" data-target="#saleModal" >mass change</button>
            </div>
        </div>
    </div>

    <!-- Modal -->
    @include('modals.mass-modal',['modalType' =>'mass_in_hand'])
    @include('modals.sold-modal')
@endsection


@section('page_level_js')
@endsection
