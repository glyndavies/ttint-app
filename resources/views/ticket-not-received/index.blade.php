@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Tickets Not Received : {{ $ticketType->tickettype }}</h1>

        <div>
            <table class="table table-bordered" id="dataTableTickets" width="100%" cellspacing="0">
                <thead>
                <tr class="text-lowercase">
                    <th><input type="checkbox" id="checkall"> All </th>
                    <th>Transaction number</th>
                    <th>Order number</th>
                    <th>Event</th>
                    <th>Arena</th>
                    <th>Site Purchased</th>
                    <th>Event Date</th>
                    <th>Time of Event</th>
                    <th>Day of event</th>
                    <th>Number of tickets</th>
                    <th class="d-none">Notes</th>
                    <th class="lg-width">Restrictions</th>
                    <th>FV</th>
                    <th>Seats</th>
                    <th>User Code</th>
                    <th>Sale ID</th>
                    <th>Total Cost per ticket</th>
                    <th>Delivery Status</th>
                    @if ($ticketType->paper())
                        <th>Chase </th>
                        <th>Pic</th>
                    @endif
                    <th>Tickets in Hand</th>
                </tr>
                </thead>

                <tbody>
                @foreach($inventory_quantities as $inventory_quantity)
                    <tr class="small">
                        <td><input class="selectCheck" value="{{ $inventory_quantity->id }}" type="checkbox" ></td>
                        <td>{{ $inventory_quantity->inventory->purchase_number }}</td>
                        <td>{{ $inventory_quantity->inventory->order_number }}</td>
                        <td>{{ $inventory_quantity->inventory->event }}</td>
                        <td>{{ $inventory_quantity->inventory->arena }}</td>
                        <td>{{ $inventory_quantity->inventory->site_purchased }}</td>
                        <td>{{ dateformat($inventory_quantity->inventory->event_date) }}</td>
                        <td>{{ $inventory_quantity->inventory->time_of_event }}</td>
                        <td>{{ $inventory_quantity->inventory->event_date ? substr($inventory_quantity->inventory->event_date->format('l'), 0, 3) : '' }}</td>
                        <td class="text-center">
                            @include('partials.badges.inventory-quantity-status', [ 'inventory_quantity' => $inventory_quantity ])
                            @include('partials.badges.inventory-notes', [ 'inventory' => $inventory_quantity->inventory ])
                        </td>
                        <td class="d-none">{{ $inventory_quantity->inventory->notes }}</td>
                        <td>{{ $inventory_quantity->inventory->restrictions }}</td>
                        <td>@money(optional($inventory_quantity->inventory->purchase)->face_value_per_ticket)</td>
                        <td>{{ $inventory_quantity->seats }}</td>
                        <td>{{ $inventory_quantity->inventory->buyer }}</td>
                        <td>{{ $inventory_quantity->soldTicket->sale_id_ref ?? '' }}</td>
                        <td>@money(optional($inventory_quantity->inventory)->purchase->VA)</td>
                        <td>
                            @include('partials.badges.inventory-quantity-handling', [ 'inventory_quantity' => $inventory_quantity ])
                        </td>
                        @if($ticketType->paper())
                            <td>
                                @include('partials.toggles.inventory-quantity-chase', [ 'inventory_quantity ' => $inventory_quantity])
                            </td>
                            <td>
                                @include('partials.toggles.inventory-quantity-pic', [ 'inventory_quantity ' => $inventory_quantity])
                            </td>
                        @endif
                        <td>
                            @include('partials.toggles.inventory-quantity-inhands', [ 'inventory_quantity ' => $inventory_quantity])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col my-2">
                <button class="btn btn-sm btn-dark" id="massChangeBtn" data-toggle="modal" data-target="#saleModal">Mass Change</button>
                <a class="btn btn-sm btn-danger" href="/admin/tnr/clearBatchNos">Clear Batches</a>
            </div>
        </div>
    </div>

    @include('modals.mass-modal',['modalType' =>'mass_in_hand'])
@endsection


@section('page_level_js')
    <script type="text/javascript">
        $('.ticketsInHand').change(function() {
            var id =$(this).data('id');
            if($(this).is(":checked")) {
            }
            else if($(this).is(":not(:checked)")){
                $(this).closest('tr').remove();
            }
        });
    </script>
@endsection
