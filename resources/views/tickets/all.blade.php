@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">All Tickets</h1>
        <div>
            <table class="table table-bordered table-searchable small" id="all-tickets-table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="not-export d-none noVis">All</th>
                        <th>Batch id</th>
                        <th class="md-width">Transaction number</th>
                        <th>Order Number</th>
                        <th>Event</th>
                        <th>Arena</th>
                        <th>Site purchased</th>
                        <th>Type of Ticket</th>
                        <th>Purchase Date</th>
                        <th class="datepicker">Event Date</th>
                        <th>Time of Event</th>
                        <th>Day of Event</th>
                        <th class="not-export">Available</th>
                        <th class="d-none noVis">Available</th>
                        <th class="not-export">Number of tickets</th>
                        <th class="1d-none noVis">Number of tickets</th>
                        <th class="d-none noVis">Notes</th>
                        <th class="lg-width">Restrictions</th>
                        <th>FV</th>
                        <th class="lg-width">Seats</th>
                        <th>Listed</th>
                        <th>Referral 1</th>
                        <th>Referral 2</th>
                        <th class="sm-width">User Code</th>
                        <th>User name</th>
                        <th>User Deposit</th>
                        <th class="d-none noVis">sold</th>
                        <th>TCP Ticket</th>
                        <th>Total Cost</th>
                        <th>Tickets in Hand</th>
                        <th class="">Date Sold</th>
                        <th class="">Site Sold</th>
                        <th class="">Sale ID</th>
                        <th class="">Refund</th>
                        <th class="">TSP</th>
                        <th class="">Gross Profit</th>
                        <th class="">Delivered</th>
                        <th class="">Delivered Status</th>
                        <th class="">Delivered Date</th>
                        <th class="">Paid</th>
                        <th class="">Paid Date</th>
                        <th class="">Paid Amount</th>
                        <th>State Ticket</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- Modals -->
    @include('modals.sale-modal')
    @include('modals.details-modal')
    
@endsection

@section('page_level_js')
@endsection