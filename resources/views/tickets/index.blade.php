@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">All Tickets</h1>
        <div>
            <table class="table table-bordered" id="dataTableTickets" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Batch id</th>
                        <th>Transaction number</th>
                        <th>Order Number</th>
                        <th>Event</th>
                        <th>Arena</th>
                        <th>Site Purchased</th>
                        <th>Type of Ticket</th>
                        <th>Purchase Date</th>
                        <th>Event Date</th>
                        <th>Time of Event</th>
                        <th>Day of Event</th>
                        <th>Available</th>
                        <th class="not-export">Number of tickets</th>
                        <th class="d-none noVis">Number of tickets</th>
                        <th class="d-none noVis">Notes</th>
                        <th>Restrictions</th>
                        <th>FV</th>
                        <th>Seats</th>
                        <th>Listed</th>
                        <th>Referral 1</th>
                        <th>Referral 2</th>
                        <th>User Code</th>
                        <th>User name</th>
                        <th>User Deposit</th>
                        <th>Sold</th>
                        <th>TCP Ticket</th>
                        <th>Total Cost</th>
                        <th>Tickets in Hand</th>
                        <th>Date Sold</th>
                        <th>Site Sold</th>
                        <th>Sale ID</th>
                        <th>Refund</th>
                        <th>TSP</th>
                        <th>Gross Profit</th>
                        <th>Delivered</th>
                        <th>Delivered Status</th>
                        <th>Delivered Date</th>
                        <th>Paid</th>
                        <th>Paid Date</th>
                        <th>Paid Amount</th>
                        <th>State Ticket</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($inventory_quantities as $inventory_quantity)
                        <tr class="@if($inventory_quantity->deliveryBatch)  batch-{{ $inventory_quantity->deliveryBatch->batch_id }} @endif small">
                                <td>@if ($inventory_quantity->deliveryBatch) {{ $inventory_quantity->deliveryBatch->batch_id }} @endif</td>
                                <td>{{ $inventory_quantity->purchase_number }}</td>
                                <td>{{ $inventory_quantity->inventory->order_number }}</td>
                                <td>{{ $inventory_quantity->inventory->event }}</td>
                                <td>{{ $inventory_quantity->inventory->arena }}</td>
                                <td>{{ $inventory_quantity->inventory->site_purchased }}</td>
                                <td>{{ $inventory_quantity->inventory->type_of_ticket }}</td>
                                <td>Purchase Date</td>
                                <td>{{ dateformat($inventory_quantity->inventory->event_date) }}</td>
                                <td>{{ $inventory_quantity->inventory->time_of_event }}</td>
                                <td>{{ $inventory_quantity->inventory->event_date ? substr($inventory_quantity->inventory->event_date->format('l'), 0, 3) : '' }}</td>
                                <td>@include('partials.badges.inventory-statuses', [ 'inventory' => $inventory_quantity->inventory])</td>
                                <td class="text-center">
                                    @include('partials.badges.inventory-quantity-status', [ 'inventory_quantity' => $inventory_quantity ])
                                    @include('partials.badges.inventory-notes', [ 'inventory' => $inventory_quantity->inventory ])
                                </td>
                                <td class="d-none">{{ $inventory_quantity->number_of_tickets }}{{ strtoupper(substr($inventory_quantity->ticketStatus->ticketStatus, 0, 1)) }}</td>
                                <td class="d-none">{{ $inventory_quantity->inventory->notes }}</td>
                                <td>{{ $inventory_quantity->inventory->restrictions }}</td>
                                <td>@money(optional($inventory_quantity->inventory->purchase)->face_value_per_ticket)</td>
                                <td>{{ $inventory_quantity->seats }}</td>
                                <td>{{ $inventory_quantity->inventory->listed }}</td>
                                <td>{{ $inventory_quantity->inventory->referral_1 }}</td>
                                <td>{{ $inventory_quantity->inventory->referral_2 }}</td>
                                <td>{{ $inventory_quantity->inventory->buyer }}</td>
                                <td>{{ optional($inventory_quantity->inventory->purchase)->card_used }}</td>
                                <td>@money(optional($inventory_quantity->inventory->purchase)->buyer_investment)</td>
                                <td>@if($inventory_quantity->ticket_status_id == 3) sold @endif</td>
                                <td>TCP Ticket</td>
                                <td>@money($inventory_quantity->cost)</td>
                                <td>@include('partials.toggles.inventory-quantity-inhands', [ 'inventory_quantity' => $inventory_quantity ])</td>
                                <td>@if(!empty($inventory_quantity->soldTicket)) {{ dateformat($inventory_quantity->soldTicket->sold_date) }}@endif</td>
                                <td>@if(!empty($inventory_quantity->soldTicket)) {{ $inventory_quantity->soldTicket->site_sold }}@endif</td>
                                <td>@if(!empty($inventory_quantity->soldTicket)){{ $inventory_quantity->soldTicket->sale_id_ref }}@endif</td>
                                <td>@money(optional(optional($inventory_quantity->inventory->purchase)->companyRefund)->refunded_amount)</td>
                                <td>@money($inventory_quantity->sell_price)</td>
                                <td>@money($inventory_quantity->profit)</td>
                                <td>@include('partials.toggles.inventory-quantity-handling', [ 'inventory_quantity' => $inventory_quantity, 'hide' => 1 ])</td>
                                <td>@include('partials.badges.inventory-quantity-handling', [ 'inventory_quantity' => $inventory_quantity ])</td>
                                <td>{{ dateformat($inventory_quantity->delivered_date) }}</td>
                                <td>
                                    @if(!empty($inventory_quantity->soldTicket))
                                        @include('partials.toggles.inventory-quantity-paid', [ 'inventory_quantity' => $inventory_quantity ])
                                    @endif
                                </td>
                                <td>@if(!empty($inventory_quantity->soldTicket)){{ dateformat($inventory_quantity->soldTicket->paid_date) }}@endif</td>
                                <td>@money($inventory_quantity->inventory->paid_amount)</td>
                                <td>@include('partials.tickets.states-ticket', ['inventory_quantity' => $inventory_quantity])</td>
                            </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- Modals -->
    @include('modals.sale-modal')
    @include('modals.details-modal')

@endsection

@section('page_level_js')
@endsection
