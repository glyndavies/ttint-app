@extends('layouts.app')

@section('page_level_css')
    <style>
        thead input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-2 mb-2">Tickets available for swap</h1>

        <div>
            <table class="table table-bordered multiple-rows" id="dataTableTickets" width="100%" cellspacing="0">
                <thead>
                <tr class="text-lowercase">
                    <th class="md-width">Transaction no.</th>
                    <th>Order no.</th>
                    <th>Event</th>
                    <th>Arena</th>
                    <th>Site</th>
                    <th>Type</th>
                    <th>Event date</th>
                    <th>Time</th>
                    <th>Event day</th>
                    <th>In hand</th>
                    <th>Available</th>
                    <th class="lg-width">Restrictions</th>
                    <th class="lg-width">Seats</th>
                    <th class="sm-width">Buyer</th>
                    <th class="sm-width">User code</th>
                    <th class="sm-width">New user</th>
                    <th class="sm-width">Token</th>
                    <th>Tcp</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($inventories as $inventory)
                    <tr  id="{{ $inventory->id }}" class="@if($inventory->broken_limit) bg-danger text-white @endif small">
                        <td>{{ $inventory->purchase_number }} </td>
                        <td>{{ $inventory->order_number }}</td>
                        <td>{{ $inventory->event }}</td>
                        <td>{{ $inventory->arena }}</td>
                        <td>{{ $inventory->site_purchased }}</td>
                        <td>{{ $inventory->type_of_ticket }}</td>
                        <td>{{ dateformat($inventory->event_date) }}</td>
                        <td>{{ $inventory->time_of_event }}</td>
                        <td>{{ dateformat($inventory->event_date) }}</td>
                        <td>
                            @if ($inventory->inventoryQuantities->count() == 1)
                                @include('partials.toggles.inventory-quantity-inhands', [ 'inventory_quantity' => $inventory->inventoryQuantities->first() ])
                            @endif
                        </td>
                        <td class="text-center">
                            @include('partials.badges.inventory-fortransfer-count', [ 'inventory' => $inventory ])
                            @include('partials.badges.inventory-notes', [ 'inventory' => $inventory ])
                        </td>
                        <td>{{ $inventory->restrictions }}</td>
                        <td>
                            @foreach($inventory->inventoryQuantities as $available)
                                <span>{{ $available->seats }}</span><br>
                            @endforeach
                        </td>
                        <td>{{ optional($inventory->purchase)->buyer }}</td>
                        <td>{{ $inventory->buyer }}</td>
                        <td>{{ $inventory->new_buyer }}</td>
                        <td>{{ $inventory->token }}</td>
                        <td>@money(optional($inventory->purchase)->cost_per_ticket)</td>
                        <td>@money(optional($inventory->purchase)->cost)</td>
                        <td class="text-center">
                            @include('partials.badges.inventory-statuses', [ 'inventory' => $inventory ])
                        </td>
                        <td class="text-nowrap text-right">
                            <a href="{{ route('tickets.show',$inventory->id) }}" title="details" class="btn btn-primary btn-xs"><i class="fas fa-ticket-alt"></i></a>
                            <button data-inventory="{{ $inventory->id }}" data-toggle="modal" data-target="#saleModal" title="sale" type="button" class="btn btn-info btn-xs text-white"><i class="fa fa-credit-card" aria-hidden="true"></i></button>
                            <button  type="button" data-id="{{ $inventory->id }}" title="expire" class="btn btn-danger btn-xs expiredTickets"><i class="fas fa-clock"></i></button>
                            <button data-inventory="{{ $inventory->id }}" data-user_code="{{ $inventory->new_buyer }}" data-token="{{ $inventory->token }}" data-toggle="modal" data-target="#tokenModal" title="Token" type="button" class="btn btn-info btn-xs text-white"><i class="fa fa-key"></i></button>
                            @if ($inventory->ifStatusAvailableForSwap())
                                <button onclick="returnToAvailable({{ $inventory->id }}, this)" title="return to inventory" type="button" data-id="{{ $inventory->id }}" class="btn btn-info btn-xs text-white"><i class="fa fa-random"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <button type="button" class="btn btn-primary" id="swapButton" disabled>Swap</button>
        </div>
    </div>

    <!-- Modal -->
    @include('modals.sale-modal')
    @include('modals.token-modal', [ 'platformUsers' => $platformUsers])
@endsection

@section('page_level_js')
    <script>
        $(document).ready(function() {
            var $table = $('#dataTableTickets');
            var $swapButton = $('#swapButton');
            var selectedRows = [];

            function onRowClick() {
                selectedRows = $table.DataTable().rows('.selected').data();
                if (selectedRows.length == 2) {
                    $swapButton.prop('disabled', false);
                } else {
                    $swapButton.prop('disabled', true);
                }
            }

            $table.on('click', 'tr', function() {
                onRowClick();
            });

            $swapButton.click(function() {
                window.location.href = "{{ url('admin/swap') }}?" + $.param({
                    'purchase_number0' : selectedRows[0][0],
                    'purchase_number1' : selectedRows[1][0],
                    });
            });
        } );
    </script>
@endsection
