@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class="">Imported</span>
                    <h4>{{count($tem)}}</h4>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class=""> Issues</span>
                    <h4>{{count($error_logs)}}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <h1 class="mt-4">Ready to process expenses
                <button href="{{ route('upload.expenses.process') }}" class="btn tbn-sm btn-primary float-right process">Import</button>
            </h1>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Date</td>
                            <td>Description</td>
                            <td>Category</td>
                            <td>Subcategory</td>
                            <td>Card</td>
                            <td>Amount</td>
                            <td>VAT</td>
                            <td>Total Amount</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tem as $row)
                            @php
                                $da = json_decode($row['data']);
                            @endphp
                            <tr>
                                <td>{{ $da->id ?? null }}</td>
                                <td>{{ $da->date }}</td>
                                <td>{{ $da->description }}</td>
                                <td>{{ $da->category }}</td>
                                <td>{{ $da->subcategory ?? '' }}</td>
                                <td>{{ $da->card }}</td>
                                <td>{{ preg_replace('/[^0-9-.]+/', '', $da->amount) }}</td>
                                <td>{{ $da->vat }}</td>
                                <td>{{ preg_replace('/[^0-9-.]+/', '', $da->total_amount) }}</td>
                                <td class='text-primary'>{{ $da->status }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection