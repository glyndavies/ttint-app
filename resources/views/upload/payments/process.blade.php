@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class="">Imported</span>
                   <h4>{{count($tem)}}</h4>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class=""> Issues</span>
                    <h4>{{count($error_logs)}}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <h1 class="mt-4">Ready to process payments
                <button href="{{ route('upload.payments.process') }}" class="btn tbn-sm btn-primary float-right process">Import</button>
            </h1>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <td>User Code</td>
                        <td>Total Amount</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tem as $row)
                        @php
                            $da = json_decode($row['data']);
                        @endphp
                        <tr>
                            <td>{{ dateFormat(spreadsheetDate($da->date)) }}</td>
                            <td>{{ $da->user_code }}</td>
                            <td>@money($da->total_amount)</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
