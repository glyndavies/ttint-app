@extends('layouts.app')
@section('page_level_css')
@endsection
@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
                <div class="card mb-4">
                    <div class="card-body">
                        <span class="">Reconcile</span>
                       <h4>{{count($tem)}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card mb-4">
                    <div class="card-body">
                        <span class=""> Issues</span>
                        <h4>{{count($error_logs)}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card mb-4">
                    <div class="card-body">
                        <span class="">Total</span>
                        <h4> <span class="card-title pricing-card-title total-cost"></span></h4>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div class="card mb-4">
                    <div class="card-body">
                        <span class="">Filtered Total </span>
                        <h4> <span class="card-title pricing-card-title filtered-cost"></span></h4>
                    </div>
                </div>
            </div>

        </div>

        <div class = "row mb-3">
            <div class="col-12">
                <h1 class="mt-4">Ready to process reconcile
                    <button href="{{ route('upload.reconcile.process') }}" class="btn tbn-sm btn-primary float-right process">Process</button>
                </h1>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable3" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Sale Ref - system </th>
                                <th>Sale-ref  - bank</th>
                                <th>Sale price - system</th>
                                <th>Sale price - Bank</th>
                                <th>Site Sold </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($tem as $da )
                            @php
                                $process  = json_decode($da['data']);
                            @endphp
                            <tr>
                                <td>{{ $process->system_id_ref ?? '' }}</td>
                                <td>{{ $process->sold_ref ?? ''}}</td>
                                <td>{{ $process->system_price ?? ''}}</td>
                                <td>{{ $process->payment_amount ?? ''}}</td>
                                <td>{{ $process->system_site ?? ''}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr style="text-align:right; display: ;">
                            <th colspan="3" style="text-align:right; display:;"> Total:</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_level_js')

    <script>
        $(document).ready(function() {
        var table2 =  $('#dataTable3').DataTable({

            "order": [],
            orderCellsTop: true,
            "cache": false,
            stateSave: true,
            fixedHeader: true,
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\£,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                // Total over all pages
                total = api
                    .column(2)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column(2, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column(2).footer() ).html(
                    '£<span class="total-filter">'+number_format(pageTotal,2,'.',',') +'</span> ( £<span class="totalAll">'+ number_format(total,2,'.',',') +'<span> total)'

                );
                $('.total-cost').html(number_format(total,2,'.',','));
                $('.filtered-cost').html(number_format(pageTotal,2,'.',','));
            },
            "language": {
                "decimal": ".",
                "thousands": ","
            },
            "pageLength": 100,
            "aButtons" : [
                {
                    "sExtends": "select_all",
                    "sButtonText": "Select Filtered",
                    "fnClick": function (nButton, oConfig, oFlash) {
                        var oTT = TableTools.fnGetInstance('dataTable');
                        oTT.fnSelectAll(true); //True = Select only filtered rows (true). Optional - default false.

                    }

                }],
            dom: 'Bfrtip',
            columnDefs: [
                { "orderable": false, "targets": 0 }
            ],
            buttons: [


                {
                    extend: 'colvis',  className: 'btn-colvis',
                    columns: ':not(.noVis)'


                },
                { extend: 'pdfHtml5', orientation: 'landscape', pageSize: 'LEGAL', className: 'btn-pdf'},
                {
                    extend: 'csvHtml5',
                    className: 'btn-csv',
                    text: 'CSV ',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                // 'copyHtml5',
                'excelHtml5'

            ]

        });
        });
    </script>


@endsection

