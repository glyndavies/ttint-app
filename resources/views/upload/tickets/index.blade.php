@extends('layouts.app')
@section('page_level_css')
@endsection
@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Ticket Upload</h1>
        <div class="card mb-4">
            <div class="card-body">
                
                <div class="row mb-2">
                    <div class="col-12">
                        <form action="{{route('upload.tickets.store')}}" method="post" enctype="multipart/form-data" style="all: unset">
                            @csrf
                            <div>
                                <input type="file" class="" id="fileupload" name="fileupload" value="upload file">
                                <button type="submit" class="btn btn-info text-white">Start upload</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
						<form action="{{ route('upload.historic.store') }}" method="post" enctype="multipart/form-data" style="all: unset">
                            @csrf
                            <div>
                                <input type="file" class="" id="fileupload" name="fileupload" value="upload file">
                                <button type="submit" class="btn btn-info text-white">Start Historic upload</button>
                            </div>
                        </form>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <form method="POST" action="{{ route('upload.destroy') }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-link">Clear data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_js')
@endsection

