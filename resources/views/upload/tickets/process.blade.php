@extends('layouts.app')
@section('page_level_css')
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class="">Imported</span>
                   <h4>{{count($tem)}}</h4>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class=""> Issues</span>
                    <h4>{{count($error_logs)}}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <h1 class="mt-4">Ready to process tickets
              <button href="{{ route('upload.tickets.process') }}" class="btn tbn-sm btn-primary float-right process" >Import</button>
            </h1>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <td>Order Number</td>
                    <td>Transaction Number</td>
                    <td>Ticket status</td>
                    <td>Event Genre</td>
                    <td>Arena</td>
                    <td>Site Purchased</td>
                    <td>Type of ticket</td>
                    <td>Purchased Date</td>
                    <td>Event date</td>
                    <td>Time of event</td>
                    <td>Day or event</td>
                    <td>Total tickets</td>
                    <td>Restrictions</td>
                    <td>Seats</td>
                    <td>Face value of tickets</td>
                    <td>Buying fees</td>
                    <td>User code</td>
                    <td>User Deposit</td>
                    <td>Total cost</td>
                    <td>Buyer</td>
                </tr>
                </thead>
                <tbody>

                @foreach($tem as $da )
                    @php
                        $process  = json_decode($da['data']);
                    @endphp
                    <tr class="small">
                        <td>{{ $process->order_number ?? '' }}</td>
                        <td>{{ $process->transaction_number ?? '' }}</td>
                        <td>{{ $process->ticket_status ?? '' }}</td>
                        <td>{{ $process->event_genre ?? '' }}</td>
                        <td>{{ $process->event ?? '' }}</td>
                        <td>{{ $process->site_purchased ?? '' }}</td>
                        <td>{{ $process->type_of_ticket ?? '' }}</td>
                        <td>
                            @if(isset($process->purchase_date))
                                {{ dateFormat(spreadsheetDate($process->purchase_date)) }}
                            @endif
                        </td>
                        <td>
                            @if(isset($process->event_date))
                                {{ dateFormat(spreadsheetDate($process->event_date)) }}
                            @endif
                        </td>
                        <td>{{ $process->time_of_event ?? '' }}</td>
                        <td>
                            @if(isset($process->day_of_event))
                                {{ spreadsheetDate($process->event_date)->isoFormat('dddd') }}
                            @endif
                        </td>
                        <td>{{ $process->number_of_tickets ?? '' }}</td>
                        <td>{{ $process->restrictions ?? '' }}</td>
                        <td>{{ $process->seats ?? '' }}</td>
                        <td>@money($process->face_value_of_tickets ?? 0)</td>
                        <td>@money($process->buying_fees ?? 0)</td>
                        <td>{{ $process->user_code ?? '' }}</td>
                        <td>@money($process->user_deposit ?? 0)</td>
                        <td>@money($process->total_cost ?? 0)</td>
                        <td>{{ $process->buyer ?? '' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

@endsection
@section('page_level_js')
@endsection

