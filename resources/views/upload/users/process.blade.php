@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class="">Imported</span>
                   <h4>{{count($tem)}}</h4>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card mb-4">
                <div class="card-body">
                    <span class=""> Issues</span>
                    <h4>{{count($error_logs)}}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <h1 class="mt-4">Ready to process users
                <button href="{{ route('upload.users.process') }}" class="btn tbn-sm btn-primary float-right process">Import</button>
            </h1>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <td>Unique ID</td>
                        <td>Full name (First)</td>
                        <td>Full name (Last)</td>
                        <td>Email address 1</td>
                        <td>Email address 2</td>
                        <td>Desired event 1</td>
                        <td>Desired event 2</td>
                        <td>Desired event 3</td>
                        <td>Desired event 4</td>
                        <td>Desired event 5</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tem as $row)
                        @php
                            $da = json_decode($row['data']);
                        @endphp
                        <tr>
                            <td>{{ $da->unique_id }}</td>
                            <td>{{ $da->full_name_first }}</td>
                            <td>{{ $da->full_name_last }}</td>
                            <td>{{ $da->email_address_1 }}</td>
                            <td>{{ $da->email_address_2 }}</td>
                            <td>{{ $da->desired_event_1 }}</td>
                            <td>{{ $da->desired_event_2 }}</td>
                            <td>{{ $da->desired_event_3 }}</td>
                            <td>{{ $da->desired_event_4 }}</td>
                            <td>{{ $da->desired_event_5 }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
