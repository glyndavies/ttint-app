@extends('layouts.app')

@section('page_level_css')

@endsection

@section('content')

    <div class="container-fluid">

        <h1 class="mt-4">Users</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Users</a></li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Surname</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Access</th>
                            <th>Last Login</th>
                            <th>Start Date</th>
                            <th>Address</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->firstname}}</td>
                            <td>{{$user->surname}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->dob}}</td>
                            <td>
                                @foreach($user->roles as $role)
                                {{$role->name}}
                                @endforeach

                         <a href="">Change</a></td>
                            <th>@if($user->lastLogin){{$user->lastLogin->format('d/m/y H:m:s')}}@endif</th>
                            <td>{{$user->created_at}}</td>
                            <td>{{$user->houseNumber}}  {{$user->Address1}}     {{$user->town}} ,  {{$user->city}}  {{$user->postcode}} </td>
                            <td>

                                @hasrole('Admin')

                                @can('manage users')
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">edit</a>
                                    <form class="" action="{{ route('users.destroy', $user->id) }}" method="POST" style="all: unset">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">delete</button>
                                    </form>
                                @endcan
                                @else
                                    Please ask for correct permissions...
                                    @endhasrole

                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Add New User
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('users.store')}}" >
                                    @csrf
                                    <div class="form-group">
                                        <input class="form-control" name="firstname" id="" aria-describedby="" placeholder="First Name">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control"  name="surname"  id="" aria-describedby="" placeholder="Surname">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control"  name="email"   id="" aria-describedby="" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="date" class="form-control"  name="DOB"  id="" aria-describedby="" placeholder="DOB">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control"  name="password"  id="" aria-describedby="" placeholder="Password">
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control"  name="roles"  id="exampleFormControlSelect1">
                                            @foreach ($roles as $role)
                                                <option value="{{$role->name}}">{{$role->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control" id="" aria-describedby="" placeholder="Start Date">
                                    </div>


                                    <button type="submit" class="btn btn-primary">Submit</button>


                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('page_level_js')

@endsection
