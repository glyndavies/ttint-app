@extends('layouts.app')

@section('page_level_css')

@endsection

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">User Edit: {{$user->name}}</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Users</a></li>
            <li class="breadcrumb-item active">Edit: {{$user->name}} </li>
        </ol>
        {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update', $user->id]]) !!}
       <div class="row">
           <div class="card mb-4 col-sm-12 col-md-7 m-3">
               <div class="card-body">
                   <h4>User Details</h4>
                   <div class="panel panel-default">
                       <div class="panel-body">
                           <div class="row">
                               <div class="col-6 form-group">

                                   {!! Form::label('firstanme', 'Firstname*', ['class' => 'control-label']) !!}
                                   {!! Form::text('firstanme', $user->firstname, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                   <p class="help-block"></p>
                                   @if($errors->has('firstanme'))
                                       <p class="help-block">
                                           {{ $errors->first('firstanme') }}
                                       </p>
                                   @endif
                               </div>
                               <div class="col-6 form-group">
                                   {!! Form::label('surname', 'Surname*', ['class' => 'control-label']) !!}
                                   {!! Form::text('surname', old('surname'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                   <p class="help-block"></p>
                                   @if($errors->has('surname'))
                                       <p class="help-block">
                                           {{ $errors->first('surname') }}
                                       </p>
                                   @endif
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-6 form-group">
                                   {!! Form::label('DOB', 'Date of birth*', ['class' => 'control-label']) !!}
                                   {!! Form::date('DOB', $user->dob, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                   <p class="help-block"></p>
                                   @if($errors->has('DOB'))
                                       <p class="help-block">
                                           {{ $errors->first('DOB') }}
                                       </p>
                                   @endif
                               </div>
                               <div class="col-6 form-group">
                                   {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
                                   {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                   <p class="help-block"></p>
                                   @if($errors->has('email'))
                                       <p class="help-block">
                                           {{ $errors->first('email') }}
                                       </p>
                                   @endif
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <div class="card mb-4 m-3 col-sm-12 col-md-4">
               <div class="card-body">
                   <h4>Access Control</h4>
                   <div class="panel panel-default">
                       <div class="panel-body">
                           <div class="row">
                               <div class="col-12 form-group">
                                   {!! Form::label('password', 'Password Reset', ['class' => 'control-label']) !!}
                                   {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
                                   <p class="help-block"></p>
                                   @if($errors->has('password'))
                                       <p class="help-block">
                                           {{ $errors->first('password') }}
                                       </p>
                                   @endif
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-12 form-group">
                                   {!! Form::label('roles', 'Roles*', ['class' => 'control-label']) !!}
                                   {!! Form::select('roles[]', $roles, old('roles') ? old('roles') : $user->roles()->pluck('name', 'name'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                   <p class="help-block"></p>
                                   @if($errors->has('roles'))
                                       <p class="help-block">
                                           {{ $errors->first('roles') }}
                                       </p>
                                   @endif
                                    <span>Current Roles</span><br>
                                        @foreach($user->roles as $currentRoles)
                                            <span class="badge badge-pill badge-primary"> {{ $currentRoles->name}}</span>
                                        @endforeach
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
        <div class="row">
            <div class="card mb-4 col-sm-12 col-md-12 m-3">
                <div class="card-body">
                    <h4>Address Details</h4>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-2 form-group">
                                    {!! Form::label('houseNumber', 'House name/Number*', ['class' => 'control-label']) !!}
                                    {!! Form::text('houseNumber', old('houseNumber'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('houseNumber'))
                                        <p class="help-block">
                                            {{ $errors->first('houseNumber') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-4 form-group">
                                    {!! Form::label('Address1', 'Address 1*', ['class' => 'control-label']) !!}
                                    {!! Form::text('Address1', old('Address1'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('Address1'))
                                        <p class="help-block">
                                            {{ $errors->first('Address1') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-4 form-group">
                                    {!! Form::label('Address2', 'Address 2', ['class' => 'control-label']) !!}
                                    {!! Form::text('Address2', old('Address2'), ['class' => 'form-control', 'placeholder' => 'address 2',]) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('Address2'))
                                        <p class="help-block">
                                            {{ $errors->first('Address2') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 form-group">
                                    {!! Form::label('town', 'Town*', ['class' => 'control-label']) !!}
                                    {!! Form::text('town', old('town'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('town'))
                                        <p class="help-block">
                                            {{ $errors->first('town') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-3 form-group">
                                    {!! Form::label('city', 'city*', ['class' => 'control-label']) !!}
                                    {!! Form::text('city', old('city'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('city'))
                                        <p class="help-block">
                                            {{ $errors->first('city') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-3 form-group">
                                    {!! Form::label('postcode', 'Postcode*', ['class' => 'control-label']) !!}
                                    {!! Form::text('postcode', old('postcode'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','maxlength'=>'10']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('postcode'))
                                        <p class="help-block">
                                            {{ $errors->first('postcode') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::submit('Update', ['class' => 'btn btn-success float-right']) !!}
        <a href="{{route('users.index')}}" class="btn btn-primary"> cancel</a>
        {!! Form::close() !!}


        <div class="card my-4">
            <div class="card-body">
                <h3> History Logs </h3>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTableDefault" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Event Type</th>
                            <th>Old Data</th>
                            <th>New Data </th>
                            <th>Logged at </th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($audits as $audit)
                            <tr>
                                <td>{{$audit->event}} {{str_replace("App\Models", "-",$audit->auditable_type)}}</td>
                                <td>{{json_encode($audit['old_values'])}}</td>
                                <td>{{json_encode($audit['new_values'])}}</td>
                                <td>{{$audit->created_at}}</td>
                            </tr>

                        @endforeach
                        @foreach($user->audits as $audit)
                            <tr>
                                <td>{{$audit->event}}</td>
                                <td>{{json_encode($audit['old_values'])}}</td>
                                <td>{{json_encode($audit['new_values'])}}</td>
                                <td>{{$audit->created_at}}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                    {{--                            <button type="button" class="btn btn-primary" id="massChange">Mass Change</button>--}}
                </div>
            </div>
        </div>





    </div>
@endsection


@section('page_level_js')

@endsection
