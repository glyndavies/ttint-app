<?php

use Illuminate\Support\Facades\Route;

// test GET-routes
Route::prefix('v1')->middleware(['web', 'auth'])->namespace('Api\Platform\V1')->group(function () {
    Route::get('/get-profile', 'ProfileController@getByUserCode');
    Route::get('/get-summary', 'SummaryController@getByUserCode');
    Route::get('/get-all-summaries', 'SummaryController@getAll');
    Route::get('/get-rebates', 'RebateController@getByUserCode');
    Route::get('/get-kpi-state', 'KpiController@getStateByUserCode');
    Route::get('/get-notifications/history', 'NotificationController@getHistoryByUserCode');
});

/*
|--------------------------------------------------------------------------
| Platform API Routes
|--------------------------------------------------------------------------
|
*/
Route::prefix('v1')->middleware('platform-api')->namespace('Api\Platform\V1')->group(function () {
    /*
     * Auth routes
     */
    Route::namespace('Auth')->group(function () {
        Route::post('/login', 'LoginController@login');
        Route::post('/logout', 'LoginController@logout')->middleware(['auth:sanctum']);
    });

    /*
     * App routes
     */
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/profile', 'ProfileController@get');
        Route::post('/profile', 'ProfileController@update');
        Route::post('/profile/fcm-client-token', 'ProfileController@createFcmToken');
        Route::delete('/profile/fcm-client-token', 'ProfileController@deleteFcmToken');

        Route::get('/summary', 'SummaryController@get');
        Route::get('/rebates', 'RebateController@get');
        Route::get('/kpi/state', 'KpiController@getState');
        Route::get('/notifications/history', 'NotificationController@history');
    });
});