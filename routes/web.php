<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/admin');

/*
|--------------------------------------------------------------------------
| Web  Routes  --- /admin views
|--------------------------------------------------------------------------
|
*/

Route::middleware(['web', 'auth'])->prefix('/admin')->namespace('Admin')->group(function () {

    Route::get('/', 'HomeController@index')->name('dashboard');

     /*
     |--------------------------------------------------------------------------
     | Web  Roles & Permissions routes
     |--------------------------------------------------------------------------
     */
    Route::resource('permissions', 'PermissionsController');
    Route::post('permissions_mass_destroy', 'PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'RolesController');
    Route::post('roles_mass_destroy', 'RolesController@massDestroy')->name('roles.mass_destroy');

    /*
    |--------------------------------------------------------------------------
    | Web  User Manager routes
    |--------------------------------------------------------------------------
    */
    Route::resource('users', 'UsersController');
    Route::post('users_mass_destroy', 'UsersController@massDestroy')->name('users.mass_destroy');

    /*
    |--------------------------------------------------------------------------
    | Web  Upload routes
    |--------------------------------------------------------------------------
    */
    //Route::resource('fileuploads','UploadController');

    Route::get('upload/tickets', 'UploadTicketsController@show')->name('upload.tickets');
    Route::post('upload/tickets/store', 'UploadTicketsController@store')->name('upload.tickets.store');
    Route::get('upload/tickets/process', 'UploadTicketsController@process')->name('upload.tickets.process');

    Route::post('upload/historic/store','UploadHistoricController@store')->name('upload.historic.store');

    Route::post('upload/reconcile/store', 'UploadReconcileController@store')->name('upload.reconcile.store');
    Route::get('upload/reconcile/process', 'UploadReconcileController@process')->name('upload.reconcile.process');

    Route::post('upload/payments/store', 'UploadPaymentsController@store')->name('upload.payments.store');
    Route::get('upload/payments/process', 'UploadPaymentsController@process')->name('upload.payments.process');

    Route::get('upload/users', 'UploadUsersController@show')->name('upload.users');
    Route::post('upload/users/store', 'UploadUsersController@store')->name('upload.users.store');
    Route::get('upload/users/process', 'UploadUsersController@process')->name('upload.users.process');

    Route::get('upload/expenses', 'UploadExpensesController@show')->name('upload.expenses');
    Route::post('upload/expenses/store', 'UploadExpensesController@store')->name('upload.expenses.store');
    Route::get('upload/expenses/process', 'UploadExpensesController@process')->name('upload.expenses.process');

    Route::get('upload/business-expenses', 'UploadBusinessExpensesController@show')->name('upload.business-expenses');
    Route::post('upload/business-expenses/store', 'UploadBusinessExpensesController@store')->name('upload.business-expenses.store');
    Route::get('upload/business-expenses/process', 'UploadBusinessExpensesController@process')->name('upload.business-expenses.process');

    Route::delete('upload', 'UploadController@destroy')->name('upload.destroy');

    /*
    |--------------------------------------------------------------------------
    | Web platform users routes
    |--------------------------------------------------------------------------
    */
    Route::get('platform-users','PlatformUserController@index')->name('platform-users');
    Route::get('platform-users/{id}','PlatformUserController@show')->name('platform-users.show');
    Route::put('platform-users/{id}','PlatformUserController@update')->name('platform-users.update');
    Route::delete('platform-users/{id}','PlatformUserController@delete')->name('platform-users.delete');

     /*
    |--------------------------------------------------------------------------
    | Web  Ticket Management/Inventory routes
    |--------------------------------------------------------------------------
    */

    Route::post('tickets/all/datatable', 'TicketController@ticketsAllDatatable');
    Route::get('tickets/all','TicketController@ticketsAll')->name('tickets.all');

    /*
    |--------------------------------------------------------------------------
    | Web  transfer routes
    |--------------------------------------------------------------------------
    */

    Route::get('transfer', 'TransferController@index')->name('transfer');
    Route::post('transfer/updateUserCode/{id}', 'TransferController@updateUserCode')->name('transfer.updateUserCode');

    /*
    |--------------------------------------------------------------------------
    | Web  Ticket Management/Inventory routes
    |--------------------------------------------------------------------------
    */

    Route::post('inventory/datatable', 'TicketController@datatable');
    Route::put('tickets/updatebatch/{id}', 'TicketController@updateBatch')->name('tickets.update_batch');
    Route::post('tickets/splitbatch/{id}', 'TicketController@splitBatch')->name('tickets.split_batch');
    Route::get('tickets/returntostock/{id}', 'TicketController@returnToStock');
    Route::get('tickets/inventoryinhand/{id}/{status}', 'TicketController@inventoryInHand');
    Route::get('/tickets/ticketsinhand/{id}/{status}', 'TicketController@ticketsInHand');
    Route::get('/tickets/handling/{id}/{status}', 'TicketController@handling');
    //Route::resource('tickets','InventoryController');
    Route::post('ticket/refund-to/{id}', 'TicketController@refundTo')->name('ticket.refundTo');

    /*
    |--------------------------------------------------------------------------
    | Web  Ticket Not Received routes
    |--------------------------------------------------------------------------
    */

    Route::get('ticket-not-received/{ticketType}', 'TicketNotReceivedController@index')->name('ticket-not-received.index');

    /*
    |--------------------------------------------------------------------------
    |Fulfillment board routes
    |--------------------------------------------------------------------------
    */
    Route::get('fulfillment-board', 'FulfillmentBoardController@index')->name('fulfillment-board.index');
    Route::post('fulfillment-board/datatable', 'FulfillmentBoardController@datatable');
    Route::get('fulfillment-board/inventory-quantities/{inventoryQuantitiesIds}', 'FulfillmentBoardController@inventoryQuantities');
    Route::post('fulfillment-board/export', 'FulfillmentBoardController@export')->name('fulfillment-board.export');
    Route::post('fulfillment-board/import', 'FulfillmentBoardController@import')->name('fulfillment-board.import');
    Route::post('fulfillment-board/updateMass', 'FulfillmentBoardController@updateMass')->name('fulfillment-board.updateMass');
    Route::post('fulfillment-board/massTicketsInHand', 'FulfillmentBoardController@massTicketsInHand')->name('fulfillment-board.massTicketsInHand');
    Route::resource('fulfillment-board', 'FulfillmentBoardController');

    /*
    |--------------------------------------------------------------------------
    | Web Fulfillment routes
    |--------------------------------------------------------------------------
    */
    Route::get('fulfillment/{ticketType}', 'FulfillmentController@index')->name('fulfillment.index');

    /*
    |--------------------------------------------------------------------------
    | Web  Ticket Management/Delivered routes
    |--------------------------------------------------------------------------
    */

    Route::post('delivered/datatable', 'DeliveredController@datatable');
    Route::get('delivered/toDelivered/{id}', 'DeliveredController@deliveredTicket')->name('delivered.deliveredTicket');
    Route::resource('delivered','DeliveredController');

    /*
    |--------------------------------------------------------------------------
    | Web  Ticket Management/Completed routes
    |--------------------------------------------------------------------------
    */

    Route::post('completed/datatable', 'CompletedController@datatable');
    Route::get('completed/toCompleted/{id}', 'CompletedController@completedTicket')->name('completed.completedTicket');
    Route::resource('completed','CompletedController');

    /*
    |--------------------------------------------------------------------------
    | Web  Ticket SOLD   YES - IN HAND YES  routes
    |--------------------------------------------------------------------------
    */

    Route::get('tickets/toDetails/{id}', 'InventoryController@detailsTicket')->name('tickets.detailsTicket');
    Route::resource('tickets','InventoryController');
    //Route::post('tickets/splittickets/{id}', 'InventoryController@splitTickets')->name('tickets.split_tickets');
    //Route::get('ticketInfo/sold/returntostock/{id}', 'InventoryController@returnTicketToStock');
    Route::post('tickets/handlingMass', 'InventoryController@handlingMass')->name('tickets.handlingMass');
    Route::post('tickets/changeInHandMass', 'InventoryController@changeInHandMass')->name('tickets.changeInHandMass');

    /*
     *
    |--------------------------------------------------------------------------
    | Web  purchases routes
    |--------------------------------------------------------------------------
    */
    Route::post('purchases/datatable', 'PurchasesController@datatable');
    Route::resource('purchases', 'PurchasesController');
    Route::post('purchases/update/{id}', 'PurchasesController@update')->name('purchases.update');
    Route::post('purchases/updateBuyerInfo/{purchase_number}', 'PurchasesController@updateBuyerInfo')->name('purchases.updateBuyerInfo');
    Route::post('purchases/reconcile', 'PurchasesController@reconcileMass')->name('purchases.reconcileMass');

    /*
     *
    |--------------------------------------------------------------------------
    | Web  Adjustments routes
    |--------------------------------------------------------------------------
    */
    Route::resource('adjustments', 'AdjustmentsController');

    /*
    |--------------------------------------------------------------------------
    | Web  ticket sales routes
    |--------------------------------------------------------------------------
    */

    Route::get('sold', 'SoldController@index')->name('sold.index');
    Route::post('sold/datatable', 'SoldController@datatable');
    Route::get('sold/{id}', 'SoldController@show')->name('sold.show');
    Route::get('sold/toSold/{id}', 'SoldController@soldTicket')->name('sold.soldTicket');
    Route::post('sold', 'SoldController@store')->name('sold.store');
    Route::get('sold/get/{id}', 'SoldController@getSoldTicket')->name('sold.get');
    Route::post('sold/refund/{id}', 'SoldController@refund')->name('sold.refund');
    Route::get('tickets/toSell/{id}', 'SoldController@sellTicket')->name('sold.sellTicket');
    Route::get('tickets/sell/{id}', 'SoldController@sell')->name('sold.sellList');

    /*
    |--------------------------------------------------------------------------
    | Web Swap actions routes
    |--------------------------------------------------------------------------
    */
    Route::post('swap/availableForSwap/{id}', 'SwapController@availableForSwap')->name('swap.availableForSwap');
    Route::post('swap/returnToAvailable/{id}', 'SwapController@returnToAvailable')->name('swap.return-to-available');
    Route::post('swap/available/{id}', 'SwapController@available')->name('swap.available');
    Route::post('swap/change/{id}', 'SwapController@change')->name('swap.change');
    Route::get('swap', 'SwapController@swap')->name('swap');

    /*
    |--------------------------------------------------------------------------
    | Web Finance actions routes
    |--------------------------------------------------------------------------
    */

    Route::middleware('role:Admin')->group(function() {
        Route::get('finances/expenses', 'ExpensesController@index')->name('finances.expenses');
        Route::get('finances/expenses/add', 'ExpensesController@add')->name('finances.expenses.add');
        Route::post('finances/expenses', 'ExpensesController@create')->name('finances.expenses.create');
        Route::get('finances/expenses/{id}', 'ExpensesController@show')->name('finances.expenses.show');
        Route::put('finances/expenses/{id}', 'ExpensesController@update')->name('finances.expenses.update');
        Route::delete('finances/expenses/{id}', 'ExpensesController@delete')->name('finances.expenses.delete');
    });

    Route::middleware('role:Admin')->group(function() {
        Route::get('finances/adm-business-expenses', 'BusinessExpensesController@index')->name('finances.business-expenses');
        Route::get('finances/adm-business-expenses/add', 'BusinessExpensesController@add')->name('finances.business-expenses.add');
        Route::post('finances/adm-business-expenses', 'BusinessExpensesController@create')->name('finances.business-expenses.create');
        Route::get('finances/adm-business-expenses/{id}', 'BusinessExpensesController@show')->name('finances.business-expenses.show');
        Route::put('finances/adm-business-expenses/{id}', 'BusinessExpensesController@update')->name('finances.business-expenses.update');
        Route::delete('finances/adm-business-expenses/{id}', 'BusinessExpensesController@delete')->name('finances.business-expenses.delete');
    });

    Route::get('finances/{name_category}-expenses', 'ExpensesController@indexCategory')->name('finances.category-expenses');
    Route::get('finances/{name_category}-expenses/add', 'ExpensesController@addCategory')->name('finances.category-expenses.add');
    Route::get('finances/{name_category}-expenses/{id}', 'ExpensesController@showCategory')->name('finances.category-expenses.show');
    Route::post('finances/{name_category}-expenses', 'ExpensesController@createCategory')->name('finances.category-expenses.create');
    Route::put('finances/{name_category}-expenses/{id}', 'ExpensesController@updateCategory')->name('finances.category-expenses.update');
    Route::delete('finances/{name_category}expenses/{id}', 'ExpensesController@deleteCategory')->name('finances.category-expenses.delete');

    Route::get('finances/user-charges', 'UserChargesController@index')->name('finances.user-charges');
    Route::get('finances/user-charges/add', 'UserChargesController@add')->name('finances.user-charges.add');
    Route::post('finances/user-charges', 'UserChargesController@create')->name('finances.user-charges.create');
    Route::get('finances/user-charges/{id}', 'UserChargesController@show')->name('finances.user-charges.show');
    Route::put('finances/user-charges/{id}', 'UserChargesController@update')->name('finances.user-charges.update');
    Route::delete('finances/user-charges/{id}', 'UserChargesController@delete')->name('finances.user-charges.delete');

    Route::get('finances/payments', 'PaymentsController@index')->name('finances.payments');
    Route::get('finances/payments/add', 'PaymentsController@add')->name('finances.payments.add');
    Route::post('finances/payments', 'PaymentsController@create')->name('finances.payments.create');
    Route::get('finances/payments/{id}', 'PaymentsController@show')->name('finances.payments.show');
    Route::put('finances/payments/{id}', 'PaymentsController@update')->name('finances.payments.update');
    Route::delete('finances/payments/{id}', 'PaymentsController@delete')->name('finances.payments.delete');

    Route::middleware('role:Admin')->group(function() {
        Route::get('finances/director-loan', 'DirectorLoanController@index')->name('finances.director-loan');
        Route::get('finances/director-loan/add', 'DirectorLoanController@add')->name('finances.director-loan.add');
        Route::post('finances/director-loan', 'DirectorLoanController@create')->name('finances.director-loan.create');
        Route::get('finances/director-loan/{id}', 'DirectorLoanController@show')->name('finances.director-loan.show');
        Route::put('finances/director-loan/{id}', 'DirectorLoanController@update')->name('finances.director-loan.update');
        Route::delete('finances/director-loan/{id}', 'DirectorLoanController@delete')->name('finances.director-loan.delete');

        Route::get('finances/loan', 'LoanController@index')->name('finances.loan');
        Route::get('finances/loan/add', 'LoanController@add')->name('finances.loan.add');
        Route::post('finances/loan', 'LoanController@create')->name('finances.loan.create');
        Route::get('finances/loan/{id}', 'LoanController@show')->name('finances.loan.show');
        Route::put('finances/loan/{id}', 'LoanController@update')->name('finances.loan.update');
        Route::delete('finances/loan/{id}', 'LoanController@delete')->name('finances.loan.delete');

        Route::get('finances/taxes', 'TaxesController@index')->name('finances.taxes');
        Route::get('finances/taxes/add', 'TaxesController@add')->name('finances.taxes.add');
        Route::post('finances/taxes', 'TaxesController@create')->name('finances.taxes.create');
        Route::get('finances/taxes/{id}', 'TaxesController@show')->name('finances.taxes.show');
        Route::put('finances/taxes/{id}', 'TaxesController@update')->name('finances.taxes.update');
        Route::delete('finances/taxes/{id}', 'TaxesController@delete')->name('finances.taxes.delete');
    });

    Route::get('finances/charges', 'ChargesController@index')->name('finances.charges');
    Route::get('finances/charges/add', 'ChargesController@add')->name('finances.charges.add');
    Route::post('finances/charges', 'ChargesController@create')->name('finances.charges.create');
    Route::get('finances/charges/{id}', 'ChargesController@show')->name('finances.charges.show');
    Route::put('finances/charges/{id}', 'ChargesController@update')->name('finances.charges.update');
    Route::delete('finances/charges/{id}', 'ChargesController@delete')->name('finances.charges.delete');

    /*
    |--------------------------------------------------------------------------
    | Web  Settings routes
    |--------------------------------------------------------------------------
    */
    Route::middleware('role:Admin')->group(function() {
        Route::resource('settings', 'SettingsController');

        // Web Event Genres
        Route::post('settings/EventGenre/{id?}', 'SettingsController@updateEventGenre')->name('settings.updateEventGenre');
        Route::get('settings/removeEventGenre/{id}', 'SettingsController@removeEventGenre')->name('settings.removeEventGenre');
        Route::get('settings/showEventGenre/{id}', 'SettingsController@showEventGenre')->name('settings.showEventGenre');

        // Web Ticket Types
        Route::post('settings/TicketTypes/{id?}', 'SettingsController@updateTicketTypes')->name('settings.updateTicketTypes');
        Route::get('settings/removeTicketTypes/{id}', 'SettingsController@removeTicketTypes')->name('settings.removeTicketTypes');
        Route::get('settings/showTicketTypes/{id}', 'SettingsController@showTicketTypes')->name('settings.showTicketTypes');

        // Web Ticket Status
        Route::post('settings/TicketStatus/{id?}', 'SettingsController@updateTicketStatus')->name('settings.updateTicketStatus');
        Route::get('settings/removeTicketStatus/{id}', 'SettingsController@removeTicketStatus')->name('settings.removeTicketStatus');
        Route::get('settings/showTicketStatus/{id}', 'SettingsController@showTicketStatus')->name('settings.showTicketStatus');

        // Web Subcategories
        Route::post('settings/subcategories/{id?}', 'SettingsController@updateSubcategories')->name('settings.updateSubcategories');
        Route::get('settings/removeSubcategory/{id}', 'SettingsController@removeSubcategory')->name('settings.removeSubcategory');
        Route::get('settings/showSubcategory/{id}', 'SettingsController@showSubcategory')->name('settings.showSubcategory');

    });

    /*
    |--------------------------------------------------------------------------
    | Web  messages & notifications
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'messages'], function () {

    Route::resource('messages', 'MessagesController');
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    });

});




/*
|--------------------------------------------------------------------------
|  API Routes  --- this way so we can use auth middleware without api tokens
|--------------------------------------------------------------------------
|
*/

Route::middleware(['web', 'auth'])->prefix('/api')->namespace('Api\v1\Tickets')->group(function () {

    Route::get('/ticketInfo/{id}', 'ticketInfo@information');
    //Route::get('/ticketInfo/sold/returntostock/{id}', 'ticketInfo@returnTicketToStock');

    Route::get('/ticketInfo/batches/{id}', 'ticketInfo@ReturnBatches');
    Route::get('/ticketInfo/sold/isPaid/{id}/{status}', 'ticketInfo@IsPaid');

    Route::get('/ticketInfo/notrecived/chase/{id}/{status}', 'ticketInfo@chase');
    Route::get('/ticketInfo/notrecived/pic/{id}/{status}', 'ticketInfo@pic');

    Route::get('/ticketInfo/splitTickets/{id}', 'ticketInfo@SplitTickets');

    Route::get('/ticketInfo/sold/isSold/{id}/{status}', 'ticketInfo@IsSold');

    Route::post('/ticketInfo/companyRefund/{id}', 'ticketInfo@companyRefund');

    Route::get('/ticketInfo/inventoryNotesTicketUpdate/{id}/{notes}', 'ticketInfo@inventoryNotesTicketUpdate');
    Route::get('/ticketInfo/inventoryQuantityNotesTicketUpdate/{id}/{notes}', 'ticketInfo@inventoryQuantityNotesTicketUpdate');

    Route::get('/ticketInfo/cancelEvent/{id}', 'ticketInfo@cancelEvent');
    Route::get('/ticketInfo/restoreCancelledEvent/{id}', 'ticketInfo@restoreCancelledEvent');
    Route::get('/ticketInfo/pendingCancel/{id}', 'ticketInfo@pendingCancel');
    Route::get('/ticketInfo/restorePendingCancelled/{id}', 'ticketInfo@restorePendingCancelled');
    Route::get('/ticketInfo/listOnResale/{id}', 'ticketInfo@listOnResale');
    Route::get('/ticketInfo/unlistOnResale/{id}', 'ticketInfo@unlistOnResale');
    Route::get('/ticketInfo/expire/{id}', 'ticketInfo@expire');
    Route::get('/ticketInfo/restoreExpired/{id}', 'ticketInfo@restoreExpired');
});


//// Change Password Routes...
//Route::group(['middleware' => ['web'], function () {
//    Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
//    Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');
//});


Auth::routes();
