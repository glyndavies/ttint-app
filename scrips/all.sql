--TRANSFER

select i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, p.face_value_of_tickets + p.buyers_fees as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, i.new_buyer as new_owner_code, i.token, ts.ticketStatus as ticket_status
from inventory_quantity as iq
left join inventory as i on i.id = iq.inventory_id
left join purchases as p on p.purchase_number = i.purchase_number
left join ticket_status as ts on ts.id = iq.ticket_status_id
where iq.ticket_status_id in (11, 12) and i.event_cancelled = 'N' and i.expired != 1
order by i.event_date;

--INVENTORY

select i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0) * iq.number_of_tickets, 2) as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, ts.ticketStatus as ticket_status
from inventory as i
right join inventory_quantity as iq on iq.inventory_id = i.id
left join purchases as p on p.purchase_number = i.purchase_number
left join ticket_status as ts on ts.id = iq.ticket_status_id
where iq.ticket_status_id in (1, 2, 12) and i.event_cancelled = 'N' and i.expired != 1
order by i.event_date;

--EXPIRED

select i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.number_of_tickets as expired_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, p.face_value_of_tickets + p.buyers_fees as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, i.new_buyer as new_owner_code, i.token, ts.ticketStatus as ticket_status
from inventory_quantity as iq
left join inventory as i on i.id = iq.inventory_id
left join purchases as p on p.purchase_number = i.purchase_number
left join ticket_status as ts on ts.id = iq.ticket_status_id
where i.expired = 1 and iq.ticket_status_id != 3
order by i.event_date;

--CANCELED

select i.id, i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, p.face_value_of_tickets + p.buyers_fees as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, ts.ticketStatus as ticket_status, date(iq.sold_date) as paid_date, cr.refunded_amount as refunded_amount, date(cr.created_at) as refunded_date
from inventory_quantity as iq
left join inventory as i on i.id = iq.inventory_id
left join purchases as p on p.purchase_number = i.purchase_number
left join company_refund as cr on cr.purchase_number = p.purchase_number
left join ticket_status as ts on ts.id = iq.ticket_status_id
where i.event_cancelled = 'Y';

--SOLD

select i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0) * iq.number_of_tickets, 2) as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, ts.ticketStatus as ticket_status, st.sold_date, st.site_sold, st.sale_id_ref, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as tsp, st.sell_price, st.sell_price - p.face_value_of_tickets - p.buyers_fees as gross_profit, st.paid_date, iq.delivered_date
from inventory_quantity as iq
left join inventory as i on i.id = iq.inventory_id
left join purchases as p on p.purchase_number = i.purchase_number
left join sold_tickets as st on st.inventory_quantity_id = iq.id
left join ticket_status as ts on ts.id = iq.ticket_status_id
where i.event_cancelled = 'N' and iq.ticket_status_id = 3 and iq.ticket_handling_id not in (2, 3, 5) and st.is_paid != 'Y'
order by i.event_date;

--DELIVERED

select i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, p.face_value_of_tickets + p.buyers_fees as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, ts.ticketStatus as ticket_status, st.sold_date, st.site_sold, st.sale_id_ref, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as tsp, st.sell_price, st.sell_price - p.face_value_of_tickets - p.buyers_fees as gross_profit, st.paid_date, iq.delivered_date
from inventory_quantity as iq
left join inventory as i on i.id = iq.inventory_id
left join purchases as p on p.purchase_number = i.purchase_number
left join sold_tickets as st on st.inventory_quantity_id = iq.id
left join ticket_status as ts on ts.id = iq.ticket_status_id
where iq.ticket_status_id = 3 and iq.ticket_handling_id in (2, 3, 5) and iq.tickets_in_hand = 2 and st.is_paid = 'N' and i.event_cancelled = 'N';

--COMPLETED

select i.order_number, i.event, i.arena, i.site_purchased, i.type_of_ticket, p.purchase_date, i.event_date, iq.number_of_tickets, iq.seats, i.referral_1, i.referral_2, p.buyer as user_code, p.deposit_rate as deposit_rate, p.buyer_investment as deposit, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as cost_per_ticket, p.face_value_of_tickets + p.buyers_fees as total_cost, i.origin_status, round((iq.number_of_tickets / nullif(p.number_of_tickets, 0)) * i.paid_amount, 2) as paid_amount, i.buyer as owner_code, ts.ticketStatus as ticket_status, st.sold_date, st.site_sold, st.sale_id_ref, round((p.face_value_of_tickets + p.buyers_fees) / nullif(p.number_of_tickets, 0), 2) as tsp, st.sell_price, st.sell_price - p.face_value_of_tickets - p.buyers_fees as gross_profit, st.paid_date, iq.delivered_date
from inventory_quantity as iq
left join inventory as i on i.id = iq.inventory_id
left join purchases as p on p.purchase_number = i.purchase_number
left join sold_tickets as st on st.inventory_quantity_id = iq.id
left join ticket_status as ts on ts.id = iq.ticket_status_id
where iq.ticket_status_id = 3 and st.is_paid = 'Y' and i.event_cancelled = 'N';

--TAXES

select t.date, t.amount_incl_vat, t.notes from taxes as t

--CHARGES-AND-CREDITS

select ch.date, ch.amount_incl_vat, ch.sale_id_ref, ch.site_sold, ch.event, ch.reason, ch.staff_code, ch.notes from charges as ch

--EXPENSES
select ex.category_id, ex.card_id, ex.description, ex.amount, ex.vat, ex.date, ex.created_at, ex.updated_at from expenses as ex;

--BUSINESS EXPENSES
select be.category_id, be.card_id, be.description, be.amount, be.vat, be.date, be.created_at, be.updated_at from business_expenses as be;